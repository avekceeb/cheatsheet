
## hub.foo.bar
enp0s3: 08:00:27:fc:89:ab  172.16.0.20/24
enp0s8: 08:00:27:32:fd:04  10.0.0.1/24

dhcp-host=08:00:27:6f:5b:e1,fry,10.0.0.11,12h
dhcp-host=08:00:27:6f:5b:e2,leela,10.0.0.12,12h
dhcp-host=08:00:27:6f:5b:e3,bender,10.0.0.13,12h
dhcp-host=08:00:27:6f:5b:e4,farnsworth,10.0.0.14,12h
dhcp-host=08:00:27:6f:5b:e5,hermes,10.0.0.15,12h
dhcp-host=08:00:27:6f:5b:e6,zoidberg,10.0.0.16,12h
dhcp-host=08:00:27:6f:5b:e7,amy,10.0.0.17,12h
dhcp-host=08:00:27:6f:5b:e8,scruffy,10.0.0.18,12h
dhcp-host=08:00:27:6f:5b:e9,zap,10.0.0.19,12h
dhcp-host=08:00:27:6f:5b:f0,kif,10.0.0.20,12h
dhcp-host=08:00:27:6f:5b:f1,nibbler,10.0.0.21,12h
dhcp-host=08:00:27:6f:5b:f2,mom,10.0.0.22,12h
dhcp-host=08:00:27:6f:5b:f3,elzar,10.0.0.23,12h
dhcp-host=08:00:27:6f:5b:f4,morbo,10.0.0.24,12h
dhcp-host=08:00:27:6f:5b:f5,linda,10.0.0.25,12h
dhcp-host=08:00:27:6f:5b:f8,roberto,10.0.0.28,12h


10.0.0.1   hub.foo.bar
10.0.0.11  fry.foo.bar
10.0.0.12  leela.foo.bar
10.0.0.13  bender.foo.bar
10.0.0.14  farnsworth.foo.bar
10.0.0.15  hermes.foo.bar
10.0.0.16  zoidberg.foo.bar
10.0.0.17  amy.foo.bar
10.0.0.18  scruffy.foo.bar
10.0.0.19  zap.foo.bar
10.0.0.20  kif.foo.bar
10.0.0.21  nibbler.foo.bar
10.0.0.22  mom.foo.bar
10.0.0.23  elzar.foo.bar
10.0.0.24  morbo.foo.bar
10.0.0.25  linda.foo.bar
10.0.0.28  roberto.foo.bar


xxh -t root@172.16.0.20 ssh root@10.0.0.20
