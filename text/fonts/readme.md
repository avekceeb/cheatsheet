
[framebuffer opennet](https://www.opennet.ru/base/X/framebuffer_setup.txt.html)

```bash
# psf fonts:
#suse:   /usr/share/kbd/consolefonts
# /etc/sysconfig/console

#debian: /usr/share/consolefonts/ 
# /etc/defaults/console-setup

/usr/share/consolefonts/Uni3-Terminus32x16.psf.gz
/usr/share/consolefonts/Lat2-Terminus32x16.psf.gz
/usr/share/consolefonts/CyrSlav-Terminus32x16.psf.gz
```

```bash
# 4.13.1
CONFIG_FONT_SUPPORT=y
CONFIG_FONTS=y
CONFIG_FONT_8x8=y
CONFIG_FONT_8x16=y
# CONFIG_FONT_6x11 is not set
# CONFIG_FONT_7x14 is not set
CONFIG_FONT_PEARL_8x8=y
# CONFIG_FONT_ACORN_8x8 is not set
# CONFIG_FONT_MINI_4x6 is not set
# CONFIG_FONT_6x10 is not set
CONFIG_FONT_10x18=y
# CONFIG_FONT_SUN8x16 is not set
CONFIG_FONT_SUN12x22=y
```
