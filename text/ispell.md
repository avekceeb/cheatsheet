```
gunzip american.med+.mwl.gz
gunzip russian.mwl.gz
iconv -o rus-utf8.wordlist -f KOI8-R -t UTF8 russian.mwl
iconv -o rus-utf8.aff -f KOI8-R -t UTF8 russian.aff

# wordchars [\x0430-\x044f] [\x0410-\x042f]

buildhash rus-utf8.wordlist rus-utf8.aff rus-utf8.hash
# -> errors
```

## Быстро (и нерекурсивно) поменять кодировку
```
for x in * ; do mv $x _${x} ; done
for x in _* ; do iconv -o ${x##_} -f WINDOWS-1251 -t UTF8 ${x} ; done
rm -f _*
```
