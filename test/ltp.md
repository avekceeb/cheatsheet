
```bash
apt install gcc git make bison flex automake autoconf
git clone https://github.com/linux-test-project/ltp.git
cd ltp/
make autotools
./configure 
make all
make install
```


/opt/ltp/runltp -f $(pwd)/triage
cat $(pwd)/triage
#DESCRIPTION:2.6.x kernel module tests
#delete_module01 delete_module01
#delete_module02 delete_module02
#delete_module03 delete_module03
#fw_load fw_load
#insmod01 insmod01.sh
#block_dev block_dev
#ltp_acpi ltp_acpi
#uaccess uaccess
#cpufreq_boost cpufreq_boost

### failed
#zram01 zram01.sh
#zram02 zram02.sh
#zram03 zram03
#utimensat01 utimensat_tests.sh
thp01 thp01 -I 120

