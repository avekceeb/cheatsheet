
# FUZZING #

[fuzz info collection](https://github.com/secfigo/Awesome-Fuzzing)

[sulley](https://github.com/OpenRCE/sulley)
[scapy](http://www.secdev.org/projects/scapy/)
[more scapy](http://www.secdev.org/projects/scapy/doc/introduction.html)

[peach fuzzer - c-sharp](http://community.peachfuzzer.com/v3/Installation.html)
[more peach](http://www.peachfuzzer.com/resources/peachcommunity/)
[peach src](https://sourceforge.net/projects/peachfuzz/)
[mozilla peach](https://github.com/MozillaSecurity/peach)
[ruxxer](https://github.com/s7ephen/Ruxxer)

--------------------------------------
## American Fuzzy Lop

[american fuzzy lop](http://lcamtuf.coredump.cx/afl/)
[lwn article](https://lwn.net/Articles/657959/)

--------------------------------------
## syzkaller

[github](https://github.com/google/syzkaller)

[steps](https://github.com/hardenedlinux/Debian-GNU-Linux-Profiles/blob/master/docs/harbian_qa/fuzz_testing/syzkaller_general.md)

[case explained](https://lwn.net/Articles/677764/)

[forum](https://groups.google.com/forum/#!forum/syzkaller)

[chat on repro](https://groups.google.com/forum/#!msg/syzkaller/R7glFvs27tU/D8XadG1mBQAJ)

[111](https://googleprojectzero.blogspot.co.uk/2017/05/exploiting-linux-kernel-via-packet.html)

[presentation](https://www.slideshare.net/DmitryVyukov/syzkaller-the-next-gen-kernel-fuzzer)

[222](https://groups.google.com/forum/#!msg/syzkaller/R7glFvs27tU/D8XadG1mBQAJ)


```bash

### ubuntu 17.04

## go 1.8

apt install libc6-dev-i386 lib32stdc++-4.8-dev linux-libc-dev 
rm /usr/bin/go
ln -s /usr/lib/go-1.8/bin/go /usr/bin/go

mkdir gopath
export GOPATH=`pwd`/gopath
go get -u -d github.com/google/syzkaller/...
cd gopath/src/github.com/google/syzkaller/
mkdir workdir
export GOROOT=/usr/lib/go-1.8
make
```

#### suse tumbleweed

```bash
## prereq

zypper install gcc-c++
    glibc-devel-32bit glibc-devel \
    libstdc++-devel-32bit libstdc++-devel \
    glibc-devel-static glibc-devel-static-32bit

## go (from tar)
wget https://storage.googleapis.com/golang/go1.8.1.linux-amd64.tar.gz
tar -xf go1.8.1.linux-amd64.tar.gz
mv go goroot
export GOROOT=`pwd`/goroot
export PATH=$PATH:$GOROOT/bin
mkdir gopath
export GOPATH=`pwd`/gopath

## build
go get -u -d github.com/google/syzkaller/...
cd gopath/src/github.com/google/syzkaller/
mkdir workdir
make

## .. or
export GOPATH=/home/qa/syzkalls/
mkdir -p $GOPATH
go get -u -d -v github.com/google/syzkaller/...
cd $GOPATH/src/github.com/google/syzkaller
make -j4
cp $GOPATH/src/github.com/google/syzkaller/bin $GOPATH/ -af

## run
cat my.json 
{
	"target": "linux/amd64",
	"http": "localhost:56741",
	"workdir": "/home/ru/not-my/gopath/workdir",
	"image": "/home/ru/vm/wheezy/wheezy.img",
	"sshkey": "/home/ru/bin/ssh/id_rsa",
	"syzkaller": "/home/ru/not-my/gopath/src/github.com/google/syzkaller",
	"enable_syscalls": ["keyctl", "add_key", "request_key", "mlock", "unlink"],
	"suppressions": ["ZZZZZZZZZZZZZZZZZZ"],
	"procs": 2,
	"type": "qemu",
	"vm": {
		"count": 1,
		"cpu": 2,
		"mem": 2048,
		"kernel": "/home/ru/build/4.13.1-kcov-qemu/bzImage"
	}
}

./bin/syz-manager -config my.json


## notes
xxh root@localhost -p 61241 -i ~/bin/ssh/id_rsa

qemu-system-x86_64 -m 2048 -net nic -net user,host=10.0.2.10,hostfwd=tcp::61241-:22 -display none -serial stdio -no-reboot -numa node,nodeid=0,cpus=0-1 -numa node,nodeid=1,cpus=2-3 -smp sockets=2,cores=2,threads=1 -enable-kvm -usb -usbdevice mouse -usbdevice tablet -soundhw all -hda /home/ru/vm/wheezy/wheezy.img -snapshot -kernel /home/ru/build/4.13.1-kcov-qemu/vmlinux -append console=ttyS0 vsyscall=native rodata=n oops=panic nmi_watchdog=panic panic_on_warn=1 panic=86400 ftrace_dump_on_oops=orig_cpu earlyprintk=serial net.ifnames=0 biosdevname=0 kvm-intel.nested=1 kvm-intel.unrestricted_guest=1 kvm-intel.vmm_exclusive=1 kvm-intel.fasteoi=1 kvm-intel.ept=1 kvm-intel.flexpriority=1 kvm-intel.vpid=1 kvm-intel.emulate_invalid_guest_state=1 kvm-intel.eptad=1 kvm-intel.enable_shadow_vmcs=1 kvm-intel.pml=1 kvm-intel.enable_apicv=1 root=/dev/sda

# on guest machine:
cat > test.txt<<EOF
getpid()
EOF
cd linux_amd64
tar cf - syz-execprog  syz-executor  syz-fuzzer  syz-stress | xxh root@localhost -p 22222 -i ~/bin/ssh/id_rsa 'cd /root ; cat | tar xf -'

./syz-execprog -repeat 10  -debug -executor ./syz-executor test.txt
# test.txt - so called 'program'


```
#### note: <linux >4.13>/Documentation/dev-tools/kcov.rst #### 

/sys/kernel/debug/kcov

[patch](https://patchwork.kernel.org/patch/9930169/)
[userspace kcov example](https://davejingtian.org/2017/06/01/understanding-kcov-play-with-fsanitize-coveragetrace-pc-from-the-user-space/)

```
./kcov-sample | addr2line -a -i -f -e vmlinux 
0xffffffff812e1ca1
SyS_read
??:0
0xffffffff8130d663
__fdget_pos
??:0
0xffffffff8130c958
__fget_light
file.c:0
0xffffffff8130c9a5
__fget_light
file.c:0
0xffffffff8130c9d8
__fget_light
file.c:0
0xffffffff8130d6b0
__fdget_pos
??:0
0xffffffff812e1d19
SyS_read
??:0
```

--------------------------------------

## Trinity

[github](https://github.com/kernelslacker/trinity)
[site](https://codemonkey.org.uk/projects/trinity/)

```bash
## atomic does not exist in old gcc
yum install devtoolset-3
scl enable devtoolset-3 bash
```

--------------------------------------
```
# gdb ./trinity 

Breakpoint 1, open_logfile (logfilename=logfilename@entry=0xf8c010 "trinity-child0.log") at log-files.c:16
16	{
(gdb) list
11	#include "shm.h"
12	
13	FILE *mainlogfile;
14	
15	static FILE *open_logfile(const char *logfilename)
16	{
17		FILE *file;
18		char *fullpath;
19		int len = strlen(logging_args) + strlen(logfilename) + 2;
20	
(gdb) p logging_args
$1 = 0x0

Program received signal SIGSEGV, Segmentation fault.
0x00007ffff78ad326 in __strlen_sse2 () from /lib64/libc.so.6
(gdb) bt
#0  0x00007ffff78ad326 in __strlen_sse2 () from /lib64/libc.so.6
#1  0x00000000004103c3 in open_logfile (logfilename=logfilename@entry=0xf8c010 "trinity-child0.log") at log-files.c:19
#2  0x00000000004104d5 in open_child_logfile (child=0x7ffff7fea000) at log-files.c:48
#3  0x0000000000413a1d in init_shm () at shm.c:81
#4  0x000000000040d2d7 in main (argc=2, argv=0x7fffffffe368) at trinity.c:125
```
--------------------------------------
```
(gdb) set follow-fork-mode child
(gdb) b child_process

run -q -l off -c uname
...
[New process 12273]
[Thread debugging using libthread_db enabled]
...
295     disable_coredumps();
(gdb) n
Cannot find user-level thread for LWP 12273: generic error
```
