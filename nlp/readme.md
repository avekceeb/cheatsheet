## Links
- [wiki NLP](https://en.wikipedia.org/wiki/Natural_language_processing)
- [NLP Progress](http://nlpprogress.com/)
- [for golang](https://github.com/avelino/awesome-go#natural-language-processing)
- [more go](https://biosphere.cc/software-engineering/go-machine-learning-nlp-libraries/)
- [prose lib in go](https://github.com/jdkato/prose/)
- [py NLTK](https://www.nltk.org/)
- [py spacy](https://spacy.io/)

## Tasks

1. Input document format recognition
1. Charset detection
2. Language recognition
2. Tokenization
