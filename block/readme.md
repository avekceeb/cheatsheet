

## Little FS
- [opennet](http://www.opennet.ru/opennews/art.shtml?num=47906)


## LVM

    ext2,3,4 bs = 1024 2048 4096
    mkfs.ext2 /dev/sdc
    dd if=/dev/sdc of=/root/4m-ext2 bs=1k count=4096
    hexdump -C /root/4m-ext2 
    dumpe2fs /dev/sdc
    mount /dev/sdc /mnt
    ls -liRa /mnt
    for x in $(seq 1024) ; do touch /mnt/file$x ; done


```bash
vgcreate /dev/mapper/zzz
lvcreate --size 1G dm_test
lvs
#  LV      VG             Attr      LSize   Pool Origin Data%  Move Log Cpy%Sync Convert
#  lvol0   dm_test        -wi-a----   1,00g                                             
#  lv_root vg_cadsfsdsd23 -wi-ao--- 149,91g                                             
#  lv_swap vg_casdfsdfd23 -wi-a---- 128,47g           

pvdisplay
lvdisplay
pvcreate /dev/vdiskd
vgdisplay
vgextend VolGroup /dev/vdiskd
vgdisplay
pvscan
lvdisplay
lvextend /dev/VolGroup/lv_root /dev/vdiskd
lvdisplay
lsblk
df -h
resize2fs /dev/VolGroup/lv_root 
df -h
```

## DM-Multipath

```bash
#
yum -y install device-mapper-multipath.sparc64 device-mapper-multipath-libs.sparc64

chkconfig multipathd on
service multipathd start

# cat /sys/block/sde/device/vendor
# cat /sys/block/sde/device/model
cat /etc/multipath/wwids

mpathconf --find_multipaths y --user_friendly_names y
service multipathd reload
# cat /etc/multipath.conf
# cat  /usr/share/doc/device-mapper-multipath-0.4.9/multipath.conf
multipath -ll

# dmsetup ls
# dmsetup ls --target=multipath
# scsi_id --whitelist --device  /dev/sde
multipath -v3
multipath -v2

multipath -f /dev/sde
multipath -F
multipath -r
multipath -p failover mpath0

#####################################################################
#####################################################################
cat >/etc/multipath.conf<<EOF 
defaults {
    find_multipaths yes
    user_friendly_names yes
}
multipaths {
    multipath {
        wwid 3600144f0c32839fa000052951df40010
        alias zzzstor
        path_grouping_policy multibus
        failback manual
    }
}
EOF
service multipathd restart
while true ; do dd if=/dev/urandom of=/dev/mapper/zzzstor bs=1k count=1024; sleep 1; done
iostat sdc sdd 3
multipathd -k
multipathd> fail path sdc
ok
multipathd> reinstate path sdc
ok
multipathd> 

#####################################################################
multipaths {
        multipath {
                wwid 3600144f0c32839fa000052951df40010
                alias zzzstor
                path_grouping_policy failover
                failback immediate
                no_path_retry fail
        }
}
zzzstor (3600144f0c32839fa000052951df40010) dm-1 SUN,ZFS Storage 7320
size=500G features='0' hwhandler='0' wp=rw
|-+- policy='round-robin 0' prio=1 status=active
| `- 4:0:4:0 sdc 8:32 active ready running
`-+- policy='round-robin 0' prio=1 status=enabled
  `- 4:0:5:0 sdd 8:48 active ready running

`
#####################################################################
multipaths {
    multipath {
        wwid 3600144f0c32839fa000052951df40010
        alias zzzstor
        path_grouping_policy multibus 
        failback immediate
        no_path_retry fail
    }
}
ervice multipathd reload
zzzstor (3600144f0c32839fa000052951df40010) dm-1 SUN,ZFS Storage 7320
size=500G features='0' hwhandler='0' wp=rw
`-+- policy='round-robin 0' prio=1 status=active
  |- 4:0:4:0 sdc 8:32 active ready running
  `- 4:0:5:0 sdd 8:48 active ready running


#####################################################################
 1181  multipath -ll
 1182  date
 1183  tail -40 /var/log/messages
 1184  date
 1185  multipath -ll
 1186  multipath -p failover dm-2
 1187  multipath -ll
 1188  multipath -v3
 1189  multipath -v3 | grep fail
 1190  multipath -p failover /dev/sde
 1191  tail -20 /var/log/messages
 1192  multipath -ll
 1193  multipath -l
 1194  multipath -ll
 1195  ls /etc/multipath
 1196  cat /etc/multipath/bindings 
 1197  cat /etc/multipath/wwids 
 1198  multipath -v3 | grep fail
 1199  multipathd -k
 1200  multipath -v2 360060160066029006afabc1d9092e211
 1201  multipath -v3 360060160066029006afabc1d9092e211
 1202  cat /etc/multipath/bindings 
 1203  multipath -v3 35000cca03c258fec
 1204  multipath -v3 mpatha
 1205  multipath -v3 mpath0
 1206  multipath
 1207  multipath -ll
 1208  ls /sys/block
 1209  ls /sys/block/sdd
 1210  ls /sys/block/sdd/device



############################################################
[root@ca-qasparc19 ~]# modinfo dm-multipath
filename:       /lib/modules/2.6.39-400.17.1.6.el6uek.sparc64/kernel/drivers/md/dm-multipath.ko
license:        GPL
author:         Sistina Software <dm-devel@redhat.com>
description:    device-mapper multipath target
srcversion:     1A8D18F56644CB7986C588E
depends:        
vermagic:       2.6.39-400.17.1.6.el6uek.sparc64 SMP mod_unload 

############################################################

[root@ca-qasparc19 ~]# cat /sys/block/sde/device/vendor
DGC     
[root@ca-qasparc19 ~]# cat /sys/block/sde/device/model
VRAID           

############################################################

scsi_id --whitelist --device  /dev/sde
360060160066029006afabc1d9092e211

############################################################

multipath -v3
Mar 22 05:08:18 | /etc/multipath.conf does not exist, blacklisting all devices.
Mar 22 05:08:18 | A sample multipath.conf file is located at
Mar 22 05:08:18 | /usr/share/doc/device-mapper-multipath-0.4.9/multipath.conf
Mar 22 05:08:18 | You can run /sbin/mpathconf to create or modify /etc/multipath.conf
Mar 22 05:08:18 | ram0: device node name blacklisted
....
Mar 22 05:08:18 | sda: device node name blacklisted
Mar 22 05:08:18 | sdb: device node name blacklisted
Mar 22 05:08:18 | sdc: device node name blacklisted
Mar 22 05:08:18 | sr0: device node name blacklisted
Mar 22 05:08:18 | sdd: device node name blacklisted
Mar 22 05:08:18 | sde: device node name blacklisted
Mar 22 05:08:18 | dm-0: device node name blacklisted
Mar 22 05:08:18 | dm-1: device node name blacklisted
===== no paths =====

############################################################
cat /etc/multipath.conf
devices {
blacklist {
	wwid "*"	
}

devices {

device {
vendor                  "DGC"
product                 "VRAID"
path_grouping_policy    multibus
no_path_retry           "5"
}
}

multipaths {

multipath {
wwid                    360060160066029006afabc1d9092e211
alias                   mpath0
path_grouping_policy    multibus
path_selector           "round-robin 0"
failback                "5"
rr_weight               priorities
no_path_retry           "5"
}
}

blacklist_exceptions {
	wwid "360060160066029006afabc1d9092e211"
}


defaults {
	user_friendly_names yes
	find_multipaths yes
}
############################################################
[root@ca-qasparc19 ~]# service multipathd reload
Reloading multipathd: [  OK  ]
[root@ca-qasparc19 ~]# device-mapper: multipath round-robin: version 1.0.0 loaded
device-mapper: table: 253:2: multipath: error getting device
device-mapper: ioctl: error adding target to table
device-mapper: table: 253:2: multipath: error getting device
device-mapper: ioctl: error adding target to table

###########################################################

# path_checker deleted

multipath -ll
35000cca025923880 dm-3 HITACHI,H106060SDSUN600G
size=559G features='0' hwhandler='0' wp=rw
`-+- policy='round-robin 0' prio=1 status=active
  `- 0:0:2:0 sdc 8:32 active ready running
mpath0 (360060160066029006afabc1d9092e211) dm-4 DGC,VRAID
size=100G features='1 queue_if_no_path' hwhandler='0' wp=rw
`-+- policy='round-robin 0' prio=1 status=active
  `- 2:0:0:0 sde 8:64 active ready running
35000c5002702d016 dm-2 ATA,SEAGATE ST95000N
size=466G features='0' hwhandler='0' wp=rw
`-+- policy='round-robin 0' prio=1 status=active
  `- 0:0:1:0 sdb 8:16 active ready running

############################################################
multipath -v3
Mar 22 05:35:08 | ram0: device node name blacklisted
Mar 22 05:35:08 | ram1: device node name blacklisted
Mar 22 05:35:08 | ram2: device node name blacklisted
Mar 22 05:35:08 | ram3: device node name blacklisted
Mar 22 05:35:08 | ram4: device node name blacklisted
Mar 22 05:35:08 | ram5: device node name blacklisted
Mar 22 05:35:08 | ram6: device node name blacklisted
Mar 22 05:35:08 | ram7: device node name blacklisted
Mar 22 05:35:08 | ram8: device node name blacklisted
Mar 22 05:35:08 | ram9: device node name blacklisted
Mar 22 05:35:08 | ram10: device node name blacklisted
Mar 22 05:35:08 | ram11: device node name blacklisted
Mar 22 05:35:08 | ram12: device node name blacklisted
Mar 22 05:35:08 | ram13: device node name blacklisted
Mar 22 05:35:08 | ram14: device node name blacklisted
Mar 22 05:35:08 | ram15: device node name blacklisted
Mar 22 05:35:08 | loop0: device node name blacklisted
Mar 22 05:35:08 | loop1: device node name blacklisted
Mar 22 05:35:08 | loop2: device node name blacklisted
Mar 22 05:35:08 | loop3: device node name blacklisted
Mar 22 05:35:08 | loop4: device node name blacklisted
Mar 22 05:35:08 | loop5: device node name blacklisted
Mar 22 05:35:08 | loop6: device node name blacklisted
Mar 22 05:35:08 | loop7: device node name blacklisted
Mar 22 05:35:08 | sda: not found in pathvec
Mar 22 05:35:08 | sda: mask = 0x3f
Mar 22 05:35:08 | sda: dev_t = 8:0
Mar 22 05:35:08 | sda: size = 585937500
Mar 22 05:35:08 | sda: subsystem = scsi
Mar 22 05:35:08 | sda: vendor = HITACHI
Mar 22 05:35:08 | sda: product = H106030SDSUN300G
Mar 22 05:35:08 | sda: rev = A2B0
Mar 22 05:35:08 | sda: h:b:t:l = 0:0:0:0
Mar 22 05:35:08 | sda: serial = 001232NNND7D        PWGNND7D
Mar 22 05:35:08 | sda: get_state
Mar 22 05:35:08 | sda: path checker = directio (config file default)
Mar 22 05:35:08 | sda: checker timeout = 30000 ms (sysfs setting)
Mar 22 05:35:08 | sda: state = running
Mar 22 05:35:08 | directio: starting new request
Mar 22 05:35:08 | directio: io finished 2048/0
Mar 22 05:35:08 | sda: state = 3
Mar 22 05:35:08 | sda: getuid = /lib/udev/scsi_id --whitelisted --device=/dev/%n (config file default)
Mar 22 05:35:08 | sda: uid = 35000cca03c258fec (callout)
Mar 22 05:35:08 | sda: state = running
Mar 22 05:35:08 | sda: detect_prio = 1 (config file default)
Mar 22 05:35:08 | sda: prio = const (config file default)
Mar 22 05:35:08 | sda: const prio = 1
Mar 22 05:35:08 | sdb: not found in pathvec
Mar 22 05:35:08 | sdb: mask = 0x3f
Mar 22 05:35:08 | sdb: dev_t = 8:16
Mar 22 05:35:08 | sdb: size = 976773168
Mar 22 05:35:08 | sdb: subsystem = scsi
Mar 22 05:35:08 | sdb: vendor = ATA
Mar 22 05:35:08 | sdb: product = SEAGATE ST95000N
Mar 22 05:35:08 | sdb: rev = SF03
Mar 22 05:35:08 | sdb: h:b:t:l = 0:0:1:0
Mar 22 05:35:08 | sdb: serial = 9SP1TZM7            
Mar 22 05:35:08 | sdb: get_state
Mar 22 05:35:08 | sdb: path checker = directio (config file default)
Mar 22 05:35:08 | sdb: checker timeout = 30000 ms (sysfs setting)
Mar 22 05:35:08 | sdb: state = running
Mar 22 05:35:08 | directio: starting new request
Mar 22 05:35:08 | directio: io finished 4096/0
Mar 22 05:35:08 | sdb: state = 3
Mar 22 05:35:08 | sdb: getuid = /lib/udev/scsi_id --whitelisted --device=/dev/%n (config file default)
Mar 22 05:35:08 | sdb: uid = 35000c5002702d016 (callout)
Mar 22 05:35:08 | sdb: state = running
Mar 22 05:35:08 | sdb: detect_prio = 1 (config file default)
Mar 22 05:35:08 | sdb: prio = const (config file default)
Mar 22 05:35:08 | sdb: const prio = 1
Mar 22 05:35:08 | sdc: not found in pathvec
Mar 22 05:35:08 | sdc: mask = 0x3f
Mar 22 05:35:08 | sdc: dev_t = 8:32
Mar 22 05:35:08 | sdc: size = 1172123568
Mar 22 05:35:08 | sdc: subsystem = scsi
Mar 22 05:35:08 | sdc: vendor = HITACHI
Mar 22 05:35:08 | sdc: product = H106060SDSUN600G
Mar 22 05:35:08 | sdc: rev = A2B0
Mar 22 05:35:08 | sdc: h:b:t:l = 0:0:2:0
Mar 22 05:35:08 | sdc: serial = 001217PLDX0B        PVJLDX0B
Mar 22 05:35:08 | sdc: get_state
Mar 22 05:35:08 | sdc: path checker = directio (config file default)
Mar 22 05:35:08 | sdc: checker timeout = 30000 ms (sysfs setting)
Mar 22 05:35:08 | sdc: state = running
Mar 22 05:35:08 | directio: starting new request
Mar 22 05:35:08 | directio: io finished 4096/0
Mar 22 05:35:08 | sdc: state = 3
Mar 22 05:35:08 | sdc: getuid = /lib/udev/scsi_id --whitelisted --device=/dev/%n (config file default)
Mar 22 05:35:08 | sdc: uid = 35000cca025923880 (callout)
Mar 22 05:35:08 | sdc: state = running
Mar 22 05:35:08 | sdc: detect_prio = 1 (config file default)
Mar 22 05:35:08 | sdc: prio = const (config file default)
Mar 22 05:35:08 | sdc: const prio = 1
Mar 22 05:35:08 | sr0: device node name blacklisted
Mar 22 05:35:08 | sdd: not found in pathvec
Mar 22 05:35:08 | sdd: mask = 0x3f
Mar 22 05:35:08 | sdd: dev_t = 8:48
Mar 22 05:35:08 | sdd: size = 62652416
Mar 22 05:35:08 | sdd: subsystem = scsi
Mar 22 05:35:08 | sdd: vendor = SMI
Mar 22 05:35:08 | sdd: product = USB DISK
Mar 22 05:35:08 | sdd: rev = 3000
Mar 22 05:35:08 | sdd: h:b:t:l = 4:0:0:0
Mar 22 05:35:08 | sdd: serial = 
Mar 22 05:35:08 | sdd: get_state
Mar 22 05:35:08 | sdd: path checker = directio (config file default)
Mar 22 05:35:08 | sdd: checker timeout = 30000 ms (sysfs setting)
Mar 22 05:35:08 | sdd: state = running
Mar 22 05:35:08 | directio: starting new request
Mar 22 05:35:08 | directio: io finished 4096/0
Mar 22 05:35:08 | sdd: state = 3
Mar 22 05:35:08 | sdd: getuid = /lib/udev/scsi_id --whitelisted --device=/dev/%n (config file default)
Mar 22 05:35:08 | /lib/udev/scsi_id exitted with 1
Mar 22 05:35:08 | error calling out /lib/udev/scsi_id --whitelisted --device=/dev/sdd
Mar 22 05:35:08 | sdd: state = running
Mar 22 05:35:08 | /lib/udev/scsi_id exitted with 1
Mar 22 05:35:08 | error calling out /lib/udev/scsi_id --whitelisted --device=/dev/sdd
Mar 22 05:35:08 | sdd: detect_prio = 1 (config file default)
Mar 22 05:35:08 | sdd: prio = const (config file default)
Mar 22 05:35:08 | sdd: const prio = 1
Mar 22 05:35:08 | sde: not found in pathvec
Mar 22 05:35:08 | sde: mask = 0x3f
Mar 22 05:35:08 | sde: dev_t = 8:64
Mar 22 05:35:08 | sde: size = 209715200
Mar 22 05:35:08 | sde: subsystem = scsi
Mar 22 05:35:08 | sde: vendor = DGC
Mar 22 05:35:08 | sde: product = VRAID
Mar 22 05:35:08 | sde: rev = 0531
Mar 22 05:35:08 | sde: h:b:t:l = 2:0:0:0
Mar 22 05:35:08 | sde: tgt_node_name = 0x50060160bea00401
Mar 22 05:35:08 | sde: serial = FNM00102900338
Mar 22 05:35:08 | sde: get_state
Mar 22 05:35:08 | sde: path checker = directio (config file default)
Mar 22 05:35:08 | sde: checker timeout = 30000 ms (sysfs setting)
Mar 22 05:35:08 | sde: state = running
Mar 22 05:35:08 | directio: starting new request
Mar 22 05:35:08 | directio: io finished 4096/0
Mar 22 05:35:08 | sde: state = 3
Mar 22 05:35:08 | sde: getuid = /lib/udev/scsi_id --whitelisted --device=/dev/%n (config file default)
Mar 22 05:35:08 | sde: uid = 360060160066029006afabc1d9092e211 (callout)
Mar 22 05:35:08 | sde: state = running
Mar 22 05:35:08 | sde: detect_prio = 1 (config file default)
Mar 22 05:35:08 | sde: prio = const (config file default)
Mar 22 05:35:08 | sde: const prio = 1
Mar 22 05:35:08 | dm-0: device node name blacklisted
Mar 22 05:35:08 | dm-1: device node name blacklisted
Mar 22 05:35:08 | dm-2: device node name blacklisted
Mar 22 05:35:08 | dm-3: device node name blacklisted
Mar 22 05:35:08 | dm-4: device node name blacklisted
Mar 22 05:35:08 | dm-5: device node name blacklisted
Mar 22 05:35:08 | dm-6: device node name blacklisted
Mar 22 05:35:08 | dm-7: device node name blacklisted
Mar 22 05:35:08 | dm-8: device node name blacklisted
Mar 22 05:35:08 | dm-9: device node name blacklisted
===== paths list =====
uuid                              hcil    dev dev_t pri dm_st chk_st vend/prod
35000cca03c258fec                 0:0:0:0 sda 8:0   1   undef ready  HITACHI,H
35000c5002702d016                 0:0:1:0 sdb 8:16  1   undef ready  ATA,SEAGA
35000cca025923880                 0:0:2:0 sdc 8:32  1   undef ready  HITACHI,H
                                  4:0:0:0 sdd 8:48  1   undef ready  SMI,USB D
360060160066029006afabc1d9092e211 2:0:0:0 sde 8:64  1   undef ready  DGC,VRAID
Mar 22 05:35:08 | params = 0 0 1 1 round-robin 0 1 1 8:32 1 
Mar 22 05:35:08 | status = 2 0 0 0 1 1 A 0 1device-mapper: table: 253:10: multipath: error getting device
device-mapper: ioctl: error adding target to table
 0 8:32 A 0 
Mar 22 05:35:08 | params = 1 queue_if_no_path 0 1 1 round-robin 0 1 1 device-mapper: table: 253:10: multipath: error getting device
device-mapper: ioctl: error adding target to table
8:64 1 
Mar 22 05:35:08 | status = 2 0 0 0 1 1 A 0 1 0 8:64 A 0 
Mar 22 05:35:08 | params = 0 0 1 1 round-robin 0 1 1 8:16 1 
Mar 22 05:35:10 | status = 2 0 0 0 1 1 A 0 1 0 8:16 A 0 
Mar 22 05:35:10 | sda: ownership set to 35000cca03c258fec
Mar 22 05:35:10 | sda: not found in pathvec
Mar 22 05:35:10 | sda: mask = 0xc
Mar 22 05:35:10 | sda: get_state
Mar 22 05:35:10 | sda: state = running
Mar 22 05:35:10 | directio: starting new request
Mar 22 05:35:10 | directio: io finished 2048/0
Mar 22 05:35:10 | sda: state = 3
Mar 22 05:35:10 | sda: state = running
Mar 22 05:35:10 | sda: const prio = 1
Mar 22 05:35:10 | 35000cca03c258fec: pgfailover = -1 (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: pgpolicy = failover (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: selector = round-robin 0 (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: features = 0 (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: hwhandler = 0 (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: rr_weight = 1 (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: minio = 1 rq (config file default)
Mar 22 05:35:10 | 35000cca03c258fec: no_path_retry = NONE (internal default)
Mar 22 05:35:10 | pg_timeout = NONE (internal default)
Mar 22 05:35:10 | 35000cca03c258fec: retain_attached_hw_handler = 1 (config file default)
Mar 22 05:35:10 | 35000cca03c258fec: set ACT_CREATE (map does not exist)
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:35:10 | 35000cca03c258fec: domap (0) failure for create/reload map
Mar 22 05:35:10 | 35000cca03c258fec: ignoring map
############################################################
# messages
Mar 22 05:32:15 ca-qasparc19 multipathd: reconfigure (SIGHUP)
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000cca025923880: stop event checker thread (18446735281932441872)
Mar 22 05:32:15 ca-qasparc19 multipathd: mpath0: stop event checker thread (18446735281932474640)
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000c5002702d016: stop event checker thread (18446735281911535888)
Mar 22 05:32:15 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:32:15 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:32:15 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:32:15 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000cca03c258fec: ignoring map
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000c5002702d016: load table [0 976773168 multipath 0 0 1 1 round-robin 0 1 1 8:16 1]
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000cca025923880: load table [0 1172123568 multipath 0 0 1 1 round-robin 0 1 1 8:32 1]
Mar 22 05:32:15 ca-qasparc19 multipathd: mpath0: load table [0 209715200 multipath 1 queue_if_no_path 0 1 1 round-robin 0 1 1 8:64 1]
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000c5002702d016: event checker started
Mar 22 05:32:15 ca-qasparc19 multipathd: 35000cca025923880: event checker started
Mar 22 05:32:15 ca-qasparc19 multipathd: mpath0: event checker started
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:32:15 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: table: 253:10: multipath: error getting device
Mar 22 05:35:10 ca-qasparc19 kernel: device-mapper: ioctl: error adding target to table
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: remove map (uevent)
Mar 22 05:35:10 ca-qasparc19 multipathd: dm-10: devmap not registered, can't remove

############################################################
[root@ca-qasparc19 ~]# dmsetup ls
VolGroup-lv_swap	(253:1)
35000cca025923880	(253:3)
VolGroup-lv_root	(253:0)
35000cca025923880p3	(253:9)
mpath0	(253:4)
35000cca025923880p2	(253:7)
35000cca025923880p1	(253:5)
35000c5002702d016	(253:2)
35000c5002702d016p2	(253:8)
35000c5002702d016p1	(253:6)
############################################################
### BLACKLIST
https://access.redhat.com/knowledge/docs/en-US/Red_Hat_Enterprise_Linux/6/html-single/DM_Multipath/index.html#blacklist_exceptions

 You can use the blacklist_exceptions section of the configuration file to enable multipathing on devices that have been blacklisted by default.
For example, if you have a large number of devices and want to multipath only one of them (with the WWID of 3600d0230000000000e13955cc3757803), instead of individually blacklisting each of the devices except the one you want, you could instead blacklist all of them, and then allow only the one you want by adding the following lines to the /etc/multipath.conf file.

blacklist {
        wwid "*"
}

blacklist_exceptions {
        wwid "3600d0230000000000e13955cc3757803"
}

When specifying devices in the blacklist_exceptions section of the configuration file, you must specify the exceptions in the same way they were specified in the blacklist. For example, a WWID exception will not apply to devices specified by a devnode blacklist entry, even if the blacklisted device is associated with that WWID. Similarly, devnode exceptions apply only to devnode entries, and device exceptions apply only to device entries. 

############################################################
[root@ca-sparc3 ~]# multipath -ll
mpathc (36006016006602900ba3a0c063145e211) dm-2 DGC,VRAID
size=100G features='1 queue_if_no_path' hwhandler='1 emc' wp=rw
|-+- policy='round-robin 0' prio=1 status=active
| `- 2:0:0:1 sdc 8:32 active ready running
`-+- policy='round-robin 0' prio=0 status=enabled
  `- 2:0:1:1 sde 8:64 active ready running
mpathb (36006016006602900b83a0c063145e211) dm-3 DGC,VRAID
size=100G features='1 queue_if_no_path' hwhandler='1 emc' wp=rw
|-+- policy='round-robin 0' prio=1 status=active
| `- 2:0:1:0 sdd 8:48 active ready running
`-+- policy='round-robin 0' prio=0 status=enabled
  `- 2:0:0:0 sdb 8:16 active ready running
###########################################################
[root@ca-sparc3 ~]# multipathd -k
multipathd> 
multipathd> 
multipathd> help
multipath-tools v0.4.9 (04/04, 2009)
CLI commands reference:
 list|show paths
 list|show paths format $format
 list|show status
 list|show daemon
 list|show maps|multipaths
 list|show maps|multipaths status
 list|show maps|multipaths stats
 list|show maps|multipaths format $format
 list|show maps|multipaths topology
 list|show topology
 list|show map|multipath $map topology
 list|show config
 list|show blacklist
 list|show devices
 list|show wildcards
 add path $path
 remove|del path $path
 add map|multipath $map
 remove|del map|multipath $map
 switch|switchgroup map|multipath $map group $group
 reconfigure
 suspend map|multipath $map
 resume map|multipath $map
 resize map|multipath $map
 disablequeueing map|multipath $map
 restorequeueing map|multipath $map
 disablequeueing maps|multipaths
 restorequeueing maps|multipaths
 reinstate path $path
 fail path $path
 paths count
 forcequeueing daemon
 restorequeueing daemon
 quit|exit
 map|multipath $map getprstatus
 map|multipath $map setprstatus
 map|multipath $map unsetprstatus
multipathd> show paths
hcil    dev dev_t pri dm_st  chk_st dev_st  next_check      
2:0:1:0 sdd 8:48  0   active ready  running XXXXXXXXXX 20/20
2:0:0:0 sdb 8:16  1   active ready  running XXXXXXXXXX 20/20
multipathd> fail path sdd
device-mapper: multipath: Failing path 8:48.
ok
multipathd> show paths
hcil    dev dev_t pri dm_st  chk_st dev_st  next_check      
2:0:1:0 sdd 8:48  0   failed faulty running X......... 3/20 
2:0:0:0 sdb 8:16  1   active ready  running XXXXXX.... 12/20
multipathd> reinstate path sdd
ok
multipathd> show paths
hcil    dev dev_t pri dm_st  chk_st dev_st  next_check      
2:0:1:0 sdd 8:48  0   active faulty running .......... 1/20 
2:0:0:0 sdb 8:16  1   active ready  running XXXXX..... 10/20
multipathd> 
multipathd> 
multipathd> show paths
hcil    dev dev_t pri dm_st  chk_st dev_st  next_check      
2:0:1:0 sdd 8:48  0   active ready  running XX........ 5/20 
2:0:0:0 sdb 8:16  1   active ready  running XXXXXXXXX. 19/20
multipathd> 
```


## ZFS

```bash
# export NOINUSE_CHECK=1
format
iscsiadm list target -v
ls /dev/rdsk
ls /dev/rdsk/*s0
newfs /dev/rdsk/c0t55CD2E40000D01B5d0s0
cat >>/etc/vfstab<<EOF
/dev/dsk/c0t55CD2E40000D01B5d0s0 /dev/rdsk/c0t55CD2E40000D01B5d0s0 /u01/app/oracle/db/dbs ufs 2 yes -
EOF
mount /u01/app/oracle/db/dbs
##############################################

#### ZFS new pool ##################
zpool status
zpool create ssdpool c0t55CD2E40000D01B5d0
zpool status
zfs list

# zpool destroy ssdpool

### Set new size
zfs get volsize qafarm/ldom-multi-disk-3
#NAME                      PROPERTY  VALUE  SOURCE
#qafarm/ldom-multi-disk-3  volsize   3G     local
zfs set volsize=100g qafarm/ldom-multi-disk-3
zfs get volsize qafarm/ldom-multi-disk-3
#NAME                      PROPERTY  VALUE  SOURCE
#qafarm/ldom-multi-disk-3  volsize   100G   local

#To Export a Physical Disk as a Virtual Disk
#For example, to export the physical disk clt48d0 as a virtual disk,
#you must export slice 2 of that disk (clt48d0s2) from the service domain as follows.
ldm add-vdsdev /dev/dsk/c1t48d0s2 c1t48d0@primary-vds0
#From the service domain, assign the disk (pdisk) to guest domain ldg1, for example.
ldm add-vdisk pdisk c1t48d0@primary-vds0 ldg1

#######################################
zfs snapshot qafarm/ca-qasparc-ldom11_disk0@solarisinstalled
zfs list -t snapshot

zfs rollback qafarm/ca-qasparc-ldom11_disk0@solarisinstalled
zfs rollback -f qafarm/ca-qasparc-ldom11_disk0@solarisinstalled

#######################################

zfs set mountpoint=/u01 XXX
```
