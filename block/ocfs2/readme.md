```bash
yum -y install ocfs2-tools.sparc64
which mkfs.ocfs2 o2cb_ctl

/etc/init.d/o2cb load

o2cb add-cluster mycluster
o2cb add-node mycluster test137 --ip 10.147.27.137
o2cb add-node mycluster test157 --ip 10.147.27.157

# mkfs.ocfs2 -C 16K -J size=128M -N 2 --fs-feature-level=max-features --fs-features=norefcount /dev/vdiskc
mkfs.ocfs2 -F /dev/vdiskc

# o2cb add-heartbeat mycluster /dev/vdiskc
# o2cb heartbeat-mode mycluster global

service o2cb configure
#Configuring the O2CB driver.
#
#This will configure the on-boot properties of the O2CB driver.
#The following questions will determine whether the driver is loaded on
#boot.  The current values will be shown in brackets ('[]').  Hitting
#<ENTER> without typing an answer will keep that current value.  Ctrl-C
#will abort.
#
#Load O2CB driver on boot (y/n) [n]: y
#Cluster stack backing O2CB [o2cb]: 
#Cluster to start on boot (Enter "none" to clear) [ocfs2]: mycluster
#Specify heartbeat dead threshold (>=7) [31]: 
#Specify network idle timeout in ms (>=5000) [30000]: 
#Specify network keepalive delay in ms (>=1000) [2000]: 
#Specify network reconnect delay in ms (>=2000) [2000]: 
#Writing O2CB configuration: OK
#Setting cluster stack "o2cb": OK
#Registering O2CB cluster "mycluster": OK
#Setting O2CB cluster timeouts : OK
#Starting global heartbeat for cluster "mycluster": OK

service o2cb online

cat /etc/ocfs2/cluster.conf 
cluster:
    heartbeat_mode = local
    node_count = 2
    name = mycluster

node:
    number = 0
    cluster = mycluster
    ip_port = 7777
    ip_address = 10.147.27.137
    name = test137

node:
    number = 1
    cluster = mycluster
    ip_port = 7777
    ip_address = 10.147.27.157
    name = test157



##################################
f_get_ocfs2_info() {
    local device=$1
    tunefs.ocfs2 -Q 'bs=%B cs=%T nodes=%N label=%V\n%M %H %O\n' ${device} 
}

f_get_free_blocks() {
    local device=$1
    df --block-size${2:-1K} ${device} | tail -1 | awk '{print $4}'
}

##################################
# if smth goes wrong:
umount /testocfs2
mkfs.ocfs2 -M local -F /dev/vdiskb
##################################
# >> /etc/profile
PYTHONPATH="/home/ocfs2test/bin/ocfs2/bin"
PATH=$PATH:/usr/lib64/openmpi/bin


#### ocfs2 test suite ###################
# as root:

yum install -y git \
 ocfs2-tools-devel openmpi libgomp openmpi-devel mpi-selector gcc autoconf libaio-devel e2fsprogs-devel nc reflink

groupadd ocfs2test
useradd -m -g ocfs2test ocfs2test
echo ocfs2test | passwd ocfs2test --stdin

sed -i 's/^.*Defaults\s*requiretty/#Defaults    requiretty/' /etc/sudoers
cat >> /etc/sudoers<<EOF
ocfs2test ALL=(ALL) NOPASSWD:ALL
EOF

# as ocfs2test:

ssh-keygen
ssh-copy-id ocfs2test@test157

PATH=$PATH:$HOME/bin:/usr/lib64/openmpi/bin:$HOME/bin/ocfs2/bin
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/lib64/openmpi/lib
export PATH
export LD_LIBRARY_PATH

git clone git://oss.oracle.com/git/ocfs2-test.git
cd ocfs2-test
make clean
./autogen.sh prefix=/
make
make install DESTDIR=/home/ocfs2test/bin/ocfs2

#
scp linux-2.6.0.tar.gz ocfs2test@10.147.27.157:/home/ocfs2test

#### run ####
sudo umount /mnt/ocfs2
cd /home/ocfs2test/bin/ocfs2/bin/
sed -i -e 's/rsh:ssh/ssh:rsh/g' *

./multiple_run.sh -i eth0 -k /home/ocfs2test/linux-2.6.0.tar.gz -n test137,test157 -d /dev/vdiskc -t multi_mmap /mnt/ocfs2

./single_run-WIP.sh -k ~/linux-2.6.0.tar.gz -m /mnt/ocfs2 -l ~/ocfs2log -d /dev/dm-3

./discontig_runner.sh -d /dev/vdiskc -m test137,test157 /mnt/ocfs2
```
