
## ol7: Configure block storage

```bash

#ipaddr=127.0.0.1
#ipaddr=10.0.0.20
ipaddr=10.162.81.55
iqn=iqn.2016-10.com.oracle
wwn=iqn.2008-08.com.example:555
device=/dev/sdb
name=blk1

systemctl stop firewalld
systemctl disable firewalld

yum -y install targetcli
#yum -y install iscsi-initiator-utils
systemctl start target.service

targetcli /backstores/block create ${name} ${device} 
targetcli iscsi/ create $wwn
targetcli iscsi/$wwn/tpg1/portals create
targetcli iscsi/$wwn/tpg1/luns create /backstores/block/${name}
targetcli ls


# client
yum -y install iscsi-initiator-utils
systemctl start iscsid.service
systemctl enable iscsid.service
initiator=$(cat /etc/iscsi/initiatorname.iscsi | sed -e 's/^InitiatorName=//')

# server
targetcli iscsi/$wwn/tpg1/acls create $initiator

# client
ipaddr=10.162.81.55
wwn=iqn.2008-08.com.example:555
iscsiadm --mode discoverydb --type sendtargets --portal $ipaddr --discover $ipaddr:3260,1 $wwn
iscsiadm --mode node --targetname $wwn --portal $ipaddr --login
lsblk

# iscsiadm --mode node --targetname $wwn --portal $ipaddr --logout
```

---------------------------

# client
```
[ 2511.130559] Loading iSCSI transport class v2.0-870.
[ 2511.176768] iscsi: registered transport (tcp)
[ 2526.466346] scsi host3: iSCSI Initiator over TCP/IP
[ 2526.471841] scsi 3:0:0:0: Direct-Access     LIO-ORG  blk1             4.0  PQ: 0 ANSI: 5
[ 2526.474092] scsi 3:0:0:0: alua: supports implicit and explicit TPGS
[ 2526.475849] scsi 3:0:0:0: alua: port group 00 rel port 01
[ 2526.476216] scsi 3:0:0:0: alua: port group 00 state A non-preferred supports TOlUSNA
[ 2526.476219] scsi 3:0:0:0: alua: Attached
[ 2526.477542] sd 3:0:0:0: Attached scsi generic sg2 type 0
[ 2526.480143] sd 3:0:0:0: [sdb] 8388608 512-byte logical blocks: (4.29 GB/4.00 GiB)
[ 2526.481408] sd 3:0:0:0: [sdb] Write Protect is off
[ 2526.481412] sd 3:0:0:0: [sdb] Mode Sense: 43 00 10 08
[ 2526.481901] sd 3:0:0:0: [sdb] Write cache: enabled, read cache: enabled, supports DPO and FUA
[ 2526.495342] sd 3:0:0:0: [sdb] Attached SCSI disk
[ 2651.569236] sd 3:0:0:0: alua: Detached
[ 2651.569424] sd 3:0:0:0: [sdb] Synchronizing SCSI cache
```

# server

```
[ 1451.520474] Rounding down aligned max_sectors from 4294967295 to 4294967288
[ 1583.596267] Unable to load target_core_user
[ 1583.597093] Rounding down aligned max_sectors from 65535 to 65528

```


yum -y install scsi-target-utils.sparc64
tgtadm --help
service tgtd start
tgtadm --lld iscsi --mode target --op show
tgtadm --lld iscsi --op new --mode target --tid=1 --targetname iqn.2013-11.com.oracle:target1
tgtadm --lld iscsi --mode target --op show
tgtadm --lld iscsi --op new --mode logicalunit --tid 1 --lun 1 --backing-store /dev/vdiskb
tgtadm --lld iscsi --mode target --op show
service iptables stop
man tgtadm
tgtadm --lld iscsi --op bind --mode target --tid 1 --initiator-address ALL
tgtadm --lld iscsi --mode target --op show

##### Client
yum -y install iscsi-initiator-utils
service iscsid status
service iscsid start
service iptables stop
iscsiadm --mode discoverydb --type sendtargets --portal 144.25.13.59 --discover 144.25.13.59:3260,1 iqn.2013-11.com.oracle:target1
iscsiadm --mode node --targetname iqn.2013-11.com.oracle:target1 --portal 144.25.13.59 --login
ls -lR /var/lib/iscsi/nodes/
fdisk -l|grep Disk

iscsiadm --mode session --sid 1
iscsiadm --mode session --logout --sid 1

#####

tgtadm --lld iscsi --op show --mode conn --tid 1

### Log out on client:
iscsiadm --mode node --targetname iqn.2013-11.com.oracle:target1 --portal 144.25.13.59 --logout

### Close conn from server
tgtadm --lld iscsi --op delete --mode conn --tid 1 --sid 4 --cid 0



##### sparc20
iscsiadm --mode discoverydb --type sendtargets --portal 144.25.12.214 --discover 144.25.12.214:3260,1 iqn.2013-12.com.oracle:targetsdb
iscsiadm --mode node --targetname iqn.2013-12.com.oracle:targetsdb --portal 144.25.12.214 --login


iscsiadm -m node --op update -n node.session.nr_sessions -v 4

iscsiadm -m session
#tcp: [1] 144.25.12.214:3260,1 iqn.2013-12.com.oracle:targetsdb 
iscsiadm --mode session -r 1 --op new
#Logging in to [iface: default, target: iqn.2013-12.com.oracle:targetsdb, portal: 144.25.12.214,3260] (multiple)
#Starting iscsid: Bridge firewalling registered
#Program python tried to access /dev/mem between f0000->100000.
#nr_pdflush_threads exported in /proc is scheduled for removal
#sysctl: The scan_unevictable_pages sysctl/node-interface has been disabled for lack of a legitimate use case.  If you have one, please send an email to linux-mm@kvack.org.


#####
  133  iscsiadm -m iface -I eth0 -o new
  134  iscsiadm -m iface -I eth1 -o new
  135  ls /var/lib/iscsi/ifaces/
  136  cat /var/lib/iscsi/ifaces/eth0

cat /var/lib/iscsi/ifaces/eth0
# BEGIN RECORD 6.2.0-873.2.el6
iface.iscsi_ifacename = eth0
iface.hwaddress = 00:14:4F:FA:92:1D
iface.transport_name = tcp
iface.vlan_id = 0
iface.vlan_priority = 0
iface.iface_num = 0
iface.mtu = 0
iface.port = 0
# END RECORD
[root@ca-qasparc-ldom12 ~]# iscsiadm -m iface -I eth1 --op=update -n iface.hwaddress -v 00:14:4F:F8:32:48
eth1 updated.
[root@ca-qasparc-ldom12 ~]# 
[root@ca-qasparc-ldom12 ~]# 
[root@ca-qasparc-ldom12 ~]# isciadm -m iface
-bash: isciadm: command not found
[root@ca-qasparc-ldom12 ~]# iscsiadm -m iface
default tcp,<empty>,<empty>,<empty>,<empty>
iser iser,<empty>,<empty>,<empty>,<empty>
eth1 tcp,00:14:4F:F8:32:48,<empty>,<empty>,<empty>
eth0 tcp,00:14:4F:FA:92:1D,<empty>,<empty>,<empty>

###########################################
iscsiadm --mode discoverydb --type sendtargets --portal 172.16.0.238 --discover 172.16.0.238:3260,1 iqn.1986-03.com.sun:02:aa1ff954-7df2-44c0-bfd7-f947299abe29
iscsiadm --mode node --targetname iqn.1986-03.com.sun:02:aa1ff954-7df2-44c0-bfd7-f947299abe29 --portal 172.16.0.238 --login --interface eth1
iscsiadm -m session
#tcp: [1] 172.16.0.238:3260,2 iqn.1986-03.com.sun:02:aa1ff954-7df2-44c0-bfd7-f947299abe29 



############
iscsiadm --mode node --targetname iqn.1986-03.com.sun:02:b465bf4a-c097-ed98-df5f-c27fad2a2981 --portal 144.25.12.214 --interface eth0 --login
iscsiadm -m session
blkid
service multipathd start
mpathconf --find_multipaths y --user_friendly_names y

cat >> /etc/multipath/wwids <<EOF
/3600144f0ec15cf00000050b4beb30001/
EOF

service multipathd reload
multipath -ll
multipath -v3

blkid | grep /dev/mapper/vmstor2

scsi_id --whitelist --device  /dev/sda
scsi_id --whitelist --device  /dev/sdb
dmsetup ls
rpm -qa | grep device-mapper-multipath
lsmod | grep dm_multipath



#### client messages: ####
scsi0 : iSCSI Initiator over TCP/IP
scsi 0:0:0:0: RAID              IET      Controller       0001 PQ: 0 ANSI: 5
scsi 0:0:0:1: Direct-Access     IET      VIRTUAL-DISK     0001 PQ: 0 ANSI: 5
sd 0:0:0:1: [sda] 209715200 512-byte logical blocks: (107 GB/100 GiB)
sd 0:0:0:1: [sda] Write Protect is off
sd 0:0:0:1: [sda] Write cache: enabled, read cache: enabled, doesnt support DPO or FUA
 sda: unknown partition table
sd 0:0:0:1: [sda] Attached SCSI disk
scsi 0:0:0:0: Attached scsi generic sg0 type 12
sd 0:0:0:1: Attached scsi generic sg1 type 0
Bridge firewalling registered
Program python tried to access /dev/mem between f0000->100000.
nr_pdflush_threads exported in /proc is scheduled for removal
sysctl: The scan_unevictable_pages sysctl/node-interface has been disabled for lack of a legitimate use case.  If you have one, please send an email to linux-mm@kvack.org.
ip_tables: (C) 2000-2006 Netfilter Core Team

######
### SOLARIS ###
root@solaris:~# pkg install group/feature/storage-server
           Packages to install:  21
       Create boot environment:  No
Create backup boot environment: Yes
            Services to change:   1

DOWNLOAD                                PKGS         FILES    XFER (MB)   SPEED
Completed                              21/21     1149/1149    72.9/72.9 23.4M/s

PHASE                                          ITEMS
Installing new actions                     2004/2004
Updating package state database                 Done 
Updating image state                            Done 
Creating fast lookup database                working -
Creating fast lookup database                   Done 
root@solaris:~# 
root@solaris:~# 
root@solaris:~# svcadm enable stmf
root@solaris:~# svcs stmf
STATE          STIME    FMRI
online         12:58:23 svc:/system/stmf:default
root@solaris:~# zpool list
NAME      SIZE  ALLOC   FREE  CAP  DEDUP  HEALTH  ALTROOT
ldompool  278G   169G   109G  60%  1.00x  ONLINE  -
rpool     278G   261G  17.4G  93%  1.00x  ONLINE  -
root@solaris:~# zfs list
NAME                                USED  AVAIL  REFER  MOUNTPOINT
ldompool                            270G  3.38G    31K  /ldompool
ldompool/ols_disk                   128G  52.7G  78.6G  -
ldompool/small_disk                14.4G  15.1G  2.67G  -
ldompool/sol_disk                   128G  43.5G  87.8G  -
rpool                               263G  10.9G  73.5K  /rpool
rpool/ROOT                          190G  10.9G    31K  legacy
rpool/ROOT/solaris                 9.59M  10.9G  1.80G  /
rpool/ROOT/solaris-1               5.60M  10.9G  2.32G  /
rpool/ROOT/solaris-1/var           2.10M  10.9G   199M  /var
rpool/ROOT/solaris-2                190G  10.9G   129G  /
rpool/ROOT/solaris-2-backup-1      1.12M  10.9G  22.1G  /
rpool/ROOT/solaris-2-backup-1/var    83K  10.9G  46.1G  /var
rpool/ROOT/solaris-2-backup-2       132K  10.9G   129G  /
rpool/ROOT/solaris-2-backup-2/var    52K  10.9G  45.8G  /var
rpool/ROOT/solaris-2/var           47.5G  10.9G  45.7G  /var
rpool/ROOT/solaris/var             2.81M  10.9G   180M  /var
rpool/VARSHARE                     1.50G  10.9G  1.50G  /var/share
rpool/dump                         66.0G  12.9G  64.0G  -
rpool/export                        962M  10.9G    32K  /export
rpool/export/home                   962M  10.9G   962M  /export/home
rpool/swap                         4.13G  11.0G  4.00G  -
root@solaris:~# ldm list
NAME             STATE      FLAGS   CONS    VCPU  MEMORY   UTIL  NORM  UPTIME
primary          active     -n-c--  UART    256   261632M  0.4%  0.2%  22m
root@solaris:~# zfs create -V 2g rpool/vol1
root@solaris:~# ls /dev/zvol/rdsk/rpool/
dump      export    ROOT      swap      VARSHARE  vol1
root@solaris:~# stmfadm create-lu /dev/zvol/rdsk/rpool/vol1
Logical unit created: 600144F00010E02DB02252949BF40001
root@solaris:~# stmfadm list-lu
LU Name: 600144F00010E02DB02252949BF40001
root@solaris:~# stmfadm add-view 600144F00010E02DB02252949BF40001
root@solaris:~# stmfadm list-view -l 600144F00010E02DB02252949BF40001
View Entry: 0
    Host group   : All
    Target Group : All
    LUN          : Auto
root@solaris:~# svcadm enable -r svc:/network/iscsi/target:default
root@solaris:~# svcs -l iscsi/target
fmri         svc:/network/iscsi/target:default
name         iscsi target
enabled      true
state        online
next_state   none
state_time   November 26, 2013 01:03:58 PM UTC
logfile      /var/svc/log/network-iscsi-target:default.log
restarter    svc:/system/svc/restarter:default
manifest     /lib/svc/manifest/network/iscsi/iscsi-target.xml
dependency   require_any/error svc:/milestone/network (online)
dependency   require_all/none svc:/system/stmf:default (online)
root@solaris:~# itadm create-target
Target iqn.1986-03.com.sun:02:b465bf4a-c097-ed98-df5f-c27fad2a2981 successfully created
root@solaris:~# itadm list-target -v
TARGET NAME                                                  STATE    SESSIONS 
iqn.1986-03.com.sun:02:b465bf4a-c097-ed98-df5f-c27fad2a2981  online   0        
        alias:                  -
        auth:                   none (defaults)
        targetchapuser:         -
        targetchapsecret:       unset
        tpg-tags:               default
root@solaris:~# 

[root@ovs147 ~]# history
    8  cat /etc/iscsi/iscsid.conf 
    9  grep -v -e "^#" -e "^$" /etc/iscsi/iscsid.conf 
   10  iscsiadm -m node
   11  iscsiadm -m discovery -t st -p 172.16.0.238
   12  route 
   13  iscsiadm -m discovery -t st -p 192.168.10.238
   14  iscsiadm -m node
   15  iscsiadm -m node -l -T iqn.1986-03.com.sun:02:aa1ff954-7df2-44c0-bfd7-f947299abe29
   16  iscsiadm -m session
   17  ifup eth1
   18  ifconfig
   19  iscsiadm -m session
   20  iscsiadm -m session -P3
   21  mpathconf -l
   22  multipath -l
   23  fdisk -l /dev/mapper/3600144f0ec15cf00000050b4bfc5000a
   24  dd if=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a of=/dev/null bs=512 count=1k
   25  top
   26  dd if=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a of=/dev/null bs=512 count=1m
   27  dd if=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a of=/dev/null bs=512 count=1M
   28  top
   29  dd if=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a of=/dev/null bs=512 count=10M
   30  dd if=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a of=/dev/null bs=1024 count=10M
   31  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/dev/urandom bs=512 count=1M
   32  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/dev/zero bs=512 count=1M
   33  sync
   34  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/dev/zero bs=512 count=10M
   35  mount
   36  ifconfig 
   37  ifconfig 1052f9169a 172.160.0.147 netmask 255.255.248.0
   38  route 
   39  ifconfig 1052f9169a 172.16.0.147 netmask 255.255.248.0
   40  route 
   41  ping 172.16.0.238
   42  iscsiadm -m node -u
   43  iscsiadm -m node -l -T iqn.1986-03.com.sun:02:aa1ff954-7df2-44c0-bfd7-f947299abe29
   44  multipath -l
   45  mkdir /tmp/nfs
   46  mount 192.168.10.238:/share/029 /tmp/nfs
   47  ls /tmp/nfs/
   48  ls /tmp/nfs/VirtualDisks/
   49  ls /tmp/nfs/Templates/
   50  uount /tmp/nfs
   51  mount 192.168.10.238:/share/002 /tmp/nfs
   52  ls /tmp/nfs/VirtualDisks/
   53  ls -lh /tmp/nfs/VirtualDisks/
   54  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/tmp/nfs/VirtualDisks/0004fb0000120000af794a093c7cf639.img
   55  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/tmp/nfs/VirtualDisks/0004fb0000120000af794a093c7cf639.img bs=4096 oflags=direct
   56  dd of=/dev/mapper/3600144f0ec15cf00000050b4bfc5000a if=/tmp/nfs/VirtualDisks/0004fb0000120000af794a093c7cf639.img bs=4096 oflag=direct
   57  uount /tmp/nfs
   58  umount /tmp/nfs
   59  umount /tmp/nfs
   60  umount /tmp/nfs
   61  iscsiadm -m node -u
   62  iscsiadm -m node -u
   63  xm list
   64  history
   65  iscsiadm -m node
   66  mount
   67  blkid
   68  exit
   69  history

