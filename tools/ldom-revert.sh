-> set /SP reset_to_defaults=factory
Set 'reset_to_defaults' to 'factory'
The configuration change will be applied the next time the SP is reset.

-> reset /SP
Resetting the SP will now reset the configuration of this system to default values.
Are you sure you want to reset /SP (y/n)? y
Performing reset on /SP

  983  ldm list-config
  984  yum list | grep ldm
  985  yum list ldoms
  986  yum list ldoms ldomsmanager
  987  yum install -y  ldoms ldomsmanager
  988  service ldmad start; service ldmd start
  989  ldm ls-config
  990  ldm ls-services
  991  ldm list
  992  ldm list-spconfig
  993  service iptables status
  994  service NetworkManager status
  995  service ldmd restart
  996  ifconfig -a
  997  ldm list-spconfig
  998  exit
  999  ldm list
 1000  ldm list-spconfig
 1001  ifconfig -a
 1002  service ldmd restart
 1003  ifconfig -a
 1004  ldm list-spconfig
 1005  ldm rm-config jfk_config
 1006  ldm set-config factory-default
