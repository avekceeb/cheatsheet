	mkdir ocv
	cd ocv/
	git clone https://github.com/opencv/opencv.git
	git clone https://github.com/opencv/opencv_contrib.git
	cd opencv
	git checkout tags/3.4.1
	mkdir build
	cd build/
	cmake -D CMAKE_BUILD_TYPE=DEBUG -D CMAKE_INSTALL_PREFIX=/home/qa/lib ..
	make -j3
	make install

