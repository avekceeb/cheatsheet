Verified with the latest UEK4QU6 kernel 4.1.12-112.9.1.el7uek.x86_64 &
the future  date (date >= 2070) setting looks fine now , the recent commit
has addressed this issue ( which were failing earlier ) , but still cant set
the old date (date <= 1969)  which is fine I think hardware clock setting
of future dates & minimal and considerable old date range.

# uname -r
4.1.12-112.8.1.el7uek.x86_64

# hwclock --show
Mon 27 Nov 2017 04:45:13 PM PST  -1.000555 seconds

# hwclock --set --date 11/27/1969
hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid
argument

# hwclock --set --date 11/27/1970
# hwclock --show
Fri 27 Nov 1970 12:00:04 AM PST  -0.391043 seconds
 
# hwclock --set --date 11/27/2070
hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid
argument

# hwclock --set --date 11/27/2069
# hwclock --show
Wed 27 Nov 2069 12:00:05 AM PST  -0.797298 seconds            

# hwclock --set --date 11/27/1917
hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid
argument

--------------------------------------------
#With the patched latest UEK4QU6 kernel ,

    #  uname -r
    4.1.12-112.9.1.el7uek.x86_64

    # hwclock --show
    Mon 27 Nov 2017 08:02:34 AM PST  -0.359817 seconds

    # hwclock --set --date 11/27/1917
    hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid argument

    # hwclock --set --date 11/27/1969
    hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid argument

    # hwclock --set --date 11/27/1970
    # hwclock --show
    Fri 27 Nov 1970 12:00:05 AM PST  -0.047316 seconds

    # hwclock --set --date 11/27/2070
    # hwclock --show
    Thu 27 Nov 2070 12:00:03 AM PST  -0.484796 seconds

    # hwclock --set --date 11/27/2117
    # hwclock --show
    Sat 27 Nov 2117 12:00:03 AM PST  -0.908321 seconds


Source inclusion verified :

# rpm -q --changelog kernel-uek-4.1.12-112.9.1.el7uek.x86_64 | grep 27025943
- rtc: cmos: century support (Sylvain Chouleur)  [Orabug: 27025943]


http://ca-git.us.oracle.com/?p=linux-uek.git;a=commit;h=885c91cc1a2793d7c2ffba5a48543b2468408a14


rtc: cmos: century support

authorSylvain Chouleur <sylvain.chouleur@intel.com>
 Mon, 8 Jun 2015 09:45:19 +0000 (11:45 +0200)
committerDhaval Giani <dhaval.giani@oracle.com>
 Thu, 16 Nov 2017 18:19:25 +0000 (13:19 -0500)
commit885c91cc1a2793d7c2ffba5a48543b2468408a14
tree67e326ad9c36979d6c3540ba69665bd7a273f9d1tree | snapshot
parent11886356708637ebbb582a2eac43d460ccf24bb6commit | diff

rtc: cmos: century support

If century field is supported by the RTC CMOS device, then we should use
it and then do not consider years greater that 169 as an error.
For information, the year field of the rtc_time structure contains the
value to add to 1970 to obtain the current year.
This was a hack to be able to support years for 1970 to 2069.
This patch remains compatible with this implementation.

Signed-off-by: Sylvain Chouleur <sylvain.chouleur@intel.com>
Signed-off-by: Alexandre Belloni <alexandre.belloni@free-electrons.com>

(cherry picked from commit 3c217e51d8a272b9301058fe845d6c69cc0651cb)
Orabug: 27025943

Signed-off-by: Kirtikar Kashyap <kirtikar.kashyap@oracle.com>
Reviewed-by: Jack Vogel <jack.vogel@oracle.com>
Signed-off-by: Dhaval Giani <dhaval.giani@oracle.com>

----------------------------------------------------------------
Bug filer pls verify  bug scenario with the latest UEK4QU6 kernel
version >= kernel-uek-4.1.12-112.9.1.

--------------------------------------------

[root@dummy74 ~]# hwclock --set --date 11/27/2070
hwclock: ioctl(RTC_SET_TIME) to /dev/rtc to set the time failed.: Invalid argument
[root@dummy74 ~]# uname -r
4.1.12-124.3.1.el7uek.x86_64
[root@dummy74 ~]# rpm -qa | grep kernel-uek
kernel-uek-firmware-4.1.12-94.3.9.el7uek.noarch
kernel-uek-4.1.12-124.3.1.el7uek.x86_64
kernel-uek-4.1.12-94.3.9.el7uek.x86_64
kernel-uek-firmware-4.1.12-124.3.1.el7uek.noarch
[root@dummy74 ~]# rpm -q --changelog kernel-uek-4.1.12-124.3.1.el7uek.x86_64 | grep 27025943

