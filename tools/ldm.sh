#!/bin/bash
exit 1

######################################################
### Initial Configuration
ldm add-vds primary-vds0 primary
ldm add-vsw net-dev=net0 primary-vsw1 primary
ldm add-vcc port-range=5000-5100 primary-vcc0 primary
#ldm set-mau 1 primary
#  crypto
ldm set-vcpu 64 primary
ldm set-memory 64G primary
######################################################
ldm set-vswitch  inter-vnet-link=off primary-vsw1
######################################################
ldm set-var auto-boot\?=false <domain>
#ldm rm-config initial
ldm add-config initial
ldm list-config
ldm set-spconfig initial
ldm list-spconfig
touch /reconfigure
shutdown -i6 -g0 -y
######################################################
zfs create rpool/linux
svcadm enable vntsd

ldm add-domain solaris
ldm add-vcpu 64 solaris
ldm add-memory 64g solaris
ldm add-vnet vnet0 primary-vsw1 solaris

######################################################
### Same disk to multi domains:
zfs create -V 8G qafarm/ldom-multi-disk-0
ldm add-vdsdev -f /dev/zvol/dsk/qafarm/ldom-multi-disk-0 multidisk0a@primary-vds0
ldm add-vdsdev -f /dev/zvol/dsk/qafarm/ldom-multi-disk-0 multidisk0b@primary-vds0
######################################################

### snapshot disk ###

vm=alexeev
disk=disk0
vds=alexeev-sys

ldm rm-vdisk $disk $vm
ldm rm-vdsdev ${vds}@primary-vds0
zfs snapshot rpool/${vds}@freshinstall
ldm add-vdsdev /dev/zvol/dsk/rpool/${vds} ${vds}@primary-vds0
ldm add-vdisk $disk ${vds}@primary-vds0 ${vm}

ldm start ${vm}

####################################


ldm add-vdsdev options=ro /qafarm/OL-201405161634-R6-U5-sparc-dvd.iso exadata-beta3-ldom11@primary-vds0

f_build_ldom () {
    local VMNAME=${1?'Ldom name should be set'}
    local DISKSIZE=16G
    local VCPU=4
    local VMEM=4G

    zfs create -V $DISKSIZE  qafarm/${VMNAME}_disk0
    ldm add-vdsdev /dev/zvol/dsk/qafarm/${VMNAME}_disk0 ${VMNAME}.disk0@primary-vds0
    ldm add-domain ${VMNAME}
    ### ??? ldm add-vdsdev -f options=ro /qafarm/OL-devel-201402080514-R6-U5-sparc-dvd.iso RC10iso@primary-vds0
    ldm set-vcpu ${VCPU} ${VMNAME}
    ldm set-mem ${VMEM} ${VMNAME}
    ldm add-vdisk dvd RC9iso@primary-vds0 ${VMNAME}
    ldm add-vdisk disk0 ${VMNAME}.disk0@primary-vds0 ${VMNAME}
    ldm add-vnet vnet0 primary-vsw0 ${VMNAME}
    ldm set-var boot-device=dvd ${VMNAME}
}

for i in 9 10 11 ; do
    f_build_ldom ca-qasparc-ldom${i} 
done

######################################################
### Save and restore config ##########################

ldm list-constraints -x >config.xml
ldm list-constraints -x ldom >ldom.xml

ldm add-domain -i ldom.xml
ldm init-system -r -i primary.xml

######################################################
  532  wget http://10.147.72.220/ISOs/build-isos/sparc64-sonoma-el6-u5-isos/ALPHA1-20160211/OL-201602111623-R6-U5-sparc-dvd.iso
  533  ls -l
  534  ln -s OL-201602111623-R6-U5-sparc-dvd.iso sonoma
  535  ls -l
  536  history
  537  ldm list
  540  ldm ls-services
  541  ldm add-vdsdev sonoma-alpha1-iso options=ro OL-201602111623-R6-U5-sparc-dvd.iso 
  542  ls -l
  543  ldm add-vdsdev options=ro OL-201602111623-R6-U5-sparc-dvd.iso sonoma-alpha1-iso@primary-vds0
  544  ldm add-vdsdev options=ro /root/alexeev/iso/OL-201602111623-R6-U5-sparc-dvd.iso sonoma-alpha1-iso@primary-vds0
  545  zfs create -V 16G /dev/zvol/dsk/rpool/alexeev-test-disk1
  546  zfs create -V 16G rpool/alexeev-test-disk1
  547  ldm add-vdsdev /dev/zvol/dsk/rpool/alexeev-test-disk1 alexeev-test-disk1@primary-vds0
  548* ldm l
  549  ldm ls-services
  550  ldm ls
  551  ldm add-dom alexeev-test1
  552  ldm add-vcpu 4 alexeev-test1
  553  ldm add-mem 4g alexeev-test1
  554  ldm add-vdisk disk1 alexeev-test-disk1@primary-vds0 alexeev-test1
  555  ldm add-vdisk dvd sonoma-alpha1-iso@primary-vds0 alexeev-test1
  556  ldm ls
  557  ldm bind alexeev-test1

########################
# restore vdisk snapshot
########################
ldm rm-vdisk disk0 alexeev-test
ldm rm-vdsdev alexeev-test-disk0@primary-vds0
zfs rollback rpool/alexeev-test-disk0@exadatabeta4
ldm add-vdsdev /dev/zvol/dsk/rpool/alexeev-test-disk0  alexeev-test-disk0@primary-vds0
ldm add-vdisk disk0 alexeev-test-disk0@primary-vds0 alexeev-test
###############
ldm rm-vdisk dvd alexeev-test2
ldm rm-vdisk disk0 alexeev-test2
ldm rm-vdsdev alexeeev-bulk4@primary-vds0
zfs rollback rpool/alexeeev-bulk4@lfsinstalled
ldm add-vdsdev /dev/zvol/dsk/rpool/alexeeev-bulk4 alexeeev-bulk4@primary-vds0
ldm add-vdisk disk0 alexeeev-bulk4@primary-vds0 alexeev-test2


##### TODO:
[root@localhost ~]# ifdown eth0
/etc/sysconfig/network-scripts/ifdown-eth: line 29: /etc/sysconfig/network: No such file or directory
/etc/sysconfig/network-scripts/ifdown-ipv6: line 39: /etc/sysconfig/network: No such file or directory
./network-functions: line 390: /etc/sysconfig/network: No such file or directory

######################################################

root@solaris:~# ldm ls -l -p dom88
VERSION 1.9
DOMAIN|name=dom88|state=active|flags=transition|cons=5087|ncpu=2|mem=2147483648|util=50.0|uptime=28|norm_util=50.0|softstate=OpenBoot Running OS Boot
UUID|uuid=7e21ac44-bb5c-6546-9fc6-ddcacd37cd54
MAC|mac-addr=00:14:4f:f9:1c:cd
HOSTID|hostid=0x84f91ccd
CONTROL|failure-policy=ignore|extended-mapin-space=off|cpu-arch=native|rc-add-policy=|shutdown-group=15
DEPENDENCY|master= 
CORE
|cid=29|cpuset=236,237
VCPU
|vid=0|pid=236|util=100%|strand=100|cid=29|norm_util=100%
|vid=1|pid=237|util=0.0%|strand=100|cid=29|norm_util=0.0%
MEMORY
|ra=0x30000000|pa=0x819b0000000|size=2147483648
CONSTRAINT|cpu=|max-cores=|threading=max-throughput|physical-bindings=
VNET|name=vnet0|dev=network@0|service=VSW0@primary|mac-addr=00:14:4f:fa:60:d2|mode=|pvid=1|vid=|mtu=1500|linkprop=|id=0
VDISK|name=hd0|vol=dom88@VDS0|timeout=|dev=disk@0|server=primary|mpgroup=|id=0
VCONS|group=dom88|service=VCC0@primary|port=5087|log=on

f_create_domain () {
    local domname=$1
    local cpucount=$2
    local memgigs=$3
    local diskpath=$4
    local diskgigs=$5
    local isopath=$6
    local switchname=$7
    local vdsname=$(f_get_vds_name)



    ldm add-domain $domname
    ldm set-vcpu $cpucount $domname
    ldm set-memory ${memgigs}G $domname

    # HD 
    # IF THERE IS NO VDS YET 
    if [ "x" == "x"${vdsname} ] ; then
        vdsname=VDS0
        ldm add-vds $vdsname primary
    fi        

    if ! (ldm list-services -p | grep -c ${diskpath} >/dev/null 2>&1) ; then 
        rm -f ${diskpath}
        mkfile ${diskgigs}g ${diskpath}
        ldm add-vdsdev ${diskpath} ${diskpath##*/}@${vdsname} 
    fi

    ldm add-vdisk hd0  ${diskpath##*/}@${vdsname}  $domname

    # NETWORK
    if [ "x" != "x"${switchname} ] ; then 
        ldm add-vnet vnet0 $switchname $domname
    fi
    
    # DVD
    if [ "x" != "x"${isopath} ] ; then
        local isoname=${isopath##*/}
        isoname=${isoname%.iso}
        if ! (ldm list-services -p | grep -c "${isopath}" >/dev/null 2>&1) ; then
            ldm add-vdsdev ${isopath} ${isoname}@${vdsname}
        fi
        ldm add-vdisk dvd ${isoname}@${vdsname} $domname
        ldm set-var boot-device=dvd $domname
    fi    
}

for i in $(seq 4 50) ; do
> cp /rpool/vd/gold /rpool/vd/dom${i} ; \
> ldm add-vdsdev /rpool/vd/dom${i} dom${i}@VDS0 ; \
> f_create_domain dom${i} 2 2 /rpool/vd/dom${i} ; done

ldm list -p | grep '|name=dom4|'
DOMAIN|name=dom3|state=active|flags=-n----|cons=5000|ncpu=2|mem=2147483648|util=0.3|uptime=40088|norm_util=0.3

for i in $(seq 5 68) ; do ldm add-vnet vnet0 VSW0 dom${i} ; done

############################################################
ldm set-vnet [mac-addr=num] [vswitch=vswitch-name] [mode=[hybrid]]
       [alt-mac-addrs=auto|num1[,auto|num2,...]] [vid=vlan-id1,vlan-id2,...]
       [pvid=port-vlan-id] [linkprop=[phys-state]] [vid=vlan-id1,vlan-id2,...] [mtu=size]

# if we want to add another disk
#mkfile 4g /rpool/linux/$disk_name
#ldm add-vdsdev /rpool/linux/$disk_name $disk_name@primary-vds0                           
#ldm add-vdisk $disk_name $disk_name@primary-vds0 $guest_name

ldm bind $guest_name
ldm start $guest_name   

#########################################3
guest postconfiguration

ldm ls 
telnet localhost port_number

login into guest(root/usual password for our sparcs)

#up the eth0 

HWADDR="` ifconfig eth0 | grep HWaddr | awk '{print $5}'`"
echo $HWADDR
ifdown eth0
cat > ifcfg-eth0 <<-EOF
DEVICE=eth0
BOOTPROTO="dhcp"
NM_CONTROLLED="yes"
HWADDR=${HWADDR}
ONBOOT=yes
TYPE="Ethernet"
EOF

cp -f ifcfg-eth0 /etc/sysconfig/network-scripts/  # looks like -f option doesn't work for our linux.
ifup eth0


#configure uln repo
mv /etc/yum.repos.d/public-yum-ol6.repo /etc/yum.repos.d/public-yum-ol6.nonworkingrepo

cat > /etc/yum.repos.d/OL-packages.repo <<-EOF
[OL-sparc64]
name = OL for SPARC 6.4
baseurl = http://144.25.12.79/rpms/
gpgcheck = 0
enabled = 1
EOF
###################################
guest destroyment

ldm stop $guest_name
ldm unbind $guest_name

ldm rm-vdisk cdrom $guest_name
#ldm rm-vdisk vdisk1 $guest_name if it was added before

ldm rm-vnet vnet0 $guest_name

ldm destroy $guest_name

ldm rm-vdsdev $cdrom@primary-vds0
#ldm rm-vdsdev $disk_name@primary-vds0 # if it was created

#####################################################

     Action alias (verb)     ls               list
     Action alias (verb)     rm               remove
     Resource alias (noun)   config           spconfig
     Resource alias (noun)   crypto           mau
     Resource alias (noun)   dom              domain
     Resource alias (noun)   mem              memory
     Resource alias (noun)   var              variable
     Resource alias (noun)   vcc              vconscon
     Resource alias (noun)   vcons            vconsole
     Resource alias (noun)   vdpcc            ndpsldcc
     Resource alias (noun)   vdpcs            ndpsldcs
     Resource alias (noun)   vds              vdiskserver
     Resource alias (noun)   vdsdev           vdiskserverdevice
     Resource alias (noun)   vsw              vswitch
     Subcommand shortcut     bind             bind-domain
     Subcommand shortcut     cancel-op        cancel-operation
     Subcommand shortcut     create           add-domain
     Subcommand shortcut     destroy          remove-domain
     Subcommand shortcut     list             list-domain
     Subcommand shortcut     migrate          migrate-domain
     Subcommand shortcut     modify           set-domain
     Subcommand shortcut     panic            panic-domain
     Subcommand shortcut     start            start-domain
     Subcommand shortcut     stop             stop-domain
     Subcommand shortcut     unbind           unbind-domain

#################################################################

zfs create -V 100G  qafarm/ca-qasparc-ldom8_disk0
ldm add-vdsdev /dev/zvol/dsk/qafarm/ca-qasparc-ldom8_disk0 ca-qasparc-ldom8.disk0@primary-vds0
zfs create -V 100G  qafarm/ca-qasparc-ldom8_disk0
ls /dev/zvol/dsk/qafarm/
ls -l /dev/zvol/dsk/qafarm/ca-qasparc-ldom8_disk0
ldm add-vdsdev /dev/zvol/dsk/qafarm/ca-qasparc-ldom8_disk0 ca-qasparc-ldom8.disk0@primary-vds0
ldm list-services
ldm add-domain ca-qasparc-ldom8
ldm list
ldm set-vcpu 4 ca-qasparc-ldom8
ldm set-mem 4G ca-qasparc-ldom8
ldm list
ldm add-vdisk dvd RC9iso@primary-vds0 ca-qasparc-ldom8
ldm add-vdisk disk0 ca-qasparc-ldom8.disk0@primary-vds0 ca-qasparc-ldom8
ldm add-vnet vnet0 
ldm add-vnet vnet0 primary-vsw0 ca-qasparc-ldom8
ldm set-var boot-device=dvd ca-qasparc-ldom8

f_build_ldom () {
    local VMNAME=${1?'Ldom name should be set'}
    local DISKSIZE=16G
    local VCPU=4
    local VMEM=4G

    zfs create -V $DISKSIZE  qafarm/${VMNAME}_disk0
    ldm add-vdsdev /dev/zvol/dsk/qafarm/${VMNAME}_disk0 ${VMNAME}.disk0@primary-vds0
    ldm add-domain ${VMNAME}
    ldm set-vcpu ${VCPU} ${VMNAME}
    ldm set-mem ${VMEM} ${VMNAME}
    ldm add-vdisk dvd RC9iso@primary-vds0 ${VMNAME}
    ldm add-vdisk disk0 ${VMNAME}.disk0@primary-vds0 ${VMNAME}
    ldm add-vnet vnet0 
    ldm add-vnet vnet0 primary-vsw0 ${VMNAME}
    ldm set-var boot-device=dvd ${VMNAME}
}
##### LDOMS preparations #########
  501  format
  502  iscsiadm list target -v
  503  ls /dev/rdsk
  504  ls /dev/rdsk/*s0
  507  zpool list
  508  zfs list
  509  zfs create -v 130G ldompool/sol_disk
  510  zfs create -V 130G ldompool/sol_disk
  511  zfs list
  512  zfs create -V 130G ldompool/ols_disk
  513  zfs list
  514  zfs create -V 14G ldompool/small_disk
  515  zpool list
  516  zfs create -V 8G ldompool/small_disk
  517  zfs create -V 6G ldompool/small_disk
  518  man zfs
  519  zfs list
  520  zfs destroy ldompool/ols_disk
  521  zfs destroy ldompool/sol_disk
  522  zfs create -V 124G ldompool/ols_disk
  523  zfs create -V 124G ldompool/sol_disk
  524  zfs list
  525  zfs create -V 14G ldompool/small_disk
  526  zfs list
  527  which ldm
  528  man ldm
  529  ldm -V
  530   pkg list ldomsmanager
  531  man pkg
  532  pkg publisher
  533  pkg uninstall ldomsmanager ldoms-incorporation entire
  534  pkg uninstall ldomsmanager ldoms-incorporation
  535  pkg search ldomsmanager
  536  pkg install system/ldoms/ldomsmanager@3.1.0.0.24-0.175.1.10.0.4.0 ldoms-incorporation
  537  pkg list ldomsmanager

