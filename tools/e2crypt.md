[habr](https://habr.com/post/327682/)

	mkfs.ext4 -O encrypt /dev/sda1

	mount /dev/sda1 /mnt/test

	e4crypt add_key
	Enter passphrase (echo disabled): 
	Added key with descriptor [849f1331f75981f1]

	keyctl show
	Session Keyring
	 167622459 --alswrv      0     0  keyring: _ses
	 629992068 --alswrv      0 65534   \_ keyring: _uid.0
	 434756266 --alsw-v      0     0   \_ logon: ext4:849f1331f75981f1

	mkdir /mnt/test/cripted

	e4crypt set_policy 849f1331f75981f1 /mnt/test/cripted
	Key with descriptor [849f1331f75981f1] applied to /mnt/test/cripted.

	e4crypt get_policy /mnt/test/cripted
	/mnt/test/cripted: 849f1331f75981f1

	echo "hello there" > /mnt/test/cripted/xxx

	reboot

	mount /dev/sda1 /mnt/test/

	cd /mnt/test/cripted
	
	ls -l
	total 4
	-rw-r--r-- 1 root root 12 Nov  1 05:22 i8OuUI22Qa6VSh+948m+PC

	e4crypt add_key /mnt/test/cripted
	Enter passphrase (echo disabled): 
	Added key with descriptor [849f1331f75981f1]
	Key with descriptor [849f1331f75981f1] applied to /mnt/test/cripted.
	
	ls -l
	total 4
	-rw-r--r-- 1 root root 12 Nov  1 05:22 xxx
	
	cat xxx 
	hello there

	mkfs.ext4 /dev/sda2
	 debugfs -R features /dev/sda2
	debugfs 1.44.4 (18-Aug-2018)
	Filesystem features: has_journal ext_attr resize_inode dir_index filetype extent 64bit flex_bg sparse_super large_file huge_file dir_nlink extra_isize metadata_csum

	tune2fs -O encrypt /dev/sda2
	tune2fs 1.44.4 (18-Aug-2018)
	debugfs -R features /dev/sda2
	debugfs 1.44.4 (18-Aug-2018)
	Filesystem features: has_journal ext_attr resize_inode dir_index filetype extent 64bit flex_bg encrypt sparse_super large_file huge_file dir_nlink extra_isize metadata_csum


