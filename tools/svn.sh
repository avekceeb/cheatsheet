#!/bin/sh
exit 1
svn add ol-functional
cat > ~/svn.externals<<EOF
shunit2 http://ca-build32.us.oracle.com/svn/repos/rhel4/qa-auto-ol/infra/prod/shunit2/src/shunit2
resultdb.py http://ca-build32.us.oracle.com/svn/repos/rhel4/qa-auto-ol/infra/prod/resultdb/resultdb.py
EOF
svn propset svn:externals -F ~/svn.externals .
svn propget svn:externals
cd ..
svn ci -m'OL-Functional suite created' ./ol-functional
cd ol-functional/
