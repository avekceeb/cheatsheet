
[tutorial](https://www.atlassian.com/git/tutorials/learn-git-with-bitbucket-cloud)

[heads](https://stackoverflow.com/questions/2221658/whats-the-difference-between-head-and-head-in-git)


## Bisect

[bissect](https://git-scm.com/docs/git-bisect)

```
git bisect start
git bisect bad
git bisect good 9afa481db4abb7f0e389804f99b6c12ccf74f8a5
#Bisecting: 2 revisions left to test after this (roughly 1 step)
#[e548aa51d8f9ddc53710efd414b0942caf63091b] rev 2
sh ./checkme.sh && echo OK
git bisect good
# Bisecting: 0 revisions left to test after this (roughly 1 step)
#[76325cf8714df8aec89f8b1811dcb0e45fea9a03] rev 4
sh ./checkme.sh && echo OK
git bisect bad
#Bisecting: 0 revisions left to test after this (roughly 0 steps)
#[40ba30a067d194c7cff83fe020e4134faae37977] rev 3 - bad
```

## Config

```
cat ~/.gitconfig
[user]
[core]
    editor = vim
[http]
    proxy = http://proxy.com:80
[https]
    proxy = http://proxy.com:80

[user]
    email = someone@gmail.com
    name = Dmitry Alexeev

git config --get remote.origin.url

git config --global core.editor "vim"
git config --global diff.guitool meld
git config --global merge.tool meld
git config --global mergetool.meld.path /usr/bin/meld

```

## Search, Log etc

* show works of author:
`git log --author='Dave Aldridge' | grep commit`

* show commit
`git show 165050c19535518a2f70856da2fc405b8a73ce7d`

* Show all commits since version v2.6.12 that changed any file in  include/scsi or drivers/scsi
`git log v2.6.12.. include/scsi drivers/scsi`


* Find in commit messages:
`git log --grep=Some`

* Find in commit contents
`git log -SSome`


## Shallow clone
```
git clone -b master --depth 1 https://github.com/IIJ-NetBSD/netbsd-src.git
```

```
git branch #list branches
git checkout .
git commit -m<vvvv> <file>
git remote -v
#origin  ssh://qa@localhost/vol/dist/git/cheatsheet (fetch)
#origin  ssh://qa@localhost/vol/dist/git/cheatsheet (push)

# new branch:
git checkout -b branch-name

# switch to temp branch
git checkout -b temp-branch-name ea3d5ed039edd6d4a07cc41bd09eb58edd1f2b3a
# ... and then remove it

# remote
# host 'Server'
cd /vol/dist/git/mylinux
git init --bare #--share

# host 'Client'
git clone ssh://qa@10.162.81.114/vol/dist/git/mylinux
git commit -a
git push origin master
#git push ssh://qa@10.162.81.114/vol/dist/git/mylinux


# On Server Side:
git init --bare --share
#Initialized empty shared Git repository in /vol/dist/git/mywebfileserver/

# On Client Side (in case of existing local repo)
git remote add origin ssh://qa@localhost/vol/dist/git/mywebfileserver
git push -u origin master
#qa@localhost's password:
#Counting objects: 15, done.
#Delta compression using up to 8 threads.
#Compressing objects: 100% (10/10), done.
#Writing objects: 100% (15/15), 9.24 KiB, done.
#Total 15 (delta 4), reused 0 (delta 0)
#To ssh://qa@localhost/vol/dist/git/mywebfileserver
# * [new branch]      master -> master

git push origin WithPygments

# On another client:
git clone ssh://qa@localhost/vol/dist/git/mywebfileserver
git checkout WithPygments
#Branch WithPygments set up to track remote branch WithPygments from origin.
#Switched to a new branch 'WithPygments'

### UNDOING LOCAL CHANGES:
git reset
git checkout .
git clean -fdx

# or

git reset --hard
```

## File permmissions problem on Windows
```
git -c core.filemode=false diff
#And the --global flag will make it be the default behavior for the logged in user.
git config --global core.filemode false
```

## Checkout tag
```
# list the tags
git tag -l
#checkout a specific tag:
git checkout tags/<tag_name>
#checkout and create a branch (other you will be on a branch named after the revision number of tag):
git checkout tags/<tag_name> -b <tag_name>
```

## Patch

```
git format-patch -k --stdout 3fd547f43a93b603923e163e862d330ea9c4190b..fbc518ae16797c2ca54ce7a69bc6a64e3efe81d7 > one.patch
git am -k ../one.patch


git diff > 1.diff
git apply 1.diff

```


## Whole history of Linux kernel

I have a repository with a clone of the following remotes:

```
    https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git
    https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git
    https://git.kernel.org/pub/scm/linux/kernel/git/tglx/history.git
    https://git.kernel.org/pub/scm/linux/kernel/git/davej/history.git
```

And the following grafts (info/grafts):

```
1da177e4c3f41524e886b7f1b8a0c1fc7321cac2 e7e173af42dbf37b1d946f9ee00219cb3b2bea6a
7a2deb32924142696b8174cdf9b38cd72a11fc96 379a6be1eedb84ae0d476afbc4b4070383681178
```

With these grafts, I have an unbroken view of the kernel history since 0.01.
The first graft glues together the very first release in Linus repository with the corresponding release of tglx/history.git.
The second graft glues together tglx/history.git and davej/history.git.
There are a few older versions missing, and the older versions have release granularity instead of patch granularity, but this is the best setup I know of.
