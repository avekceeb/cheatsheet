cat .vnc/xstartup

#!/bin/sh

[ -r /etc/sysconfig/i18n ] && . /etc/sysconfig/i18n
export LANG
export SYSFONT
vncconfig -iconic &
unset SESSION_MANAGER
unset DBUS_SESSION_BUS_ADDRESS
if [ -f /etc/X11/xinit/xinitrc ]; then
  exec sh /etc/X11/xinit/xinitrc
fi
[ -r $HOME/.Xresources ] && xrdb $HOME/.Xresources

if test -z "$DBUS_SESSION_BUS_ADDRESS" ; then
    eval `dbus-launch --sh-syntax –exit-with-session`
    echo "D-BUS per-session daemon address is: \
        $DBUS_SESSION_BUS_ADDRESS"
fi
gnome-session & 
gnome-panel &
gnome-settings-daemon &
metacity &
#nautilus &

