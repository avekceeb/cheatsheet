for i in `seq 1 5000`; do 
    echo "abcdef" >> afile
done

#And then create a lot of reflinks, each slightly different:
for i in `seq 1 1000`; do
    cp --reflink afile copy$i
    for j in `seq 1 $i`; do
        echo $i >> copy$i
    done
done
