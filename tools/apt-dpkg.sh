#!/bin/sh

[apt-vs-yum](https://help.ubuntu.com/community/SwitchingToUbuntu/FromLinux/RedHatEnterpriseLinuxAndFedora)

exit 0
dpkg -l 'libsdl*'
apt-cache search sdl | grep dev
apt-get -y --force-yes install fbreader
# list files
dpkg -L libsdl1.2-dev

### build from source
apt-get source openssl
sudo apt-get build-dep openssl
cd openssl-1.0.2g/
dpkg-buildpackage -rfakeroot -uc -b 2>&1 | tee -a mybuild.log

