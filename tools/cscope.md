
# Cscope

	export CSCOPE_EDITOR=vim
	find . -name "*.c" -o -name "*.cpp" -o -name "*.h" -o -name "*.hpp" > cscope.files
	cscope -q -R -b -i cscope.files
	cscope -d

### Vim

	:help cscope
	:cs add ./cscope.out
	:cs find s main
	"If the results return only one match, you will automatically be taken to it.
	"If there is more than one match, you will be given a selection screen to pick
	"the match you want to go to.  After you have jumped to the new location,
	"simply hit Ctrl-T to get back to the previous one.
