#!/bin/sh

cat >./run-perf.sh<<EOF
#!/bin/sh
PIDS=""

for x in \$(seq 4); do
    dd if=/dev/zero of=/dev/null &
    PIDS="\$PIDS \$!"
    ping localhost >/dev/null &
    PIDS="\$PIDS \$!"
done

echo "pids runnning: \$PIDS for 30 sec"

sleep 30

kill -9 \$PIDS
EOF
############################

rm -rf perf.data

perf record -g sh run-perf.sh

perf report -g flat -i perf.data
perf report -v --pid=1855 -g flat -i perf.data
perf report --sort comm,dso --pid=1855 -g flat -i perf.data
perf report --header -g graph -i perf.data

#####################################################

perf stat -a -e Instr_all tar czf - /lib/modules/ > /dev/null
perf list | grep 'Kernel PMU event' | awk '{print $1}'

for x in $(perf list | grep 'Kernel PMU event' | awk '{print $1}') ; do perf stat -a -e $x ping -c1 localhost >/dev/null ; done

### other perf tools:

git clone https://github.com/brendangregg/perf-tools.git
cd perf-tools/fs
./cachestat

zypper install bcc-tools

# cd /sys/bus/event_source/devices/cpu/events
# ls -l
total 0
-r--r--r-- 1 root root 4096 Feb  2 08:27 branch-instructions
-r--r--r-- 1 root root 4096 Feb  2 08:27 branch-misses
-r--r--r-- 1 root root 4096 Feb  2 08:27 bus-cycles
-r--r--r-- 1 root root 4096 Feb  2 08:27 cache-misses
-r--r--r-- 1 root root 4096 Feb  2 08:27 cache-references
-r--r--r-- 1 root root 4096 Feb  2 08:27 cpu-cycles
-r--r--r-- 1 root root 4096 Feb  2 08:27 instructions
-r--r--r-- 1 root root 4096 Feb  2 08:27 mem-loads
-r--r--r-- 1 root root 4096 Feb  2 08:27 mem-stores
-r--r--r-- 1 root root 4096 Feb  2 08:27 stalled-cycles-frontend
[root@ca-ostest452 events]# cat branch-instructions
event=0xc4

