ifconfig eth0 mtu 9000
echo 'MTU 9000' >> /etc/sysconfig/network-script/ifcfg-eth0

/etc/init.d/networking restart 
# ip route get {IP-address}
# ip route get 192.168.1.1
Output:

192.168.1.1 dev eth0  src 192.168.1.100
    cache  mtu 9000 advmss 1460 hoplimit 64

#####
ip link show
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN 
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UNKNOWN qlen 1000
    link/ether 00:14:4f:fa:f9:46 brd ff:ff:ff:ff:ff:ff

ip link set eth0 mtu 9000

