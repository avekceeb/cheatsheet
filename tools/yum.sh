#!/bin/sh
exit 1

[name]
name=name
baseurl=http://url.com/url/
priority=2
gpgcheck=0
enabled=1

[cdrom]
name=CDROM Repo
baseurl=file:///mnt/rc10iso
enabled=1

######################################

yum repolist

######################################

yum --showduplicates list httpd

######################################

yumdownloader --source grub2
yum-builddep grub2

######################################

### zypper

[zypper](http://www.thegeekstuff.com/2015/04/zypper-examples/)

zypper repos
zypper modifyrepo -e repo-source
zypper si kernel-default
#zypper search -t srcpackage -r kernel-default
ls /usr/src/packages/SPECS/
#kernel-default.spec

### dnf
dnf repoquery --source kernel
dnf install rpmdevtools rpm-build
dnf install audit-libs-devel binutils-devel \
    hmaccalc ncurses-devel newt-devel numactl-devel \
    openssl-devel pciutils-devel \
    perl-devel perl-generators \
    perl-ExtUtils-Embed pesign
# as non-root:
dnf download --source kernel-4.12.13
rpmdev-setuptree
rpm -ihv kernel-4.12.13-300.fc26.src.rpm 
rpmbuild -bb SPECS/kernel.spec
rpmbuild -bb --without debug --without debuginfo --with vanilla SPECS/kernel.spec

