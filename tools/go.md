
## Docs
- [how to do it](https://golang.org/doc/code.html)

## Build
    which go || yum -y install golang
    CGO_ENABLED=0 GOOS=linux go build -a -ldflags '-extldflags "-static"' http-dummy.go

## Debugger
     go get -u github.com/derekparker/delve/cmd/dlv
     cd $srcdir
     dlv debug

