

## Boot without initrd:

	menuentry 'my' --class oracle --class gnu-linux --class gnu --class os --unrestricted $menuentry_id_option 'my-xxxx' {
		set root='hd0,msdos1'
		linux /vmlinuz-my rootfstype=xfs root=/dev/sda2  LANG=en_US.UTF-8
	}

