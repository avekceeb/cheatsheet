#######################
rpm2cpio php-5.1.4-1.esp1.x86_64.rpm | cpio -idmv
#######################
rpm -ivh --force http://url/kernel-xxx.src.rpm
cd rpmbuild/
rpmbuild -ba  SPECS/kernel.spec 

#######################
rpm -q --changelog glibc-2.12-1.132.0.21.sparc64 | more

rpm -q --qf "%{version}-%{release}\n" pkg 

### see scripts:
rpm -qp --scripts kernel-ocl-4.13.3-4.el7ocl.src.rpm
