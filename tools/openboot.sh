setenv auto-boot? false
probe-scsi-all
devalias
show-disks

probe-nvme-all
##############################################################
cd /HOST
set send_break_action="break"
##############################################################
set /HOST/bootmode config="factory-default"
stop /SYS
#ls /SYS
#power_state = Off
start /SYS
start /SP/console
##############################################################

{0} ok boot net:dhcp,192.168.0.1
Boot device: /virtual-devices@100/channel-devices@200/network@0:dhcp,192.168.0.1  File and args: 
ERROR: /packages/obp-tftp: Last Trap: Fast Data Access MMU Miss

{0} ok reset-all
NOTICE: Entering OpenBoot.
NOTICE: Fetching Guest MD from HV.
NOTICE: Starting additional cpus.
NOTICE: Initializing LDC services.
NOTICE: Probing PCI devices.
NOTICE: Finished PCI probing.

SPARC T7-2, No Keyboard
Copyright (c) 1998, 2015, Oracle and/or its affiliates. All rights reserved.
OpenBoot 4.38.2, 4.0000 GB memory available, Serial #83391925.
Ethernet address 0:14:4f:f8:75:b5, Host ID: 84f875b5.



{0} ok " net:dhcp" select-dev
{0} ok 4000 load
ERROR: /packages/obp-tftp: Last Trap: Fast Data Access MMU Miss

{0} ok reset-all   
NOTICE: Entering OpenBoot.
NOTICE: Fetching Guest MD from HV.
NOTICE: Starting additional cpus.
NOTICE: Initializing LDC services.
NOTICE: Probing PCI devices.
NOTICE: Finished PCI probing.


/etc/dhcp/dhcpd.conf

subnet 192.168.0.0 netmask 255.255.255.0 {
       range 192.168.0.10 192.168.0.20;
       next-server 192.168.0.1;
       filename "/boot/grub/sparc64-ieee1275/core.img";
}



SPARC T7-2, No Keyboard
Copyright (c) 1998, 2015, Oracle and/or its affiliates. All rights reserved.
OpenBoot 4.38.2, 4.0000 GB memory available, Serial #83573171.
Ethernet address 0:14:4f:fb:39:b3, Host ID: 84fb39b3.



{0} ok boot net:dhcp,10.147.27.157,3711.img
Boot device: /virtual-devices@100/channel-devices@200/network@0:dhcp,10.147.27.157,3711.img  File and args: 
ERROR: /packages/obp-tftp: Last Trap: Fast Data Access MMU Miss

{0} ok 



 boot net:dhcp,10.147.27.157,boot|grub|sparc64-ieee1275|core.img


{0} ok setenv network-boot-arguments host-ip=10.147.27.164,router-ip=10.147.24.1,subnet-mask=255.255.252.0,tftp-server=10.147.27.157,file=3711.img
network-boot-arguments =  host-ip=10.147.27.164,router-ip=10.147.24.1,subnet-mask=255.255.252.0,tftp-server=10.147.27.157,file=3711.img



setenv network-boot-arguments host-ip=144.25.12.170,router-ip=144.25.12.1,subnet-mask=255.255.252.0,file=http://144.25.12.76:5555/cgi-bin/wanboot-cgi

#network-boot-arguments =  host-ip=144.25.12.170,router-ip=144.25.12.1,subnet-mask=255.255.252.0,file=144.25.12.76:5555/cgi-bin/wanboot-cgi
boot net - install


boot rcdrom text vnc vncpassword=sparc1e nodmraid nompath
boot rcdrom text vnc nodmraid nompath ksdevice=eth0 ks=http://ca-ostest186/ks.cfg
boot rcdrom text vnc nodmraid nompath ksdevice=eth0 ks=http://ca-qasparc-ldom9.us.oracle.com/20-ks.conf
boot rcdrom linux install vnc ksdevice=eth0 ks=http://ca-qasparc-ldom9.us.oracle.com/20-custom-ks.conf

probe-scsi-all
#############################################################

boot net:dhcp

Boot device: /pci@400/pci@2/pci@0/pci@6/network@0:dhcp  File and args: 
1000 Mbps full duplex Link up
Timed out waiting for BOOTP/DHCP reply
The file just loaded does not appear to be executable.

############################################################
