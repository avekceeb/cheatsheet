#!/bin/sh
exit 0

gpg --gen-key --homedir /root/gpg
gpg --list-keys --homedir=/root/gpg
gpg --list-secret-keys --homedir=/root/gpg 
gpg --list-keys --homedir=/root/gpg TEMPUSER
gpg --detach-sign --homedir /root/gpg ./sample.txt
gpg --verify --homedir /root/gpg  sample.txt.sig sample.txt
gpg --clearsign --homedir /root/gpg ./sample.txt
gpg --homedir /root/gpg  --delete-secret-keys TMPUSER
gpg --homedir /root/gpg  --delete-key TMPUSER
