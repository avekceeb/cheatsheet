#on the ILOM
cd /SP/services/kvms/host_storage_device/remote
ls
set server_URI=""
#set server_URI=nfs://10.147.24.82:/tftpboot/OL-devel-201507281002-R6-U5-sparc-dvd.iso   
set server_URI=nfs://10.147.27.164:/srv/OL-devel-201603010347-R6-U5-sparc-dvd.iso
cd /SP/services/kvms/host_storage_device
#/SP/services/kvms/host_storage_device
set mode=remote
#Set 'mode' to 'remote'
 
cd /SP/services/kvms/host_storage_device/remote
#/SP/services/kvms/host_storage_device/remote
 
show
 
# /SP/services/kvms/host_storage_device/remote
#    Targets:
# 
#    Properties:
#        password = (none)
#        server_URI =
#nfs://10.147.24.82:/tftpboot/OL-devel-201507281002-R6-U5-sparc-dvd.iso
#        username = (none)
# 
#    Commands:
#        cd
#        show
 
cd ../
 
#/SP/services/kvms/host_storage_device
 
show
 
# /SP/services/kvms/host_storage_device
#    Targets:
#        remote
# 
#    Properties:
#        mode = remote
#        status = operational  <<cool     # NOTE: status = device not
#mounted   << BAD NEWS (then do a "reset-all" from obp)
#cool 
#    Commands:
#        cd
#        show
 
 
#then... from the console .. at the OBP prompt ...
# {ok}
devalias   # should see rcdrom  (if not, do a: " reset-all"
 
boot rcdrom

##########################
#After you finish, do this to unmount the iso:
cd /SP/services/kvms/host_storage_device
pwd
#Current default target: /SP/services/kvms/host_storage_device
set mode=disabled
#Set 'mode' to 'disabled'

