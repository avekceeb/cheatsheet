	cat >>~/.gdbinit<<EOF
	set env LD_LIBRARY_PATH=/home/qa/_src/libav-0.8.9/libavcodec:/home/qa/_src/libav-0.8.9/libavdevice:/home/qa/_src/libav-0.8.9/libavfilter:/home/qa/_src/libav-0.8.9/llibavformat:/home/qa/_src/libav-0.8.9/libavutil:/home/qa/_src/libav-0.8.9/libpostproc
	EOF

	# conditional breaks
	break events.c:458 if strcmp(event_entry->d_name, "Pick_any_cyc") == 0

	# setting source dirs:
	set directories /root/openssl-1.0.1e/apps:/root/openssl-1.0.1e/crypto/evp:$cdir:$cwd

	# assembly window
	layout asm

	# TUI:
	gdb -tui

	(gdb) info registers eax
	eax            0x400584 4195716
	(gdb) p (char*)(0x400584)
	$3 = 0x400584 "hello there"
	(gdb) 

	### debug forks:
	gdb ./abort01 

	(gdb) set follow-fork-mode child

	(gdb) b main

	(gdb) info inferiors
	Num  Description       Executable        
	* 1    <null>            /opt/ltp/testcases/bin/abort01 
	(gdb) run
	(gdb) n
	  71			kidpid = FORK_OR_VFORK();
	(gdb) info inferiors
	Num  Description       Executable        
	* 1    process 22273     /opt/ltp/testcases/bin/abort01 

	71			kidpid = FORK_OR_VFORK();
	[New process 22278]
	[Switching to process 22278]

	(gdb) n
	main (argc=<optimized out>, argv=<optimized out>) at abort01.c:72
	72			if (kidpid == 0) {
	(gdb) n
	81				do_child();
	(gdb) n

	Thread 2.1 "abort01" received signal SIGABRT, Aborted.
	0x00007ffff7a53b30 in raise () from /lib64/libc.so.6

	(gdb) info inferiors
	Num  Description       Executable        
	1    <null>            /opt/ltp/testcases/bin/abort01 
	* 2    process 22278     /opt/ltp/testcases/bin/abort01 
	(gdb) 

