#include <stdio.h>

#define crypto_caps_sz 15

/*
static const char *hwcaps[] = {
    "flush", "stbar", "swap", "muldiv", "v9",
    "ultra3", "blkinit", "n2",
    "mul32", "div32", "fsmuld", "v8plus", "popc", "vis", "vis2",
    "ASIBlkInit", "fmaf", "vis3", "hpc", "random", "trans", "fjfmau",
    "ima", "cspare", "pause", "cbcond", NULL,
    "adp", "vis3b", "pause-nsec", "mwait", "sparc5", "vamask"
};
*/

static const char *crypto_hwcaps[crypto_caps_sz] = {
    "aes", "des", "kasumi", "camellia", "md5", "sha1", "sha256",
    "sha512", "mpmul", "montmul", "montsqr", "crc32c", "xmpmul",
    "xmontmul", "xmontsqr"
};

/*
void cpucap_info(struct seq_file *m)
{
    unsigned long caps = sparc64_elf_hwcap;
    int i, printed = 0;

    seq_puts(m, "cpucaps\t\t: ");
    for (i = 0; i < ARRAY_SIZE(hwcaps); i++) {
        unsigned long bit = 1UL << i;
        if (hwcaps[i] && (caps & bit)) {
            seq_printf(m, "%s%s",
                   printed ? "," : "", hwcaps[i]);
            printed++;
        }
    }
    if (caps & HWCAP_SPARC_CRYPTO) {
        unsigned long cfr;

        __asm__ __volatile__("rd %%asr26, %0" : "=r" (cfr));
        for (i = 0; i < ARRAY_SIZE(crypto_hwcaps); i++) {
            unsigned long bit = 1UL << i;
            if (cfr & bit) {
                seq_printf(m, "%s%s",
                       printed ? "," : "", crypto_hwcaps[i]);
                printed++;
            }
        }
    }
    seq_putc(m, '\n');
}
*/

void crypto_caps_info() {
    int i, printed = 0;

    printf("cpucaps\t\t: ");

	unsigned long cfr;

	__asm__ __volatile__("rd %%asr26, %0" : "=r" (cfr));

	for (i = 0; i < crypto_caps_sz; i++) {
		unsigned long bit = 1UL << i;
		if (cfr & bit) {
			printf("%s%s", printed ? "," : "", crypto_hwcaps[i]);
			printed++;
		}
	}

    printf("\n");

}

int main () {
	crypto_caps_info();
	return 0;
}

