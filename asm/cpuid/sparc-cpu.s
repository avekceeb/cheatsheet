	.file	"caps.c"
	.section	".rodata"
	.align 8
.LLC0:
	.asciz	"aes"
	.align 8
.LLC1:
	.asciz	"des"
	.align 8
.LLC2:
	.asciz	"kasumi"
	.align 8
.LLC3:
	.asciz	"camellia"
	.align 8
.LLC4:
	.asciz	"md5"
	.align 8
.LLC5:
	.asciz	"sha1"
	.align 8
.LLC6:
	.asciz	"sha256"
	.align 8
.LLC7:
	.asciz	"sha512"
	.align 8
.LLC8:
	.asciz	"mpmul"
	.align 8
.LLC9:
	.asciz	"montmul"
	.align 8
.LLC10:
	.asciz	"montsqr"
	.align 8
.LLC11:
	.asciz	"crc32c"
	.align 8
.LLC12:
	.asciz	"xmpmul"
	.align 8
.LLC13:
	.asciz	"xmontmul"
	.align 8
.LLC14:
	.asciz	"xmontsqr"
	.section	".data"
	.align 8
	.type	crypto_hwcaps, #object
	.size	crypto_hwcaps, 120
crypto_hwcaps:
	.xword	.LLC0
	.xword	.LLC1
	.xword	.LLC2
	.xword	.LLC3
	.xword	.LLC4
	.xword	.LLC5
	.xword	.LLC6
	.xword	.LLC7
	.xword	.LLC8
	.xword	.LLC9
	.xword	.LLC10
	.xword	.LLC11
	.xword	.LLC12
	.xword	.LLC13
	.xword	.LLC14
	.section	".rodata"
	.align 8
.LLC15:
	.asciz	"cpucaps\t\t: "
	.align 8
.LLC16:
	.asciz	"%s%s"
	.align 8
.LLC17:
	.asciz	","
	.align 8
.LLC18:
	.asciz	""
	.section	".text"
	.align 4
	.global crypto_caps_info
	.type	crypto_caps_info, #function
	.proc	020
crypto_caps_info:
	.register	%g2, #scratch
	.register	%g3, #scratch
	save	%sp, -208, %sp
	st	%g0, [%fp+2027]
	sethi	%hi(.LLC15), %g1
	or	%g1, %lo(.LLC15), %g1
	mov	%g1, %o0
	call	printf, 0
	 nop
#APP
! 61 "caps.c" 1
	rd %asr26, %g1
! 0 "" 2
#NO_APP
	stx	%g1, [%fp+2031]
	st	%g0, [%fp+2023]
	ba,pt	%xcc, .LL2
	 nop
.LL6:
	mov	1, %g2
	ld	[%fp+2023], %g1
	sllx	%g2, %g1, %g1
	stx	%g1, [%fp+2039]
	ldx	[%fp+2031], %g2
	ldx	[%fp+2039], %g1
	and	%g2, %g1, %g1
	brz	%g1, .LL3
	 nop
	sethi	%hi(.LLC16), %g1
	or	%g1, %lo(.LLC16), %g3
	ld	[%fp+2027], %g1
	cmp	%g1, 0
	be	%icc, .LL4
	 nop
	sethi	%hi(.LLC17), %g1
	or	%g1, %lo(.LLC17), %g1
	ba,pt	%xcc, .LL5
	 nop
.LL4:
	sethi	%hi(.LLC18), %g1
	or	%g1, %lo(.LLC18), %g1
.LL5:
	ld	[%fp+2023], %g2
	sethi	%hi(crypto_hwcaps), %g4
	or	%g4, %lo(crypto_hwcaps), %g4
	sra	%g2, 0, %g2
	sllx	%g2, 3, %g2
	ldx	[%g4+%g2], %g2
	mov	%g3, %o0
	mov	%g1, %o1
	mov	%g2, %o2
	call	printf, 0
	 nop
	ld	[%fp+2027], %g1
	add	%g1, 1, %g1
	st	%g1, [%fp+2027]
.LL3:
	ld	[%fp+2023], %g1
	add	%g1, 1, %g1
	st	%g1, [%fp+2023]
.LL2:
	ld	[%fp+2023], %g1
	cmp	%g1, 14
	ble	%icc, .LL6
	 nop
	mov	10, %o0
	call	putchar, 0
	sha1
	aes_kexpand1 %f0, %f2, 0x0, %f4
	 nop
	return	%i7+8
	 nop
	.size	crypto_caps_info, .-crypto_caps_info
	.align 4
	.global main
	.type	main, #function
	.proc	04
main:
	save	%sp, -176, %sp
	call	crypto_caps_info, 0
	 nop
	mov	0, %g1
	sra	%g1, 0, %g1
	mov	%g1, %i0
	return	%i7+8
	 nop
	.size	main, .-main
	.ident	"GCC: (GNU) 4.4.7 20120313 (Red Hat 4.4.7-16.0.6)"
	.section	.note.GNU-stack,"",@progbits
