
# Benchmarks

- [Lies, Damn Lies and Benchmarks](http://www.linux-mag.com/id/7464/)
- [Хабр: Как правильно мерять производительность диска](https://habrahabr.ru/post/154235/)
- [filebench](https://github.com/filebench/filebench/wiki)


## network
- [http benchmarking page](https://gist.github.com/denji/8333630)
- [ab - Apache HTTP server benchmarking tool](https://httpd.apache.org/docs/2.4/programs/ab.html)
- [Hey benchmark](https://github.com/rakyll/hey)

