#!/bin/bash

f_prepare() {
	rm -rf test*
	sync
	echo 3 > /proc/sys/vm/drop_caches
}

f_dd() {
	echo "--------------------"
	f_prepare
	dd if=/dev/zero of=test bs=${1}k count=${2}k conv=fdatasync
}

f_xxx() {
cd /mnt/t
echo ">>>>>>>>>>>> NFS >>>>>>>>>>>>>>>>>"
f_dd 128 32
f_dd 64 64
f_dd 32 128

cd /mnt/nfstest
echo ">>>>>>>>>>>> LOCAL >>>>>>>>>>>>>>>"
f_dd 128 32
f_dd 64 64
f_dd 32 128
}

f_combined() {
	local sz=$1
	local fs=$2
	f_prepare
	fio --bs=4k --direct=1 --iodepth=1 --randrepeat=1 --size=${sz} \
	--name=job_aio --ioengine=libaio --rw=randrw --filename=test1:test2 \
	--name=job_posixaio --ioengine=posixaio --rw=randrw --filename=test1:test2 \
	--name=job_sync --ioengine=sync --rw=randrw --filename=test1:test2 \
	--output=/root/fio/report-combined-${fs}-${sz}.txt
}

f_multifile() {
	local sz=$1
	local fs=$2
	f_prepare
	mkdir -p test/x/y/z
	fio --bs=4k --direct=1 --iodepth=1 --randrepeat=1 --size=${sz} \
	--output=/root/fio/report-multifile-${fs}-${sz}.txt
	--nrfiles=4 --filename_format='testfile.$jobnum.$filenum' \
	--ioengine=sync --rw=randrw --numjobs=4 \
	--name=job_x --directory=test/x \
	--name=job_y --directory=test/x/y \
	--name=job_z --directory=test/x/y/z 
}

for sz in 512m 1G 4G 8G ; do
	cd /mnt/nfstest
	f_combined ${sz} nfs
	cd /mnt/test
	f_combined ${sz} local
done

for sz in 128m 256m 512m 1024m ; do
	cd /mnt/nfstest
	multifile ${sz} nfs
	cd /mnt/test
	multifile ${sz} local
done

