
## fio

--------------------------------------------------------------------------------

### 1) последовательная запись/чтение (--rw=rw)

--------------------------------------------------------------------------------
	a) LIBAIO:
	
	fio  --bs=4k --direct=1 --iodepth=1 --randrepeat=1 --size=8G \
	     --ioengine=libaio --rw=rw --filename=test1 --name=SEQUENTIAL \
	     --runtime=600 --output=/root/fio/seq-libaio-ext4-nfs-8g.txt
	     
--------------------------------------------------------------------------------

	dd if=test1 of=test2 bs=4k iflag=direct oflag=direct
	8589934592 bytes (8,6 GB) copied, 387,412 s, 22,2 MB/s

--------------------------------------------------------------------------------

	read : io=4095.4MB, bw=34153KB/s, iops=8538, runt=122788msec
		slat (usec): min=4, max=92, avg= 9.36, stdev= 1.86
		clat (usec): min=2, max=23430, avg=47.83, stdev=37.35
		lat (usec): min=37, max=23459, avg=57.19, stdev=37.47
	write: io=4096.8MB, bw=34165KB/s, iops=8541, runt=122788msec
		slat (usec): min=5, max=109, avg=10.70, stdev= 1.87
		clat (usec): min=2, max=8993, avg=45.59, stdev=22.44
		lat (usec): min=32, max=9004, avg=56.30, stdev=22.57
	READ: io=4095.4MB, aggrb=34153KB/s, minb=34153KB/s, maxb=34153KB/s, mint=122788msec, maxt=122788msec
	WRITE: io=4096.8MB, aggrb=34164KB/s, minb=34164KB/s, maxb=34164KB/s, mint=122788msec, maxt=122788msec

	=> 22 vs 34 M/s
	
--------------------------------------------------------------------------------
	# локально примонтированный NFS:
	read : io=4095.4MB, bw=13093KB/s, iops=3273, runt=320297msec
		slat (usec): min=7, max=148, avg=26.44, stdev= 7.18
		clat (usec): min=2, max=334, avg=46.66, stdev= 8.96
		lat (usec): min=49, max=361, avg=73.10, stdev=14.69
	write: io=4096.8MB, bw=13097KB/s, iops=3274, runt=320297msec
		slat (usec): min=9, max=136, avg=28.31, stdev= 7.40
		clat (usec): min=78, max=15914, avg=199.66, stdev=110.89
		lat (usec): min=106, max=15941, avg=227.97, stdev=113.19
	READ: io=4095.4MB, aggrb=13092KB/s, minb=13092KB/s, maxb=13092KB/s, mint=320297msec, maxt=320297msec
	WRITE: io=4096.8MB, aggrb=13097KB/s, minb=13097KB/s, maxb=13097KB/s, mint=320297msec, maxt=320297msec

	=> 13 M/s
	
	dd if=test1 of=test2 bs=4k iflag=direct oflag=direct
	8589934592 bytes (8,6 GB) copied, 1081,54 s, 7,9 MB/s

--------------------------------------------------------------------------------
	b) SYNC
	read : io=4095.4MB, bw=40793KB/s, iops=10198, runt=102802msec
		clat (usec): min=29, max=20362, avg=47.65, stdev=41.85
		lat (usec): min=29, max=20362, avg=47.74, stdev=41.86
	write: io=4096.8MB, bw=40807KB/s, iops=10201, runt=102802msec
		clat (usec): min=29, max=15937, avg=48.16, stdev=27.89
		lat (usec): min=29, max=15937, avg=48.30, stdev=27.89
	READ: io=4095.4MB, aggrb=40792KB/s, minb=40792KB/s, maxb=40792KB/s, mint=102802msec, maxt=102802msec
	WRITE: io=4096.8MB, aggrb=40806KB/s, minb=40806KB/s, maxb=40806KB/s, mint=102802msec, maxt=102802msec

	=> 40 M/s
	
--------------------------------------------------------------------------------
	# 10 jobs 10 iodepth:
	read : io=4095.2MB, bw=19416KB/s, iops=4853, runt=215983msec
		slat (usec): min=3, max=798, avg=17.14, stdev=12.83
		clat (usec): min=64, max=76410, avg=1101.60, stdev=532.26
		lat (usec): min=69, max=76435, avg=1118.74, stdev=532.30
	write: io=4096.9MB, bw=19424KB/s, iops=4855, runt=215983msec
		slat (usec): min=3, max=654, avg=17.68, stdev=12.69
		clat (usec): min=53, max=22325, avg=921.10, stdev=234.71
		lat (usec): min=63, max=22345, avg=938.79, stdev=235.61
	READ: io=40959MB, aggrb=194149KB/s, minb=19406KB/s, maxb=19426KB/s, mint=215965msec, maxt=216027msec
	WRITE: io=40961MB, aggrb=194163KB/s, minb=19404KB/s, maxb=19434KB/s, mint=215965msec, maxt=216027msec

	=> 194 M/s

--------------------------------------------------------------------------------

### 2) случайная запись / чтение
	
	# fio     --bs=4k --direct=1 --iodepth=10 --randrepeat=1 --size=8G   --ioengine=libaio --rw=randrw --filename=test1  --runtime=600 --output=/root/fio/rand-jobs-depth10.txt --numjobs=10 --name=XXX
	
	read : io=94900KB, bw=161499B/s, iops=39, runt=601722msec
		slat (usec): min=3, max=1601.7K, avg=177.90, stdev=11987.65
		clat (usec): min=51, max=2533.2K, avg=201155.43, stdev=196675.29
		lat (usec): min=62, max=2533.3K, avg=201333.32, stdev=196948.46
	write: io=94976KB, bw=161628B/s, iops=39, runt=601722msec
		slat (usec): min=4, max=1673.1K, avg=160.81, stdev=11142.97
		clat (usec): min=42, max=1780.7K, avg=52008.86, stdev=44447.60
		lat (usec): min=49, max=1780.7K, avg=52169.67, stdev=45724.32
	READ: io=961240KB, aggrb=1597KB/s, minb=157KB/s, maxb=160KB/s, mint=600116msec, maxt=601725msec
	WRITE: io=961036KB, aggrb=1597KB/s, minb=157KB/s, maxb=163KB/s, mint=600116msec, maxt=601725msec

	=> 1.5 M/s
	
