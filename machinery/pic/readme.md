- [](http://ra3xdh.blogspot.co.uk/2013/12/pic-linux-1-mplab.html)
- [MpLab & Matlab](https://habrahabr.ru/post/321542/)
- [](https://hackaday.com/2010/11/03/how-to-program-pics-using-linux/)
- [JDM Serial Программатор](https://bovs.org/post/71/PIC-JDM-%E2%80%93-prostejsij-programmator-dla-PIC-(ICSP))
- [](http://pic.rkniga.ru/programmirovanie/com-programmatory/)
- [](http://cxem.net/mc/mc191.php)
- [](http://go-radio.ru/usb-programmator-pic-svoimi-rukami.html)
- [](https://www.qrz.ru/schemes/contribute/constr/extra-pic/)
- [](https://www.chipdip.ru/product/pic18f2550-i-so)

## PICpgm
- [install linux](http://picpgm.picprojects.net/install_linux.html)
- [home](http://www.members.aon.at/electronics/pic/picpgm/index.html)
- []()
- []()

## MPLab X-IDE
- [page](https://www.microchip.com/mplab/mplab-x-ide)


	picpgm -port /dev/ttyS0 -p dummy.X.production.hex 

	==================================================================
	PICPgm 2.9.2.5
	PIC Development Programmer, http://picpgm.picprojects.net
	Copyright 2002-2017 Christian Stadler (picpgm@picprojects.net)
	built on Aug  3 2017, 20:50:48
	==================================================================

	Autodetecting Programmer ...
	Programmer: JDM Programmer
				at /dev/ttyS0

	Autodetecting PIC ...

	PIC name:    PIC18F242
	Device ID:   0x048F
	Flash:       16 kByte
	EEPROM:      256 Byte

	Erasing whole device ... done!
	Programming Code Memory 0x00000008
	Verifying Code Memory 0x00000000
	Verify Error: Code Mem 0x000000: PIC=0x0088 Buf=0x00D4
	Verifying Code Memory 0x00000001
	Verify Error: Code Mem 0x000001: PIC=0x00DF Buf=0x00EF
	Verifying Code Memory 0x00000002
	Verify Error: Code Mem 0x000002: PIC=0x007F Buf=0x003F
	Verifying Code Memory 0x00000003
	Verify Error: Code Mem 0x000003: PIC=0x00E0 Buf=0x00F0
	Programming Data Memory 0x00000000
	Verifying Data Memory 0x00000000   => Data memory OK!
	Programming Config Memory 0x0000000e
	Verifying Config Memory 0x00000002
	Verify Error: Cfg Mem 0x000002: PIC=0x0001 Buf=0x0009
	Verifying Config Memory 0x00000003
	Verify Error: Cfg Mem 0x000003: PIC=0x000C Buf=0x000E
	Verifying Config Memory 0x00000006
	Verify Error: Cfg Mem 0x000006: PIC=0x0001 Buf=0x0081
	Verifying Config Memory 0x00000009
	Verify Error: Cfg Mem 0x000009: PIC=0x0080 Buf=0x00C0
	Verifying Config Memory 0x0000000B
	Verify Error: Cfg Mem 0x00000B: PIC=0x00C0 Buf=0x00E0
	Verifying Config Memory 0x0000000D
	Verify Error: Cfg Mem 0x00000D: PIC=0x0000 Buf=0x0040
	Verifying Config Memory 0x0000000E

	Programming finished with Verify Error(s)!

	finished in 8.0 seconds!

-------------------------------

	picpgm -port /dev/ttyS0

	==================================================================
	PICPgm 2.9.2.5
	PIC Development Programmer, http://picpgm.picprojects.net
	Copyright 2002-2017 Christian Stadler (picpgm@picprojects.net)
	built on Aug  3 2017, 20:50:48
	==================================================================

	Autodetecting Programmer ...
	Programmer: JDM Programmer
				at /dev/ttyS0

	Autodetecting PIC ...
	picpgm: /home/christian/gitrepo/picpgm/core/picpgm.cpp:589: virtual UINT32 CPicPgm::GetCfgMemLen(): Assertion `m_nCurDev != -1' failed.
	Aborted
