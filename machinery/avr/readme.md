# AVR

- [avr beginners](http://avrbeginners.net/)
- [Avr Studio Archives](https://www.microchip.com/mplab/avr-support/avr-and-sam-downloads-archive)
- [atmel docs](http://www.atmel.com/webdoc/AVRLibcReferenceManual/FAQ_1faq_rom_array.html)
- [examples for different atmels](https://github.com/akafugu/helloworld)
- [pgmspace](http://www.nongnu.org/avr-libc/user-manual/pgmspace.html)
- [Простенький Вольтметр и разные уроки](http://avr-start.ru/?p=590)

##avrdude

- [avrdude](http://www.ladyada.net/learn/avr/avrdude.html)

### to /etc/avrdude.conf
    programmer
      id = "nikolaew";
      desc = "serial port banging, reset=dtr sck=rts mosi=txd miso=cts";
      type = "serbb";
      reset = 4;
      sck = 7;
      mosi = 3;
      miso = 8;
    ;

### set baud rate and properties for ttyS0 ###

    stty 9600 ignbrk -brkint -icrnl -imaxbel -opost -isig -icanon -iexten -echo noflsh </dev/ttyS0

### flash
avrdude -b 9660 \
		-p attiny2313 \
		-c nikolaew \
		-P /dev/ttyS0 -v \
		-U flash:w:program.hex:i

### write fuse

- [про фьюзы](http://www.getchip.net/posts/068-kak-pravilno-proshit-avr-fyuzy-fuse-bit/)

    avrdude -b 9660 -p atmega8 -c nikolaew -P /dev/ttyS0 -v -U lfuse:w:0xEF:m


##v-usb

```
Bus 003 Device 017: ID 16c0:05dc Van Ooijen Technische Informatica shared ID for use with libusb
```

- [example: cpu load indicator](http://rux.vc/2010.03/cpu-load-indicator/#more-378)
- [example: fan controller](https://github.com/neffs/fan_controller/blob/master/firmware/main.c)
- [vusb hardware](http://vusb.wikidot.com/hardware)

## ubuntu

	apt install gcc-avr avr-libc binutils-avr avrdude

	uisp/artful 20050207-4.2ubuntu1 amd64
	  Micro In-System Programmer for Atmel's AVR MCUs

	usbprog/artful 0.2.0-2.2build1 amd64
	  Firmware programming tool for the USBprog hardware

	usbprog-gui/artful 0.2.0-2.2build1 amd64
	  GUI firmware programming tool for the USBprog hardware

