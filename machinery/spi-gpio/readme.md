
- [custom SPI-GPIO драйвер для OpenWRT](https://randomcoderdude.wordpress.com/2013/08/15/spi-over-gpio-in-openwrt/)
- [SPI on AVR](http://maxembedded.com/2013/11/the-spi-of-the-avr/)
- [AVR пример с индикаторами](http://avr.ru/ready/inter/spi/spi)
- [Простенький пример](http://avr-start.ru/?p=647)
- [userial - USB-SPI на базе AT90USB](http://microsin.net/programming/avr-working-with-usb/userial.html)
- [Эмуляция GPIO QEMU](https://habrahabr.ru/post/303060/)
- [sample2](http://www.circuitstoday.com/how-to-work-with-spi-in-avr-micro-controllers)
- [sample3](http://www.ermicro.com/blog/?p=1050)
- [sample4](http://www.elecdude.com/2012/12/avr-spi-serial-pheripheral-interface.html)
- [sample5](http://www.electronicwings.com/avr-atmega/atmega1632-spi)

