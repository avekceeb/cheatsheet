
## Разная
- [составы для травления плат](http://radiokot.ru/lab/hardwork/62/)


## USB

- [docs](https://www.keil.com/pack/doc/mw/USB/html/index.html)
- [usb docs](http://www.beyondlogic.org/usbnutshell/usb4.shtml)


## Misc Bots

- [bot with shaft encoders](https://www.youtube.com/watch?v=lXboFaHMyTY)
- [DIY Autonomous Robot - Vex](https://www.youtube.com/watch?v=HaaRVp0IuyU)

## Rotary encoders
- [wiki](https://en.wikipedia.org/wiki/Rotary_encoder)

## IR
- [IR remote control project](http://www.techdesign.be/projects/011/011.htm)
- [RC-5 protocol](https://en.wikipedia.org/wiki/RC-5)
- [remote sample HVAC](https://www.cooking-hacks.com/documentation/tutorials/control-hvac-infrared-devices-from-the-internet-with-ir-remote/)
- [sparkfun sample](https://learn.sparkfun.com/tutorials/ir-control-kit-hookup-guide?_ga=2.192911530.1034419164.1519049694-1695188569.1519049694)
- [...](https://www.sparkfun.com/products/11759?_ga=2.89778203.1867794527.1519817815-1695188569.1519049694)
- [proximity sensor](https://www.makerlab-electronics.com/product/ir-proximity-sensor/)
- [proximity sensor schematic](http://maxembedded.com/2013/08/how-to-build-an-ir-sensor/)
- [h88101p transmitter IC](https://www.infraredremote.com/RS117.htm)
- [rc5 IR приемник на мега8](https://avrlab.com/node/74)
- [rc5 tiny2313](http://avrproject.ru/publ/protokol_rc5_bascom_avr/1-1-0-30)
- [github: avr-rc5 lib](https://github.com/pinkeen/avr-rc5)
- [rc5 decoding](http://radioparty.ru/prog-avr/program-c/392-lesson-rc5)
- []()

## EagleCAD
- [азы. как распечатать](http://cxem.net/comp/comp142.php)
- [routing tips](https://electronics.stackexchange.com/questions/125349/what-are-some-tips-to-routing-a-one-sided-pcb)
- как сделать одностороннюю плату: In the auto options set layer 1 to , layer 16 to N/A


## Signal Generators
- http://linux-sound.org/
- https://www.instructables.com/id/How-to-Make-a-Signal-Generator-Learn-to-Generate-E/
- https://hackaday.com/2011/04/03/using-your-pc-as-a-simple-signal-generator/
- http://manpages.ubuntu.com/manpages/bionic/man1/signalgen.1.html
- https://community.linuxmint.com/software/view/siggen
- https://arachnoid.com/SignalGen/
- https://sourceforge.net/projects/pcm-oszi-gen/

## Ethernet
- https://books.google.co.uk/books?id=MRChaUQr0Q0C&pg=PA156&redir_esc=y#v=onepage&q&f=false
- https://www.intel.com/content/dam/www/public/us/en/documents/brochures/ethernet-controllers-phys-brochure.pdf
- http://netwinder.osuosl.org/pub/netwinder/docs/nw/PHY/1890.pdf
- https://en.wikipedia.org/wiki/Media-independent_interface
- [ENC28J60-I/SP, Автономный Ethernet контроллер с последовательным интерфейсом SPI](https://static.chipdip.ru/lib/286/DOC000286027.pdf)
- [B78476-A8065-A003, Трансформатор 10/100 Base-T, 350мкГн 1500В, 1:1](https://static.chipdip.ru/lib/296/DOC000296697.pdf)
- [tuxgraphics ENC28J60 project](http://tuxgraphics.org/electronics/200606/article06061.pdf)
- [projects using ENC28J60](https://github.com/search?p=3&q=enc28j60&type=Repositories)
- [NXP MC9S Ethernet card schematics](https://www.nxp.com/docs/en/application-note/AN2759.pdf)

## Mobile (and other) Cams
- [why this is hard](https://robotics.stackexchange.com/questions/2147/how-can-i-interface-my-cmos-camera-module-to-an-arduino)
- [ ... and more](https://electronics.stackexchange.com/questions/67605/using-standalone-smartphone-camera)
- [samsung e700 cam](https://www.sparkfun.com/products/retired/637)
- [interface cam with at91..](http://hands.com/~lkcl/eoma/kde_tablet/TCM8230MD_arm7_appnote.pdf)
- [Game Boy camera in robotics](http://ww1.microchip.com/downloads/en/DeviceDoc/issue4_pg39_43_Robotics.pdf)
- [cell cam + pic32](https://www.eevblog.com/forum/microcontrollers/interfacing-a-cheap-phone-camera-module-to-a-pic32/)
- [MIPI](https://www.electronicdesign.com/communications/understanding-mipi-alliance-interface-specifications)
- [UniPro](https://en.wikipedia.org/wiki/UniPro_protocol_stack)
- [CSI](https://en.wikipedia.org/wiki/Camera_Serial_Interface)

- [Arduino jpeg](https://www.open-electronics.org/an-arduino-based-jpeg-camera-with-ir-and-pir/)
- [Nokia spectrum analizer](http://320volt.com/en/nokia-3410-atmega8-2-4-ghz-spectrum-analyzer-devresi/)
- [keychain cam](https://www.instructables.com/id/Hacking-A-Keychain-Digital-Camera-for-Arduino-Cont/)
- [arduino + eos](https://www.circuitsathome.com/camera-control/arduino-based-controller-for-canon-eos-cameras/)
- [phone to arduino](https://www.circuitsathome.com/category/mcu/page/6/)
- [Arduino & Raspberry Pi Camera](https://www.electroschematics.com/11140/arduino-raspberry-pi-camera-interface/)

## Atmel ARM7
- [chipdip](https://www.chipdip.ru/catalog/ic-microcontrollers?sort=priceup&gq=AT91SAM)
- [howto](http://microsin.net/programming/arm/introduction-to-programming-at91sam7s-arm.html)
- [about](http://www.efo.ru/doc/Atmel/Atmel.pl?1216)

## Sensor Networks etc
- [ZigBee wiki](https://ru.wikipedia.org/wiki/ZigBee)
- [TinyOS](https://ru.wikipedia.org/wiki/TinyOS)
- [IEE802.15.4](https://ru.wikipedia.org/wiki/IEEE_802.15.4)
