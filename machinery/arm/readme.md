
# STM32F103

32-bit MCU with 64 or 128 KB Flash, USB, CAN, 7 timers, 2 ADCs, 9 com. interface

- [программатор из blue pill](https://geektimes.ru/post/292459/)
- [прошивка через послед порт и st-link](http://www.avislab.com/blog/stm32_st_link_ru/)
- [STM32 bootloader](http://www.avislab.com/blog/stm32-bootloader_ru/)
- [STM быстрый старт](https://habrahabr.ru/post/128734/)
- [STM программатор своими руками](https://cdeblog.ru/post/st-linkv2-clone)
- [Много про STM32 и STlink v2](http://www.count-zero.ru/2016/stm32_intro/)

