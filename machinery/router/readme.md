## OpenWRT TP-Link MR3020

[mr3020 openwrt](https://wiki.openwrt.org/toh/tp-link/tl-mr3020)
[memory card](https://wiki.openwrt.org/toh/tp-link/tl-mr3420/deep.mmc.hack)
[GPIO](https://wiki.openwrt.org/doc/hardware/port.gpio)

## Восстановка прошивки через сериал-консоль

    apt-get install tftpd-hpa tftp
    wget https://downloads.openwrt.org/latest/ar71xx/generic/openwrt-15.05.1-ar71xx-generic-tl-mr3020-v1-squashfs-factory.bin
    mkdir -p /srv/tftp
    cp openwrt*.bin /srv/tftp/img.bin
    in.tftpd -l -s /srv/tftp
    screen /dev/ttyS0 115200
    # power on TL and type tpl in 1-2 sec
    # in u-boot console:
    setenv ipaddr 192.168.1.2
    setenv serverip 192.168.1.1
    tftpboot 0x80000000 img.bin
    erase 0x9f020000 +0x3c0000
    cp.b 0x80000000 0x9f020000 0x3c0000
    bootm 9f020000

## Фейлсейф
https://wiki.openwrt.org/doc/howto/generic.failsafe

- [Исследование Wi-Fi-роутера TP-LINK TL-MR3020](http://robocraft.ru/blog/electronics/1053.html)
- [Перепрошивка WRD1043](https://docs.google.com/file/d/0B8RHAmW4AacRYzZjMjZkMGUtZjFiNi00MTRhLTgyN2YtMDM1YjRjNjJlN2Y4/edit?hl=en&pli=1)
- [MR3020 Firmware recovery](http://wiki.villagetelco.org/Serial_Port_Access_and_Firmware_Recovery_for_TL_MR3020)

###Build

```bash
yum -y install zlib-static
yum -y install unzip gawk
yum -y install ncurses-devel
yum -y install openssl-devel
yum -y install asciidoc
yum -y install git svn 
yum -y install libgcj \
    flex \
    util-linux \
    gtk2-devel \
    intltool \
    zlib-devel \
    perl-ExtUtils-MakeMaker \
    rsync \
    unzip \
    wget \
    gettext \
    perl-XML-Parser

buildroot=/olt-storage/local

cd $buildroot

git clone git://github.com/openwrt/openwrt.git

cd openwrt

#PATH="$buildroot/openwrt/staging_dir/host/bin:$buildroot/openwrt/staging_dir/toolchain-mips_34kc_gcc-5.3.0_musl-1.1.15/bin:$PATH"
#export PATH

git checkout tags/v15.05.1

./scripts/feeds update -a
./scripts/feeds install -a

make menuconfig
make defconfig

make
```

```bash
##############################
## usb storage suppport ######
##############################

kmod-usb-storage
kmod-fs-...
block-mount
kmod-scsi-core
```
```
make V=s package/utils/60t/prepare
make V=s package/utils/60t/compile
make V=s package/utils/60t/install
```
[repo](https://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/packages/)

### WebCam+mjpeg

- [howto tpl](http://mainloop.ru/linux-os/make-lowcost-wifi-webcam.html)
- [openwrt how to](https://wiki.openwrt.org/doc/howto/webcam)
- [more how to](https://autohome.org.ua/12-openwrt/16-podklyuchenie-veb-kamery-v-openwrt)
- [танк с камерой](https://geektimes.ru/post/265186/)

	opkg install kmod-video-uvc
	kmod-i2c-core, kmod-video-core,  kmod-input-core, kmod-video-videobuf2
	opkg install mjpg-streamer

	config mjpg-streamer 'core'
		option enabled '1'
		option input 'uvc'
		option output 'http'
		option device '/dev/video0'
		option resolution '640x480'
		option yuv '0'
		option quality '80'
		option fps '5'
		option led 'auto'
		option www '/www/webcam'
		option port '8080'
		option username 'openwrt'
		option password 'openwrt'

	/etc/init.d/mjpg-streamer start

```
rootfs on / type rootfs (rw)
/dev/root on /rom type squashfs (ro,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,noatime)
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,noatime)
tmpfs on /tmp type tmpfs (rw,nosuid,nodev,noatime)
/dev/mtdblock3 on /overlay type jffs2 (rw,noatime)
overlayfs:/overlay on / type overlay (rw,noatime,lowerdir=/,upperdir=/overlay/upper,workdir=/overlay/work)
tmpfs on /dev type tmpfs (rw,nosuid,relatime,size=512k,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,mode=600)
debugfs on /sys/kernel/debug type debugfs (rw,noatime)

cat /etc/config/network 

config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd44:5d40:3d62::/48'

config interface 'lan'
	option ifname 'eth0'
	option force_link '1'
	option type 'bridge'
	option proto 'static'
	option netmask '255.255.255.0'
	option ip6assign '60'
	option ipaddr '172.16.0.1'
cat dhcp 

config dnsmasq
	option domainneeded '1'
	option boguspriv '1'
	option filterwin2k '0'
	option localise_queries '1'
	option rebind_protection '1'
	option rebind_localhost '1'
	option local '/lan/'
	option domain 'lan'
	option expandhosts '1'
	option nonegcache '0'
	option authoritative '1'
	option readethers '1'
	option leasefile '/tmp/dhcp.leases'
	option resolvfile '/tmp/resolv.conf.auto'
	option localservice '1'

config dhcp 'lan'
	option interface 'lan'
	option start '100'
	option limit '150'
	option leasetime '12h'
	option dhcpv6 'server'
	option ra 'server'
	option ra_management '1'

config dhcp 'wan'
	option interface 'wan'
	option ignore '1'

config odhcpd 'odhcpd'
	option maindhcp '0'
	option leasefile '/tmp/hosts/odhcpd'
	option leasetrigger '/usr/sbin/odhcpd-update'

root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# cat luci 

config core 'main'
	option lang 'auto'
	option mediaurlbase '/luci-static/bootstrap'
	option resourcebase '/luci-static/resources'

config extern 'flash_keep'
	option uci '/etc/config/'
	option dropbear '/etc/dropbear/'
	option openvpn '/etc/openvpn/'
	option passwd '/etc/passwd'
	option opkg '/etc/opkg.conf'
	option firewall '/etc/firewall.user'
	option uploads '/lib/uci/upload/'

config internal 'languages'

config internal 'sauth'
	option sessionpath '/tmp/luci-sessions'
	option sessiontime '3600'

config internal 'ccache'
	option enable '1'

config internal 'themes'
	option Bootstrap '/luci-static/bootstrap'

root@OpenWrt:/etc/config# cat system

config system
	option hostname 'OpenWrt'
	option timezone 'UTC'

config timeserver 'ntp'
	list server '0.openwrt.pool.ntp.org'
	list server '1.openwrt.pool.ntp.org'
	list server '2.openwrt.pool.ntp.org'
	list server '3.openwrt.pool.ntp.org'
	option enabled '1'
	option enable_server '0'

config led 'led_usb'
	option name 'USB'
	option sysfs 'tp-link:green:3g'
	option trigger 'usbdev'
	option dev '1-1'
	option interval '50'

config led 'led_wlan'
	option name 'WLAN'
	option sysfs 'tp-link:green:wlan'
	option trigger 'phy0tpt'

config led 'led_lan'
	option name 'LAN'
	option sysfs 'tp-link:green:lan'
	option trigger 'netdev'
	option dev 'eth0'
	option mode 'link tx rx'

root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# cat wireless 

config wifi-device 'radio0'
	option type 'mac80211'
	option channel '11'
	option hwmode '11g'
	option path 'platform/ar933x_wmac'
	option htmode 'HT20'
	option txpower '15'
	option country 'US'

config wifi-iface
	option device 'radio0'
	option network 'lan'
	option mode 'ap'
	option ssid '60t'
	option encryption 'psk2'
	option key 'password123'

-----

br-lan    Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          inet addr:172.16.0.1  Bcast:172.16.0.255  Mask:255.255.255.0
          inet6 addr: fe80::9ade:d0ff:fef7:1fa0/64 Scope:Link
          inet6 addr: fd44:5d40:3d62::1/60 Scope:Global
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11729 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9681 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:1474774 (1.4 MiB)  TX bytes:1975841 (1.8 MiB)

eth0      Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11544 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9542 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:1615752 (1.5 MiB)  TX bytes:1969128 (1.8 MiB)
          Interrupt:4 

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:8035 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8035 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:546385 (533.5 KiB)  TX bytes:546385 (533.5 KiB)

wlan0     Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)


```

Openrobotics
------------
[OR-WRT](http://roboforum.ru/wiki/OR-WRT)
[opensource robotics??](http://www.osrfoundation.org/osrf-projects/)

Misc
----
- [метеостанция](http://www.bhv.ru/data_location/192858/04/Arduino04.php)
- [ардуино к mr3020](https://istarik.ru/blog/arduino/18.html)


```bash

qemu-system-mipsel \
    -kernel openwrt-malta-le-vmlinux-initramfs.elf \
    -nographic -m 256

qemu-system-mipsel \
    -kernel openwrt-malta-le-vmlinux-initramfs.elf \
    -nographic -m 256 \
    -net nic \
    -net tap,ifname=tap0,script=no,downscript=no

```

## Клиент

[how](https://wiki.openwrt.org/doc/recipes/ap_sta)
[explained](https://wiki.openwrt.org/doc/howto/clientmode)

	cat /etc/config/network 

	config interface 'loopback'
			option ifname 'lo'
			option proto 'static'
			option ipaddr '127.0.0.1'
			option netmask '255.0.0.0'

	config globals 'globals'
			option ula_prefix 'fdc5:d1a5:43b3::/48'

	config interface 'wwan'
			option proto 'dhcp'

	cat /etc/config/wireless 

	config wifi-device 'radio0'
			option type 'mac80211'
			option channel '11'
			option hwmode '11g'
			option path 'platform/ar933x_wmac'
			option htmode 'HT20'
			option txpower '15'
			option country 'US'

	config wifi-iface
			option device 'radio0'
			option network 'wwan'
			option encryption 'psk2'
			option mode 'sta'
			option ssid '**base ssid**'
			option key '**base password**'

	wlan0     Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          inet addr:172.16.0.11  Bcast:172.16.0.255  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:35 errors:0 dropped:0 overruns:0 frame:0
          TX packets:25 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:4317 (4.2 KiB)  TX bytes:3365 (3.2 KiB)


