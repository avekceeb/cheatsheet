
f_nfsometer() {
	yum -y install python-matplotlib numpy python-mako nfs-utils time
	./nfsometer.py workloads
	yum -y install filebench
	./nfsometer.py localhost:/export filebench_networkfs
}

f_compare_tmpfs() {
	mkdir -p /mnt/tmp /mnt/nfstest
	mount -t tmpfs tmpfs /mnt/tmp -o size=50G
	dd if=/dev/zero of=/mnt/tmp/test bs=4k count=1M
	# 1.7 G/s
	# note fsid=0
	exportfs -vi -o rw,fsid=0,sync,insecure,no_root_squash *:/mnt/tmp
	# note mount :/
	mount -o vers=4.1 $(hostname -f):/ /mnt/nfstest
	cd /mnt/nfstest
	dd if=/dev/zero of=/mnt/tmp/test bs=4k count=1M
	# 1.7 G/s
}

f_run_mix_4g() {
    fio \
    --bs=4k --direct=1 --iodepth=1 --randrepeat=1 --size=4G \
    --name=job_aio --ioengine=libaio --rw=randrw --filename=test1:test2 \
    --name=job_posixaio --ioengine=posixaio --rw=randrw --filename=test1:test2 \
    --name=job_sync --ioengine=sync --rw=randrw --filename=test1:test2

    # tmpfs (direct=0)
    #READ: io=6141.6MB, aggrb=576603KB/s,
    #      minb=192302KB/s, maxb=599307KB/s, mint=3496msec, maxt=10906msec
    #WRITE: io=6146.1MB, aggrb=577156KB/s,
    #       minb=192283KB/s, maxb=600435KB/s, mint=3496msec, maxt=10906msec

    # nfs (direct=1)
    #READ: io=6141.6MB, aggrb=73874KB/s,
    #      minb=24637KB/s, maxb=25286KB/s,mint=82891msec, maxt=85123msec
    #WRITE: io=6146.1MB, aggrb=73945KB/s,
    #       minb=24635KB/s, maxb=25313KB/s, mint=82891msec, maxt=85123msec

}

    fio \
    --bs=4k --direct=1 --iodepth=8 --randrepeat=1 --size=4G \
    --name=job_aio --ioengine=libaio --rw=randrw --filename=test1:test2 \
    --name=job_posixaio --ioengine=posixaio --rw=randrw --filename=test1:test2 \
    --name=job_sync --ioengine=sync --rw=randrw --filename=test1:test2

#--lockfile=exclusive
#--rw=rw


fio --bs=4k --direct=0 --iodepth=8 --randrepeat=1 --size=4G \
    --nrfiles=4 --filename_format='testfile.$jobnum.$filenum' \
    --ioengine=libaio --rw=randrw --lockfile=exclusive --numjobs=16 \
    --runtime=600 --name=LCK-AIO

f_run_sequential_multi() {
	fio --bs=4k --direct=1 --iodepth=10 --randrepeat=1 --size=8G \
	    --ioengine=libaio --rw=rw --filename=test1  --runtime=600 \
	    --numjobs=10 --name=XXX
}
