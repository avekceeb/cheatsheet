
## Common Info

- [main linux-nfs wiki](http://wiki.linux-nfs.org/wiki/index.php/Main_Page)
- [IETF RFC5661](https://tools.ietf.org/html/rfc5661)

## pNFS

- [info](http://sniablog.org/category/pnfs/)
- [pnfs.com](http://www.pnfs.com/)
- [linux pnfs roadmap](http://wiki.linux-nfs.org/wiki/index.php/PNFS_Development_Road_Map)
- [pnfs linux server](https://serverfault.com/questions/772666/how-to-use-pnfs-in-nfsv4-1)
- [dCache](https://www.dcache.org/)
- [Dev setup Instuctions 2012](http://wiki.linux-nfs.org/wiki/index.php/PNFS_Block_Server_Setup_Instructions)
- [How to in Fedora](https://ervikrant06.wordpress.com/2015/07/05/how-to-configure-pnfs-server-in-fedora-22/)

## Server Side Copy
- [linux ssc](http://wiki.linux-nfs.org/wiki/index.php/Server_Side_Copy)

## Migration ???

- [linux recovery and migration](http://wiki.linux-nfs.org/wiki/index.php/NFS_Recovery_and_Client_Migration)
- [clients lease after tsm bug IETF discussion](https://www.ietf.org/mail-archive/web/nfsv4/current/msg15353.html)
- [more on bug](https://www.ietf.org/mail-archive/web/nfsv4/current/msg15390.html)
- [patch fixing bug](https://patchwork.kernel.org/patch/9742925/)
- [ietf migration and trunking draft](https://tools.ietf.org/html/draft-ietf-nfsv4-migration-issues-13)
- [red hat 6 pnfs](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Storage_Administration_Guide/ch09s02.html)
- [ietf multiserver update](https://tools.ietf.org/id/draft-dnoveck-nfsv4-mv1-msns-update-00.html)

## Referrals

- [Referrals example](https://ervikrant06.wordpress.com/2015/07/19/how-to-use-referrals-in-nfs/)
- [Replicas bug](https://bugzilla.redhat.com/show_bug.cgi?id=248070)

## I/O
- [fio notes](https://www.binarylane.com.au/support/solutions/articles/1000055889-how-to-benchmark-disk-i-o)
- [more fio](https://community.emc.com/community/products/isilon/blog/2015/12/08/nfs-throughput-benchmarks-with-fio)

## How To Debug:
- enable nfs rpc msgs `rpcdebug -m nfs -s all`
- disable `rpcdebug -m nfs -c all`
- tpcdump -i <interface> -w ./capture.pcap host <nfs-server-ip>
- strace -ttTvf -o /tmp/strace.out mount -t nfs ...


--------------------------------------------------------------------------------
## Refer

### on 'refer server'
	# exportfs -v
	/srv/210		<world>(sync,wdelay,nohide,no_subtree_check,sec=sys,rw,insecure,no_root_squash,no_all_squash)
	# ls -l /srv/210
	total 4
	-rw-r--r--. 1 root root 9 Nov 14 17:21 hello-from-210

### on 'main server'
	[root@x200 ~]# mkdir -p /ref/210
	[root@x200 ~]# mount --bind /ref/210 /ref/210

	[root@x200 ~]# ls -l /ref
	total 4
	drwxr-xr-x. 2 root root 6 Nov 14 17:43 210
	-rw-r--r--. 1 root root 6 Nov 14 17:49 hello-from-200

	[root@x200 ~]# ls -l /ref/210
	total 0
	
	[root@x200 ~]# exportfs -v
	/ref			<world>(sync,wdelay,nohide,no_subtree_check,sec=sys,rw,insecure,no_root_squash,no_all_squash)
	/ref/210		<world>(sync,wdelay,nohide,no_subtree_check,refer=/srv/210@172.16.0.210,sec=sys,rw,insecure,no_root_squash,no_all_squash)


### on client:
	root@t440:/# mount 172.16.0.200:/ref /mnt

	root@t440:/# ls -l /mnt
	total 4
	dr-xr-xr-x 2 4294967294 4294967294 0 Jan  1  1970 210
	-rw-r--r-- 1 root       root       6 Nov 14 17:49 hello-from-200

	root@t440:/# ls -l /mnt/210
	total 4
	-rw-r--r-- 1 root root 9 Nov 14 17:21 hello-from-210

	root@t440:/# mount | grep nfs
	
	172.16.0.200:/ref on /mnt type nfs4
		(rw,relatime,vers=4.0,rsize=524288,wsize=524288,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=172.16.0.8,local_lock=none,addr=172.16.0.200)

	172.16.0.210:/srv/210 on /mnt/210 type nfs4
		(rw,relatime,vers=4.0,rsize=524288,wsize=524288,namlen=255,hard,proto=tcp,timeo=600,retrans=2,sec=sys,clientaddr=172.16.0.8,local_lock=none,addr=172.16.0.210)


--------------------------------------------------------------------------------

## Ubuntu exports sample:

	# Example for NFSv2 and NFSv3:
	# /srv/homes       hostname1(rw,sync,no_subtree_check) hostname2(ro,sync,no_subtree_check)
	#
	# Example for NFSv4:
	# /srv/nfs4        gss/krb5i(rw,sync,fsid=0,crossmnt,no_subtree_check)
	# /srv/nfs4/homes  gss/krb5i(rw,sync,no_subtree_check)


--------------------------------------------------------------------------------


# OL7

	yum install -y nfs-utils

	mkdir -p /share /mount/export
	chmod -R 777 /share /mount

	cat >/etc/exports<<EOF
	/mount/export *(rw,no_root_squash,nohide,insecure)
	EOF
	systemctl start nfs
	#sleep 90

	mount -v -t nfs4 127.0.0.1:/mount/export /share

	# 127.0.0.1:/mount/export on /share type nfs4 
	# (rw,relatime,vers=4.1,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp,port=0,timeo=600,retrans=2,sec=sys,clientaddr=127.0.0.1,local_lock=none,addr=127.0.0.1)


#####
/etc/sysconfig/nfs
# The default is 8. 
RPCNFSDCOUNT=16

--------------------------------------------------------------------------------

## Selinux Stuff

https://www.nsa.gov/what-we-do/research/selinux/documentation/assets/files/presentations/2005-implementing-selinux-support-for-nfs-presentation.pdf

https://linux.die.net/man/8/nfs_selinux


	cat /etc/exports
	#/share *(rw,no_root_squash,nohide,insecure)

	## -------------------------------------
	### nfs4.1 - no seclabel

	mount -vvv  -o vers=4.1 localhost:/share /mnt
	cat /proc/mounts | grep 'localhost:/share '
	#localhost:/share /mnt nfs4 rw,relatime,vers=4.1,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp6,port=0,timeo=600,retrans=2,sec=sys,clientaddr=::1,local_lock=none,addr=::1 0 0

	mount -vvv  -o vers=4.2 localhost:/share /mnt
	cat /proc/mounts | grep 'localhost:/share '
	#localhost:/share /mnt nfs4 rw,seclabel,relatime,vers=4.2,rsize=131072,wsize=131072,namlen=255,hard,proto=tcp6,port=0,timeo=600,retrans=2,sec=sys,clientaddr=::1,local_lock=none,addr=::1 0 0



### run SELinux tests:

	yum install -y nfs4-acl-tools
	yum install libselinux-devel selinux-policy-devel perl-Test perl-Test-Harness perl-Test-Simple net-tools netlabel_tools libibverbs libibverbs-devel
	yum install wget

	cd /mnt
	# wget http://ca-oltf-storage1-ps.us.oracle.com/shares/export/tests_workarea/SELINUX/selinux-testsuite-master.zip
	# or git clone : 
	# unzip ...
	cd selinux-testsuite-master/
	make -C policy load
	make -C tests test | tee ../testrun.log

	UEK5-4.14.7-3
	UEK5: generic/089
	Thu Jan 11 01:55:27 PST 2018 [ 734.492899] NFS: nfs4_reclaim_open_state: Lock reclaim failed!


## SElinux :
	man -k selinux

--------------------------------------------------------------------------------

	fs/nfs/pnfs.c:
    LAYOUT_SCSI,                                                                 
    LAYOUT_BLOCK_VOLUME,                                                         
    LAYOUT_OSD2_OBJECTS,                                                         
    LAYOUT_FLEX_FILES,                                                           
    LAYOUT_NFSV4_1_FILES,  

# New Features: 

    4.13:
    NFS filesystems can now be re-exported over NFS. [Note: that turns out not to be entirely true. Open-by-handle support has been added, but full re-exporting is still not possible.]

    4.12:
    Support for parallel NFS (pNFS) on top of object storage devices has been removed; that support is unused and has been unmaintained for some time.

    4.11:
    Security labels for NFS-exported filesystems are now off by default, a change from previous kernels. As NFS maintainer Bruce Fields put it: "But, having them on by default is a disaster, as it generally only makes sense if all your clients and servers have similar enough selinux policies".

    
    4.9:
    The NFS server now supports the NFS4.2 COPY operation, allowing file data to be copied without traveling to the client and back.

    4.7:
    The NFS client now implements the copy_file_range() system call, making use of the NFS 4.2 COPY command to optimize the operation on the remote server.
    4.6:
    The pNFS subsystem (and the NFS server in particular) now supports a "SCSI layout" mode. See pnfs-scsi-server.txt for some more information. 

    4.5:
    The copy_file_range() system call has been merged. It allows for the quick copying of a portion of a file, with the operation possibly optimized by the underlying filesystem. The support code for copy_file_range() has also enabled an easy implementation of the NFSv4.2 CLONE operation. Copy offloading with new copy_file_range(2) system call .

    4.4:
    The NFS client now supports the NFSv4.2 CLONE operation (which makes a fast copy of a file) using an ioctl() that looks suspiciously like the Btrfs clone command.

    4.0:
    The parallel NFS (pNFS) subsystem has gained support for the under-development FlexFile layout. This layout allows file metadata to be stored in a different location from the file contents.

    3.19:
    The NFS client and server both now support the NFS 4.2 ALLOCATE and DEALLOCATE options. The former can be used to request preallocation of storage for a file, while the latter is useful for punching holes.


## Oracle tests:

- No Multiserver pNFS

## Ganesha

[Build](https://github.com/nfs-ganesha/nfs-ganesha/wiki/BuildingV2)

	git clone --recursive git://github.com/nfs-ganesha/nfs-ganesha.git
	mkdir build
	cd build/
	cmake /home/dima/src/nfs-ganesha/src
	make
	make install
	sudo make install
	cp /usr/share/doc/ganesha/config_samples/vfs.conf /etc/gansha/ganesha.conf
	/usr/bin/ganesha.nfsd

----------------------------

# NFS I/O

	dd if=/dev/zero of=/mnt/xxx bs=4k count=1M oflag=direct
	135553+0 records in
	135553+0 records out
	555225088 bytes (555 MB) copied, 422.333 s, 1.3 MB/s
	------------------------------------------------------------------------
	nfsiostat 2 4
	kif.foo.bar:/mnt/test mounted on /mnt:
	------------------------------------------------------------------------
	# first report = to skip ?
	------------------------------------------------------------------------
	op/s        rpc bklog
	323.50      0.00
	------------------------------------------------------------------------
	read: ops/s   kB/s    kB/op   retrans   avg RTT (ms)   avg exe (ms)
	      0.000   0.000   0.000   0 (0.0%)  0.000          0.000
	------------------------------------------------------------------------
	write: ops/s    kB/s      kB/op  retrans   avg RTT (ms)  avg exe (ms)
	       323.000  1394.199  4.316  0 (0.0%)  2.978         3.057
	------------------------------------------------------------------------

	# on server:
	iostat 2 4
	------------------------------------------------------------------------
	# first report = to skip ?
	------------------------------------------------------------------------
	avg-cpu:  %user   %nice %system %iowait  %steal   %idle
	          0.00    0.00    6.79    1.85    0.00   91.36
	------------------------------------------------------------------------
	Device: tps      kB_read/s  kB_wrtn/s  kB_read  kB_wrtn
	sda     819.75   0.00       2049.38    0        3320
	------------------------------------------------------------------------

	# Вопрос: на клиенте 1.4М/сек, а на сервере - 2.0М/сек
	# значит ли это, что учитывается только время от начала до конца
	# "транзакции" (IO) ???

	extended stats:
	------------------------------------------------------------------------
	Device: rrqm/s  wrqm/s  r/s   w/s     rkB/s wkB/s    avgrq-sz avgqu-sz await r_await w_await svctm  %util
	sda     0.00    0.12    0.00  734.67  0.00  1836.92  5.00     1.00     1.36  0.00    1.36    1.36   99.57
	------------------------------------------------------------------------
	rrqm/s  wrqm/s
	The number of read/write requests merged per second that were queued to the device.

	r/s  w/s
	The number (after merges) of read/write requests completed per second for the device.

	rsec/s (rkB/s, rMB/s)  wsec/s (wkB/s, wMB/s)
	The number of sectors (kilobytes, megabytes) read from the device per second.

	avgrq-sz
	The average size (in sectors) of the requests that were issued to the device.

	avgqu-sz
	The average queue length of the requests that were issued to the device.

	await
	The average time (in milliseconds) for I/O requests issued to the  device  to  be  served.
	This includes the time spent by the requests in queue and the time spent servicing them.

	r_await
	The  average  time  (in milliseconds) for read requests issued to the device to be served.
	This includes the time spent by the requests in queue and the time spent servicing them.

	w_await
	The average time (in milliseconds) for write requests issued to the device to  be  served.
	This includes the time spent by the requests in queue and the time spent servicing them.


	------------------------------------------------------------------------
	# fio --bs=4k --direct=1 --iodepth=1 --randrepeat=1 --size=4G \
    --name=job_aio --ioengine=libaio --rw=randrw --filename=test1:test2 \
    --name=job_posixaio --ioengine=posixaio --rw=randrw --filename=test1:test2 \
    --name=job_sync --ioengine=sync --rw=randrw --filename=test1:test2 
	------------------------------------------------------------------------
	# 1) Через NFS (примонтированный локально):
	# iostat -x 60 3 
	Linux 4.1.12-124.1.1.el7uek.x86_64 (ca-ostest452.us.oracle.com) 	22.01.2018 	_x86_64_	(32 CPU)

	avg-cpu:  %user   %nice %system %iowait  %steal   %idle
			   0,04    0,00    0,68    5,29    0,00   93,99
	Device:   rrqm/s  wrqm/s   r/s   w/s      rkB/s  wkB/s    avgrq-sz  avgqu-sz  await  r_await  w_await  svctm  %util
	sdb       0,00    461,00   0,02  2059,20  0,13   9446,60  9,18      1,84      0,89   2442,00  0,87     0,46    95,55
	------------------------------------------------------------------------
	# nfsstat 60 3
	ca-ostest452.us.oracle.com:/mnt/test mounted on /mnt/nfstest:
	op/s     rpc bklog
	1932.02	 0.00
	read:   ops/s   kB/s      kB/op  retrans   avg RTT (ms) avg exe (ms)
			962.800 4148.314  4.309  0 (0.0%)  0.151        0.167
	write:  ops/s   kB/s      kB/op  retrans   avg RTT (ms) avg exe (ms)
			969.217	4187.319  4.320  0 (0.0%)  2.859	    2.879
	------------------------------------------------------------------------
	# 2) на локальную ext4 партицию:
	avg-cpu:  %user   %nice %system %iowait  %steal   %idle
           0,01    0,00    0,04    7,12    0,00   92,82
	Device: rrqm/s wrqm/s r/s     w/s     rkB/s    wkB/s   avgrq-sz  avgqu-sz  await  r_await  w_await  svctm  %util
	sdb     0,00   0,20   270,18  272,32  1089,47  1090,00 8,03      3,65      6,73   13,45    0,07     1,84   100,00

	## ??? Почему на sdb скорость ниже чем на NFS ???

