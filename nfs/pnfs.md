	[root@f28s ~]# cat /etc/fedora-release 
	Fedora release 28 (Twenty Eight)
	[root@f28s ~]# 
	[root@f28s ~]# uname -r
	4.16.3-301.fc28.x86_64
	[root@f28s ~]# egrep -e 'PNFS|LAYOUT' /boot/config-$(uname -r)
	CONFIG_PNFS_FILE_LAYOUT=m
	CONFIG_PNFS_BLOCK=m
	CONFIG_PNFS_FLEXFILE_LAYOUT=m
	CONFIG_NFSD_PNFS=y
	CONFIG_NFSD_BLOCKLAYOUT=y
	CONFIG_NFSD_SCSILAYOUT=y
	CONFIG_NFSD_FLEXFILELAYOUT=y

