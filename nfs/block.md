	cat /etc/exports
	/srv *(rw,no_root_squash,nohide,insecure)
	/vol1 *(rw,no_root_squash,nohide,insecure,pnfs)


	devicetest=/dev/sdb
	server_ipv4=...
	remote_iqn=iqn.1999-11.com.oracle:123456789abc
	wwn=iqn.2008-08.com.example:555
	name=blk1
	#echo "InitiatorName=${clientiqn}" > /etc/iscsi/initiatorname.iscsi
	local_initiator=$(cat /etc/iscsi/initiatorname.iscsi | sed -e 's/^InitiatorName=//')

	systemctl start target.service
	targetcli /backstores/block create ${name} ${devicetest} 
	targetcli iscsi/ create $wwn
	targetcli iscsi/$wwn/tpg1/portals create
	targetcli iscsi/$wwn/tpg1/luns create /backstores/block/${name}
	targetcli iscsi/$wwn/tpg1/acls create $local_initiator
	targetcli iscsi/$wwn/tpg1/acls create $remote_iqn

	# or localhost ???
	iscsiadm --mode discoverydb --type sendtargets 	--portal ${server_ipv4} --discover ${server_ipv4}:3260,1 $wwn
	iscsiadm --mode node --targetname $wwn --portal ${server_ipv4} --login

	targetcli saveconfig

	cat /usr/lib/systemd/system/iscsi.service
	After=systemd-remount-fs.service network.target iscsid.service iscsiuio.service target.service

	cat /usr/lib/systemd/system/nfs-server.service
	After= nfs-idmapd.service rpc-statd.service remote-fs.target


	cat /etc/fstab 
	/dev/sdc               /vol1                    xfs     _netdev         0 0

