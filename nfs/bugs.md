
[Patchwork NFS](https://patchwork.kernel.org/project/linux-nfs/list/)


### rpm -q --changelog kernel-uek-4.1.12-112.16.4.el7uek.x86_64 | grep -i nfs

- NFS: Add static NFS I/O tracepoints (Chuck Lever)   
- NFSv4.1: Don't deadlock the state manager on the SEQUENCE status flags (Trond Myklebust)   
- NFSv4.1: Defer bumping the slot sequence number until we free the slot (Trond Myklebust)   
- NFSv4: Leases are renewed in sequence_done when we have sessions (Trond Myklebust)   
- NFSv4.1: nfs41_sequence_done should handle sequence flag errors (Trond Myklebust)   
- NFSv4.1: Use seqid returned by EXCHANGE_ID after state migration (Chuck Lever)  [Orabug: 25802443]  
- nfsd: encoders mustn't use unitialized values in error cases (J. Bruce Fields)  [Orabug: 26376568]  {CVE-2017-8797} 
- nfsd: fix undefined behavior in nfsd4_layout_verify (Ari Kauppi)  [Orabug: 26376568]  {CVE-2017-8797} 
- nfsd: don't hold i_mutex over userspace upcalls (NeilBrown)  [Orabug: 26401569]  
- NFSv4.1: Handle EXCHGID4_FLAG_CONFIRMED_R during NFSv4.1 migration (Chuck Lever)  [Orabug: 25727872]  
- Revert "NFSv4: Fix callback server shutdown" (Dhaval Giani)  [Orabug: 26479081]  
- NFSv4: Fix callback server shutdown (Trond Myklebust)  [Orabug: 26143102]  {CVE-2017-9059} 
- nfsd: check for oversized NFSv2/v3 arguments (J. Bruce Fields)  [Orabug: 25917857]  {CVE-2017-7645} 
- dtrace: io provider probes for nfs (Nicolas Droux)  [Orabug: 26242655]
- nfsd: stricter decoding of write-like NFSv2/v3 ops (J. Bruce Fields)  [Orabug: 25974739]  {CVE-2017-7895} 
- xenfs: Use proc_create_mount_point() to create /proc/xen (Seth Forshee)   
- nfs: Fix "Don't increment lock sequence ID after NFS4ERR_MOVED" (Chuck Lever)  [Orabug: 25416941]  
- nfs: Don't increment lock sequence ID after NFS4ERR_MOVED (Chuck Lever)  [Orabug: 25416941]  
- nfsd: check permissions when setting ACLs (Ben Hutchings)  [Orabug: 25308145]  
- NFS: Fix another OPEN_DOWNGRADE bug (Trond Myklebust)  [Orabug: 25308034]  
- make nfs_atomic_open() call d_drop() on all ->open_context() errors. (Al Viro)  [Orabug: 25308032]  
- pnfs_nfs: fix _cancel_empty_pagelist (Weston Andros Adamson)  [Orabug: 25308027]  
- nfs: avoid race that crashes nfs_init_commit (Weston Andros Adamson)  [Orabug: 25308026]  
- pNFS: Tighten up locking around DS commit buckets (Trond Myklebust)  [Orabug: 25308025]  
- NFS: Fix an LOCK/OPEN race when unlinking an open file (Chuck Lever)  [Orabug: 24476280]  
- svcrdma: Support IPv6 with NFS/RDMA (Shirley Ma)  [Orabug: 22695683]  
- xprtrdma: Add rdma6 option to support NFS/RDMA IPv6 (Shirley Ma)  [Orabug: 22695683]
- nfsd: fix deadlock secinfo+readdir compound (J. Bruce Fields)  [Orabug: 23331119]  
- nfsd4: fix bad bounds checking (J. Bruce Fields)  [Orabug: 23331077]  
- NFSv4: Fix a dentry leak on alias use (Benjamin Coddington)  [Orabug: 23330965]  
- pNFS/flexfiles: Fix an XDR encoding bug in layoutreturn (Trond Myklebust)  [Orabug: 23330776]  
- ocfs2: NFS hangs in __ocfs2_cluster_lock due to race with ocfs2_unblock_lock (Tariq Saeed)  [Orabug: 23330775] [Orabug: 20933419]  
- NFS: Fix attribute cache revalidation (Trond Myklebust)  [Orabug: 23330606]  
- nfs: Fix race in __update_open_stateid() (Andrew Elble)  [Orabug: 23330604]  
- [sunrpc] Fix NFS/RDMA client mount point hangs when clustered server fails over (Chuck Lever)  [Orabug: 23035067]  
- svcrdma: Fix NFS server crash triggered by 1MB NFS WRITE (Chuck Lever)  [Orabug: 22204799]  
- nfs4: start callback_ident at idr 1 (Benjamin Coddington)  [Orabug: 22623874]  
- nfsd: eliminate sending duplicate and repeated delegations (Andrew Elble)  [Orabug: 22623873]  
- nfsd: serialize state seqid morphing operations (Jeff Layton)  [Orabug: 22623872]  
- NFSoRDMA: for local permissions, pull lkey value from the correct ib_mr (Todd Vierling)  [Orabug: 22202841]  
- Revert "nfs: take extra reference to fl->fl_file when running a LOCKU operation" (Dan Duval)  [Orabug: 22186705]
- nfs4: have do_vfs_lock take an inode pointer (Jeff Layton)   
- nfsd/blocklayout: accept any minlength (Christoph Hellwig)   
- nfs/filelayout: Fix NULL reference caused by double freeing of fh_array (Kinglong Mee)   
- NFS: Fix a write performance regression (Trond Myklebust)   
- nfs: fix pg_test page count calculation (Peng Tao)   
- NFS: Do cleanup before resetting pageio read/write to mds (Kinglong Mee)   
- uek-rpm: unset CONFIG_NFS_USE_LEGACY_DNS for OL7 debug kernel too (Todd Vierling)  [Orabug: 21483381]  
- Revert "NFSv4: Remove incorrect check in can_open_delegated()" (Trond Myklebust)   
- NFSv4.1: Fix a protocol issue with CLOSE stateids (Trond Myklebust)   
- NFSv4.1/flexfiles: Fix a protocol error in layoutreturn (Trond Myklebust)   
- NFS41/flexfiles: zero out DS write wcc (Peng Tao)   
- NFSv4: Force a post-op attribute update when holding a delegation (Trond Myklebust)   
- NFS41/flexfiles: update inode after write finishes (Peng Tao)   
- NFS: nfs_set_pgio_error sometimes misses errors (Trond Myklebust)   
- NFS: Fix a NULL pointer dereference of migration recovery ops for v4.2 client (Kinglong Mee)   
- NFSv4.1/pNFS: Fix borken function _same_data_server_addrs_locked() (Trond Myklebust)   
- NFS: Don't let the ctime override attribute barriers. (Trond Myklebust)   
- NFSv4: don't set SETATTR for O_RDONLY|O_EXCL (NeilBrown)   
- nfsd: ensure that delegation stateid hash references are only put once (Jeff Layton)   
- nfsd: ensure that the ol stateid hash reference is only put once (Jeff Layton)   
- nfsd: Fix an FS_LAYOUT_TYPES/LAYOUT_TYPES encode bug (Kinglong Mee)   
- NFSv4/pnfs: Ensure we don't miss a file extension (Trond Myklebust)   
- block: loop: Enable directIO on nfs (Dave Kleikamp)   
- nfs: don't dirty kernel pages read by direct-io (Dave Kleikamp)   
- NFS: Enable client side NFSv4.1 backchannel to use other transports (Chuck Lever)   
- xprtrdma: Enable swap-on-NFS/RDMA (Chuck Lever)   
- xprtrdma: Fix large NFS SYMLINK calls (Chuck Lever)   
- xprtrdma: Always provide a write list when sending NFS READ (Chuck Lever)   
- NFS/RDMA Release resources in svcrdma when device is removed (Shirley Ma)   
- uek-rom: config: Unset CONFIG_NFS_USE_LEGACY_DNS for OL7 (Todd Vierling)  [Orabug: 21483381]  
- nfs: take extra reference to fl->fl_file when running a LOCKU operation (Jeff Layton)  [Orabug: 21687670]  
- NFS hangs in __ocfs2_cluster_lock due to race with ocfs2_unblock_lock (Tariq Saeed)  [Orabug: 20933419]  
- nfsd: do nfs4_check_fh in nfs4_check_file instead of nfs4_check_olstateid (Jeff Layton)   
- nfsd: refactor nfs4_preprocess_stateid_op (Christoph Hellwig)   
- nfsd: Drop BUG_ON and ignore SECLABEL on absent filesystem (Kinglong Mee)   
- NFS: Fix a memory leak in nfs_do_recoalesce (Trond Myklebust)   
- NFSv4: We must set NFS_OPEN_STATE flag in nfs_resync_open_stateid_locked (Trond Myklebust)   
- NFS: Don't revalidate the mapping if both size and change attr are up to date (Trond Myklebust)   
- nfs: always update creds in mirror, even when we have an already connected ds (Jeff Layton)   
- nfs: fix potential credential leak in ff_layout_update_mirror_cred (Jeff Layton)   
- NFS: Ensure we set NFS_CONTEXT_RESEND_WRITES when requeuing writes (Trond Myklebust)   
- nfs: increase size of EXCHANGE_ID name string buffer (Jeff Layton)   
- NFS: Fix size of NFSACL SETACL operations (Chuck Lever)   
- pNFS/flexfiles: Fix the reset of struct pgio_header when resending (Trond Myklebust)   
- pNFS: Fix a memory leak when attempted pnfs fails (Trond Myklebust)   
- kernfs: Add support for always empty directories. (Eric W. Biederman)   
- selinux: fix setting of security labels on NFS (J. Bruce Fields)   
- block: loop: Enable directIO on nfs (Dave Kleikamp)   
- nfs: don't dirty kernel pages read by direct-io (Dave Kleikamp)   

### rpm -q --changelog kernel-uek-4.1.12-112.16.4.el7uek.x86_64 | grep -i nfs | grep deadlock
- NFSv4.1: Don't deadlock the state manager on the SEQUENCE status flags (Trond Myklebust)   
- nfsd: fix deadlock secinfo+readdir compound (J. Bruce Fields)  [Orabug: 23331119]  

## OraBUG 28775861 - v4.1 nfs4_discover_server_trunking unhandled error -512. Exiting
- [Redhat reproducer](https://bugzilla.redhat.com/show_bug.cgi?id=1506382)

