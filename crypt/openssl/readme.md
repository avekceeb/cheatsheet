

```bash
https://en.wikipedia.org/wiki/Block_cipher_mode_of_operation
https://www.madboa.com/geek/openssl/

## random numbers:
openssl rand [-out file] [-rand file(s)] [-base64] [-hex] num
openssl rand -hex 8
bdbe8722faa933e4


openssl list-cipher-commands
openssl list-public-key-algorithms

### bin to base64
openssl enc -base64 -in plain.txt -out base64.txt
### from bas64 to bin
### NOTE: newline!!!
openssl enc -d -base64 -out plain-decoded.txt -in base64.txt

## asking password
openssl enc -aes256 -in plain.txt -out aes256.txt
openssl enc -d -aes256 -in aes256.txt -out plain-decoded.txt


openssl enc -aes256 -base64 -in plain.txt -out aes256.txt
enter aes-256-cbc encryption password:
Verifying - enter aes-256-cbc encryption password:

cat aes256.txt
U2FsdGVkX18JUyhSci20hrSYZQju8qIeXYYEykOjPRz6QjxZXB7XWzLmTKehLQCP

cat aes256-2.txt
U2FsdGVkX1+/6A9NHob61crmIds49YH73pUAMStCrrG1UfiNXnR2hpRwby1POo/O

cat aes256-3.txt
U2FsdGVkX1+Y/V4Im5MuaPKPNlSnd7U11dyHiXqUeHJtu4tqEXPoNb9Uyj5VViIM

openssl enc -d -base64  -aes256 -in aes256-3.txt -out decipered.txt
enter aes-256-cbc decryption password:

cat decipered.txt
Hello
World
!!!

openssl enc -aes256 -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1
openssl enc -d -base64 -aes256 -in crypted.txt -out decipered.txt -pass pass:Welcome1

org@atom:~/ssl$ openssl enc -aes256 -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1 -p
salt=F6DD23770714D551
key=4039CEEBA83CFA97D555CC86D7775E8D3E111008362E9F7338710FEC8F259BE3
iv =3EB246F794ADDC9E5B08B773320145C5
org@atom:~/ssl$ 
org@atom:~/ssl$ cat crypted.txt
U2FsdGVkX1/23SN3BxTVUWX1VUoJKxq9UyTfJ2apRFY6meopWmLT+juyb2acrpwE
org@atom:~/ssl$ 
org@atom:~/ssl$ openssl enc -aes256 -base64 -in plain.txt -out crypted-2.txt -pass pass:Welcome1 -p
salt=5E299E2C83F527C1
key=5BD1C27EE22FF8BDE31BE14C916DAA16D95BFC525A990B2B9FF972F543E53510
iv =58F6B184F3D1C137321426FBD054C686
org@atom:~/ssl$ 
org@atom:~/ssl$ cat crypted-2.txt
U2FsdGVkX19eKZ4sg/Unwa8fnFzyD5xO/plGWLRqHddAAcbO9Rn42EvvNByCoktq

openssl enc -aes-128-cbc -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1 -p
salt=13B5BB19EF43C2D2
key=A222A1897346FB1020010ADB53686A09
iv =072EED73AD58E6B711B61F22CF3B3907
org@atom:~/ssl$ cat crypted.txt
U2FsdGVkX18TtbsZ70PC0nSZKH4m5qYMFK1VrwPKhTpZhWROEbDs97eSLlOEPl6A

# using same key and iv
openssl enc -aes-128-cbc -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1 -p -K A222A1897346FB1020010ADB53686A09 -iv 072EED73AD58E6B711B61F22CF3B3907
salt=A086874799C0DBB7
key=A222A1897346FB1020010ADB53686A09
iv =072EED73AD58E6B711B61F22CF3B3907

org@atom:~/ssl$ cat crypted.txt
U2FsdGVkX1+ghodHmcDbt3SZKH4m5qYMFK1VrwPKhTpZhWROEbDs97eSLlOEPl6A

#without salt 
openssl enc -aes-128-cbc -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1 -p -K A222A1897346FB1020010ADB53686A09 -iv 072EED73AD58E6B711B61F22CF3B3907 -nosalt
key=A222A1897346FB1020010ADB53686A09
iv =072EED73AD58E6B711B61F22CF3B3907
org@atom:~/ssl$ cat crypted.txt
dJkofibmpgwUrVWvA8qFOlmFZE4RsOz3t5IuU4Q+XoA=
org@atom:~/ssl$ 
org@atom:~/ssl$ 
org@atom:~/ssl$ openssl enc -aes-128-cbc -base64 -in plain.txt -out crypted.txt -pass pass:Welcome1 -p -K A222A1897346FB1020010ADB53686A09 -iv 072EED73AD58E6B711B61F22CF3B3907 -nosalt
key=A222A1897346FB1020010ADB53686A09
iv =072EED73AD58E6B711B61F22CF3B3907
org@atom:~/ssl$ cat crypted.txt
dJkofibmpgwUrVWvA8qFOlmFZE4RsOz3t5IuU4Q+XoA=


#########################################
## View cert:
openssl x509 -noout -text -in CERT.crt

openssl engine
#(t4) SPARC T4 engine support
#(dynamic) Dynamic engine loading support

cat /proc/cpuinfo | grep cpucaps
#cpucaps     : flush,stbar,swap,muldiv,v9,blkinit,n2,mul32,div32,v8plus,popc,vis,vis2,ASIBlkInit,fmaf,vis3,hpc,ima,pause,cbcond,aes,des,kasumi,camellia,md5,sha1,sha256,sha512,mpmul,montmul,montsqr,crc32c

ls /proc/self/auxv
#/proc/self/auxv

strace -f java AesSample 2>&1 | grep crypt
 [pid 41881] read(11, "gcrypt-1", 8)     = 8
 [pid 41881] read(11, " m2crypt", 8)     = 8
 [pid 41881] read(11, "cryptset", 8)     = 8

#This is an example of not using openssl :(
#But heres an example of openssl using libcrypto:
strace -f -o /tmp/openssl openssl speed -evp aes-128-cbc
...
$ grep crypt /tmp/openssl
5579  open("/lib64/libk5crypto.so.3", O_RDONLY|O_CLOEXEC) = 3
5579  open("/lib64/libcrypto.so.10", O_RDONLY|O_CLOEXEC) = 3
5579  open("/proc/sys/crypto/fips_enabled", O_RDONLY) = 3


openssl speed
#Available values:
#md2      md4      md5      hmac     sha1     sha256   sha512   whirlpoolrmd160
#seed-cbc rc2-cbc  bf-cbc
#des-cbc  des-ede3 aes-128-cbc aes-192-cbc aes-256-cbc aes-128-ige aes-192-ige aes-256-ige 
#camellia-128-cbc camellia-192-cbc camellia-256-cbc rc4
#rsa512   rsa1024  rsa2048  rsa4096
#dsa512   dsa1024  dsa2048
#seed     rc2      des      aes      camellia rsa      blowfish
#Available options:
#-engine e       use engine e, possibly a hardware device.
#-evp e          use EVP e.
#-decrypt        time decryption instead of encryption (only EVP).
#-mr             produce machine readable output.
#-multi n        run n benchmarks in parallel.
```

