
## Script for single-host setup

```bash
# !!! Since this is for single host test, no NTP required
# !!! FQDN should be configured (via DNS or /etc/hosts)
host=$(hostname -f)
dom=$(domainname)
if [ "x(none)" = "x$dom" ] ; then
    dom=${host#[^.]*.}
fi
# to uppercase (in bash4.0+)
DOM=${dom^^}
 
password=test4oel
 
yum install -y krb5-server krb5-workstation pam_krb5 nfs-utils
 
# for the sake of simplicity
setenforce 0
systemctl stop firewalld
 
cat > /var/kerberos/krb5kdc/kdc.conf<<EOF
[kdcdefaults]
 kdc_ports = 88
 kdc_tcp_ports = 88
 
[realms]
 ${DOM} = {
  master_key_type = aes256-cts
  default_principal_flags = +preauth
  acl_file = /var/kerberos/krb5kdc/kadm5.acl
  dict_file = /usr/share/dict/words
  admin_keytab = /etc/krb5.keytab
  supported_enctypes = aes256-cts:normal aes128-cts:normal des3-hmac-sha1:normal arcfour-hmac:normal camellia256-cts:normal camellia128-cts:normal des-hmac-sha1:normal des-cbc-md5:normal des-cbc-crc:normal
 }
EOF
 
cat > /var/kerberos/krb5kdc/kadm5.acl<<EOF
*/admin@${DOM}    *
EOF
 
cat > /etc/krb5.conf<<EOF
includedir /etc/krb5.conf.d/
 
[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log
 
[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 default_realm = ${DOM}
 default_ccache_name = KEYRING:persistent:%{uid}
 
[realms]
 ${DOM} = {
  kdc = ${host}
  admin_server = ${host}
 }
 
[domain_realm]
 .${dom} = ${DOM}
 ${dom} = ${DOM}
EOF
 
kdb5_util create -s -r ${DOM} -P ${password}
 
systemctl start krb5kdc kadmin
systemctl enable krb5kdc kadmin
 
# add some user to check ssh with
useradd -m qa
echo ${password} | passwd --stdin qa
 
kadmin.local -q 'addprinc root/admin'
# ... password
kadmin.local -q "ktadd host/${host}"
kadmin.local -q "addprinc -randkey host/${host}"
kadmin.local -q "addprinc -randkey nfs/${host}"
kadmin.local -q "ktadd nfs/${host}"
kadmin.local -q "addprinc qa"
# ... password
 
# !!! this would spoil the setup for reasons I don't understand:
# kadmin.local -q "ktadd qa"
 
####################################
# to check ssh gss-krb5 auth:
# enable gss in /etc/ssh/sshd_config
# GSSAPIAuthentication yes
# GSSAPICleanupCredentials yes
 
# .. and /etc/ssh/ssh_config
# Host *
#    GSSAPIAuthentication yes
#    GSSAPIDelegateCredentials yes
 
systemctl reload sshd
authconfig --enablekrb5 --update
su - qa
$ kinit
$ klist # check that ticket is granted
$ ssh ${host} # should not be asked for password
####################################
 
# check nfs krb5 security:
 
mkdir -p /share
chmod 777 /share
 
cat > /etc/exports<<EOF
/share ${host}(rw,no_root_squash,sec=krb5)
EOF
 
systemctl start nfs-server
systemctl start nfs-client.target
 
mount -t nfs4 -o sec=krb5 ${host}:/share /mnt
```


```bash
#################################
### working config            ###
#################################

4.1.12-112.12.1.el7uek.x86_64

cat /etc/krb5.conf
# Configuration snippets may be placed in this directory as well
includedir /etc/krb5.conf.d/

[logging]
 default = FILE:/var/log/krb5libs.log
 kdc = FILE:/var/log/krb5kdc.log
 admin_server = FILE:/var/log/kadmind.log

[libdefaults]
 dns_lookup_realm = false
 ticket_lifetime = 24h
 renew_lifetime = 7d
 forwardable = true
 rdns = false
 default_realm = WONDER.LAND
 default_ccache_name = KEYRING:persistent:%{uid}

[realms]
 WONDER.LAND = {
  kdc = chopper.wonder.land
  admin_server = chopper.wonder.land
 }

[domain_realm]
 .wonder.land = WONDER.LAND
 wonder.land = WONDER.LAND
#################################
kadmin.local -q 'listprincs'
Authenticating as principal root/admin@WONDER.LAND with password.
K/M@WONDER.LAND
host/chopper.wonder.land@WONDER.LAND
kadmin/admin@WONDER.LAND
kadmin/changepw@WONDER.LAND
kadmin/chopper.wonder.land@WONDER.LAND
kiprop/chopper.wonder.land@WONDER.LAND
krbtgt/WONDER.LAND@WONDER.LAND
nfs/chopper.wonder.land@WONDER.LAND
qa@WONDER.LAND
root/admin@WONDER.LAND
#################################
klist -kte /etc/krb5.keytab 
Keytab name: FILE:/etc/krb5.keytab
KVNO Timestamp           Principal
---- ------------------- ------------------------------------------------------
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (aes256-cts-hmac-sha1-96) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (aes128-cts-hmac-sha1-96) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (des3-cbc-sha1) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (arcfour-hmac) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (camellia256-cts-cmac) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (camellia128-cts-cmac) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (des-hmac-sha1) 
   2 08.12.2017 03:43:50 host/chopper.wonder.land@WONDER.LAND (des-cbc-md5) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (aes256-cts-hmac-sha1-96) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (aes128-cts-hmac-sha1-96) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (des3-cbc-sha1) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (arcfour-hmac) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (camellia256-cts-cmac) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (camellia128-cts-cmac) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (des-hmac-sha1) 
   2 08.12.2017 04:34:06 nfs/chopper.wonder.land@WONDER.LAND (des-cbc-md5) 
#################################
authconfig --test
caching is disabled
nss_files is always enabled
nss_compat is disabled
nss_db is disabled
nss_hesiod is disabled
 hesiod LHS = ""
 hesiod RHS = ""
nss_ldap is disabled
 LDAP+TLS is disabled
 LDAP server = ""
 LDAP base DN = ""
nss_nis is disabled
 NIS server = ""
 NIS domain = ""
nss_nisplus is disabled
nss_winbind is disabled
 SMB workgroup = ""
 SMB servers = ""
 SMB security = "user"
 SMB realm = ""
 Winbind template shell = "/bin/false"
 SMB idmap range = "16777216-33554431"
nss_sss is enabled by default
nss_wins is disabled
nss_mdns4_minimal is disabled
myhostname is enabled
DNS preference over NSS or WINS is disabled
pam_unix is always enabled
 shadow passwords are enabled
 password hashing algorithm is sha512
pam_krb5 is enabled
 krb5 realm = "WONDER.LAND"
 krb5 realm via dns is disabled
 krb5 kdc = "chopper.wonder.land"
 krb5 kdc via dns is disabled
 krb5 admin server = "chopper.wonder.land"
pam_ldap is disabled
 LDAP+TLS is disabled
 LDAP server = ""
 LDAP base DN = ""
 LDAP schema = "rfc2307"
pam_pkcs11 is disabled
SSSD smartcard support is disabled
 use only smartcard for login is disabled
 smartcard module = ""
 smartcard removal action = ""
pam_fprintd is disabled
pam_ecryptfs is disabled
pam_winbind is disabled
 SMB workgroup = ""
 SMB servers = ""
 SMB security = "user"
 SMB realm = ""
pam_sss is disabled by default
 credential caching in SSSD is enabled
 SSSD use instead of legacy services if possible is enabled
IPAv2 is disabled
IPAv2 domain was not joined
 IPAv2 server = ""
 IPAv2 realm = ""
 IPAv2 domain = ""
pam_pwquality is enabled (try_first_pass local_users_only retry=3 authtok_type=)
pam_passwdqc is disabled ()
pam_access is disabled ()
pam_faillock is disabled (deny=4 unlock_time=1200)
pam_mkhomedir or pam_oddjob_mkhomedir is disabled (umask=0077)
Always authorize local users is enabled ()
Authenticate system accounts against network services is disabled

```


debug1: Authentication succeeded (gssapi-with-mic).

---------------------------------------------

## Pyhton gssapi

[gssapi python](https://pythonhosted.org/gssapi/basic-tutorial.html)

```
[root@kif nfs4.1]# kadmin.local
Authenticating as principal root/admin@FOO.BAR with password.
kadmin.local:  listprincs
K/M@FOO.BAR
host/amy.foo.bar@FOO.BAR
host/kif.foo.bar@FOO.BAR
kadmin/admin@FOO.BAR
kadmin/changepw@FOO.BAR
kadmin/kif.foo.bar@FOO.BAR
kiprop/kif.foo.bar@FOO.BAR
krbtgt/FOO.BAR@FOO.BAR
nfs/amy.foo.bar@FOO.BAR
nfs/kif.foo.bar@FOO.BAR
qa@FOO.BAR
root/admin@FOO.BAR
kadmin.local:  quit
[root@kif nfs4.1]# 
[root@kif nfs4.1]# kinit qa@FOO.BAR
Password for qa@FOO.BAR: 
[root@kif nfs4.1]# 
[root@kif nfs4.1]# klist
Ticket cache: KEYRING:persistent:0:0
Default principal: qa@FOO.BAR

Valid starting       Expires              Service principal
12/12/2017 19:16:19  12/13/2017 19:16:15  krbtgt/FOO.BAR@FOO.BAR
[root@kif nfs4.1]# 
[root@kif nfs4.1]# python
Python 2.7.5 (default, May 29 2017, 20:42:36) 
[GCC 4.8.5 20150623 (Red Hat 4.8.5-11)] on linux2
Type "help", "copyright", "credits" or "license" for more information.
>>> import gssapi
>>> server_name = gssapi.Name('qa@FOO.BAR')
>>> client_ctx = gssapi.SecurityContext(name=server_name, usage='initiate')
>>> initial_client_token = client_ctx.step()
>>> initial_client_token
'`\x82\x02.\x06\t*\x86H\x86\xf7\x12\x01\x02\x02\x01\x00n\x82\x02\x1d0\x82\x02\x19\xa0\x03\x02\x01\x05\xa1\x03\x02\x01\x0e\xa2\x07\x03\x05\x00 \x00\x00\x00\xa3\x82\x018a\x82\x0140\x82\x010\xa0\x03\x02\x01\x05\xa1\t\x1b\x07FOO.BAR\xa2\x0f0\r\xa0\x03\x02\x01\x01\xa1\x060\x04\x1b\x02qa\xa3\x82\x01\x0b0\x82\x01\x07\xa0\x03\x02\x01\x12\xa1\x03\x02\x01\x01\xa2\x81\xfa\x04\x81\xf7/\xb6meZU\x8f\xd7\x9d\xf9\x10\x98P\xa5\xfa;\xb4T\x97t=\xd9\x98s\xe8W\xf6\xbf1\x99\x96y\xb6\x93\x95\na7\xba0f\x94"\x1e\xb1\x87:\x7fU\xcf\xce\xe0f2\xbb\xd6\x8a#\xdeV\x82\x9bX\x0e\x07e\x8dW>\xa3\n2\x989\xff\xcfx\xf0D\xe5\xf4\xc6\x97yZ\xa1\x991H\x93\xdaJ\xdb\xbe\x81&\x90\xab\xe09\xf5\x88=\xe6\xcan\x1cl\x0e.\xf8\x0b\x82\x9c\x0b\x0c!\xb8q\x9c\x83.\x8aH\xb9\xb8\x9f@\xf1\x94\xe7\xa1\xa1\xd8Hi\xf5\xb83tn\xd8\x11\xe7\xbe\x94\xb1\x93\xe3_\xca\x9e\xae@UL\xf6\xe2\x07\x91\xc1)\xfd\xcdg\xf6\x1eL\xce@&\x17\xc7=;\x8e\xd7\xde\x8d*\xf6\xc5Zxg\xa0\xb7\xd8\x97\x0fs\x97\xecL|d\x90\xa6\xf0\xd4\x16\x19?W\x84LC\xbe\xc7\x03\xd9W\xcb\xb5\x8f\x8a[9sw\x91\xefW @\xc3\xb2X\x86\xe29<D\xac\x06\xb5?\x98Z[w\x84\x13!=F\x9f\xa4\x81\xc70\x81\xc4\xa0\x03\x02\x01\x12\xa2\x81\xbc\x04\x81\xb95\xd9\x19\x05\xe5\xd6b\x15\xe7\x1c[\xf9\x10\x0bs\x91\x12\xa9^\x86\tcN\x8a\x0f>R\xee\t\xa5\xc5\xe7\xf8R\'\xf0\xac\xb4Z\x91`\x8d\xd3\xdd\x03U\xa2\xdf\x0c\xb8\x16\xfb\x90K\x9f\xb4\x99\xbfv\x8f\xe4N\xfc\xaa.x\xaa6L\xbf\x17\x1e\xeb\xdd4\x8d\xde\xfcln\xa5OI7Iu@\xaf>\x14a&\x84\xa6\xcc\xdb<a\x1eL\x19\xe4;\x98?\rd\xc1\x8cVFp\xe5\x84\x89\xdf\xfb<\xebH8e\xc8\xdeo\xa93\x0eO|\x92\xaf\x82\xaa\xc4\xfd\x95\xe5\xc6i\xf9\xfc\xb6op\xf8<Qv \xee\xfb\x93\x10\x9070>\x1e\xec\xd7\x8ct\x87c\xe3\x94s\x11\'4\x1b\x15B\xf2:\xfc\xcc\x1c>Q\xd1g\x02>'
>>> client_ctx.complete
False
>>> 
```

