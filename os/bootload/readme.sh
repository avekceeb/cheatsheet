# http://prefetch.net/blog/index.php/2006/09/09/digging-through-the-mbr/

sudo dd if=/dev/sda of=./mbr.bin bs=512 count=1
sudo dd if=/dev/sda of=./mbr-boot.bin bs=446 count=1
objdump -D -b binary -mi386 -Maddr16,data16 ./mbr-boot.bin > mbr-boot.S
ndisasm mbr-boot.bin
ndisasm mbr-boot.bin > mbr-boot.asm

objdump -D -b binary -mi386 -Maddr16,data16  /boot/grub/i386-pc/boot.img


#gcc -DHAVE_CONFIG_H -I. -I..  -Wall -W  -DGRUB_MACHINE_PCBIOS=1 -DGRUB_MACHINE=I386_PC -m32 -nostdinc -isystem /usr/lib/gcc/x86_64-linux-gnu/5/include -I../include -I../include -DGRUB_FILE=\"boot/i386/pc/startup_raw.S\" -I. -I. -I.. -I.. -I../include -I../include -I../grub-core/lib/libgcrypt-grub/src/    -D_FILE_OFFSET_BITS=64 -g  -m32 -msoft-float -DGRUB_FILE=\"boot/i386/pc/startup_raw.S\" -I. -I. -I.. -I.. -I../include -I../include -I../grub-core/lib/libgcrypt-grub/src/ -DASM_FILE=1     -MT boot/i386/pc/lzma_decompress_image-startup_raw.o -MD -MP -MF boot/i386/pc/.deps-core/lzma_decompress_image-startup_raw.Tpo -c -o boot/i386/pc/lzma_decompress_image-startup_raw.o `test -f 'boot/i386/pc/startup_raw.S' || echo './'`boot/i386/pc/startup_raw.S



#gcc -Os -Wall -W -Wshadow -Wpointer-arith -Wundef -Wchar-subscripts -Wcomment -Wdeprecated-declarations -Wdisabled-optimization -Wdiv-by-zero -Wfloat-equal -Wformat-extra-args -Wformat-security -Wformat-y2k -Wimplicit -Wimplicit-function-declaration -Wimplicit-int -Wmain -Wmissing-braces -Wmissing-format-attribute -Wmultichar -Wparentheses -Wreturn-type -Wsequence-point -Wshadow -Wsign-compare -Wswitch -Wtrigraphs -Wunknown-pragmas -Wunused -Wunused-function -Wunused-label -Wunused-parameter -Wunused-value  -Wunused-variable -Wwrite-strings -Wnested-externs -Wstrict-prototypes -g -Wredundant-decls -Wmissing-prototypes -Wmissing-declarations  -Wextra -Wattributes -Wendif-labels -Winit-self -Wint-to-pointer-cast -Winvalid-pch -Wmissing-field-initializers -Wnonnull -Woverflow -Wvla -Wpointer-to-int-cast -Wstrict-aliasing -Wvariadic-macros -Wvolatile-register-var -Wpointer-sign -Wmissing-include-dirs -Wmissing-prototypes -Wmissing-declarations -Wformat=2 -march=i386 -m32 -mrtd -mregparm=3 -falign-jumps=1 -falign-loops=1 -falign-functions=1 -freg-struct-return -mno-mmx -mno-sse -mno-sse2 -mno-sse3 -mno-3dnow -msoft-float -fno-dwarf2-cfi-asm -mno-stack-arg-probe -fno-asynchronous-unwind-tables -fno-unwind-tables -Qn -fno-stack-protector -Wtrampolines -Werror   -fno-builtin   -m32 -Wl,-melf_i386 -Wl,--build-id=none  -nostdlib -Wl,-N -Wl,-S -Wl,-N -Wl,-Ttext,0x8200   -o lzma_decompress.image boot/i386/pc/lzma_decompress_image-startup_raw.o

cd ${grub_src}/grub-core

gcc -DHAVE_CONFIG_H -I. -I..  -Wall -W  \
    -DGRUB_MACHINE_PCBIOS=1 -DGRUB_MACHINE=I386_PC \
    -m32 -nostdinc -isystem /usr/lib/gcc/x86_64-linux-gnu/5/include \
    -I../include -I. -I.. \
    -DGRUB_FILE=\"boot/i386/pc/boot.S\" \
    -I../grub-core/lib/libgcrypt-grub/src/ \
    -D_FILE_OFFSET_BITS=64 \
    -g -msoft-float \
    -I../grub-core/lib/libgcrypt-grub/src/ \
    -DASM_FILE=1 \
    -MT boot/i386/pc/boot_image-boot.o \
    -MD -MP -MF boot/i386/pc/.deps-core/boot_image-boot.Tpo \
    -c -o boot/i386/pc/boot_image-boot.o \
    boot/i386/pc/boot.S


gcc -Os -Wall -W -Wshadow -Wpointer-arith -Wundef -Wchar-subscripts -Wcomment -Wdeprecated-declarations -Wdisabled-optimization -Wdiv-by-zero -Wfloat-equal -Wformat-extra-args -Wformat-security -Wformat-y2k -Wimplicit -Wimplicit-function-declaration -Wimplicit-int -Wmain -Wmissing-braces -Wmissing-format-attribute -Wmultichar -Wparentheses -Wreturn-type -Wsequence-point -Wshadow -Wsign-compare -Wswitch -Wtrigraphs -Wunknown-pragmas -Wunused -Wunused-function -Wunused-label -Wunused-parameter -Wunused-value  -Wunused-variable -Wwrite-strings -Wnested-externs -Wstrict-prototypes -g -Wredundant-decls -Wmissing-prototypes -Wmissing-declarations  -Wextra -Wattributes -Wendif-labels -Winit-self -Wint-to-pointer-cast -Winvalid-pch -Wmissing-field-initializers -Wnonnull -Woverflow -Wvla -Wpointer-to-int-cast -Wstrict-aliasing -Wvariadic-macros -Wvolatile-register-var -Wpointer-sign -Wmissing-include-dirs -Wmissing-prototypes -Wmissing-declarations -Wformat=2 -march=i386 -m32 -mrtd -mregparm=3 -falign-jumps=1 -falign-loops=1 -falign-functions=1 -freg-struct-return -mno-mmx -mno-sse -mno-sse2 -mno-sse3 -mno-3dnow -msoft-float -fno-dwarf2-cfi-asm -mno-stack-arg-probe -fno-asynchronous-unwind-tables -fno-unwind-tables -Qn -fno-stack-protector -Wtrampolines -Werror   -fno-builtin \
    -m32 -Wl,-melf_i386 -Wl,--build-id=none \
    -nostdlib -Wl,-N -Wl,-S -Wl,-N -Wl,-Ttext,0x7C00 \
    -o boot.image \
    boot/i386/pc/boot_image-boot.o

objcopy  -O binary  --strip-unneeded \
    -R .note -R .comment -R .note.gnu.build-id -R .MIPS.abiflags -R .reginfo -R .rel.dyn -R .note.gnu.gold-version -R .ARM.exidx \
    boot.image boot.img

