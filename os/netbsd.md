

## cross build on linux
```bash
git clone -b master --depth 1 https://github.com/IIJ-NetBSD/netbsd-src.git ~/usr/src
cd usr/src
sudo zypper install -y gmp-devel \
    mpfr-devel \
    mpc-devel \
    flex gcc gcc-c++ \
    make
./build.sh -U -u -m amd64 tools
./build.sh -U -u -m amd64 release
./build.sh -U -u -m amd64 iso-image


#  -U             Set MKUNPRIVED=yes; build without requiring root privileges,
#                 install from an UNPRIVED build with proper file permissions.
#  -u             Set MKUPDATE=yes; do not run "make cleandir" first.
#                 Without this, everything is rebuilt, including the tools.
#  -m mach        Set MACHINE to mach; not required if NetBSD native.
```

```bash
===> Summary of results:
	 build.sh command:    build.sh -j 1 -m i386 -O ../obj.i386 -D ../obj.i386/destdir.i386 -U -u release
	 build.sh started:    Fri Oct 13 21:49:09 MSK 2017
	 MINIX version:       3.4.0
	 MACHINE:             i386
	 MACHINE_ARCH:        i386
	 Build platform:      Linux 4.10.0-37-generic x86_64
	 HOST_SH:             /bin/sh
	 MAKECONF file:       /etc/mk.conf (File not found)
	 TOOLDIR path:        /home/ru/src/minix-build/minix/../obj.i386/tooldir.Linux-4.10.0-37-generic-x86_64
	 DESTDIR path:        /home/ru/src/minix-build/minix/../obj.i386/destdir.i386
	 RELEASEDIR path:     /home/ru/src/minix-build/minix/../obj.i386/releasedir
	 Updated makewrapper: /home/ru/src/minix-build/minix/../obj.i386/tooldir.Linux-4.10.0-37-generic-x86_64/bin/nbmake-i386
	 Successful make release
	 build.sh ended:      Sat Oct 14 00:17:44 MSK 2017
===> .
Building work directory...
 * Extracting minix-base...
 * Extracting minix-comp...
 * Extracting minix-games...
 * Extracting minix-man...
 * Extracting minix-tests...
 * Extracting tests...
Adding extra files...
Bundling packages...
 * PACKAGE_DIR not set, skipping package bundling...
Creating specification files...
Writing disk image...
 * ROOT
 * USR
 * HOME
Part     First         Last         Base      Size       Kb
  0      0/001/00   127/063/31        32    262112   131056
  1    128/000/00  1919/063/31    262144   3670016  1835008
  2   1920/000/00  2047/063/31   3932160    262144   131072
  3      0/000/00  2097151/063/31         0         0        0

Disk image at /home/ru/src/minix-build/minix/minix_x86.img

To boot this image on kvm using the bootloader:
qemu-system-i386 --enable-kvm -m 256 -hda /home/ru/src/minix-build/minix/minix_x86.img

To boot this image on kvm:
cd /home/ru/src/minix-build/obj.i386/destdir.i386/boot/minix/.temp && qemu-system-i386 --enable-kvm -m 256M -kernel kernel -append "rootdevname=c0d0p0" -initrd "mod01_ds,mod02_rs,mod03_pm,mod04_sched,mod05_vfs,mod06_memory,mod07_tty,mod08_mib,mod09_vm,mod10_pfs,mod11_mfs,mod12_init" -hda /home/ru/src/minix-build/minix/minix_x86.img
To boot this image on kvm with EFI (tianocore OVMF):
qemu-system-i386 -L . -bios OVMF-i32.fd -m 256M -drive file=minix_x86.img,if=ide,format=raw
```

