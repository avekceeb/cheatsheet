OpenWRT
-------

###Build

```bash
yum -y install zlib-static \
    unzip gawk \
    ncurses-devel \
    openssl-devel \
    asciidoc \
    git svn  \
    libgcj \ \
    flex \
    util-linux \
    gtk2-devel \
    intltool \
    zlib-devel \
    perl-ExtUtils-MakeMaker \
    rsync \
    unzip \
    wget \
    gettext \
    perl-XML-Parser

#buildroot=/olt-storage/local
#cd $buildroot
#PATH="$buildroot/openwrt/staging_dir/host/bin:$buildroot/openwrt/staging_dir/toolchain-mips_34kc_gcc-5.3.0_musl-1.1.15/bin:$PATH"
#export PATH

git clone git://github.com/openwrt/openwrt.git

cd openwrt

git checkout tags/v15.05.1

./scripts/feeds update -a
./scripts/feeds install -a

make menuconfig
make oldconfig

make
```

## usb storage suppport

```bash

kmod-usb-storage
kmod-fs-...
block-mount
kmod-scsi-core
```

```
make V=s package/utils/60t/prepare
make V=s package/utils/60t/compile
make V=s package/utils/60t/install
```
[repo](https://downloads.openwrt.org/chaos_calmer/15.05.1/ar71xx/generic/packages/)



```
rootfs on / type rootfs (rw)
/dev/root on /rom type squashfs (ro,relatime)
proc on /proc type proc (rw,nosuid,nodev,noexec,noatime)
sysfs on /sys type sysfs (rw,nosuid,nodev,noexec,noatime)
tmpfs on /tmp type tmpfs (rw,nosuid,nodev,noatime)
/dev/mtdblock3 on /overlay type jffs2 (rw,noatime)
overlayfs:/overlay on / type overlay (rw,noatime,lowerdir=/,upperdir=/overlay/upper,workdir=/overlay/work)
tmpfs on /dev type tmpfs (rw,nosuid,relatime,size=512k,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,mode=600)
debugfs on /sys/kernel/debug type debugfs (rw,noatime)

cat /etc/config/network 

config interface 'loopback'
	option ifname 'lo'
	option proto 'static'
	option ipaddr '127.0.0.1'
	option netmask '255.0.0.0'

config globals 'globals'
	option ula_prefix 'fd44:5d40:3d62::/48'

config interface 'lan'
	option ifname 'eth0'
	option force_link '1'
	option type 'bridge'
	option proto 'static'
	option netmask '255.255.255.0'
	option ip6assign '60'
	option ipaddr '172.16.0.1'
cat dhcp 

config dnsmasq
	option domainneeded '1'
	option boguspriv '1'
	option filterwin2k '0'
	option localise_queries '1'
	option rebind_protection '1'
	option rebind_localhost '1'
	option local '/lan/'
	option domain 'lan'
	option expandhosts '1'
	option nonegcache '0'
	option authoritative '1'
	option readethers '1'
	option leasefile '/tmp/dhcp.leases'
	option resolvfile '/tmp/resolv.conf.auto'
	option localservice '1'

config dhcp 'lan'
	option interface 'lan'
	option start '100'
	option limit '150'
	option leasetime '12h'
	option dhcpv6 'server'
	option ra 'server'
	option ra_management '1'

config dhcp 'wan'
	option interface 'wan'
	option ignore '1'

config odhcpd 'odhcpd'
	option maindhcp '0'
	option leasefile '/tmp/hosts/odhcpd'
	option leasetrigger '/usr/sbin/odhcpd-update'

root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# cat luci 

config core 'main'
	option lang 'auto'
	option mediaurlbase '/luci-static/bootstrap'
	option resourcebase '/luci-static/resources'

config extern 'flash_keep'
	option uci '/etc/config/'
	option dropbear '/etc/dropbear/'
	option openvpn '/etc/openvpn/'
	option passwd '/etc/passwd'
	option opkg '/etc/opkg.conf'
	option firewall '/etc/firewall.user'
	option uploads '/lib/uci/upload/'

config internal 'languages'

config internal 'sauth'
	option sessionpath '/tmp/luci-sessions'
	option sessiontime '3600'

config internal 'ccache'
	option enable '1'

config internal 'themes'
	option Bootstrap '/luci-static/bootstrap'

root@OpenWrt:/etc/config# cat system

config system
	option hostname 'OpenWrt'
	option timezone 'UTC'

config timeserver 'ntp'
	list server '0.openwrt.pool.ntp.org'
	list server '1.openwrt.pool.ntp.org'
	list server '2.openwrt.pool.ntp.org'
	list server '3.openwrt.pool.ntp.org'
	option enabled '1'
	option enable_server '0'

config led 'led_usb'
	option name 'USB'
	option sysfs 'tp-link:green:3g'
	option trigger 'usbdev'
	option dev '1-1'
	option interval '50'

config led 'led_wlan'
	option name 'WLAN'
	option sysfs 'tp-link:green:wlan'
	option trigger 'phy0tpt'

config led 'led_lan'
	option name 'LAN'
	option sysfs 'tp-link:green:lan'
	option trigger 'netdev'
	option dev 'eth0'
	option mode 'link tx rx'

root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# 
root@OpenWrt:/etc/config# cat wireless 

config wifi-device 'radio0'
	option type 'mac80211'
	option channel '11'
	option hwmode '11g'
	option path 'platform/ar933x_wmac'
	option htmode 'HT20'
	option txpower '15'
	option country 'US'

config wifi-iface
	option device 'radio0'
	option network 'lan'
	option mode 'ap'
	option ssid '60t'
	option encryption 'psk2'
	option key 'Password123'

-----

br-lan    Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          inet addr:172.16.0.1  Bcast:172.16.0.255  Mask:255.255.255.0
          inet6 addr: fe80::9ade:d0ff:fef7:1fa0/64 Scope:Link
          inet6 addr: fd44:5d40:3d62::1/60 Scope:Global
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11729 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9681 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:1474774 (1.4 MiB)  TX bytes:1975841 (1.8 MiB)

eth0      Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:11544 errors:0 dropped:0 overruns:0 frame:0
          TX packets:9542 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:1615752 (1.5 MiB)  TX bytes:1969128 (1.8 MiB)
          Interrupt:4 

lo        Link encap:Local Loopback  
          inet addr:127.0.0.1  Mask:255.0.0.0
          inet6 addr: ::1/128 Scope:Host
          UP LOOPBACK RUNNING  MTU:65536  Metric:1
          RX packets:8035 errors:0 dropped:0 overruns:0 frame:0
          TX packets:8035 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:0 
          RX bytes:546385 (533.5 KiB)  TX bytes:546385 (533.5 KiB)

wlan0     Link encap:Ethernet  HWaddr 98:DE:D0:F7:1F:A0  
          UP BROADCAST MULTICAST  MTU:1500  Metric:1
          RX packets:0 errors:0 dropped:0 overruns:0 frame:0
          TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)


```


Wi-Fi
-----

```bash
http://www.linuxjournal.com/content/wi-fi-command-line
https://pentestmag.com/wifi-scanning-tools-on-ubuntu-14-04/
# network manager ...
cat >> /etc/NetworkManager/NetworkManager.conf<<EOF
[keyfile]
unmanaged-devices=mac:30:b5:c2:16:d5:4a
EOF
##################################################
ifconfig wlan0 up
##################################################
iwlist wlan0 scan
##################################################
iwconfig
##################################################
iwconfig wlan0 channel 6
##################################################
iwpriv wlan0
##################################################
nmcli -t -f RUNNING nm
##################################################
nmcli dev status
##################################################
nmcli dev wifi
##################################################
nmcli  dev list iface wlan0
##################################################
iwconfig wlan1 essid smthg
iw wlan1 link
cat  /etc/wpa_supplicant.conf
man wpa_supplicant
wpa_passphrase smthg > wpa_supplicant.conf
man wpa_supplicant
wpa_supplicant -c./wpa_supplicant.conf -iwlan1 -d

###############
modinfo r8188eu
###############
/sbin/wpa_supplicant -B -P /run/sendsigs.omit.d/wpasupplicant.pid -u -s -O /var/run/wpa_supplicant
```
```
modinfo usbserial
```

Misc
----


```
./base-files/files/sbin/sysupgrade:  Flash image even if image checks fail, this is dangerous!

./luci/modules/luci-mod-admin-full/luasrc/view/admin_system/flashops.htm:
<legend><%:Flash new firmware image%></legend>

./target/linux/ar71xx/base-files/lib/upgrade/platform.sh

```


lua
---

```
local j = require("luci.jsonc")
j.stringify({XXX=123, YYY=4674})
```
- [cgi on lua](http://www.gammon.com.au/forum/?id=6498)

