# Load the module
modprobe mac80211_hwsim
# Run hostapd (AP) for wlan0
hostapd hostapd.conf
# Run wpa_supplicant (station) for wlan1
wpa_supplicant -Dnl80211 -iwlan1 -c wpa_supplicant.conf

git://w1.fi/srv/git/hostap.git and mac80211_hwsim/tests subdirectory
[gitweb](http://w1.fi/gitweb/gitweb.cgi?p=hostap.git;a=tree;f=mac80211_hwsim/tests)

[on nl80211](https://stackoverflow.com/questions/21456235/how-nl80211-library-cfg80211-work)

```
# hostapd.conf
interface=wlan0
driver=nl80211
hw_mode=g
channel=1
ssid=mac80211 test
wpa=2
wpa_key_mgmt=WPA-PSK
wpa_pairwise=CCMP
wpa_passphrase=12345678

#wpa_supplicant.conf
ctrl_interface=/var/run/wpa_supplicant
network={
	ssid="mac80211 test"
	psk="12345678"
	key_mgmt=WPA-PSK
	proto=WPA2
	pairwise=CCMP
	group=CCMP
}
```


```
Using interface wlan0 with hwaddr 02:00:00:00:00:00 and ssid "mac80211 test"
wlan0: interface state UNINITIALIZED->ENABLED
wlan0: AP-ENABLED 
wlan0: STA 02:00:00:00:01:00 IEEE 802.11: authenticated
wlan0: STA 02:00:00:00:01:00 IEEE 802.11: associated (aid 1)
wlan0: AP-STA-CONNECTED 02:00:00:00:01:00
wlan0: STA 02:00:00:00:01:00 RADIUS: starting accounting session 5A2E5016-00000000
wlan0: STA 02:00:00:00:01:00 WPA: pairwise key handshake completed (RSN)
wlan0: AP-STA-DISCONNECTED 02:00:00:00:01:00


wpa_supplicant -Dnl80211 -iwlan1 -c ~qa/src/linux/Documentation/networking/mac80211_hwsim/wpa_supplicant.conf 
Successfully initialized wpa_supplicant
Could not read interface p2p-dev-wlan1 flags: No such device
wlan1: SME: Trying to authenticate with 02:00:00:00:00:00 (SSID='mac80211 test' freq=2412 MHz)
wlan1: Trying to associate with 02:00:00:00:00:00 (SSID='mac80211 test' freq=2412 MHz)
wlan1: Associated with 02:00:00:00:00:00
wlan1: WPA: Key negotiation completed with 02:00:00:00:00:00 [PTK=CCMP GTK=CCMP]
wlan1: CTRL-EVENT-CONNECTED - Connection to 02:00:00:00:00:00 completed [id=0 id_str=]
```


