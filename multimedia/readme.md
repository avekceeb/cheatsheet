
[examples](https://trac.ffmpeg.org/wiki/Map)

## copy video stream #0
## aac code audio #2 
	
	ffmpeg -i in.mkv -map 0:0 -map 0:2 -c:v copy -c:a aac out.mkv

# -sn -without subtitles:
	
	ffmpeg -i 1.mkv -sn -vcodec copy -acodec libvorbis  -map 0.0:v:0 -map 0.2:a:1  out.mkv
	ffmpeg -i in.mkv -sn -vcodec mpeg4 -acodec libvorbis -map 0.0:v:0 -map 0.1:a:1 s01e01.mkv

	# ffmpeg -i 1.mkv -vcodec copy -acodec libvorbis -scodec copy -map 0.0:v:0 -map 0.1:a:1 -map 0.5:s:2 3.mkv

## для андроида:

	ffmpeg -i in.avi -map 0:0 -map 0:2 -c:v h264 -c:a aac out.mp4

### copy part of video

	ffmpeg -ss 00:12:08 -t 00:01:17 -i int.avi -vcodec copy -acodec copy chacha.avi

#### img <--> video

	ffmpeg -r 10 -b 1800 -i frame-%d.jpeg test1800.mp4

	ffmpeg -i Chroma.avi -vframes 100 -ss 00:13:06 -f image2 bbb-%03d.jpeg

## avconv
	
	# DE h264            raw H.264 video
	# DE ogg             Ogg
	# video 0 -> 0
	# audio 2 -> 1
	avconv -i 1.avi -c:v libx264 -c:a copy -map 0:v:0 -map 0:a:1 01.mp4


## mkvtools

	apt install mkvtoolnix
	mkvmerge -i 'film.mkv'
	mkvextract tracks Un.illustre.inconnu.2014.BDRip.1080p-ylnian.mkv 4:Un.illustre.inconnu.srt


## id3v2
```
	for x in *.mp3 ; do
		id3v2 --delete-v1 ${x}
	done

	z=1
	for x in *.mp3 ; do
		id3v2 --album "Amphibia" \
		--song ${x%.mp3} \
		--track $z \
		--artist "Belyaev" \
		$x
		z=$(($z + 1))
	done
```
## Settings:

### unmute all channels:
    `amixer sset Master unmute`

    `${yum} install alsa-utils ; alsamixer`

### check sound
    `speaker-test -c 2 ; speaker-test -c 8 ; aplay -L | grep :CARD` 

```bash
f_get_random_positions() {
    local count=${3:-10}
    local from=$1
    local to=$2
    python -c 'import random ; print "\n".join([ "+%d+%d"%(random.randint('$from','$to'),random.randint('$from','$to')) for x in range('$count') ]) '
}

###################################
f_get_random_w_xy() {
    local count=${5:-10}
    local from=$1
    local to=$2
    local minw=$3
    local maxw=$4
    python -c 'import random ; print " ".join([ "skull-%d.png -geometry %dx+%d+%d -composite"%(random.randint(1,5),random.randint('$minw','$maxw'),random.randint('$from','$to'),random.randint('$from','$to')) for x in range('$count') ]) '
}

convert -size 600x600 xc:none $(f_get_random_w_xy 10 540 20 60 300) new.png
##################################

f_get_random_w_xy() {
    local count=${5:-10}
    local from=$1
    local to=$2
    local minw=$3
    local maxw=$4
    python -c 'import random ; print "\n".join([ "%dx+%d+%d"%(random.randint('$minw','$maxw'),random.randint('$from','$to'),random.randint('$from','$to')) for x in range('$count') ]) '
}

convert -size 600x600 xc:none \
smile.png -geometry 60x60+10+10 -composite \
smile.png -geometry 40x40+10+10 -composite new.png


convert -size 600x600 xc:none \
$(for z in $(f_get_random_positions 40 500 100) ; do echo -n "smile.png -geometry $z -composite "; done) new.png
```
--------------------

## split image to 2 (vert or horiz) ##

```
convert me.jpg -crop 2x1+0+0@  +repage  +adjoin  horiz%d.jpg

convert me.jpg -crop 1x2+0+0@  +repage  +adjoin  vert%d.jpg
```

### making pdf from 2 page on the list scan

# erase lower right stripes
```bash
for x in *.jpg ; do
	convert $x  -crop 2365x3290+0+0  +repage ./book1/$x
done
```

# rotate all images
```bash
for x in *.jpg ; do
	convert $x -rotate "90" $x
done 
```

# resize all images
```bash
for x in *.jpg ; do
	convert $x -resize 40% $x
done
```

# split each image vertically into two
```bash
for x in *.jpg ; do
	convert $x -crop 2x1+0+0@  +repage  +adjoin  ../book2/${x%.jpg}%d.jpg
done
```

# make pdf
```bash
cd ../book2
convert *.jpg -quality 100 chemistry.pdf
```

# mp4 to gif
[howto](https://askubuntu.com/questions/648603/create-animated-gif-from-mp4-video-via-command-line)

```bash
mkdir frames
ffmpeg -i cast.mp4  -r 5 'frames/frame-%03d.jpg'
cd frames/
convert -delay 20 -loop 0 *.jpg cast.gif

# or smaller:

ffmpeg -i cast.mp4 -vf scale=540:-1 -r 10 -f image2pipe -vcodec ppm - |\
    convert -delay 5 -loop 0 - output2.gif

```

## GStreamer Python
- [how to habr](https://habr.com/post/179167/)
- [PyGst tutorial](https://brettviren.github.io/pygst-tutorial-org/pygst-tutorial.pdf)

