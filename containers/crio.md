
/usr/bin/kubelet \
	--bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf \
	--kubeconfig=/etc/kubernetes/kubelet.conf \
	--pod-manifest-path=/etc/kubernetes/manifests \
	--allow-privileged=true \
	--network-plugin=cni \
	--cni-conf-dir=/etc/cni/net.d \
	--cni-bin-dir=/opt/cni/bin \
	--cluster-dns=10.96.0.10 \
	--cluster-domain=cluster.local \
	--authorization-mode=Webhook \
	--client-ca-file=/etc/kubernetes/pki/ca.crt \
	--cgroup-driver=cgroupfs \
	--cadvisor-port=0 \
	--rotate-certificates=true \
	--pod-infra-container-image=ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.0 \
	--fail-swap-on=false \
	--container-runtime-endpoint=unix:///var/run/crio/crio.sock \
	--runtime-request-timeout=10m



## before configured

[yum.repos.d]# ss crio
● crio.service - Open Container Initiative Daemon
   Loaded: loaded (/usr/lib/systemd/system/crio.service; disabled; vendor preset: disabled)
  Drop-In: /etc/systemd/system/crio.service.d
           └─http-proxy.conf
   Active: inactive (dead)
     Docs: https://github.com/kubernetes-incubator/cri-o

Sep 12 13:20:07 ca-ostest452.us.oracle.com systemd[1]: Stopped Open Container Initiative Daemon.
Sep 12 13:22:24 ca-ostest452.us.oracle.com systemd[1]: Starting Open Container Initiative Daemon...
Sep 12 13:22:24 ca-ostest452.us.oracle.com crio[3855]: time="2018-09-12 13:22:24.868538666-07:00" level=warning msg="hooks path: "/usr/share/containers/oci/hooks.d" does not exist"
Sep 12 13:22:24 ca-ostest452.us.oracle.com crio[3855]: time="2018-09-12 13:22:24.868630951-07:00" level=warning msg="hooks path: "/etc/containers/oci/hooks.d" does not exist"
Sep 12 13:22:24 ca-ostest452.us.oracle.com crio[3855]: time="2018-09-12 13:22:24.868907666-07:00" level=info msg="CNI network crio-bridge (type=bridge) is used from /etc/cni/net.d/100-crio-bridge.conf"
Sep 12 13:22:24 ca-ostest452.us.oracle.com crio[3855]: time="2018-09-12 13:22:24.869060258-07:00" level=info msg="CNI network crio-bridge (type=bridge) is used from /etc/cni/net.d/100-crio-bridge.conf"
Sep 12 13:22:24 ca-ostest452.us.oracle.com systemd[1]: Started Open Container Initiative Daemon.
Sep 13 02:26:21 ca-ostest452.us.oracle.com systemd[1]: Stopping Open Container Initiative Daemon...
Sep 13 02:26:21 ca-ostest452.us.oracle.com crio[3855]: time="2018-09-13 02:26:21.369252660-07:00" level=error msg="Failed to start streaming server: http: Server closed"


## docker images after 'pure' kube

ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-sidecar-amd64           1.14.8              678a9b3e45a0        5 months ago        155MB
ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-kube-dns-amd64          1.14.8              886d7f9b2edc        5 months ago        163MB
ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-dnsmasq-nanny-amd64     1.14.8              9cd7a5c905e9        5 months ago        154MB
ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64                     3.1                 198adec58f2a        5 months ago        742kB
ca-docker-registry.us.oracle.com:5000/kubernetes/kubernetes-dashboard-amd64      v1.7.0              f3121bdb4b3c        9 months ago        128MB
ca-docker-registry.us.oracle.com:5000/kubernetes/flannel                         v0.9.0-amd64        b9073e7ec52a        10 months ago       269MB


## docker cnt pure kube:
CONTAINER ID        IMAGE                                                                            COMMAND                  CREATED             STATUS              PORTS               NAMES
b3d41f60194d        ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-sidecar-amd64           "/sidecar --v=2 --lo…"   31 minutes ago      Up 31 minutes                           k8s_sidecar_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_0
d41cf033992b        ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-dnsmasq-nanny-amd64     "/dnsmasq-nanny -v=2…"   31 minutes ago      Up 31 minutes                           k8s_dnsmasq_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_0
c1aec4ed7aae        ca-docker-registry.us.oracle.com:5000/kubernetes/kubernetes-dashboard-amd64      "/dashboard --insecu…"   31 minutes ago      Up 31 minutes                           k8s_kubernetes-dashboard_kubernetes-dashboard-84cffcc95c-8h76f_kube-system_532b3a8e-b74c-11e8-a379-0010e0bafa00_0
836bbfb0dac6        ca-docker-registry.us.oracle.com:5000/kubernetes/k8s-dns-kube-dns-amd64          "/kube-dns --domain=…"   31 minutes ago      Up 31 minutes                           k8s_kubedns_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_0
e417af132ca4        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 31 minutes ago      Up 31 minutes                           k8s_POD_kubernetes-dashboard-84cffcc95c-8h76f_kube-system_532b3a8e-b74c-11e8-a379-0010e0bafa00_0
22454eb8ecec        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 32 minutes ago      Up 32 minutes                           k8s_POD_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_0
dfc81b8d5f8c        b9073e7ec52a                                                                     "/opt/bin/flanneld -…"   32 minutes ago      Up 32 minutes                           k8s_kube-flannel_kube-flannel-ds-ppgsn_kube-system_52f40c33-b74c-11e8-a379-0010e0bafa00_0
aefd5e699693        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 32 minutes ago      Up 32 minutes                           k8s_POD_kube-flannel-ds-ppgsn_kube-system_52f40c33-b74c-11e8-a379-0010e0bafa00_0
2bb2b7e2d82b        cfc6870b0cac                                                                     "/usr/local/bin/kube…"   32 minutes ago      Up 32 minutes                           k8s_kube-proxy_kube-proxy-wgkjl_kube-system_515cc156-b74c-11e8-a379-0010e0bafa00_0
0ae4ed4fe171        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 32 minutes ago      Up 32 minutes                           k8s_POD_kube-proxy-wgkjl_kube-system_515cc156-b74c-11e8-a379-0010e0bafa00_0
40b7c8a0a7f7        ca-docker-registry.us.oracle.com:5000/kubernetes/kube-controller-manager-amd64   "kube-controller-man…"   32 minutes ago      Up 32 minutes                           k8s_kube-controller-manager_kube-controller-manager-bmx033-ps.mgmtnet_kube-system_8c6218ffdbded3ac8e16de31bf597552_0
4421f36ab4f2        ca-docker-registry.us.oracle.com:5000/kubernetes/etcd-amd64                      "etcd --client-cert-…"   32 minutes ago      Up 32 minutes                           k8s_etcd_etcd-bmx033-ps.mgmtnet_kube-system_5743648a151df95b96662b1b28d1483f_0
e396506e4f9b        ca-docker-registry.us.oracle.com:5000/kubernetes/kube-apiserver-amd64            "kube-apiserver --re…"   32 minutes ago      Up 32 minutes                           k8s_kube-apiserver_kube-apiserver-bmx033-ps.mgmtnet_kube-system_a3153434e14a3e98da5656a2ea37bca1_0
966f525ea031        ca-docker-registry.us.oracle.com:5000/kubernetes/kube-scheduler-amd64            "kube-scheduler --ad…"   33 minutes ago      Up 33 minutes                           k8s_kube-scheduler_kube-scheduler-bmx033-ps.mgmtnet_kube-system_9f159626be58b46cde4338feb98be458_0
56479401cbde        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 33 minutes ago      Up 33 minutes                           k8s_POD_kube-controller-manager-bmx033-ps.mgmtnet_kube-system_8c6218ffdbded3ac8e16de31bf597552_0
327a53a2d781        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 33 minutes ago      Up 33 minutes                           k8s_POD_kube-apiserver-bmx033-ps.mgmtnet_kube-system_a3153434e14a3e98da5656a2ea37bca1_0
770b3463b9a7        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 33 minutes ago      Up 33 minutes                           k8s_POD_etcd-bmx033-ps.mgmtnet_kube-system_5743648a151df95b96662b1b28d1483f_0
31e470a13232        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1                 "/pause"                 33 minutes ago      Up 33 minutes                           k8s_POD_kube-scheduler-bmx033-ps.mgmtnet_kube-system_9f159626be58b46cde4338feb98be458_0


# docker on pure kubel client
[root@bmx004-ps pods]# docker ps
CONTAINER ID        IMAGE                                                              COMMAND                  CREATED             STATUS              PORTS                    NAMES
4dc4c55e472a        b9073e7ec52a                                                       "/opt/bin/flanneld -…"   4 hours ago         Up 4 hours                                   k8s_kube-flannel_kube-flannel-ds-z8fw9_kube-system_9dc4c1ea-b73e-11e8-8c00-0021f600043a_0
39c4d4166a50        cfc6870b0cac                                                       "/usr/local/bin/kube…"   4 hours ago         Up 4 hours                                   k8s_kube-proxy_kube-proxy-j8ld5_kube-system_9dc4f709-b73e-11e8-8c00-0021f600043a_0
ede2ea80cedc        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 4 hours ago         Up 4 hours                                   k8s_POD_kube-proxy-j8ld5_kube-system_9dc4f709-b73e-11e8-8c00-0021f600043a_0
fb8cb4ea6bd0        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 4 hours ago         Up 4 hours                                   k8s_POD_kube-flannel-ds-z8fw9_kube-system_9dc4c1ea-b73e-11e8-8c00-0021f600043a_0
51040855dee7        registry:2                                                         "/entrypoint.sh /etc…"   4 hours ago         Up 4 hours          0.0.0.0:5000->5000/tcp   registry
 


######
[root@sst-x6250-05 ~]# 
[root@sst-x6250-05 ~]# 
[root@sst-x6250-05 ~]# kubectl -v 100 get nodes
I0913 11:26:51.837927   13594 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" http://localhost:8080/api
I0913 11:26:51.840479   13594 round_trippers.go:405] GET http://localhost:8080/api  in 2 milliseconds
I0913 11:26:51.840555   13594 round_trippers.go:411] Response Headers:
I0913 11:26:51.840648   13594 cached_discovery.go:124] skipped caching discovery info due to Get http://localhost:8080/api: dial tcp [::1]:8080: getsockopt: connection refused
I0913 11:26:51.840804   13594 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" http://localhost:8080/api
I0913 11:26:51.841663   13594 round_trippers.go:405] GET http://localhost:8080/api  in 0 milliseconds
I0913 11:26:51.841700   13594 round_trippers.go:411] Response Headers:
I0913 11:26:51.841764   13594 cached_discovery.go:124] skipped caching discovery info due to Get http://localhost:8080/api: dial tcp [::1]:8080: getsockopt: connection refused
I0913 11:26:51.842178   13594 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" http://localhost:8080/api
I0913 11:26:51.842943   13594 round_trippers.go:405] GET http://localhost:8080/api  in 0 milliseconds
I0913 11:26:51.842964   13594 round_trippers.go:411] Response Headers:
I0913 11:26:51.842996   13594 cached_discovery.go:124] skipped caching discovery info due to Get http://localhost:8080/api: dial tcp [::1]:8080: getsockopt: connection refused
I0913 11:26:51.843019   13594 factory_object_mapping.go:93] Unable to retrieve API resources, falling back to hardcoded types: Get http://localhost:8080/api: dial tcp [::1]:8080: getsockopt: connection refused
I0913 11:26:51.843613   13594 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" http://localhost:8080/api/v1/nodes?limit=500
I0913 11:26:51.844179   13594 round_trippers.go:405] GET http://localhost:8080/api/v1/nodes?limit=500  in 0 milliseconds
I0913 11:26:51.844196   13594 round_trippers.go:411] Response Headers:
I0913 11:26:51.844263   13594 helpers.go:219] Connection error: Get http://localhost:8080/api/v1/nodes?limit=500: dial tcp [::1]:8080: getsockopt: connection refused
F0913 11:26:51.844331   13594 helpers.go:119] The connection to the server localhost:8080 was refused - did you specify the right host or port?

##########
[crio]# k -v 100 get nodes
I0913 04:25:45.663522   30094 loader.go:357] Config loaded from file /etc/kubernetes/admin.conf
I0913 04:25:45.665088   30094 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" https://10.147.84.59:6443/api
I0913 04:25:45.701953   30094 round_trippers.go:405] GET https://10.147.84.59:6443/api  in 36 milliseconds
I0913 04:25:45.702007   30094 round_trippers.go:411] Response Headers:
I0913 04:25:45.702085   30094 cached_discovery.go:124] skipped caching discovery info due to Get https://10.147.84.59:6443/api: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs
I0913 04:25:45.702219   30094 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" https://10.147.84.59:6443/api
I0913 04:25:45.740026   30094 round_trippers.go:405] GET https://10.147.84.59:6443/api  in 37 milliseconds
I0913 04:25:45.740077   30094 round_trippers.go:411] Response Headers:
I0913 04:25:45.740139   30094 cached_discovery.go:124] skipped caching discovery info due to Get https://10.147.84.59:6443/api: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs
I0913 04:25:45.740701   30094 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json, */*" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" https://10.147.84.59:6443/api
I0913 04:25:45.777471   30094 round_trippers.go:405] GET https://10.147.84.59:6443/api  in 36 milliseconds
I0913 04:25:45.777531   30094 round_trippers.go:411] Response Headers:
I0913 04:25:45.777631   30094 cached_discovery.go:124] skipped caching discovery info due to Get https://10.147.84.59:6443/api: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs
I0913 04:25:45.777720   30094 factory_object_mapping.go:93] Unable to retrieve API resources, falling back to hardcoded types: Get https://10.147.84.59:6443/api: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs
I0913 04:25:45.778691   30094 round_trippers.go:386] curl -k -v -XGET  -H "Accept: application/json" -H "User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb" https://10.147.84.59:6443/api/v1/nodes?limit=500
I0913 04:25:45.815351   30094 round_trippers.go:405] GET https://10.147.84.59:6443/api/v1/nodes?limit=500  in 36 milliseconds
I0913 04:25:45.815416   30094 round_trippers.go:411] Response Headers:
I0913 04:25:45.815517   30094 helpers.go:219] Connection error: Get https://10.147.84.59:6443/api/v1/nodes?limit=500: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs
F0913 04:25:45.815567   30094 helpers.go:119] Unable to connect to the server: x509: cannot validate certificate for 10.147.84.59 because it doesn't contain any IP SANs



[yum.repos.d]# k -v 8 get nodes
I0913 05:09:45.401054   11135 loader.go:357] Config loaded from file /root/.kube/config
I0913 05:09:45.407684   11135 round_trippers.go:383] GET https://192.168.1.74:6443/api/v1/nodes?limit=500
I0913 05:09:45.407694   11135 round_trippers.go:390] Request Headers:
I0913 05:09:45.407699   11135 round_trippers.go:393]     Accept: application/json
I0913 05:09:45.407703   11135 round_trippers.go:393]     User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb
I0913 05:09:45.418041   11135 round_trippers.go:408] Response Status: 200 OK in 10 milliseconds
I0913 05:09:45.418059   11135 round_trippers.go:411] Response Headers:
I0913 05:09:45.418081   11135 round_trippers.go:414]     Content-Type: application/json
I0913 05:09:45.418087   11135 round_trippers.go:414]     Date: Thu, 13 Sep 2018 12:09:45 GMT
I0913 05:09:45.418283   11135 request.go:874] Response Body: {"kind":"NodeList","apiVersion":"v1","metadata":{"selfLink":"/api/v1/nodes","resourceVersion":"1204"},"items":[{"metadata":{"name":"bmx033-ps.mgmtnet","selfLink":"/api/v1/nodes/bmx033-ps.mgmtnet","uid":"46241a09-b74c-11e8-a379-0010e0bafa00","resourceVersion":"1195","creationTimestamp":"2018-09-13T11:58:05Z","labels":{"beta.kubernetes.io/arch":"amd64","beta.kubernetes.io/os":"linux","kubernetes.io/hostname":"bmx033-ps.mgmtnet","node-role.kubernetes.io/master":""},"annotations":{"flannel.alpha.coreos.com/backend-data":"{\"VtepMAC\":\"de:72:63:5d:2b:17\"}","flannel.alpha.coreos.com/backend-type":"vxlan","flannel.alpha.coreos.com/kube-subnet-manager":"true","flannel.alpha.coreos.com/public-ip":"192.168.1.74","node.alpha.kubernetes.io/ttl":"0","volumes.kubernetes.io/controller-managed-attach-detach":"true"}},"spec":{"podCIDR":"10.244.0.0/24","externalID":"bmx033-ps.mgmtnet","taints":[{"key":"node-role.kubernetes.io/master","effect":"NoSchedule"}]},"status":{"capacity":{"cpu":"40","ephemeral-storage":"32752Mi","hug [truncated 5860 chars]
I0913 05:09:45.420006   11135 round_trippers.go:383] GET https://192.168.1.74:6443/openapi/v2
I0913 05:09:45.420031   11135 round_trippers.go:390] Request Headers:
I0913 05:09:45.420039   11135 round_trippers.go:393]     Accept: application/com.github.proto-openapi.spec.v2@v1.0+protobuf
I0913 05:09:45.420047   11135 round_trippers.go:393]     User-Agent: kubectl/v1.10.5+2.0.2.el7 (linux/amd64) kubernetes/03e76fb
I0913 05:09:45.426040   11135 round_trippers.go:408] Response Status: 200 OK in 5 milliseconds
I0913 05:09:45.426049   11135 round_trippers.go:411] Response Headers:
I0913 05:09:45.426054   11135 round_trippers.go:414]     Vary: Accept-Encoding
I0913 05:09:45.426058   11135 round_trippers.go:414]     Vary: Accept
I0913 05:09:45.426062   11135 round_trippers.go:414]     X-Varied-Accept: application/com.github.proto-openapi.spec.v2@v1.0+protobuf
I0913 05:09:45.426068   11135 round_trippers.go:414]     Content-Type: application/octet-stream
I0913 05:09:45.426072   11135 round_trippers.go:414]     Etag: "4FD5391784852FB7553E6356798592468A216E6A093A04544067211665407E84FC6CD4EB832430B6E8C7C5B7B6F27A368E0BEA14B30BC39DD458732BFF562A2D"
I0913 05:09:45.426078   11135 round_trippers.go:414]     Date: Thu, 13 Sep 2018 12:09:45 GMT
I0913 05:09:45.426082   11135 round_trippers.go:414]     Last-Modified: Thu, 13 Sep 2018 11:58:05 GMT
I0913 05:09:45.426087   11135 round_trippers.go:414]     X-From-Cache: 1
I0913 05:09:45.426091   11135 round_trippers.go:414]     Accept-Ranges: bytes
I0913 05:09:45.507630   11135 request.go:872] Response Body:
00000000  0a 03 32 2e 30 12 1f 0a  0a 4b 75 62 65 72 6e 65  |..2.0....Kuberne|
00000010  74 65 73 12 11 76 31 2e  31 30 2e 35 2b 32 2e 30  |tes..v1.10.5+2.0|
00000020  2e 32 2e 65 6c 37 42 a2  a5 66 12 ca 02 0a 05 2f  |.2.el7B..f...../|
00000030  61 70 69 2f 12 c0 02 12  bd 02 0a 04 63 6f 72 65  |api/........core|
00000040  1a 1a 67 65 74 20 61 76  61 69 6c 61 62 6c 65 20  |..get available |
00000050  41 50 49 20 76 65 72 73  69 6f 6e 73 2a 12 67 65  |API versions*.ge|
00000060  74 43 6f 72 65 41 50 49  56 65 72 73 69 6f 6e 73  |tCoreAPIVersions|
00000070  32 10 61 70 70 6c 69 63  61 74 69 6f 6e 2f 6a 73  |2.application/js|
00000080  6f 6e 32 10 61 70 70 6c  69 63 61 74 69 6f 6e 2f  |on2.application/|
00000090  79 61 6d 6c 32 23 61 70  70 6c 69 63 61 74 69 6f  |yaml2#applicatio|
000000a0  6e 2f 76 6e 64 2e 6b 75  62 65 72 6e 65 74 65 73  |n/vnd.kubernetes|
000000b0  2e 70 72 6f 74 6f 62 75  66 3a 10 61 70 70 6c 69  |.protobuf:.appli|
000000c0  63 61 74 69 6f 6e 2f 6a  73 6f 6e 3a 10 61 70 70  |cation/json:.ap [truncated 11209306 chars]
NAME                STATUS    ROLES     AGE       VERSION
bmx033-ps.mgmtnet   Ready     master    11m       v1.10.5+2.0.2.el7
[yum.repos.d]# 


### in oltf
[yum.repos.d]# echo $http_proxy
http://192.168.1.1:3128
[yum.repos.d]# echo $https_proxy
https://192.168.1.1:3128


[tests]# ps -ef | grep kata
root      2974     1  1 00:51 ?        00:06:33 /usr/bin/dockerd -D --add-runtime kata-runtime=/usr/bin/kata-runtime
root      6240 26473  0 06:38 ttyS0    00:00:00 grep --color=auto kata
root     30691     1  0 06:02 ?        00:00:00 /usr/libexec/crio/conmon -c 3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8 -u 3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8 -r /usr/bin/kata-runtime -b /var/run/containers/storage/btrfs-containers/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/userdata -p /var/run/containers/storage/btrfs-containers/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/userdata/pidfile -l /var/log/pods/38a92d41-b755-11e8-9750-0010e0bafa00/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     30706 30691  0 06:02 ?        00:00:02 /usr/bin/qemu-system-x86_64 -name sandbox-3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8 -uuid 0a31f8d9-c90b-4a69-863f-2c7cbc84c360 -machine pc,accel=kvm,kernel_irqchip,nvdimm -cpu host -qmp unix:/run/vc/vm/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/qmp.sock,server,nowait -m 7168M,slots=2,maxmem=258564M -device pci-bridge,bus=pci.0,id=pci-bridge-0,chassis_nr=1,shpc=on,addr=2 -device virtio-serial-pci,id=serial0 -device virtconsole,chardev=charconsole0,id=console0 -chardev socket,id=charconsole0,path=/run/vc/vm/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/console.sock,server,nowait -device nvdimm,id=nv0,memdev=mem0 -object memory-backend-file,id=mem0,mem-path=/usr/share/kata-containers/kata-containers-1.2.0.img,size=536870912 -device virtio-scsi-pci,id=scsi0 -device virtserialport,chardev=charch0,id=channel0,name=agent.channel.0 -chardev socket,id=charch0,path=/run/vc/vm/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/kata.sock,server,nowait -device virtio-9p-pci,fsdev=extra-9p-kataShared,mount_tag=kataShared -fsdev local,id=extra-9p-kataShared,path=/run/kata-containers/shared/sandboxes/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8,security_model=none -rtc base=utc,driftfix=slew -global kvm-pit.lost_tick_policy=discard -vga none -no-user-config -nodefaults -nographic -daemonize -kernel /usr/share/kata-containers/vmlinuz-4.14.35-1818.2.0.el7.container -append tsc=reliable no_timer_check rcupdate.rcu_expedited=1 i8042.direct=1 i8042.dumbkbd=1 i8042.nopnp=1 i8042.noaux=1 noreplace-smp reboot=k console=hvc0 console=hvc1 iommu=off cryptomgr.notests net.ifnames=0 pci=lastbus=0 root=/dev/pmem0p1 rootflags=dax,data=ordered,errors=remount-ro rw rootfstype=ext4 quiet systemd.show_status=false panic=1 nr_cpus=40 init=/usr/lib/systemd/systemd systemd.unit=kata-containers.target systemd.mask=systemd-networkd.service systemd.mask=systemd-networkd.socket -smp 1,cores=1,threads=1,sockets=1,maxcpus=40
root     30715 30691  0 06:02 ?        00:00:00 /usr/libexec/kata-containers/kata-proxy -listen-socket unix:///run/vc/sbs/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/proxy.sock -mux-socket /run/vc/vm/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/kata.sock -sandbox 3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8
root     30788 30691  0 06:02 ?        00:00:00 /usr/libexec/kata-containers/kata-shim -agent unix:///run/vc/sbs/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/proxy.sock -container 3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8 -exec-id 3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8
root     31377     1  0 06:02 ?        00:00:00 /usr/libexec/crio/conmon -c 3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab -u 3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab -r /usr/bin/kata-runtime -b /var/run/containers/storage/btrfs-containers/3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab/userdata -p /var/run/containers/storage/btrfs-containers/3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab/userdata/pidfile -l /var/log/pods/38a92d41-b755-11e8-9750-0010e0bafa00/xxxxnginx-kata-box/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     31391 31377  0 06:02 ?        00:00:00 /usr/libexec/kata-containers/kata-shim -agent unix:///run/vc/sbs/3fff31e696ff103a4458902014b7c8bc22d22f63d5a9debe627f80cf6bba2bd8/proxy.sock -container 3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab -exec-id 3b0d44d3e520e84aaa613e6114ef5301ffb2b9edfcd5c2ab3299d364500f9cab
[tests]# 


[crio]# ps -ef | grep kube
root      5328     1  0 04:58 ?        00:00:01 /usr/bin/kubectl --kubeconfig=/etc/kubernetes/admin.conf proxy --port 8001
root     11579 26473  0 06:52 ttyS0    00:00:00 grep --color=auto kube
root     19254 19238  2 05:41 ?        00:01:36 kube-apiserver --requestheader-client-ca-file=/etc/kubernetes/pki/front-proxy-ca.crt --kubelet-preferred-address-types=InternalIP,ExternalIP,Hostname --requestheader-username-headers=X-Remote-User --service-account-key-file=/etc/kubernetes/pki/sa.pub --client-ca-file=/etc/kubernetes/pki/ca.crt --tls-cert-file=/etc/kubernetes/pki/apiserver.crt --kubelet-client-key=/etc/kubernetes/pki/apiserver-kubelet-client.key --secure-port=6443 --proxy-client-key-file=/etc/kubernetes/pki/front-proxy-client.key --enable-bootstrap-token-auth=true --requestheader-group-headers=X-Remote-Group --requestheader-allowed-names=front-proxy-client --service-cluster-ip-range=10.96.0.0/12 --kubelet-client-certificate=/etc/kubernetes/pki/apiserver-kubelet-client.crt --proxy-client-cert-file=/etc/kubernetes/pki/front-proxy-client.crt --insecure-port=0 --admission-control=NamespaceLifecycle,LimitRanger,ServiceAccount,DefaultStorageClass,DefaultTolerationSeconds,NodeRestriction,MutatingAdmissionWebhook,ValidatingAdmissionWebhook,ResourceQuota --requestheader-extra-headers-prefix=X-Remote-Extra- --advertise-address=192.168.1.74 --tls-private-key-file=/etc/kubernetes/pki/apiserver.key --allow-privileged=true --authorization-mode=Node,RBAC --etcd-servers=https://127.0.0.1:2379 --etcd-cafile=/etc/kubernetes/pki/etcd/ca.crt --etcd-certfile=/etc/kubernetes/pki/apiserver-etcd-client.crt --etcd-keyfile=/etc/kubernetes/pki/apiserver-etcd-client.key
root     19306 19259  0 05:41 ?        00:00:24 kube-scheduler --address=127.0.0.1 --leader-elect=true --kubeconfig=/etc/kubernetes/scheduler.conf
root     19313 19269  2 05:41 ?        00:01:48 kube-controller-manager --leader-elect=true --use-service-account-credentials=true --controllers=*,bootstrapsigner,tokencleaner --service-account-private-key-file=/etc/kubernetes/pki/sa.key --address=127.0.0.1 --kubeconfig=/etc/kubernetes/controller-manager.conf --root-ca-file=/etc/kubernetes/pki/ca.crt --cluster-signing-cert-file=/etc/kubernetes/pki/ca.crt --cluster-signing-key-file=/etc/kubernetes/pki/ca.key --allocate-node-cidrs=true --cluster-cidr=10.244.0.0/16 --node-cidr-mask-size=24
root     19315 19270  0 05:41 ?        00:00:41 etcd --client-cert-auth=true --peer-client-cert-auth=true --key-file=/etc/kubernetes/pki/etcd/server.key --peer-key-file=/etc/kubernetes/pki/etcd/peer.key --listen-client-urls=https://127.0.0.1:2379 --advertise-client-urls=https://127.0.0.1:2379 --trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt --peer-cert-file=/etc/kubernetes/pki/etcd/peer.crt --peer-trusted-ca-file=/etc/kubernetes/pki/etcd/ca.crt --data-dir=/var/lib/etcd --cert-file=/etc/kubernetes/pki/etcd/server.crt
root     19858 19809  0 05:41 ?        00:00:07 /usr/local/bin/kube-proxy --config=/var/lib/kube-proxy/config.conf
root     19900 19869  0 05:41 ?        00:00:02 /kube-dns --domain=cluster.local. --dns-port=10053 --config-dir=/kube-dns-config --v=2
nobody   20082 20059  0 05:41 ?        00:00:02 /sidecar --v=2 --logtostderr --probe=kubedns,127.0.0.1:10053,kubernetes.default.svc.cluster.local,5,SRV --probe=dnsmasq,127.0.0.1:53,kubernetes.default.svc.cluster.local,5,SRV
root     20312 20296  0 05:41 ?        00:00:05 /opt/bin/flanneld --ip-masq --kube-subnet-mgr
root     28126     1  2 06:01 ?        00:01:23 /usr/bin/kubelet --bootstrap-kubeconfig=/etc/kubernetes/bootstrap-kubelet.conf --kubeconfig=/etc/kubernetes/kubelet.conf --pod-manifest-path=/etc/kubernetes/manifests --allow-privileged=true --network-plugin=cni --cni-conf-dir=/etc/cni/net.d --cni-bin-dir=/opt/cni/bin --cluster-dns=10.96.0.10 --cluster-domain=cluster.local --authorization-mode=Webhook --client-ca-file=/etc/kubernetes/pki/ca.crt --cgroup-driver=cgroupfs --cadvisor-port=0 --rotate-certificates=true --pod-infra-container-image=ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1 --fail-swap-on=false --container-runtime-endpoint=unix:///var/run/crio/crio.sock --runtime-request-timeout=10m --container-runtime=remote
root     28791     1  0 06:01 ?        00:00:00 /usr/libexec/crio/conmon -c 572d0c72a8ac1b9a524ecefd2c1250169030ab3f29fc9d2ef7f3fda7245a95a1 -u 572d0c72a8ac1b9a524ecefd2c1250169030ab3f29fc9d2ef7f3fda7245a95a1 -r /usr/bin/runc -b /var/run/containers/storage/btrfs-containers/572d0c72a8ac1b9a524ecefd2c1250169030ab3f29fc9d2ef7f3fda7245a95a1/userdata -p /var/run/containers/storage/btrfs-containers/572d0c72a8ac1b9a524ecefd2c1250169030ab3f29fc9d2ef7f3fda7245a95a1/userdata/pidfile -l /var/log/pods/9f159626be58b46cde4338feb98be458/kube-scheduler/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     28803 28791  0 06:01 ?        00:00:13 kube-scheduler --address=127.0.0.1 --leader-elect=true --kubeconfig=/etc/kubernetes/scheduler.conf
root     29774     1  0 06:01 ?        00:00:00 /usr/libexec/crio/conmon -c 7eae6b6268b8856fe84d8a4527e94d86a20c4bc64d977e5ca3d892ee05b78668 -u 7eae6b6268b8856fe84d8a4527e94d86a20c4bc64d977e5ca3d892ee05b78668 -r /usr/bin/runc -b /var/run/containers/storage/btrfs-containers/7eae6b6268b8856fe84d8a4527e94d86a20c4bc64d977e5ca3d892ee05b78668/userdata -p /var/run/containers/storage/btrfs-containers/7eae6b6268b8856fe84d8a4527e94d86a20c4bc64d977e5ca3d892ee05b78668/userdata/pidfile -l /var/log/pods/52f40c33-b74c-11e8-a379-0010e0bafa00/kube-flannel/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     29785 29774  0 06:01 ?        00:00:03 /opt/bin/flanneld --ip-masq --kube-subnet-mgr
root     29910     1  0 06:01 ?        00:00:00 /usr/libexec/crio/conmon -c 0e79446df5855efaa4fcd564499e794c8b20f1f9124e4061546f5aced5a4cbca -u 0e79446df5855efaa4fcd564499e794c8b20f1f9124e4061546f5aced5a4cbca -r /usr/bin/runc -b /var/run/containers/storage/btrfs-containers/0e79446df5855efaa4fcd564499e794c8b20f1f9124e4061546f5aced5a4cbca/userdata -p /var/run/containers/storage/btrfs-containers/0e79446df5855efaa4fcd564499e794c8b20f1f9124e4061546f5aced5a4cbca/userdata/pidfile -l /var/log/pods/515cc156-b74c-11e8-a379-0010e0bafa00/kube-proxy/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     29921 29910  0 06:01 ?        00:00:05 /usr/local/bin/kube-proxy --config=/var/lib/kube-proxy/config.conf
root     30246     1  0 06:02 ?        00:00:00 /usr/libexec/crio/conmon -c af39c17346d425d39cc5af0bf0d5fe8bef5c1e3091d250ec0a1d240637cd95a3 -u af39c17346d425d39cc5af0bf0d5fe8bef5c1e3091d250ec0a1d240637cd95a3 -r /usr/bin/runc -b /var/run/containers/storage/btrfs-containers/af39c17346d425d39cc5af0bf0d5fe8bef5c1e3091d250ec0a1d240637cd95a3/userdata -p /var/run/containers/storage/btrfs-containers/af39c17346d425d39cc5af0bf0d5fe8bef5c1e3091d250ec0a1d240637cd95a3/userdata/pidfile -l /var/log/pods/516c6872-b74c-11e8-a379-0010e0bafa00/kubedns/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
root     30261 30246  0 06:02 ?        00:00:01 /kube-dns --domain=cluster.local. --dns-port=10053 --config-dir=/kube-dns-config --v=2
root     30412     1  0 06:02 ?        00:00:00 /usr/libexec/crio/conmon -c 2a6d6bbbc20dbfea04a9c68be1cbb5b6922ab98f50f19be5c5cb76e25abbdd5b -u 2a6d6bbbc20dbfea04a9c68be1cbb5b6922ab98f50f19be5c5cb76e25abbdd5b -r /usr/bin/runc -b /var/run/containers/storage/btrfs-containers/2a6d6bbbc20dbfea04a9c68be1cbb5b6922ab98f50f19be5c5cb76e25abbdd5b/userdata -p /var/run/containers/storage/btrfs-containers/2a6d6bbbc20dbfea04a9c68be1cbb5b6922ab98f50f19be5c5cb76e25abbdd5b/userdata/pidfile -l /var/log/pods/532b3a8e-b74c-11e8-a379-0010e0bafa00/kubernetes-dashboard/0.log --exit-dir /var/run/crio/exits --socket-dir-path /var/run/crio --log-level info
nobody   30774 30763  0 06:02 ?        00:00:03 /sidecar --v=2 --logtostderr --probe=kubedns,127.0.0.1:10053,kubernetes.default.svc.cluster.local,5,SRV --probe=dnsmasq,127.0.0.1:53,kubernetes.default.svc.cluster.local,5,SRV


[root@bmx004-ps pods]# docker run --rm \
> --runtime=kata-runtime oraclelinux uname -r
[14490.815266] docker0: port 2(veth00db74d) entered blocking state
[14490.886804] docker0: port 2(veth00db74d) entered disabled state
[14490.958380] device veth00db74d entered promiscuous mode
[14491.021620] IPv6: ADDRCONF(NETDEV_UP): veth00db74d: link is not ready
[14491.292889] eth0: renamed from vetha1d315a
[14491.351124] IPv6: ADDRCONF(NETDEV_CHANGE): veth00db74d: link becomes ready
[14491.434037] docker0: port 2(veth00db74d) entered blocking state
[14491.505440] docker0: port 2(veth00db74d) entered forwarding state
[14491.590388] device eth0 entered promiscuous mode
[14491.654561] tun: Universal TUN/TAP device driver, 1.6
[14491.728830] L1TF CPU bug present and SMT on, data leak possible. See CVE-2018-3646 and https://www.kernel.org/doc/html/latest/admin-guide/l1tf.html for details.
4.14.35-1818.2.0.el7.container


----------------------------
# /etc/sysconfig/docker

# Modify these options if you want to change the way the docker daemon runs
OPTIONS="-D --add-runtime kata-runtime=/usr/bin/kata-runtime"
DOCKER_CERT_PATH=/etc/docker

# Enable insecure registry communication by appending the registry URL
# to the INSECURE_REGISTRY variable below and uncommenting it
# INSECURE_REGISTRY='--insecure-registry '

# On SELinux System, if you remove the --selinux-enabled option, you
# also need to turn on the docker_transition_unconfined boolean.
# setsebool -P docker_transition_unconfined

# Location used for temporary files, such as those created by
# docker load and build operations. Default is /var/lib/docker/tmp
# Can be overriden by setting the following environment variable.
# DOCKER_TMPDIR=/var/tmp

# Controls the /etc/cron.daily/docker-logrotate cron job status.
# To disable, uncomment the line below.
# LOGROTATE=false

# Allow creation of core dumps
GOTRACEBACK=crash
----------------------------


## running kube server+crio
d ps
CONTAINER ID        IMAGE                                                              COMMAND                  CREATED             STATUS              PORTS               NAMES
d24adff6e9d1        b9073e7ec52a                                                       "/opt/bin/flanneld -…"   7 hours ago         Up 7 hours                              k8s_kube-flannel_kube-flannel-ds-ppgsn_kube-system_52f40c33-b74c-11e8-a379-0010e0bafa00_1
5d5d550e118b        678a9b3e45a0                                                       "/sidecar --v=2 --lo…"   7 hours ago         Up 7 hours                              k8s_sidecar_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_1
b71795cd80f6        9cd7a5c905e9                                                       "/dnsmasq-nanny -v=2…"   7 hours ago         Up 7 hours                              k8s_dnsmasq_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_1
b3b3221aa038        886d7f9b2edc                                                       "/kube-dns --domain=…"   7 hours ago         Up 7 hours                              k8s_kubedns_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_1
667632a8929e        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-flannel-ds-ppgsn_kube-system_52f40c33-b74c-11e8-a379-0010e0bafa00_1
d797b2265db9        cfc6870b0cac                                                       "/usr/local/bin/kube…"   7 hours ago         Up 7 hours                              k8s_kube-proxy_kube-proxy-wgkjl_kube-system_515cc156-b74c-11e8-a379-0010e0bafa00_1
721a2c44b304        f3121bdb4b3c                                                       "/dashboard --insecu…"   7 hours ago         Up 7 hours                              k8s_kubernetes-dashboard_kubernetes-dashboard-84cffcc95c-8h76f_kube-system_532b3a8e-b74c-11e8-a379-0010e0bafa00_1
6c7f8eb1506a        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-proxy-wgkjl_kube-system_515cc156-b74c-11e8-a379-0010e0bafa00_1
2df6d666a48e        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kubernetes-dashboard-84cffcc95c-8h76f_kube-system_532b3a8e-b74c-11e8-a379-0010e0bafa00_1
153c0d030a5d        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-dns-7b897f4785-kvgf5_kube-system_516c6872-b74c-11e8-a379-0010e0bafa00_1
8436a3e7c66b        668349ea0328                                                       "etcd --client-cert-…"   7 hours ago         Up 7 hours                              k8s_etcd_etcd-bmx033-ps.mgmtnet_kube-system_5743648a151df95b96662b1b28d1483f_1
8d5067851e14        887b8144f94f                                                       "kube-controller-man…"   7 hours ago         Up 7 hours                              k8s_kube-controller-manager_kube-controller-manager-bmx033-ps.mgmtnet_kube-system_8c6218ffdbded3ac8e16de31bf597552_1
07111edf8228        13d5ac8db7ef                                                       "kube-scheduler --ad…"   7 hours ago         Up 7 hours                              k8s_kube-scheduler_kube-scheduler-bmx033-ps.mgmtnet_kube-system_9f159626be58b46cde4338feb98be458_1
33b32eb27206        180d41f47e69                                                       "kube-apiserver --re…"   7 hours ago         Up 7 hours                              k8s_kube-apiserver_kube-apiserver-bmx033-ps.mgmtnet_kube-system_a3153434e14a3e98da5656a2ea37bca1_1
0751bfc0a54a        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-scheduler-bmx033-ps.mgmtnet_kube-system_9f159626be58b46cde4338feb98be458_1
b701be7acb2f        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-controller-manager-bmx033-ps.mgmtnet_kube-system_8c6218ffdbded3ac8e16de31bf597552_1
5ce30d879f8e        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_kube-apiserver-bmx033-ps.mgmtnet_kube-system_a3153434e14a3e98da5656a2ea37bca1_1
709a62ab9f63        ca-docker-registry.us.oracle.com:5000/kubernetes/pause-amd64:3.1   "/pause"                 7 hours ago         Up 7 hours                              k8s_POD_etcd-bmx033-ps.mgmtnet_kube-system_5743648a151df95b96662b1b28d1483f_1



----------------------------------------
## build on ubuntu 16.04
   66  wget https://github.com/opencontainers/runc/releases/download/v1.0.0-rc5/runc.amd64
   67  chmod +x runc.amd64 
   68  ./runc.amd64 -version
   69  cd ..
   70  wget wget https://storage.googleapis.com/golang/go1.8.5.linux-amd64.tar.gz
   71  sudo tar -xvf go1.8.5.linux-amd64.tar.gz -C /usr/local/
   72  mkdir -p $HOME/go/src
export GOPATH=$HOME/go
export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
   75  go version
   76  crictl version
   77  apt-get update && apt-get install -y libglib2.0-dev                                           libseccomp-dev                                           libgpgme11-dev                                           libdevmapper-dev                                           make                                           git
  114  export GOPATH=$HOME/go
  115  export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin
  116  go get -d github.com/kubernetes-sigs/cri-o
  117  cd ~/go/src/github.com/kubernetes-sigs/cri-o
  118  ls -la
  119  git tags
  120  git tag
  121  git checkout tags/v1.11.3
  122  make install.tools
  123  git describe
  124  git status
  125  make
  126  go env
  127  apt search golang
  128  apt install golang-1.9
  129  ls /usr/local
  130  ls /usr/local/bin
  131  export GOROOT="/usr/lib/go-1.9"
  132  which go
  133  echo $PATH
  134  export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/root/go/bin
  135  go env
  136  ls -l /usr/lib/go-1.9
  137  ls -l /usr/lib/go-1.9/bin
  138  export PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/snap/bin:/root/go/bin:/usr/lib/go-1.9/bin
  139  go env
  140  make clean
  141  make install.tools
  142  make
  143  make install
  144  which crio
  145  crio version
  146  make install.config
  147  vim /etc/crio/crio.conf
  148  ls -l /usr/local/libexec/crio/conmon
  149  crio version
  150  echo "[Unit]
Description=OCI-based implementation of Kubernetes Container Runtime Interface
Documentation=https://github.com/kubernetes-sigs/cri-o

[Service]
ExecStart=/usr/local/bin/crio
Restart=on-failure
RestartSec=5

[Install]
WantedBy=multi-user.target" > /etc/systemd/system/crio.service
  151  systemctl enable crio
  152  systemctl start crio
  153  cp /root/bin/runc.amd64 /usr/bin/runc
  154  crictl version
  155  history
crictl version
Version:  0.1.0
RuntimeName:  cri-o
RuntimeVersion:  1.11.3
RuntimeApiVersion:  v1alpha1
