#!/bin/bash

# k8s-crio-setup.sh
#
# simple script to force Kubernetes and crio install and config.

DOCKERDEV="/dev/sdb"
MASTERHOST="$(hostname)"
MASTERIP=$(host `hostname`|awk '{print $4}')

#
yum remove -y qemu-img \
                qemu-kvm-common \
                qemu-guest-agent \
                qemu-kvm \
                qemu-kvm-tools \
                qemu-system-x86 \
                qemu-system-x86-core \
                qemu-common

export http_proxy=www-proxy.us.oracle.com:80
export https_proxy=www-proxy.us.oracle.com:80

# Docker
yum-config-manager --enable ol7_addons
yum-config-manager --disable ol7_preview
yum makecache fast

yum install -y docker-engine

cat <<EOF>/etc/systemd/system/docker.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://www-proxy.us.oracle.com:80"
Environment="HTTPS_PROXY=https://www-proxy.us.oracle.com:80"
EOF

#systemctl stop docker
#systemctl disable docker
#rm -rf /var/lib/docker/*
#docker-storage-config -s btrfs -f -d ${DOCKERDEV}

mkdir /var/lib/docker
mount /var/lib/docker

systemctl enable docker
systemctl start docker

if [[ $(docker version 2>/dev/null 1>&2;echo $?) -ne 0 ]] ; then
	printf "Docker install failed, exiting ...\n"
	exit 1
fi

if [[ $( docker run -it --rm --runtime=runc nginx uname -a 2>/dev/null 1>&2;echo $?) -ne 0 ]] ; then
	printf "Docker container creation failed, exiting ...\n"
	exit 1
fi

# K8S
yum install -y kubeadm
export KUBE_REPO_PREFIX=ca-docker-registry.us.oracle.com:5000/kubernetes

/sbin/iptables -P FORWARD ACCEPT
firewall-cmd --add-masquerade --permanent
firewall-cmd --add-port=10250/tcp --permanent
firewall-cmd --add-port=8472/udp --permanent
firewall-cmd --add-port=6443/tcp --permanent
systemctl restart firewalld

/usr/sbin/setenforce 0

swapoff -a
export no_proxy="${MASTERIP}"
kubeadm-setup.sh up --skip
if [[ $? -ne 0 ]] ; then
	printf "Error running kubeadm-setup.sh, exiting ...\n"
	exit 1
fi

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/admin.conf
chown $(id -u):$(id -g) $HOME/.kube/admin.conf

cat<<EOF>>$HOME/.bashrc
export http_proxy=www-proxy.us.oracle.com:80
export https_proxy=www-proxy.us.oracle.com:80
export no_proxy="MASTERIP"
export KUBE_REPO_PREFIX=ca-docker-registry.us.oracle.com:5000/kubernetes
export KUBECONFIG=$HOME/.kube/admin.conf
EOF
. ~/.bashrc

kubectl taint nodes ${MASTERHOST} node-role.kubernetes.io/master:NoSchedule-
if [[ $? -ne 0 ]] ; then
	printf "Error applying taint to master node, exiting ...\n"
	exit 1
fi

cat <<EOF>/var/tmp/simple-pod.yaml
apiVersion: v1
kind: Pod
metadata:
  name: simple-pod
  labels:
    name: simple-pod
spec:
  containers:
  - name: nginx
    image: nginx
    ports:
    - containerPort: 80
EOF

kubectl create -f /var/tmp/simple-pod.yaml
if [[ $? -ne 0 ]] ; then
	printf "Error creating test pod, exiting ...\n"
	exit 1
else
	kubectl delete pod simple-pod
fi

## cc-runtime
cat <<EOF>/etc/yum.repos.d/clear-containers-3.repo
[clear-containers-3-internal]
name=Clear Containers 3 for OL7
baseurl=http://ca-artifacts.us.oracle.com/auto-build/clearcontainers/x86_64
gpgcheck=0
enabled=1
priority=1
EOF

yum makecache fast
yum install -y cc-runtime
ln -s /usr/share/clear-containers/oclc-0.0.2-container.img /usr/share/clear-containers/clear-containers.img

cc-runtime cc-check
if [[ $? -ne 0 ]] ; then
	printf "Warning, cc-check failed, exiting ...\n"
fi

cat <<EOF>>/etc/sysconfig/docker
OPTIONS='-D --add-runtime cc-runtime=/usr/bin/cc-runtime --default-runtime=runc'
EOF

systemctl daemon-reload
systemctl restart docker

docker run -it --rm --runtime=cc-runtime nginx uname -a
if [[ $? -ne 0 ]] ; then
	printf "Warnng, could not create cc-runtime container, exinting ...\n"
fi

# cri-o
yum install -y crio
crio --version
if [[ $? -ne 0 ]] ; then
	printf "Error, crio binary reported an error, exiting ...\n"
	exit 1
fi

# CONFIG
mkdir -p /etc/systemd/system/crio.service.d
cat <<EOF>/etc/systemd/system/crio.service.d/http-proxy.conf
[Service]
Environment="HTTP_PROXY=http://www-proxy.us.oracle.com:80"
Environment="HTTPS_PROXY=https://www-proxy.us.oracle.com:80"
EOF

sed -i  's/cgroup_manager = "systemd"/cgroup_manager = "cgroupfs"/' /etc/crio/crio.conf
sed -i  's/registries = \[/registries = \[\n\t "docker.io"/' /etc/crio/crio.conf
sed -i  's/runtime_untrusted_workload = ""/runtime_untrusted_workload = ""/usr/bin/cc-runtime/' /etc/crio/crio.conf

systemctl daemon-reload
systemctl restart crio
systemctl status crio
if [[ $? -ne 0 ]] ; then
	printf "Error, cri-o service failure after mods, exiting ...\n"
	exit 1
fi

# Kubelet
systemctl stop kubelet

cat <<EOF>/etc/containers/policy.json
{
   "default": [
     {
       "type": "insecureAcceptAnything"
     }
   ],
   "transports":
   {
     "docker-daemon":
     {
       "": [{"type":"insecureAcceptAnything"}]
     }
   }
}
EOF

cat <<EOF>/etc/systemd/system/kubelet.service.d/10-kubeadm.conf
ExecStart=/usr/bin/kubelet $KUBELET_KUBECONFIG_ARGS $KUBELET_SYSTEM_PODS_ARGS $KUBELET_NETWORK_ARGS $KUBELET_DNS_ARGS $KUBELET_AUTHZ_ARGS $KUBELET_CGROUP_ARGS $KUBELET_CADVISOR_ARGS $KUBELET_CERTIFICATE_ARGS $KUBELET_EXTRA_ARGS $KUBELET_FLAGS --container-runtime=remote --container-runtime-endpoint=unix:///var/run/crio/crio.sock --runtime-request-timeout=10m
EOF

cat <<EOF>/etc/systemd/system/kubelet.service
[Unit]
Description=kubelet: The Kubernetes Node Agent
Documentation=http://kubernetes.io/docs/
Wants=docker.socket crio.service

[Service]
User=root
ExecStart=/usr/bin/kubelet
Restart=always
StartLimitInterval=0
RestartSec=10

[Install]
WantedBy=multi-user.target
EOF

chmod 644 /etc/systemd/system/kubelet.service
chmod 644 /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
chmod 644 /etc/systemd/system/kubelet.service.d/20-pod-infra-image.conf


systemctl daemon-reload
systemctl start kubelet
systemctl status kubelet
if [[ $? -ne 0 ]] ; then
	printf "Error, kubelet service reported errors after mods, exiting ...\n"
	exit 1
fi

exit 0
