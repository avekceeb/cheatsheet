
- https://git.centos.org/rpms/slirp4netns/blob/c7-extras/f/SPECS/slirp4netns.spec
- https://confluence.oraclecorp.com/confluence/display/LVRelease/Triggering+package+build+from+SVN
- https://linux-git.us.oracle.com/oraclelinux/slirp4netns/tree/SRPM/slirp4netns-0.3.0-3.module+el8.1.0+3402+f73c5901

# prepare

    yum install git gcc vim autoconf automake glib2-devel
    yum install libcap-devel
    yum install --enablerepo=*develop*,*option*,*addon* --disablerepo=*archive* libseccomp-devel

    git clone https://github.com/rootless-containers/slirp4netns
    cd slirp4netns/
    ./autogen.sh 
    ./configure --prefix=/usr
    make
    make install
    curl -O http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/runc-1.0.0-65.rc8.el7.x86_64.rpm
    curl -O http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/podman-1.4.4-4.0.1.el7.x86_64.rpm
    curl -O http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/containernetworking-plugins-0.8.1-2.el7.x86_64.rpm
    curl -O http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/containers-common-0.1.37-3.0.1.el7.x86_64.rpm

    yum --enablerepo=*addon* install *.rpm

    podman pull busybox
    podman run --rm -it busybox /bin/sh
    useradd qa
    passwd qa
    cat /etc/subuid
    cat /etc/subgid


# build

git checkout 4992082b2af77c09bca6bd8504e2ebfa5e118c18
./autogen.sh 
./configure --prefix=/usr
make 
mkdir rpmbuild

curl -O https://git.centos.org/rpms/slirp4netns/raw/c7-extras/f/SPECS/slirp4netns.spec
yum install rpm-build
yum install --enablerepo=*option* golang-github-cpuguy83-go-md2man
cd SOURCES/
curl -O https://github.com/rootless-containers/slirp4netns/archive/v0.3.0.tar.gz
rpmbuild -ba ./slirp4netns.spec 
```
svn co http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns
`
`
mkdir slirp4netns-0.3.0
xcp root@10.162.81.58:/root/rpmbuild/SOURCES/slirp4netns-4992082.tar.gz .
xcp root@10.162.81.58:/root/rpmbuild/SPEC/slirp4netns.spec .
xcp root@10.162.81.58:/root/rpmbuild/SPECS/slirp4netns.spec .
ls -l
cd ../..
ls -l
svn status
svn add branches-7/
svn status
svn ci -m'slirp4netns-0.3.0 branch for OL7 created'
svn info
svn cp http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/branches-7/slirp4netns-0.3.0/ http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/tags-7/slirp4netns-0.3.0.dev -m 'create tag slirp4netns-0.3.0.dev'
svn status
svn add tags-7/
svn ci -m'slirp4netns-0.3.0 tags for OL7 created'
svn cp http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/branches-7/slirp4netns-0.3.0/ http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/tags-7/slirp4netns-0.3.0.dev -m 'create tag slirp4netns-0.3.0.dev'
ls -l
svn mv tags-7/ ..
cd ..
svn ci -m'slirp4netns-0.3.0 tags for OL7 created'
svn cp http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/branches-7/slirp4netns-0.3.0/ http://ca-svn.us.oracle.com/svn/repos/rhel4/slirp4netns/tags-7/slirp4netns-0.3.0.dev -m 'create tag slirp4netns-0.3.0.dev'
svn up .
echo "el7" > dist
echo "7.7" > update-el
touch build-dev
echo 'x86_64:aarch64' > arch-el
svn status
svn ci -m 'trigger build'
svn update

```

