
- [Architecture](https://github.com/kata-containers/documentation/blob/master/architecture.md)
- [Dev docs](https://github.com/kata-containers/documentation/blob/master/Developer-Guide.md)
- [Jenkins](http://jenkins.katacontainers.io/)

- [Limitations and difference vs Docker](https://github.com/kata-containers/documentation/blob/master/Limitations.md)

* Kubernetes
    * [kubernetes e2e tests](https://github.com/kubernetes/community/blob/master/contributors/devel/e2e-tests.md)


* CRI and CRI-O
    * [crio github](https://github.com/kubernetes-sigs/cri-o)
    * [cri-validation](https://github.com/kubernetes/community/blob/master/contributors/devel/cri-validation.md)
    * [containerd-cri (with support matrix)](https://github.com/containerd/cri)

* OCI hooks
    * [hooks](https://github.com/containers/libpod/blob/v0.6.2/pkg/hooks/README.md)
    * [libpod hooks](https://github.com/containers/libpod/blob/v0.6.2/pkg/hooks/docs/oci-hooks.5.md)

## Build Kata

    apt install golang qemu gcc make
    mkdir gopath
    export GOPATH=`pwd`/gopath
    # export https_proxy=...

### kata-runtime

    go get -d -u github.com/kata-containers/runtime
    cd $GOPATH/src/github.com/kata-containers/runtime
    make
    # make test
    sudo -E PATH=$PATH make install
    sudo kata-runtime kata-check

### kata-proxy

    go get -d -u github.com/kata-containers/proxy
    cd $GOPATH/src/github.com/kata-containers/proxy
    make
    # make test
    sudo make install

### kata-shim

    go get -d -u github.com/kata-containers/shim
    cd $GOPATH/src/github.com/kata-containers/shim
    make
    # make test
    sudo make install

###  kata-throttler

    go get -d -u github.com/kata-containers/ksm-throttler
    cd $GOPATH/src/github.com/kata-containers/ksm-throttler
    make



### get image and kernel (not building):
	wget http://download.opensuse.org/repositories/home:/katacontainers:/releases:/x86_64:/stable-1.2/xUbuntu_17.10/amd64/kata-containers-image_1.2.0-2_amd64.deb
	wget http://download.opensuse.org/repositories/home:/katacontainers:/releases:/x86_64:/stable-1.2/xUbuntu_17.10/amd64/kata-linux-container_4.14.51.9-2_amd64.deb
	dpkg -i .deb

### Run
	mkdir /etc/systemd/system/docker.service.d
	 cat > kata-containers.conf<<EOF
[Service]
ExecStart=
ExecStart=/usr/bin/dockerd -D --add-runtime kata-runtime=/usr/local/bin/kata-runtime --default-runtime=kata-runtime
EOF


## Grep logs:
    journalctl --output=short-monotonic --no-hostname -S '2018-08-22 19:19:23' -u docker --no-pager | sed -e 's/^\[[0-9. ]*\]//g' -e 's/time="[^\"]*" //g' -e 's/level=[^ ]* //g'


## gRPC
- [tutorials](https://grpc.io/docs/tutorials/)

/usr/bin/qemu-system-x86_64
    -name sandbox-cb48343c47d4dcab1c504b5addbebb73427670ffc99490e3644e1943c8939da3
    -uuid 484127de-ec0f-4168-9d51-a7317f243efe
    -machine pc,accel=kvm,kernel_irqchip,nvdimm
    -cpu host
# QMP server
    -qmp unix:/run/vc/vm/cb48343c47d4dcab1c504b5addbebb73427670ffc99490e3644e1943c8939da3/qmp.sock,server,nowait
    -m 2048M,slots=2,maxmem=8692M
    -device pci-bridge,bus=pci.0,id=pci-bridge-0,chassis_nr=1,shpc=on,addr=2
    -device virtio-serial-pci,id=serial0
    -device virtconsole,chardev=charconsole0,id=console0
    -chardev socket,id=charconsole0,path=/run/vc/vm/cb48343c47d4dcab1c504b5addbebb73427670ffc99490e3644e1943c8939da3/console.sock,server,nowait

    -device nvdimm,id=nv0,memdev=mem0
    -object memory-backend-file,id=mem0,mem-path=/usr/share/kata-containers/kata-containers-image_clearlinux_1.2.0_agent_fcfa054a757.img,size=536870912
    -device virtio-scsi-pci,id=scsi0
# gRPC server (kata-agent)
    -device virtserialport,chardev=charch0,id=channel0,name=agent.channel.0
    -chardev socket,id=charch0,path=/run/vc/vm/cb48343c47d4dcab1c504b5addbebb73427670ffc99490e3644e1943c8939da3/kata.sock,server,nowait
    
    -device virtio-9p-pci,fsdev=extra-9p-kataShared,mount_tag=kataShared
    -fsdev local,id=extra-9p-kataShared,path=/run/kata-containers/shared/sandboxes/cb48343c47d4dcab1c504b5addbebb73427670ffc99490e3644e1943c8939da3,security_model=none
    -netdev tap,id=network-0,vhost=on,vhostfds=3:4:5:6:7:8:9:10,fds=11:12:13:14:15:16:17:18
    -device driver=virtio-net-pci,netdev=network-0,mac=02:42:ac:11:00:02,mq=on,vectors=18
    
    -rtc base=utc,driftfix=slew
    -global kvm-pit.lost_tick_policy=discard
    -vga none
    -no-user-config -nodefaults -nographic -daemonize
    -kernel /usr/share/kata-containers/vmlinuz-4.14.51.9-2.container
    -append tsc=reliable no_timer_check rcupdate.rcu_expedited=1 i8042.direct=1 i8042.dumbkbd=1 i8042.nopnp=1 i8042.noaux=1 noreplace-smp reboot=k console=hvc0 console=hvc1 iommu=off cryptomgr.notests net.ifnames=0 pci=lastbus=0 root=/dev/pmem0p1 rootflags=dax,data=ordered,errors=remount-ro rw rootfstype=ext4 quiet systemd.show_status=false panic=1 nr_cpus=4 init=/usr/lib/systemd/systemd systemd.unit=kata-containers.target systemd.mask=systemd-networkd.service systemd.mask=systemd-networkd.socket -smp 1,cores=1,threads=1,sockets=1,maxcpus=4


### QMP:

- [QMP commands](https://qemu.weilnetz.de/doc/qemu-qmp-ref.html)
- [QMP util](https://github.com/qemu/qemu/tree/master/scripts/qmp)
- [info](https://doc.opensuse.org/documentation/leap/virtualization/html/book.virt/cha.qemu.monitor.html)

    ./qmp-shell $p/qmp.sock
        query-commands


    ./qmp --path=/var/run/vc/vm/c12416a435fc85383d562caf33c9e47d036b760106679857fde8a803012f0a38/qmp.sock query-status
        status: running
        singlestep: False
        running: True


## gRPC:
    kata-runtime
        level=debug
        msg="sending request"
        arch=amd64
        command=ps
        container=f861dd4b72b04f0d9b168fc5a82a27bd53c9f6e8057661322718984ccc03702c
        name=grpc.ListProcessesRequest
        pid=8733
        req="container_id:\"f861dd4b72b04f0d9b168fc5a82a27bd53c9f6e8057661322718984ccc03702c\" format:\"json\" args:\"-ef\" "
        sandbox=f861dd4b72b04f0d9b168fc5a82a27bd53c9f6e8057661322718984ccc03702c
        source=virtcontainers
        subsystem=kata_agent



## Files and Configs
    /usr/share/kata-containers
        kata-containers-1.2.0.20180808+git.fcfa054.ol7_201808210824.img
        kata-containers.img
        kata-containers-initrd-1.2.0.20180808+git.fcfa054.ol7_201808210824.img
        kata-containers-initrd.img
        vmlinux-4.14.35-1818.2.0.el7.container
        vmlinux.container
        vmlinuz-4.14.35-1818.2.0.el7.container
        vmlinuz.container

    /etc/cni/net.d
        100-crio-bridge.conf
        10-flannel.conf
        200-loopback.conf

    /etc/containers
        policy.json
        registries.d
        storage.conf

    /etc/crio
        crio.conf
        crio.conf.rpmsave
        seccomp.json

    /usr/share/containers
        mounts.conf
        oci

    /usr/share/defaults/kata-containers
        configuration.toml

    /etc/systemd/system/crio.service.d
        http-proxy.conf

    /var/run/vc/
        sbs/
        vm/


## kvm-intel.nested=1


### steps ubuntu ###

	## docker
	apt-get install apt-transport-https ca-certificates curl software-properties-common
	curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
	add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
	apt-get update
	apt-get install docker-ce

	## clear containers
	sh -c "echo 'deb http://download.opensuse.org/repositories/home:/clearcontainers:/clear-containers-3/xUbuntu_$(lsb_release -rs)/ /' >> /etc/apt/sources.list.d/clear-containers.list"
	curl -fsSL http://download.opensuse.org/repositories/home:/clearcontainers:/clear-containers-3/xUbuntu_$(lsb_release -rs)/Release.key | apt-key add -
	apt-get update
	apt-get install -y cc-runtime cc-proxy cc-shim

	## config
	mkdir -p /etc/systemd/system/docker.service.d/
	cat << EOF | tee /etc/systemd/system/docker.service.d/clear-containers.conf
	[Service]
	ExecStart=
	ExecStart=/usr/bin/dockerd -D --add-runtime cc-runtime=/usr/bin/cc-runtime --default-runtime=cc-runtime
	EOF

	# grep options /etc/sysconfig/docker
	# modify these options if you want to change the way the docker daemon runs
	OPTIONS='-D --add-runtime cc-runtime=/usr/bin/cc-runtime --default-runtime=cc-runtime'

	systemctl daemon-reload
	systemctl restart docker
	systemctl enable cc-proxy.socket
	systemctl start cc-proxy.socket


	docker run -i --runtime=cc-runtime oraclelinux bash

### steps centos ###
	# echo "qa ALL=(ALL:ALL) NOPASSWD: ALL" | (EDITOR="tee -a" visudo)
	mkdir -p $HOME/go/src/github/clearcontainers
	cd $HOME/go/src/github/clearcontainers
	git clone https://github.com/clearcontainers/runtime
	cd runtime/
	script -efc ./installation/centos-setup.sh
	cc-runtime --version
	# systemctl start docker
	# docker run hello-world


	vim /etc/clear-containers/configuration.toml
	#yum provides /usr/bin/qemu-system-x86_64
	#yum install qemu-system-x86-2.0.0-1.el7.6.x86_64
	vim /etc/clear-containers/configuration.toml


### builder ###

	git clone https://github.com/clearcontainers/osbuilder.git
	cd osbuilder/
	#make help
	sudo -E make image
	sudo -E make rootfs
	sudo -E make image
	sudo -E make kernel-src
	sudo -E make kernel
	sudo install --owner root --group root --mode 0755 workdir/container.img /usr/share/clear-containers/mycc.img
	sudo install --owner root --group root --mode 0755 workdir/vmlinuz.container /usr/share/clear-containers/mycc-vmlinuz
	sudo install --owner root --group root --mode 0755 workdir/vmlinux.container /usr/share/clear-containers/mycc-vmlinux


cat /etc/systemd/system/docker.service.d/docker-sysconfig.conf 
[Service]
ExecStart=
EnvironmentFile=-/etc/sysconfig/docker
EnvironmentFile=-/etc/sysconfig/docker-storage
EnvironmentFile=-/etc/sysconfig/docker-network
ExecStart=/usr/bin/dockerd \
          $OPTIONS \
          $DOCKER_STORAGE_OPTIONS \
          $DOCKER_NETWORK_OPTIONS \
          $INSECURE_REGISTRY 



