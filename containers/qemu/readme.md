QEMU
----

# рабочие #

```bash

# centos7 (installed from iso) on sda1

qemu-system-x86_64 \
	-kernel /home/ru/build/4.14.0-rc1-vanilla/bzImage \
	-drive file=centos7.raw,format=raw \
	-device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::22222-:22 \
	-enable-kvm -m 2G -smp 2 \
	-nographic \
	-append 'root=/dev/sda1 console=ttyS0'
```

```bash
#####################################
## create disk and install from cd ##
#####################################

qemu-img create -f qcow2 disk1 8G

qemu-system-x86_64 \
 -cdrom slackware64-14.2-install-dvd.iso \
 -bootorder=d \
 -drive file=disk1,format=qcow2 \
 -m 2G

#######################

qemu-system-mipsel \
    -kernel openwrt-malta-le-vmlinux-initramfs.elf \
    -nographic -m 256


qemu-system-x86_64 -kernel $(pwd)/vmlinuz-4.10.0-33-generic \
    -append "console=ttyS0 root=/dev/sda debug earlyprintk=serial slub_debug=QUZ" \
    -drive file=/home/ru/vm/wheezy/wheezy.img,format=raw \
    -device e1000,netdev=net0 -netdev user,id=net0,hostfwd=tcp::10021-:22 \
    -enable-kvm -nographic -m 2G -smp 2 -pidfile vm.pid

qemu-system-x86_64 -m 2048 \
	-net nic -net user,hostfwd=tcp::22222-:22 \
	-display none -serial stdio -no-reboot \
	-numa node,nodeid=0,cpus=0-1 \
	-numa node,nodeid=1,cpus=2-3 \
	-smp sockets=2,cores=2,threads=1 \
	-enable-kvm -usb -usbdevice mouse -usbdevice tablet \
	-soundhw all \
	-hda /home/ru/vm/wheezy/wheezy.img \
	-snapshot \
	-kernel /home/ru/build/4.13.1-kcov-qemu/vmlinux \
	-append console=ttyS0 vsyscall=native rodata=n oops=panic nmi_watchdog=panic panic_on_warn=1 panic=86400 ftrace_dump_on_oops=orig_cpu earlyprintk=serial net.ifnames=0 biosdevname=0 kvm-intel.nested=1 kvm-intel.unrestricted_guest=1 kvm-intel.vmm_exclusive=1 kvm-intel.fasteoi=1 kvm-intel.ept=1 kvm-intel.flexpriority=1 kvm-intel.vpid=1 kvm-intel.emulate_invalid_guest_state=1 kvm-intel.eptad=1 kvm-intel.enable_shadow_vmcs=1 kvm-intel.pml=1 kvm-intel.enable_apicv=1 root=/dev/sda


# or??? -net user,hostfwd=tcp::10021-:22 -net nic

qemu-system-x86_64 -cdrom ~/iso/debian-9.1.0-amd64-xfce-CD-1.iso \
    -drive file=hd.qcow2,format=qcow2 -m 2G -enable-kvm


```
ppdev                  20480  0
kvm_intel             192512  0
kvm                   589824  1 kvm_intel
irqbypass              16384  1 kvm
bochs_drm              20480  1
pcspkr                 16384  0
ttm                    98304  1 bochs_drm
joydev                 20480  0
evdev                  24576  1
serio_raw              16384  0
sg                     32768  0
drm_kms_helper        155648  1 bochs_drm
parport_pc             28672  0
parport                49152  2 parport_pc,ppdev
drm                   360448  4 bochs_drm,ttm,drm_kms_helper
acpi_cpufreq           20480  0
button                 16384  0
ip_tables              24576  0
x_tables               36864  1 ip_tables
autofs4                40960  2
ext4                  585728  1
crc16                  16384  1 ext4
jbd2                  106496  1 ext4
crc32c_generic         16384  2
fscrypto               28672  1 ext4
ecb                    16384  0
glue_helper            16384  0
lrw                    16384  0
gf128mul               16384  1 lrw
ablk_helper            16384  0
cryptd                 24576  1 ablk_helper
aes_x86_64             20480  0
mbcache                16384  2 ext4
sd_mod                 45056  3
sr_mod                 24576  0
cdrom                  61440  1 sr_mod
ata_generic            16384  0
ata_piix               36864  2
floppy                 69632  0
psmouse               135168  0
e1000                 143360  0
i2c_piix4              24576  0
libata                249856  2 ata_piix,ata_generic
scsi_mod              225280  4 sd_mod,libata,sr_mod,sg


modinfo e1000
filename:       /lib/modules/4.9.0-3-amd64/kernel/drivers/net/ethernet/intel/e1000/e1000.ko
version:        7.3.21-k8-NAPI
license:        GPL
description:    Intel(R) PRO/1000 Network Driver


root@scratch:~# ls -l /sys/dev/*/*/device/driver
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/block/11:0/device/driver -> ../../../../../../../bus/scsi/drivers/sr
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/block/2:0/device/driver -> ../../../bus/platform/drivers/floppy
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/block/8:0/device/driver -> ../../../../../../../bus/scsi/drivers/sd
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/21:0/device/driver -> ../../../../../../../bus/scsi/drivers/sd
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/21:1/device/driver -> ../../../../../../../bus/scsi/drivers/sr
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/226:0/device/driver -> ../../../bus/pci/drivers/bochs-drm
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/226:64/device/driver -> ../../../bus/pci/drivers/bochs-drm
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/250:0/device/driver -> ../../../../../../../bus/scsi/drivers/sd
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/250:1/device/driver -> ../../../../../../../bus/scsi/drivers/sr
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/252:0/device/driver -> ../../../bus/pnp/drivers/rtc_cmos
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/29:0/device/driver -> ../../../bus/pci/drivers/bochs-drm
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/4:64/device/driver -> ../../../bus/pnp/drivers/serial
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/4:65/device/driver -> ../../../bus/platform/drivers/serial8250
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/4:66/device/driver -> ../../../bus/platform/drivers/serial8250
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/4:67/device/driver -> ../../../bus/platform/drivers/serial8250
lrwxrwxrwx 1 root root 0 Sep 30 16:21 /sys/dev/char/99:0/device/driver -> ../../../bus/pnp/drivers/parport_pc

##############
root@scratch:~# lsmod
Module                  Size  Used by
af_packet             122880  2
kvm_intel             397312  0
kvm                  1449984  1 kvm_intel
irqbypass              20480  1 kvm
sg                     98304  0
joydev                 32768  0
e1000                 372736  0
i2c_piix4              32768  0
pcspkr                 16384  0
button                 28672  0
ip_tables              53248  0
x_tables               61440  1 ip_tables
sr_mod                 53248  0
cdrom                 143360  1 sr_mod
ata_generic            16384  0
pata_acpi              16384  0
serio_raw              24576  0
floppy                180224  0
ata_piix               53248  2

root@scratch:~# uname -r
4.13.1-3-crashtest+



```

#############
## monitor ##
#############

-monitor telnet:127.0.0.1:1234,server,nowait


#####################
## with tap iface ###
#####################
qemu-system-mipsel \
    -kernel openwrt-malta-le-vmlinux-initramfs.elf \
    -nographic -m 256 \
    -net nic \
    -net tap,ifname=tap0,script=no,downscript=no

###################
## tap on ubintu ##
###################

apt-get install uml-utilities bridge-utils
cat >/etc/network/interfaces<<EOF
auto lo
iface lo inet loopback
####
auto br0
iface br0 inet dhcp
pre-up tunctl -t tap0 -u qa -g qa
pre-up ip link set dev eth0 down
pre-up brctl addbr br0
pre-up brctl addif br0 eth0
pre-up brctl addif br0 tap0
pre-up ip link set dev tap0 up
up chmod 0666 /dev/net/tun
post-down ip link set dev eth0 down
post-down ip link set dev tap0 down
post-down ip link set dev br0 down
post-down brctl delif br0 eth0
post-down brctl delif br0 tap0
post-down brctl delbr br0
EOF
```

# creating qemu image with debootstrap #

- [how to](http://diogogomes.com/2012/07/13/debootstrap-kvm-image/index.html)
- [script](https://gist.github.com/spectra/10301941)
- [how to also on vbox](http://www.olafdietsche.de/2016/03/24/debootstrap-bootable-image)
- [emdebian cross debootstrap](http://www.olafdietsche.de/2016/03/24/debootstrap-bootable-image)
- [ubuntu arm](http://www.olafdietsche.de/2016/03/24/debootstrap-bootable-image)
- [more](https://unix.stackexchange.com/questions/275429/creating-bootable-debian-image-with-debootstrap)
- [uuid based image](http://www.grulic.org.ar/~mdione/glob/posts/create-a-disk-image-with-a-booting-running-debian/)

