#!/bin/sh
#
# Copyright (c) 2017, Oracle. All rights reserved
#
# File      : kvm-guest-install.sh
# Developer : Sriharsha - sriharsha.devdas@oracle.com
# Purpose   : Script to verify sanity OL guest install using virt-install
# Discription : This script downloads the required OL ISO & setups the 
#               required guest install env & installs the OL guests using
#               virt-install interface. Script also updates OS to latest and
#               installs the same version of kernel on guest as running on host
#               and post install also  verifies guest boot & shutdwon.  
#
# Usage: kvm-guest-install.sh [-m] [-c] [-r] [-u] [-s] [-d] [-n] [-g] [-e] [-l][-h]
#Used to  Install & create Oracle Linux KVM guest
# Options:
#       -m Guest memory in MB
#       -c Number of cpus to be exported to guest 
#       -r Guest OL release version
#       -u Guest OL update version
#Note : All the above options are mandatory & below settings are optional
#       -s Guest image size in GB, default size 10GB
#       -d Disk driver to be used, supported drivers : virtio(virtio-blk),scsi(virtio-scsi),ide. default :virtio-blk 
#       -n Nic driver to be used, Supported drivers :virtio,rtl8139,e1000. default :virtio-net
#       -g Sets /dev/random passthrough to Guest. default :none
#       -l Sets virtio channel to guest .default : none
#       -e Create guest to boot from UEFI. .default :non-UEFI      
#       -h help
# 
#
######################################################################


RESULT_LOC=$PWD/log
IMG_LOC=$PWD
rm -rf $RESULT_LOC
mkdir -p $RESULT_LOC
RESULT_LOG=$RESULT_LOC/kvm_guest_create_test.log
SCP="scp -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
SSH="ssh -q -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null"
export RNG=0 
export CONSOLE=0
export IMG_SIZE=0
export ENABLE_UEFI=0

f_print ()
{
  print_log=$1
  echo $print_log | tee -a ${RESULT_LOG}

}

f_getoptions()
{

    if [ $# -eq 0 ]; then
        f_usage;
        exit
    fi

    while getopts "m:c:r:u:s:d:n:gleh" options; do
       case $options in 
          m ) export MEMORY="$OPTARG"; echo "export MEMORY=$MEMORY" >>$RESULT_LOC/main.env ;;
          c ) export CPU="$OPTARG"; echo "export CPU=$CPU" >>$RESULT_LOC/main.env ;;
          r ) export OL_REL="$OPTARG"; echo "export OL_REL=$OL_REL " >>$RESULT_LOC/main.env ;;
          u ) export OL_UPD="$OPTARG"; echo "export OL_UPD=$OL_UPD " >>$RESULT_LOC/main.env ;;
          s ) export IMG_SIZE="$OPTARG"; echo "export IMG_SIZE=$IMG_SIZE " >>$RESULT_LOC/main.env ;;
          d ) export DISK_DRV="$OPTARG" ;;
          n ) export NIC_DRV="$OPTARG" ;;
          g ) export RNG=1; echo "export RNG=$RNG " >>$RESULT_LOC/main.env ;;
          l ) export CONSOLE=1; echo "export CONSOLE=$CONSOLE " >>$RESULT_LOC/main.env ;; 
          e ) export ENABLE_UEFI=1; echo "export ENABLE_UEFI=$ENABLE_UEFI " >>$RESULT_LOC/main.env ;;
          h ) f_usage 
              exit 1;;
          * ) f_usage
              exit 1;;
        esac
    done
 
    if [ $IMG_SIZE -eq 0 ];then
       echo "export IMG_SIZE=10 " >>$RESULT_LOC/main.env
    fi

    if [ "x$NIC_DRV" == "xvirtio" -o "x$NIC_DRV" == "xrtl8139" -o "x$NIC_DRV" == "xe1000" ];then
        echo "export NIC_MODEL=$NIC_DRV " >>$RESULT_LOC/main.env 
    else 
        #default to virtio-net 
        echo "export NIC_MODEL=virtio " >>$RESULT_LOC/main.env
    fi    

    if [ "x$DISK_DRV" == "xvirtio" -o "x$DISK_DRV" == "xscsi" -o "x$DISK_DRV" == "xide" ];then 
        echo "export DISK_BUS=$DISK_DRV " >>$RESULT_LOC/main.env   
    else 
        #default to virtio-blk
        echo "export DISK_BUS=virtio " >>$RESULT_LOC/main.env
    fi

    if [ $RNG -eq 1 ];then 
       export rng="--rng /dev/random"
    else 
       export rng=""
    fi 

    if [ $CONSOLE -eq 1 ];then
       export channel="--channel pty,target_type=virtio,name='org.qemu.guest_agent.0'"
    else 
       export channel=""
    fi
 
}

f_usage()
{
   cat << EOF

Usage: `basename $0` [-m] [-c] [-r] [-u] [-s] [-d] [-n] [-g] [-l] [-e] [-h]
Used to  Install & create Oracle Linux KVM guest 

Options:
       -m Guest memory in MB 
       -c Number of cpus to be exported to guest
       -r Guest OL release version 
       -u Guest OL update version 
Note : All the above options are mandatory & below settings are optional
       -s Guest image size in GB, default size 10
       -d Disk driver to be used, supported drivers : virtio(virtio-blk),scsi(virtio-scsi),ide. default :virtio-blk
       -n Nic driver to be used, Supported drivers :virtio,rtl8139,e1000..default :virtio-net  
       -g Sets /dev/random passthrough to Guest. default :none
       -l Sets virtio channel to guest. default :none  
       -e Create guest to boot from UEFI. default :non-UEFI
       -h help 

EOF
}
   
f_get_guest_iso()
{

  #get the OL ISO 
  rm -f OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso
  f_print "Downloading the OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso from ca-artifacts.us.oracle.com ......."

  curl http://ca-artifacts.us.oracle.com/ISOs/build-isos/x86_64-el$OL_REL-u$OL_UPD-isos/LATEST/OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso -o $RESULT_LOC/OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso | tee -a ${RESULT_LOG}
  if [ $? -eq 0 ];then 
     f_print ""
     f_print " Downloading the OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso completed ."
     touch $RESULT_LOC/ol_iso_download.suc 
  else 
     f_print ""
     f_print "Failed to download OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso, hence exiting the run "
     touch $RESULT_LOC/ol_iso_download.dif 
     exit 1 
  fi

}
 
f_create_guest ()
{


  f_print "Installing OracleLinux-R$OL_REL-U$OL_UPD kvm guest  using virt-install...."
  f_print ""
  f_print "virt-install  --name OL$OL_REL-U$OL_UPD --memory $MEMORY  --vcpus $CPU  --disk path=$IMG_LOC/OL$OL_REL-$OL_UPD-kvm.img,size=$IMG_SIZE,device=disk,bus=$DISK_BUS,sparse=yes --location  $RESULT_LOC/OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso --nographics --initrd-inject=$RESULT_LOC/OL-$OL_REL-$OL_UPD-ks.cfg --extra-args=ks=file:/OL-$OL_REL-$OL_UPD-ks.cfg  ip=dhcp console=tty0 console=ttyS0,115200n8 --network bridge=virbr0,model=$NIC_MODEL  --os-type linux --os-variant ol$OL_REL --noreboot $rng $channel $UEFI"

  virt-install  --name OL$OL_REL-U$OL_UPD --memory $MEMORY  --vcpus $CPU  --disk path=$IMG_LOC/OL$OL_REL-$OL_UPD-kvm.img,size=$IMG_SIZE,device=disk,bus=$DISK_BUS,sparse=yes --location  $RESULT_LOC/OracleLinux-R$OL_REL-U$OL_UPD-Server-x86_64-dvd.iso --nographics --initrd-inject=$RESULT_LOC/OL-$OL_REL-$OL_UPD-ks.cfg --extra-args="ks=file:/OL-$OL_REL-$OL_UPD-ks.cfg  ip=dhcp console=tty0 console=ttyS0,115200n8" --network bridge=virbr0,model=$NIC_MODEL  --os-type linux --os-variant ol$OL_REL --noreboot $rng $channel $UEFI | tee  $RESULT_LOC/guest-OL-$OL_REL-U$OL_UPD-install.log

  if [ $? -eq 0 ];then 
     f_print ""
     f_print "Installing OracleLinux-R$OL_REL-U$OL_UPD guest is completed ,Refer $RESULT_LOC/guest-OL$OL_REL-U$OL_UPD-install.log for install logs"
     touch $RESULT_LOC/guest-install.suc
  else
     f_print "Installing OracleLinux-R$OL_REL-U$OL_UPD guest failed ,Refer"
     f_print "$RESULT_LOC/guest-$OL_REL-U$OL_UPD-install.log for install logs"
     touch $RESULT_LOC/guest-install.dif
   fi

}


f_print_guest_details ()
{

   virsh net-dhcp-leases  --network default | grep "`virsh dumpxml OL$OL_REL-U$OL_UPD | grep "mac address" | sed "s/.*'\(.*\)'.*/\1/g"`" | awk '{print $5}' | awk -F"/" '{print $1}' > $RESULT_LOC/guest_ip
   guest_ip=`cat $RESULT_LOC/guest_ip`
      
     f_print ""
     f_print "#######################################################"
     f_print "Installed KVM guest details : "
     f_print "  Guest name       : OL$OL_REL-U$OL_UPD" 
     f_print "  OS version       : OL$OL_REL-U$OL_UPD"
     f_print "  Kernel Installed : kernel-uek-`uname -r | awk -F ".el" '{print $1}'` "
     f_print "  Guest memory     : $MEMORY " 
     f_print "  Guest CPU        : $CPU    "
     f_print "  Guest IP         : $guest_ip "
     f_print "  Guest Disk driver : $DISK_BUS "
     f_print "  Guest nic driver : $NIC_MODEL "
     f_print "  Guest root password : kvmroot "
     f_print "  Guest system image file: $IMG_LOC/OL$OL_REL-$OL_UPD-kvm.img "
     f_print ""
     f_print "Guest install log : $RESULT_LOC/guest-OL$OL_REL-U$OL_UPD-install.log"
     f_print "Guest message file : $RESULT_LOC/OL$OL_REL-U$OL_UPD-messages "
     f_print "Guest dmesg   file : $RESULT_LOC/OL$OL_REL-U$OL_UPD-dmesg "  
     f_print "Complete test log  : $RESULT_LOG"
     f_print "#######################################################"
     f_print ""
}


f_guest_boot_verify ()
{

  f_print "Running KVM guest verification test: "
  f_print "Starting OL$OL_REL-U$OL_UPD KVM guest ....."
  f_print "  virsh start OL$OL_REL-U$OL_UPD "
  virsh start OL$OL_REL-U$OL_UPD | tee -a ${RESULT_LOG}
  sleep 60 
  
  f_print ""
  f_print "Getting KVM guest status : "
  virsh dominfo OL$OL_REL-U$OL_UPD | tee -a ${RESULT_LOG}  
  
  #get the guest syslogs for verification 
  f_print ""
  f_print "Copying the KVM guest syslogs ...."
  virsh net-dhcp-leases  --network default | grep "`virsh dumpxml OL$OL_REL-U$OL_UPD | grep "mac address" | sed "s/.*'\(.*\)'.*/\1/g"`" | awk '{print $5}' | awk -F"/" '{print $1}' > $RESULT_LOC/guest_ip
  guest_ip=`cat $RESULT_LOC/guest_ip`

  $SCP $guest_ip:/var/log/messages $RESULT_LOC/OL$OL_REL-U$OL_UPD-messages
  $SSH $guest_ip "dmesg " > $RESULT_LOC/OL$OL_REL-U$OL_UPD-dmesg   
 
  f_print ""
  f_print "Shutting down the KVM guest ...." 
  virsh shutdown OL$OL_REL-U$OL_UPD | tee -a ${RESULT_LOG}
  sleep 30
  f_print "Verify the kvm guest status "
  virsh dominfo OL$OL_REL-U$OL_UPD | tee -a ${RESULT_LOG}
  f_print ""
  
  f_print "KVM guest boot verification completed"

}

f_create_ks_cfg ()
{

  #get the host server kernel info 
  ker_version=`uname -r | awk -F ".el" '{print $1}'`

  if [ "x$DISK_BUS" == "xvirtio" ];then 
     disk_name=vda 
  elif [ "x$DISK_BUS" == "xscsi" ];then
     disk_name=sda
  elif [ "x$DISK_BUS" == "xide" ];then
     disk_name=hda
  fi  

  #setup for public key access between host & kvm guest 
  test -f /root/.ssh/id_rsa || ssh-keygen -q -t rsa -f /root/.ssh/id_rsa -N ""
  key=`cat /root/.ssh/id_rsa.pub`
      
  f_print "Creating OL install kickstart file ......" 
  #create ks.cfg file 


  if [ "${OL_REL}" = "7" ]; then
  
     ker_rpm=kernel-uek-$ker_version.el7uek.x86_64.rpm
     firm_rpm=kernel-uek-firmware-$ker_version.el7uek.noarch.rpm 

echo "
auth --enableshadow --passalgo=sha512
repo --name="Server-HighAvailability" --baseurl=file:///run/install/repo/addons/HighAvailability
repo --name="Server-ResilientStorage" --baseurl=file:///run/install/repo/addons/ResilientStorage
repo --name="Server-Mysql" --baseurl=file:///run/install/repo/addons/Mysql
cdrom
text
firstboot --disable
ignoredisk --only-use=$disk_name
keyboard --vckeymap=us --xlayouts='us'
lang en_US.UTF-8
network  --hostname=localhost.localdomain
network --onboot yes --device eth0 --bootproto dhcp --noipv6

rootpw --plaintext kvmroot
services --enabled="chronyd"
skipx
timezone America/Los_Angeles
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --boot-drive=$disk_name
autopart --type=plain
# Partition clearing information
clearpart --all --initlabel --drives=$disk_name

poweroff

%packages
@core
chrony
kexec-tools
net-tools
%end

%addon com_redhat_kdump --enable --reserve-mb='auto'
%end

%post

mkdir -p /root/.ssh/
echo "$key" >> /root/.ssh/authorized_keys

echo "proxy=http://www-proxy.us.oracle.com:80/ " >> /etc/yum.conf
yum update -y 

rpm -ivh http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/$ker_rpm http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/$firm_rpm
%end

" > $RESULT_LOC/OL-$OL_REL-$OL_UPD-ks.cfg

  else 

     ker_rpm=kernel-uek-$ker_version.el6uek.x86_64.rpm
     firm_rpm=kernel-uek-firmware-$ker_version.el6uek.noarch.rpm

echo "
#auth --enableshadow --passalgo=sha512
cdrom
text
firstboot --disable
ignoredisk --only-use=$disk_name
keyboard us
lang en_US.UTF-8
network  --hostname=localhost.localdomain
network --onboot yes --device eth0 --bootproto dhcp --noipv6
selinux --disabled

rootpw --plaintext kvmroot
skipx
timezone America/Los_Angeles
# System bootloader configuration
bootloader --append=" crashkernel=auto" --location=mbr --driveorder=$disk_name
autopart 
# Partition clearing information
clearpart --all --drives=$disk_name
zerombr

poweroff

%packages
@core
kexec-tools
net-tools
%end

%post
mkdir -p /root/.ssh/
echo "$key" >> /root/.ssh/authorized_keys

echo "nameserver 192.168.122.1" >> /etc/resolv.conf
echo "proxy=http://www-proxy.us.oracle.com:80/ " >> /etc/yum.conf
yum update -y

rpm -ivh http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-6/$ker_rpm http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-6/$firm_rpm

%end 
" > $RESULT_LOC/OL-$OL_REL-$OL_UPD-ks.cfg

  fi

}

f_verify_syslog ()
{
 
   #verify for any error/warnings during test
   dmesg > $RESULT_LOC/dmesg_post.out
 
   f_print "Verifying host syslogs for any error/warning during test ..."
   f_print ""
   diff -w $RESULT_LOC/messages_pre.out /var/log/messages | tee -a ${RESULT_LOG}
   if [ `diff -w $RESULT_LOC/messages_pre.out /var/log/messages | egrep -i 'error|warn|fail|fatal|invalid' | wc -l` -ne 0 ]; then 
      f_print ""
      f_print "Error/warnings are seen in server syslog during test "
      diff -w $RESULT_LOC/messages_pre.out /var/log/messages | egrep -i 'error|warn|fail|fatal|invalid' > $RESULT_LOC/messages_err.log 
      touch $RESULT_LOC/verify_messages.dif
   else 
      f_print ""
      f_print "No Error/warnings are seen in server syslog during test "
      touch $RESULT_LOC/verify_messages.suc
   fi 

   f_print ""
   diff -w $RESULT_LOC/dmesg_pre.out $RESULT_LOC/dmesg_post.out | tee -a ${RESULT_LOG}
   if [ `diff -w $RESULT_LOC/dmesg_pre.out $RESULT_LOC/dmesg_post.out | egrep -i 'error|warn|fail|fatal|invalid' | wc -l` -ne 0 ]; then
      f_print ""
      f_print "Error/warnings are seen in server dmesg during test"
      diff -w $RESULT_LOC/dmesg_pre.out $RESULT_LOC/dmesg_post.out | egrep -i 'error|warn|fail|fatal|invalid' > $RESULT_LOC/dmesg_err.log
      touch $RESULT_LOC/verify_dmesg.dif 
   else
      f_print ""
      f_print "No error/warnings are seen in server dmesg during test"      
      touch $RESULT_LOC/verify_dmesg.suc  
   fi
      
   f_print "#######################################################"
  
}
f_cleanup ()
{

  rm -f $RESULT_LOC/*.iso
  rm -f $RESULT_LOC/initrd_install* $RESULT_LOC/vmlinuz_install*

}

f_setup ()
{

  f_print "Installing required packages for  kvm guest creation ...."
  f_print "Running yum groupinstall Virtualization Host"
  yum groupinstall "Virtualization Host" -y | tee -a $RESULT_LOG

  f_print ""
  f_print "Installing virt-install package ....."
  yum install virt-install -y | tee -a $RESULT_LOG

  f_print ""
  f_print "Checking if the host hardware has virtualization support ....."
  #check for CPU virtualization extension, vmx for intel based hosts &
  #svm for amd based systems 
  if [ `egrep -c  'vmx|svm' /proc/cpuinfo` -eq 0 ];then 
     f_print "Host system doesnt support the hardware virtualization extensions"
     f_print "or the same is not enabled in BIOS"   
     exit 1 
  else 
     f_print "Host system support the hardware virtualization extensions"
  fi

  f_print ""
  f_print "Checking if the KVM modules are loaded in the kernel ....."
  if [ `lsmod | grep -c kvm` -ge 2 ];then 
     f_print "KVM related modules are present & loaded" 
     lsmod | grep kvm | tee -a $RESULT_LOG
  else 
     f_print "KVM related modules are missing or not loaded in the kernel"
     f_print "Pls. verify. "
     exit 1 
  fi

  #set QEMU processes user & group as root
  sed -i 's/#user =/user =/g' /etc/libvirt/qemu.conf
  sed -i 's/#group =/group =/g' /etc/libvirt/qemu.conf

  f_print ""
  f_print "Starting the libvirtd service ......"
  systemctl start  libvirtd.service
  f_print "Checking libvirtd service status ..."
  systemctl status  libvirtd.service
  if [ $? -eq 0 ];then 
     f_print "libvirtd service started fine"
     systemctl status  libvirtd.service | tee -a $RESULT_LOG
  else 
     f_print "libvirtd service failed to start"
     systemctl status  libvirtd.service | tee -a $RESULT_LOG   
     exit 1
  fi 

  #Setup UEFI related env if required 
  f_print ""
  if [ $ENABLE_UEFI -eq 1 ];then 
     f_print "Guest boot from UEFI is enabled,Hecne setting up the required env ...."
     f_print "Installing OVMF binary file package on the host..."
     rpm -ivh http://ca-artifacts.us.oracle.com/auto-build/x86_64-build-output-7/edk2-ovmf-x64-20170914gitg7f2f96f1a8-1.noarch.rpm 
     if [ $? -eq 0 ];then 
        f_print "" 
        f_print "edk2-ovmf-x64-20170914gitg7f2f96f1a8-1.noarch.rpm installed on the server " 
        f_print "Creating copy of efi firmware file for each guest..."
        cp /usr/share/edk2/ovmf-x64/OVMF_CODE-pure-efi.fd /usr/share/edk2/ovmf-x64/OVMF_CODE-pure-efi-OL$OL_REL-U$OL_UPD.fd
        cp /usr/share/edk2/ovmf-x64/OVMF_VARS-pure-efi.fd /usr/share/edk2/ovmf-x64/OVMF_VARS-pure-efi-OL$OL_REL-U$OL_UPD.fd

        LDR="loader=/usr/share/edk2/ovmf-x64/OVMF_CODE-pure-efi-OL$OL_REL-U$OL_UPD.fd,loader_ro=yes,loader_type=pflash,loader_secure=no,nvram=/usr/share/edk2/ovmf-x64/OVMF_VARS-pure-efi-OL$OL_REL-U$OL_UPD.fd"
        export UEFI="--boot=hd,cdrom,$LDR" 
        f_print "Setting up UEFI related env completed."
      else 
        f_print "Failed to install OVMF binary file package on the host"
        f_print "Hence exiting the guest install "
        exit 1  
      fi 
   else 
       export UEFI=""
   fi    
    
}    

###################################################################

f_getoptions $*

. $RESULT_LOC/main.env

#get server details,build,kernel etc
f_print ""
f_print "#############################################################"
f_print ""
f_print "Starting KVM guest install sanity test on : "
f_print ""
f_print "Hostname : `hostname`"
f_print "Kernel   : `uname -r`"
f_print "Release  : `cat /etc/system-release`"
f_print "QEMU version : `rpm -q qemu-kvm`"
f_print "libvirt version : `rpm -q libvirt`"
f_print ""
f_print "#############################################################"

#take back up of syslog before test
dmesg >  $RESULT_LOC/dmesg_pre.out
cp /var/log/messages $RESULT_LOC/messages_pre.out

f_print ""
f_setup

f_print ""
f_get_guest_iso

f_print ""
f_create_ks_cfg

f_print ""
f_create_guest

f_print ""
f_guest_boot_verify 

f_print ""
f_verify_syslog

if [ -f $RESULT_LOC/guest-install.suc ];then 
   f_print_guest_details
fi 

f_cleanup

