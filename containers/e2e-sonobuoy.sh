
# https://github.com/heptio/sonobuoy
# https://github.com/heptio/sonobuoy/blob/master/docs/snapshot.md


wget https://github.com/heptio/sonobuoy/releases/download/v0.11.4/
sonobuoy_0.11.4_linux_amd64.tar.gz

tar xzf sonobuoy_0.11.4_linux_amd64.tar.gz

./sonobuoy run --sonobuoy-image gcr.io/heptio-images/sonobuoy:444e3e42

# kubectl get cm -n heptio-sonobuoy
# kubectl delete cm sonobuoy-config-cm -n heptio-sonobuoy
# kubectl delete cm sonobuoy-plugins-cm -n heptio-sonobuoy
# kubectl delete serviceaccount default -n heptio-sonobuoy
# kubectl delete serviceaccount sonobuoy-serviceaccount -n heptio-sonobuoy
# kubectl delete service sonobuoy-master -n heptio-sonobuoy
