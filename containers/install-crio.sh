Part 2 - Configuration of Kubernetes to use cc-runtime
--------------------------------------------------------
crio and the kubelet process controlled by systemd on each worker node
needs to be configured to use cc-runtime.  The following steps need to be
undertaken:

- configure crio
- prepraing kubelet
- configure flannel network

2.1 configuring crio
-----------------------
Modify crio configuration for cgroups and runtimes used by trusted and
untrusted pods.

Modify /etc/crio/crio.conf as follows:

!!'cgroup_manager' to use cgroupfs
cgroup_manager = "cgroupfs"

!! set default registry endpoint
registries = [
        "docker.io"
]

!! for cc configuration, set 'runtime_untrusted_workload'
runtime_untrusted_workload = "/usr/bin/cc-runtime"

This should ensure that pods that are trusted with use
runtime = "/usr/bin/runc"

You may optionally set 'default_workload_trust' to "untrusted"
(and always use cc-runtime) by changing the following line from trusted
to untrusted:

default_workload_trust = "untrusted"

# systemctl start crio

Get the status of the crio service to confirm:
# systemctl status crio

● crio.service - Open Container Initiative Daemon
   Loaded: loaded (/usr/lib/systemd/system/crio.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2018-03-27 22:51:53 PDT; 6 days ago
     Docs: https://github.com/kubernetes-incubator/cri-o
 Main PID: 14262 (crio)
   Memory: 25.7M
   CGroup: /system.slice/crio.service
           └─14262 /usr/bin/crio

Mar 27 22:51:53 pae-x52-05 systemd[1]: Starting Open Container Initiative Daemon...
Mar 27 22:51:53 pae-x52-05 crio[14262]: time="2018-03-27 22:51:53.520303846-07:00" ...."
Mar 27 22:51:53 pae-x52-05 crio[14262]: time="2018-03-27 22:51:53.601633178-07:00" ...t"
Mar 27 22:51:53 pae-x52-05 crio[14262]: time="2018-03-27 22:51:53.601651255-07:00" ...t"
Mar 27 22:51:53 pae-x52-05 crio[14262]: time="2018-03-27 22:51:53.601798691-07:00" ...f"
Mar 27 22:51:53 pae-x52-05 crio[14262]: time="2018-03-27 22:51:53.601895982-07:00" ...f"
Mar 27 22:51:53 pae-x52-05 systemd[1]: Started Open Container Initiative Daemon.
Hint: Some lines were ellipsized, use -l to show in full.


2.2 Configure kubelet
---------------------
Before changing the confguration stop the kubelet service on the worker node:

# systemctl stop kubelet
The kubelet policy must be changed to allow insecure 
/etc/containers/policy.json

{
    "default": [
        {
            "type": "insecureAcceptAnything"
        }
    ],
    "transports":
        {
            "docker-daemon":
                {
                    "": [{"type":"insecureAcceptAnything"}]
                }
        }
}

Ensure the service configuration includes the socket endpoint by adding to
the ExecStart= line in /etc/systemd/system/kubelet.service.d/10-kubeadm.conf

ExecStart=/usr/bin/kubelet \$KUBELET_KUBECONFIG_ARGS \$KUBELET_SYSTEM_PODS_ARGS \$KUBELET_NETWORK_ARGS \$KUBELET_DNS_ARGS \$KUBELET_AUTHZ_ARGS \$KUBELET_CGROUP_ARGS \$KUBELET_CADVISOR_ARGS \$KUBELET_CERTIFICATE_ARGS \$KUBELET_EXTRA_ARGS \$KUBELET_FLAGS --container-runtime-endpoint=unix:///var/run/crio/crio.sock --runtime-request-timeout=10m

The kubelet service requires dependancies set as follows in
/etc/systemd/system/kubelet.service

[Unit]
Description=kubelet: The Kubernetes Node Agent
Documentation=http://kubernetes.io/docs/
Wants=docker.socket crio.service

[Service]
User=root
ExecStart=/usr/bin/kubelet
Restart=always
StartLimitInterval=0
RestartSec=10

[Install]
WantedBy=multi-user.target

Reload thge configuration and start the kubelet service with the following
commands:

# systemctl daemon-reload

# systemctl start kubelet

Confirm the status of the kubelet service with:

# systemctl status -l kubelet
