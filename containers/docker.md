
## Docker export/import image
	docker save --output /root/oraclelinux.tar container-registry.oracle.com/os/oraclelinux
	docker load --input /root/oraclelinux.tar

## Full image path
	docker run --rm  container-registry.oracle.com/os/oraclelinux:latest uname -r

## Docker set storage-driver = overlayfs2
	f_docker_mrproper
	systemctl stop docker
	cp -au /var/lib/docker /var/lib/docker.bk
	cat >>/etc/docker/daemon.json
	{   "storage-driver": "overlay2",  "storage-opts": [     "overlay2.override_kernel_check=true"   ] }
	vim /etc/sysconfig/docker-storage
	systemctl start docker
	docker info | grep Storage

- [how to oracle image](https://docs.oracle.com/cd/E52668_01/E87205/html/oracle-registry-server.html)

	docker login container-registry.oracle.com
	docker pull container-registry.oracle.com/os/oraclelinux:latest
	docker logout container-registry.oracle.com
	docker image ls
	REPOSITORY                                     TAG                 IMAGE ID            CREATED             SIZE
	container-registry.oracle.com/os/oraclelinux   latest              f426f15a5793        2 months ago        229 MB
	docker run -i --runtime=cc-runtime container-registry.oracle.com/os/oraclelinux bash

## Debug mode:
cat > /etc/docker/daemon.json<<EOF
{
    "debug": true
}
EOF


## Initial (empty)

	/usr/bin/dockerd -H fd://

	docker-containerd 
		-l unix:///var/run/docker/libcontainerd/docker-containerd.sock
		--metrics-interval=0
		--start-timeout 2m
		--state-dir /var/run/docker/libcontainerd/containerd
		--shim docker-containerd-shim
		--runtime docker-runc

docker-contain /var/run/docker/libcontainerd/docker-containerd.sock
dockerd /run/docker/libnetwork/4572f439c4bc41c5eb30d68f87d4b4f56965c7ff2522653d655f5d3a8e78dd4b.sock
1/init    /var/run/docker.sock


cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,net_cls,net_prio)
cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,perf_event)
cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,hugetlb)
cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,devices)

/dev/sda1 on /var/lib/docker/aufs type ext4 (rw,relatime,errors=remount-ro,data=ordered)

none on /var/lib/docker/aufs/mnt/90069ef64d68a0c54af8f502961e6df8811e0204906c450dfe76158551ca35e5 type aufs (rw,relatime,si=6904752fb1540701,dio,dirperm1)

shm on /var/lib/docker/containers/a1953e6a9b89831939d0dd7eff321e7c8f8c30da1177876fa1ae22939812d416/shm 

nsfs on /run/docker/netns/e6800ff3b2e7 type nsfs (rw)


3: docker0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:8f:89:8f:8b brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
    inet6 fe80::42:8fff:fe89:8f8b/64 scope link 
       valid_lft forever preferred_lft forever

5: veth8d24cc2@if4: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue master docker0 state UP group default 
    link/ether 3e:4f:cd:1f:38:e4 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::3c4f:cdff:fe1f:38e4/64 scope link 
       valid_lft forever preferred_lft forever

root@xenial:~# pstree -l -A -S -c
systemd-+-accounts-daemon-+-{gdbus}
        |                 `-{gmain}
        |-acpid
        |-agetty
        |-atd
        |-cron
        |-dbus-daemon
        |-dhclient
        |-dockerd-+-docker-containe-+-docker-containe-+-nginx(ipc,mnt,net,pid,uts)---nginx
        |         |                 |                 `-{docker-containe}
        |         |                 `-{docker-containe}
        |         |-{dockerd}
        |         `-{dockerd}
        |-iscsid

ls -l /proc/3653/ns
total 0
lrwxrwxrwx 1 root root 0 Sep 26 21:52 cgroup -> cgroup:[4026531835]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 ipc -> ipc:[4026531839]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 mnt -> mnt:[4026531840]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 net -> net:[4026531957]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 pid -> pid:[4026531836]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 user -> user:[4026531837]
lrwxrwxrwx 1 root root 0 Sep 26 21:43 uts -> uts:[4026531838]
root@xenial:~# 


d exec lucid_knuth mount
none on / type aufs (rw,relatime,si=6904752fb0797701,dio,dirperm1)
proc on /proc type proc (rw,nosuid,nodev,noexec,relatime)
tmpfs on /dev type tmpfs (rw,nosuid,mode=755)
devpts on /dev/pts type devpts (rw,nosuid,noexec,relatime,gid=5,mode=620,ptmxmode=666)
sysfs on /sys type sysfs (ro,nosuid,nodev,noexec,relatime)
tmpfs on /sys/fs/cgroup type tmpfs (ro,nosuid,nodev,noexec,relatime,mode=755)
cgroup on /sys/fs/cgroup/systemd type cgroup (ro,nosuid,nodev,noexec,relatime,xattr,release_agent=/lib/systemd/systemd-cgroups-agent,name=systemd)
cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (ro,nosuid,nodev,noexec,relatime,net_cls,net_prio)
cgroup on /sys/fs/cgroup/blkio type cgroup (ro,nosuid,nodev,noexec,relatime,blkio)
cgroup on /sys/fs/cgroup/memory type cgroup (ro,nosuid,nodev,noexec,relatime,memory)
cgroup on /sys/fs/cgroup/perf_event type cgroup (ro,nosuid,nodev,noexec,relatime,perf_event)
cgroup on /sys/fs/cgroup/freezer type cgroup (ro,nosuid,nodev,noexec,relatime,freezer)
cgroup on /sys/fs/cgroup/pids type cgroup (ro,nosuid,nodev,noexec,relatime,pids)
cgroup on /sys/fs/cgroup/cpuset type cgroup (ro,nosuid,nodev,noexec,relatime,cpuset)
cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (ro,nosuid,nodev,noexec,relatime,cpu,cpuacct)
cgroup on /sys/fs/cgroup/hugetlb type cgroup (ro,nosuid,nodev,noexec,relatime,hugetlb)
cgroup on /sys/fs/cgroup/devices type cgroup (ro,nosuid,nodev,noexec,relatime,devices)
mqueue on /dev/mqueue type mqueue (rw,nosuid,nodev,noexec,relatime)
/dev/sda1 on /etc/resolv.conf type ext4 (rw,relatime,errors=remount-ro,data=ordered)
/dev/sda1 on /etc/hostname type ext4 (rw,relatime,errors=remount-ro,data=ordered)
/dev/sda1 on /etc/hosts type ext4 (rw,relatime,errors=remount-ro,data=ordered)
shm on /dev/shm type tmpfs (rw,nosuid,nodev,noexec,relatime,size=65536k)
proc on /proc/bus type proc (ro,nosuid,nodev,noexec,relatime)
proc on /proc/fs type proc (ro,nosuid,nodev,noexec,relatime)
proc on /proc/irq type proc (ro,nosuid,nodev,noexec,relatime)
proc on /proc/sys type proc (ro,nosuid,nodev,noexec,relatime)
proc on /proc/sysrq-trigger type proc (ro,nosuid,nodev,noexec,relatime)
tmpfs on /proc/kcore type tmpfs (rw,nosuid,mode=755)
tmpfs on /proc/timer_list type tmpfs (rw,nosuid,mode=755)
tmpfs on /proc/timer_stats type tmpfs (rw,nosuid,mode=755)
tmpfs on /proc/sched_debug type tmpfs (rw,nosuid,mode=755)
tmpfs on /sys/firmware type tmpfs (ro,relatime)

root      3374     1  0 21:24 ?        00:00:20 /usr/bin/dockerd -H fd://
root      3379  3374  0 21:24 ?        00:00:04 docker-containerd -l unix:///var/run/docker/libcontainerd/docker-containerd.sock --metrics-interval=0 --start-timeout 2m --state-dir /var/run/docker/libcontainerd/containerd --shim docker-containerd-shim --runtime docker-runc --debug
root      4254  3379  0 22:22 ?        00:00:00 docker-containerd-shim 2916696dc575e9085884f88b312980408015640bb00bd2630ae3d4be029cbb79 /var/run/docker/libcontainerd/2916696dc575e9085884f88b312980408015640bb00bd2630ae3d4be029cbb79 docker-runc




--------- KATA -----------------------
        |-dockerd-+-docker-containe-+-docker-containe-+-kata-proxy---10*[{kata-proxy}]
        |         |                 |                 |-kata-shim(net,pid)---9*[{kata-shim}]
        |         |                 |                 |-qemu-system-x86(net)---2*[{qemu-system-x86}]
        |         |                 |                 `-8*[{docker-containe}]
        |         |                 `-20*[{docker-containe}]
        |         |-docker-proxy---6*[{docker-proxy}]
        |         `-39*[{dockerd}]
