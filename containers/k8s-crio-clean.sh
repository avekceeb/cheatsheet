#!/bin/bash

export http_proxy=www-proxy.us.oracle.com:80
export https_proxy=www-proxy.us.oracle.com:80

systemctl daemon-reload

kubeadm-setup.sh down --skip
systemctl stop kubelet
systemctl disable kubelet

#yum history undo -y `yum history|grep 'install -y kubeadm'|sort -nr|head -1|awk '{print $1}'`

yum remove -y kubeadm \
              kubernetes \
              kubectl \
              kubelet \
              kubernetes-cni \
              kubernetes-client \
              kubernetes-dns \
              kubernetes-master \
              kubernetes-node \
              kubernetes-cni-plugins

rm -rf /var/lib/kublet
rm -rf /etc/kubernetes
rm -rf /var/run/kubeadm
rm -f /usr/local/bin/kube*
rm -rf /usr/local/share/kubeadm
rm -rf $HOME/.kube
rm -rf /etc/systemd/system/kubelet.service
rm -rf /etc/systemd/system/kubelet.service.d/10-kubeadm.conf
rm -rf /var/lib/etcd
  
yum remove -y kata-image kata-shim kata-runtime kata-ksm-throttler kernel-uek-container

# in case of upstream rpms:
rpm -e kata-containers-image kata-linux-container kata-proxy-bin kata-shim-bin kata-proxy

rm -rf /usr/share/kata-containers
      
yum remove -y \
		qemu-img \
		qemu-kvm-common \
		qemu-guest-agent \
		qemu-kvm \
		qemu-kvm-tools \
		qemu-system-x86 \
		qemu-system-x86-core \
		qemu-common

  #if [[ -f /usr/share/clear-containers/clear-containers.img ]] ; then
   # rm -f /usr/share/clear-containers/clear-containers.img
  #fi
  #if [[ -f /etc/yum.repos.d/clear-containers-3.repo ]] ; then
  #  rm -f /etc/yum.repos.d/clear-containers-3.repo
  #fi

# cri-o
echo "Removing cri-o ..."
systemctl stop crio
systemctl disable crio

yum remove -y crio \
              skopeo \
              skopeo-containers \
              ostree \
              ostree-libs \
              runc

rm -f /usr/local/bin/crictl
rm -f /usr/local/bin/crio
rm -f /usr/local/bin/crio-runc
rm -f /etc/systemd/system/crio.service
rm -rf /opt/cni
rm -rf /etc/cni
rm -rf /etc/crio
rm -rf /etc/containers
rm -rf /etc/sysconfig/docker-storage
rm -rf /etc/systemd/system/crio.service.d/http-proxy.conf
rmdir /etc/systemd/system/crio.service.d
rm -rf /usr/local/bin/crictl
rm -rf /etc/cni/net.d/10-mynet.conf
  

########################################
 # Docker
echo "Removing Docker ..."
systemctl stop docker
systemctl disable docker
yum remove -y docker-engine \
			docker \
			docker-devel
 
rm -rf /etc/docker
rm -rf /etc/systemd/system/docker.service.d
rm -rf /var/lib/docker/*

umount /var/lib/docker
rmdir /var/lib/docker
rm -rf /var/run/docker/*
rmdir /var/run/docker
 

rm -rf /etc/docker
rm -rf /var/lib/cni/*
rm -rf /var/lib/kubelet
rm -rf /var/lib/dockershim
rm -rf /var/lib/virtcontaiers


# mount points
rm -rf /var/lib/docker/*
rm -rf /var/lib/containers/*

 
unset http_proxy
unset https_proxy
unset KUBECONFIG
unset no_proxy
unset CGROUP_DRIVER
unset CONTAINER_RUNTIME
unset CONTAINER_RUNTIME_ENDPOINT 

/sbin/iptables -F
firewall-cmd --remove-masquerade --permanent
firewall-cmd --remove-port=10250/tcp --permanent
firewall-cmd --remove-port=8472/udp --permanent
firewall-cmd --remove-port=6443/tcp --permanent
systemctl restart firewalld

exit 0
