
https://kubernetes.io/docs/tutorials/kubernetes-basics/update/update-intro/
https://kubernetes.io/docs/concepts/overview/object-management-kubectl/overview/

https://kubernetes.io/docs/concepts/workloads/pods/pod/
https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#pod-v1-core
https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#replicaset-v1-apps

https://kubernetes.io/docs/tasks/run-application/rolling-update-replication-controller/

## [cheatsheet](https://kubernetes.io/docs/reference/kubectl/cheatsheet/)

## https://kubernetes.io/docs/concepts/containers/images/
## https://github.com/kubernetes/kubernetes/issues/1293
## local img: https://github.com/kubernetes/kubernetes/issues/24903
## https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.9/#_v1_container
## https://stackoverflow.com/questions/42564058/how-to-use-local-docker-images-in-kubernetes

## https://kukulinski.com/10-most-common-reasons-kubernetes-deployments-fail-part-1/

## pull the image from private repository https://github.com/kubernetes/kubernetes/issues/41536

## Allow running pods on the master https://github.com/kubernetes/kops/issues/204
## taints https://github.com/kubernetes/kubernetes/issues/41070
## https://github.com/kubernetes/kubernetes/issues/48147
## https://github.com/kubernetes/kubernetes/issues/49440

## http://kamalmarhubi.com/blog/2015/11/17/kubernetes-from-the-ground-up-the-scheduler/

## https://kubernetes.io/docs/concepts/configuration/taint-and-toleration/

## [101] https://kubernetes.io/docs/user-guide/walkthrough/

## https://github.com/kubernetes/kubernetes/blob/master/pkg/kubectl/cmd/get/get.go

## https://github.com/kubernetes-sigs/cri-tools/blob/master/docs/crictl.md

## [CRI-O git](https://github.com/kubernetes-sigs/cri-o)

## [Testing frm](https://github.com/kubernetes-sigs/testing_frameworks)

## [CNI git](https://github.com/containernetworking/cni)
## [CNI Spec](https://github.com/containernetworking/cni/blob/master/SPEC.md)
## [cnitool](https://github.com/containernetworking/cni/tree/master/cnitool)
## [cni plugins](https://github.com/containernetworking/plugins)

## [CNI slides](https://www.slideshare.net/weaveworks/introduction-to-the-container-network-interface-cni)
## [CNI habr](https://habr.com/company/flant/blog/329830/)
## [CNI explained habr](https://habr.com/company/flant/blog/346304/)
## [CNI in cli](https://www.dasblinkenlichten.com/understanding-cni-container-networking-interface/)
## [Container Networking](http://events17.linuxfoundation.org/sites/events/files/slides/Container%20Networking%20Deep%20Dive.pdf)

-------------------------------------------------------------


## kubectl

	kubectl taint nodes $(hostname -f) node-role.kubernetes.io/master:NoSchedule-

	kubectl run dummyserver --image=httpdummy:v2 --port=8888
	kubectl expose deployment dummyserver
	dummyaddr=$(${runregular} kubectl get services | grep dummyserver | awk '{print $3}')
	curl --noproxy ${dummyaddr} http://${dummyaddr}:8888/

	kubectl get deploy dummyserver -oyaml
	kubectl get pod
	kubectl get pod dummyserver-6885cdff4-9hds2 -o yaml

	docker inspect 8545d191f7d0 | grep Runtime
	k get pod nginx-deployment-75675f5897-gnrq8 --template="{{(index .spec.containers 0).image}}"

	kubectl delete deployment dummyserver

## update deployment

- [doc](https://kubernetes.io/docs/tasks/run-application/run-stateless-application-deployment/#updating-the-deployment)

d pull nginx:1.8
d pull nginx:1.7.9

k apply -f nx179.yaml
k get pods
NAME                                READY     STATUS    RESTARTS   AGE
nginx-deployment-75675f5897-gnrq8   1/1       Running   0          1m
nginx-deployment-75675f5897-w9845   1/1       Running   0   1m

k get pod nginx-deployment-75675f5897-gnrq8 --template={{.status.podIP}}
curl --noproxy 10.244.0.5 http://10.244.0.5/ | grep 'Welcome to nginx'
k get pod nginx-deployment-75675f5897-gnrq8 --template="{{(index .spec.containers 0).image}}"
# nginx:1.7.9
# update to 1.8
k apply -f nx80.yaml
# scale to 4 replicas

k get deploy nginx-deployment --template="{{.status.updatedReplicas}}"
# 4
k get deploy nginx-deployment --template="{{.metadata.generation}}"
# 3



- [Deployments doc 1.11](https://kubernetes.io/docs/concepts/workloads/controllers/deployment/#rolling-back-a-deployment)

-------------------------------------------------------------

- [services](https://kubernetes.io/docs/concepts/services-networking/connect-applications-service/)


	kubectl replace --force -f service.yaml
	service "sshdservice" deleted
	deployment "mysshd" deleted
	service "sshdservice" replaced
	deployment "mysshd" replaced


-------------------------------------------------------------

# MINIKUBE

## https://kubernetes.io/docs/getting-started-guides/minikube/

## https://kubernetes.io/docs/tasks/access-application-cluster/configure-access-multiple-clusters/

## https://github.com/kubernetes/minikube/blob/master/docs/drivers.md#kvm2-driver

## https://sweetcode.io/learning-kubernetes-getting-started-minikube/

## https://codefresh.io/kubernetes-tutorial/intro-minikube-kubernetes-entities/


    curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.9.0/bin/linux/amd64/kubectl
    sudo mv kubectl /usr/bin

    curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.25.0/minikube-linux-amd64 
    chmod +x minikube && sudo mv minikube /usr/bin

    curl -LO https://storage.googleapis.com/minikube/releases/latest/docker-machine-driver-kvm2 && \
       chmod +x docker-machine-driver-kvm2 && sudo mv docker-machine-driver-kvm2 /usr/bin/

    #sudo apt install libvirt-bin qemu-kvm
    yum install libvirt-daemon-kvm qemu-kvm

    # this will download ISO to ~/.minikube
    minikube start --vm-driver kvm2

    kubectl config use-context minikube
    kubectl get pods --context=minikube
    kubectl run hello-minikube --image=k8s.gcr.io/echoserver:1.4 --port=8080
    kubectl expose deployment hello-minikube --type=NodePort
    kubectl get pod
    curl $(minikube service hello-minikube --url)
    ip addr
    kubectl delete deployment hello-minikube

    minikube start
    kubectl get nodes
    kubectl get pods
    kubectl --help
    kubectl get pods --all-namespaces
    pstree
    ip addr
    gethistory
    eval $(minikube docker-env)
    kubectl run http-dummy --image=http-dummy:latest --port=80
    kubectl get pods
    kubectl get deployments
    kubectl expose deployment hello-node --type=NodePort
    kubectl expose deployment http-dummy --type=NodePort
    kubectl get services
    which curl
    curl http://localhost:31165/
    minikube stop

---------------------------


- [resources](https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/)


--------------------------------
# Python
yum install python-urllib3
yum install python-requests

git clone https://github.com/certifi/python-certifi
git clone https://github.com/GoogleCloudPlatform/google-auth-library-python
git clone https://github.com/websocket-client/websocket-client

git clone --recursive https://github.com/kubernetes-client/python.git

https://github.com/kubernetes-client/python/blob/master/requirements.txt
certifi>=14.05.14 # MPL
six>=1.9.0  # MIT
python-dateutil>=2.5.3  # BSD
setuptools>=21.0.0  # PSF/ZPL
urllib3>=1.19.1,!=1.21  # MIT
pyyaml>=3.12  # MIT
google-auth>=1.0.1  # Apache-2.0
ipaddress>=1.0.17  # PSF
websocket-client>=0.32.0,!=0.40.0,!=0.41.*,!=0.42.* # LGPLv2+
requests # Apache-2.0
requests-oauthlib # ISC

--------------------------------


## INSTALL ON UBUNTU 16.04


apt update

apt install curl
systemctl stop ufw
systemctl disable ufw

apt install docker.io
systemctl enable docker
systemctl start docker

#docker ps
#docker info

iptables -P FORWARD ACCEPT
swapoff -a

curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg |  apt-key add -
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
apt-get update

apt search kubelet

apt install -y kubeadm  kubelet kubernetes-cni

crictl help

kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=172.16.0.8

mkdir -p $HOME/.kube
cp -i /etc/kubernetes/admin.conf $HOME/.kube/config

kubectl get nodes
ip addr
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml


root@ubu1604:~# kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=172.16.0.102
[init] using Kubernetes version: v1.11.3
[preflight] running pre-flight checks
I0919 22:13:11.295587    7192 kernel_validator.go:81] Validating kernel version
I0919 22:13:11.296065    7192 kernel_validator.go:96] Validating kernel config
[preflight/images] Pulling images required for setting up a Kubernetes cluster
[preflight/images] This might take a minute or two, depending on the speed of your internet connection
[preflight/images] You can also perform this action in beforehand using 'kubeadm config images pull'
[kubelet] Writing kubelet environment file with flags to file "/var/lib/kubelet/kubeadm-flags.env"
[kubelet] Writing kubelet configuration to file "/var/lib/kubelet/config.yaml"
[preflight] Activating the kubelet service
[certificates] Generated ca certificate and key.
[certificates] Generated apiserver certificate and key.
[certificates] apiserver serving cert is signed for DNS names [ubu1604 kubernetes kubernetes.default kubernetes.default.svc kubernetes.default.svc.cluster.local] and IPs [10.96.0.1 172.16.0.102]
[certificates] Generated apiserver-kubelet-client certificate and key.
[certificates] Generated sa key and public key.
[certificates] Generated front-proxy-ca certificate and key.
[certificates] Generated front-proxy-client certificate and key.
[certificates] Generated etcd/ca certificate and key.
[certificates] Generated etcd/server certificate and key.
[certificates] etcd/server serving cert is signed for DNS names [ubu1604 localhost] and IPs [127.0.0.1 ::1]
[certificates] Generated etcd/peer certificate and key.
[certificates] etcd/peer serving cert is signed for DNS names [ubu1604 localhost] and IPs [172.16.0.102 127.0.0.1 ::1]
[certificates] Generated etcd/healthcheck-client certificate and key.
[certificates] Generated apiserver-etcd-client certificate and key.
[certificates] valid certificates and keys now exist in "/etc/kubernetes/pki"
[kubeconfig] Wrote KubeConfig file to disk: "/etc/kubernetes/admin.conf"
[kubeconfig] Wrote KubeConfig file to disk: "/etc/kubernetes/kubelet.conf"
[kubeconfig] Wrote KubeConfig file to disk: "/etc/kubernetes/controller-manager.conf"
[kubeconfig] Wrote KubeConfig file to disk: "/etc/kubernetes/scheduler.conf"
[controlplane] wrote Static Pod manifest for component kube-apiserver to "/etc/kubernetes/manifests/kube-apiserver.yaml"
[controlplane] wrote Static Pod manifest for component kube-controller-manager to "/etc/kubernetes/manifests/kube-controller-manager.yaml"
[controlplane] wrote Static Pod manifest for component kube-scheduler to "/etc/kubernetes/manifests/kube-scheduler.yaml"
[etcd] Wrote Static Pod manifest for a local etcd instance to "/etc/kubernetes/manifests/etcd.yaml"
[init] waiting for the kubelet to boot up the control plane as Static Pods from directory "/etc/kubernetes/manifests" 
[init] this might take a minute or longer if the control plane images have to be pulled
[apiclient] All control plane components are healthy after 39.004163 seconds
[uploadconfig] storing the configuration used in ConfigMap "kubeadm-config" in the "kube-system" Namespace
[kubelet] Creating a ConfigMap "kubelet-config-1.11" in namespace kube-system with the configuration for the kubelets in the cluster
[markmaster] Marking the node ubu1604 as master by adding the label "node-role.kubernetes.io/master=''"
[markmaster] Marking the node ubu1604 as master by adding the taints [node-role.kubernetes.io/master:NoSchedule]
[patchnode] Uploading the CRI Socket information "/var/run/dockershim.sock" to the Node API object "ubu1604" as an annotation
[bootstraptoken] using token: wtvss6.aknz0moavna221r8
[bootstraptoken] configured RBAC rules to allow Node Bootstrap tokens to post CSRs in order for nodes to get long term certificate credentials
[bootstraptoken] configured RBAC rules to allow the csrapprover controller automatically approve CSRs from a Node Bootstrap Token
[bootstraptoken] configured RBAC rules to allow certificate rotation for all node client certificates in the cluster
[bootstraptoken] creating the "cluster-info" ConfigMap in the "kube-public" namespace
[addons] Applied essential addon: CoreDNS
[addons] Applied essential addon: kube-proxy

Your Kubernetes master has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

You can now join any number of machines by running the following on each node
as root:

  kubeadm join 172.16.0.102:6443 --token wtvss6.aknz0moavna221r8 --discovery-token-ca-cert-hash sha256:e0b964a0526506ac6cc4fb82b03438d2edd5c6817b0d157ef97c5d56449aabb4

root@ubu1604:~# mkdir -p $HOME/.kube
root@ubu1604:~#   sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
root@ubu1604:~# 
root@ubu1604:~# kubectl get nodes
NAME      STATUS     ROLES     AGE       VERSION
ubu1604   NotReady   master    36s       v1.11.3
root@ubu1604:~# 
root@ubu1604:~# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:61:57:b9 brd ff:ff:ff:ff:ff:ff
    inet 172.16.0.102/24 brd 172.16.0.255 scope global dynamic enp0s3
       valid_lft 85702sec preferred_lft 85702sec
    inet6 fe80::299b:ed55:5e8e:43b4/64 scope link 
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:3f:09:d5:7a brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
root@ubu1604:~# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
clusterrole.rbac.authorization.k8s.io/flannel created
clusterrolebinding.rbac.authorization.k8s.io/flannel created
serviceaccount/flannel created
configmap/kube-flannel-cfg created
daemonset.extensions/kube-flannel-ds-amd64 created
daemonset.extensions/kube-flannel-ds-arm64 created
daemonset.extensions/kube-flannel-ds-arm created
daemonset.extensions/kube-flannel-ds-ppc64le created
daemonset.extensions/kube-flannel-ds-s390x created
root@ubu1604:~# kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/k8s-manifests/kube-flannel-rbac.yml
clusterrole.rbac.authorization.k8s.io/flannel configured
clusterrolebinding.rbac.authorization.k8s.io/flannel configured
root@ubu1604:~# 
root@ubu1604:~# 
root@ubu1604:~# ip addr
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:61:57:b9 brd ff:ff:ff:ff:ff:ff
    inet 172.16.0.102/24 brd 172.16.0.255 scope global dynamic enp0s3
       valid_lft 85627sec preferred_lft 85627sec
    inet6 fe80::299b:ed55:5e8e:43b4/64 scope link 
       valid_lft forever preferred_lft forever
3: docker0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc noqueue state DOWN group default 
    link/ether 02:42:3f:09:d5:7a brd ff:ff:ff:ff:ff:ff
    inet 172.17.0.1/16 scope global docker0
       valid_lft forever preferred_lft forever
4: flannel.1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UNKNOWN group default 
    link/ether 6a:82:14:7f:4a:1f brd ff:ff:ff:ff:ff:ff
    inet 10.244.0.0/32 scope global flannel.1
       valid_lft forever preferred_lft forever
5: cni0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue state UP group default qlen 1000
    link/ether 0a:58:0a:f4:00:01 brd ff:ff:ff:ff:ff:ff
    inet 10.244.0.1/24 scope global cni0
       valid_lft forever preferred_lft forever
    inet6 fe80::10bd:69ff:fee0:6531/64 scope link 
       valid_lft forever preferred_lft forever
6: veth7b821173@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master cni0 state UP group default 
    link/ether 32:a1:fe:8d:24:0b brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet6 fe80::30a1:feff:fe8d:240b/64 scope link 
       valid_lft forever preferred_lft forever
7: vethc41a79dc@if3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1450 qdisc noqueue master cni0 state UP group default 
    link/ether 3a:6f:8b:a9:51:7d brd ff:ff:ff:ff:ff:ff link-netnsid 1
    inet6 fe80::386f:8bff:fea9:517d/64 scope link 
       valid_lft forever preferred_lft forever
root@ubu1604:~# 
root@ubu1604:~# kubectl get nodes
NAME      STATUS    ROLES     AGE       VERSION
ubu1604   Ready     master    2m        v1.11.3
root@ubu1604:~# kubectl get pod --all-namespaces
NAMESPACE     NAME                              READY     STATUS    RESTARTS   AGE
kube-system   coredns-78fcdf6894-qwd2l          1/1       Running   0          2m
kube-system   coredns-78fcdf6894-ttcph          1/1       Running   0          2m
kube-system   etcd-ubu1604                      1/1       Running   0          1m
kube-system   kube-apiserver-ubu1604            1/1       Running   0          1m
kube-system   kube-controller-manager-ubu1604   1/1       Running   0          1m
kube-system   kube-flannel-ds-amd64-4zphv       1/1       Running   0          1m
kube-system   kube-proxy-jnwwx                  1/1       Running   0          2m
kube-system   kube-scheduler-ubu1604            1/1       Running   0          1m


------------------------------------------------------------------------

kubectl help run
Create and run a particular image, possibly replicated. 

Creates a deployment or job to manage the created container(s).

Examples:
  # Start a single instance of nginx.
  kubectl run nginx --image=nginx
  
  # Start a single instance of hazelcast and let the container expose port 5701 .
  kubectl run hazelcast --image=hazelcast --port=5701
  
  # Start a single instance of hazelcast and set environment variables "DNS_DOMAIN=cluster" and "POD_NAMESPACE=default" in the container.
  kubectl run hazelcast --image=hazelcast --env="DNS_DOMAIN=cluster" --env="POD_NAMESPACE=default"
  
  # Start a single instance of hazelcast and set labels "app=hazelcast" and "env=prod" in the container.
  kubectl run hazelcast --image=nginx --labels="app=hazelcast,env=prod"
  
  # Start a replicated instance of nginx.
  kubectl run nginx --image=nginx --replicas=5
  
  # Dry run. Print the corresponding API objects without creating them.
  kubectl run nginx --image=nginx --dry-run
  
  # Start a single instance of nginx, but overload the spec of the deployment with a partial set of values parsed from JSON.
  kubectl run nginx --image=nginx --overrides='{ "apiVersion": "v1", "spec": { ... } }'
  
  # Start a pod of busybox and keep it in the foreground, don't restart it if it exits.
  kubectl run -i -t busybox --image=busybox --restart=Never
  
  # Start the nginx container using the default command, but use custom arguments (arg1 .. argN) for that command.
  kubectl run nginx --image=nginx -- <arg1> <arg2> ... <argN>
  
  # Start the nginx container using a different command and custom arguments.
  kubectl run nginx --image=nginx --command -- <cmd> <arg1> ... <argN>
  
  # Start the perl container to compute π to 2000 places and print it out.
  kubectl run pi --image=perl --restart=OnFailure -- perl -Mbignum=bpi -wle 'print bpi(2000)'
  
  # Start the cron job to compute π to 2000 places and print it out every 5 minutes.
  kubectl run pi --schedule="0/5 * * * ?" --image=perl --restart=OnFailure -- perl -Mbignum=bpi -wle 'print bpi(2000)'

Options:
      --allow-missing-template-keys=true: If true, ignore any errors in templates when a field or map key is missing in the template. Only applies to golang and jsonpath output formats.
      --attach=false: If true, wait for the Pod to start running, and then attach to the Pod as if 'kubectl attach ...' were called.  Default false, unless '-i/--stdin' is set, in which case the default is true. With '--restart=Never' the exit code of the container process is returned.
      --command=false: If true and extra arguments are present, use them as the 'command' field in the container, rather than the 'args' field which is the default.
      --dry-run=false: If true, only print the object that would be sent, without sending it.
      --env=[]: Environment variables to set in the container
      --expose=false: If true, a public, external service is created for the container(s) which are run
      --generator='': The name of the API generator to use, see http://kubernetes.io/docs/user-guide/kubectl-conventions/#generators for a list.
      --hostport=-1: The host port mapping for the container port. To demonstrate a single-machine container.
      --image='': The image for the container to run.
      --image-pull-policy='': The image pull policy for the container. If left empty, this value will not be specified by the client and defaulted by the server
  -l, --labels='': Comma separated labels to apply to the pod(s). Will override previous values.
      --leave-stdin-open=false: If the pod is started in interactive mode or with stdin, leave stdin open after the first attach completes. By default, stdin will be closed after the first attach completes.
      --limits='': The resource requirement limits for this container.  For example, 'cpu=200m,memory=512Mi'.  Note that server side components may assign limits depending on the server configuration, such as limit ranges.
      --no-headers=false: When using the default or custom-column output format, don't print headers (default print headers).
  -o, --output='': Output format. One of: json|yaml|wide|name|custom-columns=...|custom-columns-file=...|go-template=...|go-template-file=...|jsonpath=...|jsonpath-file=... See custom columns [http://kubernetes.io/docs/user-guide/kubectl-overview/#custom-columns], golang template [http://golang.org/pkg/text/template/#pkg-overview] and jsonpath template [http://kubernetes.io/docs/user-guide/jsonpath].
      --overrides='': An inline JSON override for the generated object. If this is non-empty, it is used to override the generated object. Requires that the object supply a valid apiVersion field.
      --pod-running-timeout=1m0s: The length of time (like 5s, 2m, or 3h, higher than zero) to wait until at least one pod is running
      --port='': The port that this container exposes.  If --expose is true, this is also the port used by the service that is created.
      --quiet=false: If true, suppress prompt messages.
      --record=false: Record current kubectl command in the resource annotation. If set to false, do not record the command. If set to true, record the command. If not set, default to updating the existing annotation value only if one already exists.
  -r, --replicas=1: Number of replicas to create for this container. Default is 1.
      --requests='': The resource requirement requests for this container.  For example, 'cpu=100m,memory=256Mi'.  Note that server side components may assign requests depending on the server configuration, such as limit ranges.
      --restart='Always': The restart policy for this Pod.  Legal values [Always, OnFailure, Never].  If set to 'Always' a deployment is created, if set to 'OnFailure' a job is created, if set to 'Never', a regular pod is created. For the latter two --replicas must be 1.  Default 'Always', for CronJobs `Never`.
      --rm=false: If true, delete resources created in this command for attached containers.
      --save-config=false: If true, the configuration of current object will be saved in its annotation. Otherwise, the annotation will be unchanged. This flag is useful when you want to perform kubectl apply on this object in the future.
      --schedule='': A schedule in the Cron format the job should be run with.
      --service-generator='service/v2': The name of the generator to use for creating a service.  Only used if --expose is true
      --service-overrides='': An inline JSON override for the generated service object. If this is non-empty, it is used to override the generated object. Requires that the object supply a valid apiVersion field.  Only used if --expose is true.
      --serviceaccount='': Service account to set in the pod spec
      --show-labels=false: When printing, show all labels as the last column (default hide labels column)
      --sort-by='': If non-empty, sort list types using this field specification.  The field specification is expressed as a JSONPath expression (e.g. '{.metadata.name}'). The field in the API resource specified by this JSONPath expression must be an integer or a string.
  -i, --stdin=false: Keep stdin open on the container(s) in the pod, even if nothing is attached.
      --template='': Template string or path to template file to use when -o=go-template, -o=go-template-file. The template format is golang templates [http://golang.org/pkg/text/template/#pkg-overview].
  -t, --tty=false: Allocated a TTY for each container in the pod.

Usage:
  kubectl run NAME --image=image [--env="key=value"] [--port=port] [--replicas=replicas] [--dry-run=bool] [--overrides=inline-json] [--command] -- [COMMAND] [args...] [options]

Use "kubectl options" for a list of global command-line options (applies to all commands).
