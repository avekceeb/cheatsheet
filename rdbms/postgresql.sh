
# Ububtu 16.04
apt install postgresql postgresql-contrib



zypper install -y postgresql postgresql-server
rcpostgresql start
su - postgres
psql
psql (9.3.5)
Type "help" for help.

postgres=# ALTER USER postgres WITH PASSWORD 'postgres';
ALTER ROLE
postgres=# \q
postgres@sles12-alexed-dev-01:~> exit
logout
sles12-alexed-dev-01:~ # rcpostgresql restart
redirecting to systemctl restart postgresql.service
sles12-alexed-dev-01:~ # su - postgres
postgres@sles12-alexed-dev-01:~> vim /var/lib/pgsql/data/pg_hba.conf
sles12-alexed-dev-01:~ # rcpostgresql restart


postgres=# \dt
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | dummies | table | postgres
(1 row)

postgres=# select * from dummies ;
    item
------------
 pinoccio
 quazimodo
 nutcracker
(3 rows)


postgres=# create table test (testid int, testname varchar(255));
CREATE TABLE
postgres=# create table results (testid int, result varchar(16));
CREATE TABLE
postgres=# insert into test ( 1, 'sanity 1111');
ERROR:  syntax error at or near "1"
LINE 1: insert into test ( 1, 'sanity 1111');
                           ^
postgres=# insert into test values ( 1, 'sanity 1111');
INSERT 0 1
postgres=# insert into test values ( 2, 'sanity aaaaaa');
INSERT 0 1
postgres=# insert into test values ( 3, 'sanity ddddd');
INSERT 0 1
postgres=# insert into test values ( 3, 'performance ddddd');
INSERT 0 1
postgres=# delete test where testname is ''sanity ddddd' ;
postgres'# select * from test ;
postgres'# commit ;
postgres'# select * from test ;
postgres'#
postgres'# insert into test values ( 1, 'sanity 1111');
postgres'# commit ;
postgres'#  select * from test ;
postgres'# \dt
postgres'# \q
postgres'# exit
postgres'# quit
postgres'#
postgres'# quit;
postgres'# \q
postgres@sles12-alexed-dev-01:~>
postgres@sles12-alexed-dev-01:~>
postgres@sles12-alexed-dev-01:~> psql
psql (9.3.5)
Type "help" for help.

postgres=# \dt
          List of relations
 Schema |  Name   | Type  |  Owner
--------+---------+-------+----------
 public | dummies | table | postgres
 public | results | table | postgres
 public | test    | table | postgres
(3 rows)

postgres=# select * from test ;
 testid |     testname
--------+-------------------
      1 | sanity 1111
      2 | sanity aaaaaa
      3 | sanity ddddd
      3 | performance ddddd
(4 rows)

postgres=# delete test where testname is 'sanity ddddd' ;
ERROR:  syntax error at or near "test"
LINE 1: delete test where testname is 'sanity ddddd' ;
               ^
postgres=# delete from test where testname is 'sanity ddddd' ;
ERROR:  syntax error at or near "'sanity ddddd'"
LINE 1: delete from test where testname is 'sanity ddddd' ;
                                           ^
postgres=# delete from test where testname ='sanity ddddd' ;
DELETE 1
postgres=# select * from test ;
 testid |     testname
--------+-------------------
      1 | sanity 1111
      2 | sanity aaaaaa
      3 | performance ddddd
(3 rows)

postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=#
postgres=# select * from test ;
 testid |     testname
--------+-------------------
      1 | sanity 1111
      2 | sanity aaaaaa
      3 | performance ddddd
(3 rows)

postgres=# insert int results values (1, 'pass');
ERROR:  syntax error at or near "int"
LINE 1: insert int results values (1, 'pass');
               ^
postgres=# insert into results values (1, 'pass');
INSERT 0 1
postgres=# insert into results values (1, 'pass');
INSERT 0 1
postgres=# insert into results values (1, 'pass');
INSERT 0 1
postgres=# insert into results values (2, 'pass');
INSERT 0 1
postgres=# insert into results values (3, 'pass');
INSERT 0 1
postgres=# insert into results values (2, 'fail');
INSERT 0 1
postgres=# select * from results ;
 testid | result
--------+--------
      1 | pass
      1 | pass
      1 | pass
      2 | pass
      3 | pass
      2 | fail
(6 rows)

postgres=# insert into results values (1, 'fail');
INSERT 0 1
postgres=# insert into results values (1, 'pass');
INSERT 0 1
postgres=#
postgres=#
postgres=#
postgres=#
postgres=# select * from results where result = 'fail' ;
 testid | result
--------+--------
      2 | fail
      1 | fail
(2 rows)

postgres=# select * from results inner join test on test.testid = results.testid where result = 'fail' ;
 testid | result | testid |   testname
--------+--------+--------+---------------
      1 | fail   |      1 | sanity 1111
      2 | fail   |      2 | sanity aaaaaa
(2 rows)

postgres=# select test.name from results inner join test on test.testid = results.testid where result = 'fail' ;
ERROR:  column test.name does not exist
LINE 1: select test.name from results inner join test on test.testid...
               ^
postgres=# select name from results inner join test on test.testid = results.testid where result = 'fail' ;
ERROR:  column "name" does not exist
LINE 1: select name from results inner join test on test.testid = re...
               ^
postgres=# select * from test;
 testid |     testname
--------+-------------------
      1 | sanity 1111
      2 | sanity aaaaaa
      3 | performance ddddd
(3 rows)

postgres=# select test.testname from results inner join test on test.testid = results.testid where result = 'fail' ;
   testname
---------------
 sanity 1111
 sanity aaaaaa
(2 rows)

postgres=# select test.testname from results left join test on test.testid = results.testid where result = 'fail' ;
   testname
---------------
 sanity 1111
 sanity aaaaaa
(2 rows)

postgres=# select test.testname from results right join test on test.testid = results.testid where result = 'fail' ;
   testname
---------------
 sanity 1111
 sanity aaaaaa
(2 rows)

postgres=# insert int test values (5, 'xxxxxxx');
ERROR:  syntax error at or near "int"
LINE 1: insert int test values (5, 'xxxxxxx');
               ^
postgres=# insert into test values (5, 'xxxxxxx');
INSERT 0 1
postgres=# select test.testname from results right join test on test.testid = results.testid where result = 'fail' ;
   testname
---------------
 sanity 1111
 sanity aaaaaa
(2 rows)

postgres=# select test.testname from results left join test on test.testid = results.testid where result = 'fail' ;
   testname
---------------
 sanity 1111
 sanity aaaaaa
(2 rows)

postgres=# select test.testname from results left join test on test.testid = results.testid ;
     testname
-------------------
 sanity 1111
 sanity 1111
 sanity 1111
 sanity aaaaaa
 performance ddddd
 sanity aaaaaa
 sanity 1111
 sanity 1111
(8 rows)

postgres=# select test.testname, result.result from results left join test on test.testid = results.testid ;
ERROR:  missing FROM-clause entry for table "result"
LINE 1: select test.testname, result.result from results left join t...
                              ^
postgres=# select test.testname, results.result from results left join test on test.testid = results.testid ;
     testname      | result
-------------------+--------
 sanity 1111       | pass
 sanity 1111       | pass
 sanity 1111       | pass
 sanity aaaaaa     | pass
 performance ddddd | pass
 sanity aaaaaa     | fail
 sanity 1111       | fail
 sanity 1111       | pass
(8 rows)

postgres=# select test.testname, results.result from results right join test on test.testid = results.testid ;
     testname      | result
-------------------+--------
 sanity 1111       | pass
 sanity 1111       | pass
 sanity 1111       | pass
 sanity aaaaaa     | pass
 performance ddddd | pass
 sanity aaaaaa     | fail
 sanity 1111       | fail
 sanity 1111       | pass
 xxxxxxx           |
(9 rows)

postgres=#
postgres=#
postgres=#
postgres=#
postgres=# insert into results values (99, 'issue');
INSERT 0 1
postgres=# select test.testname, results.result from results left join test on test.testid = results.testid ;
     testname      | result
-------------------+--------
 sanity 1111       | pass
 sanity 1111       | pass
 sanity 1111       | pass
 sanity aaaaaa     | pass
 performance ddddd | pass
 sanity aaaaaa     | fail
 sanity 1111       | fail
 sanity 1111       | pass
                   | issue
(9 rows)

postgres=# select test.testname, results.result from results right join test on test.testid = results.testid ;
     testname      | result
-------------------+--------
 sanity 1111       | pass
 sanity 1111       | pass
 sanity 1111       | pass
 sanity aaaaaa     | pass
 performance ddddd | pass
 sanity aaaaaa     | fail
 sanity 1111       | fail
 sanity 1111       | pass
 xxxxxxx           |
(9 rows)

postgres=# select test.testname, results.result from results inner join test on test.testid = results.testid ;
     testname      | result
-------------------+--------
 sanity 1111       | pass
 sanity 1111       | pass
 sanity 1111       | pass
 sanity aaaaaa     | pass
 performance ddddd | pass
 sanity aaaaaa     | fail
 sanity 1111       | fail
 sanity 1111       | pass
(8 rows)

dima@t440:~/ora$ su - postgres
Password: 
postgres@t440:~$ 
postgres@t440:~$ 
postgres@t440:~$ which createdb
/usr/bin/createdb
postgres@t440:~$ 
postgres@t440:~$ createdb bookstore
postgres@t440:~$ 
postgres@t440:~$ 
postgres@t440:~$ psql
psql (9.5.14)
Type "help" for help.

postgres=# \dt
        List of relations
 Schema | Name | Type  |  Owner   
--------+------+-------+----------
 public | test | table | postgres
(1 row)

postgres=# \dd
         Object descriptions
 Schema | Name | Object | Description 
--------+------+--------+-------------
(0 rows)

postgres=# \coninfo
Invalid command \coninfo. Try \? for help.
postgres=# \conninfo
You are connected to database "postgres" as user "postgres" via socket in "/var/run/postgresql" at port "5432".
postgres=# \q
postgres@t440:~$ psql bookstore
psql (9.5.14)
Type "help" for help.

bookstore=# \dt
No relations found.
bookstore=# CREATE TABLE books (
bookstore(#   isbn    char(14) NOT NULL,
bookstore(#   title   varchar(255) NOT NULL,
bookstore(#   author  varchar(255) NOT NULL,
bookstore(#   price   decimal(5,2) NOT NULL
bookstore(# );
CREATE TABLE
bookstore=# INSERT INTO books (isbn, title, author, price) VALUES
bookstore-# ('978-1503261969', 'Emma', 'Jayne Austen', 9.44),
bookstore-# ('978-1505255607', 'The Time Machine', 'H. G. Wells', 5.99),
bookstore-# ('978-1503379640', 'The Prince', 'Niccolò Machiavelli', 6.99);
INSERT 0 3
bookstore=# ALTER TABLE books ADD PRIMARY KEY (isbn);
ALTER TABLE
bookstore=# 

