

```
yum install mariadb mariadb-devel
yum install mariadb-server
systemctl status mariadb
systemctl start mariadb
mysql_secure_installation
```

-----------------

_mysqladmin -u root password xxxx_

## get server info:
mysqladmin --user=root --password=sparc1e extended-status
echo "show variables;" | mysql --user=root --password=sparc1e
echo "SHOW INNODB STATUS ; " | mysql --user=root --password=sparc1e

-----------------
```
git clone https://github.com/akopytov/sysbench.git
yum -y install make automake libtool pkgconfig libaio-devel
cd sysbench/
git checkout tags/1.0.8
./autogen.sh
./configure 
make

echo "create database sbtest ;" | mysql --user=root --password=sparc1e

cd /root/sysbench

./src/sysbench \
--test=src/lua/oltp_insert.lua \
--mysql-port=3306 --mysql-user=root --mysql-password=Welcome1 --mysql-db=sbtest \
--rate=100000 \
--threads=5 \
--report-interval=5 \
--report-checkpoints=10 \
--verbosity=5 \
prepare
run
```
-----------------

### v 0.4

```
# for sparc (no luajit available)
git clone https://github.com/akopytov/sysbench.git
cd sysbench/
git fetch origin
git branch -v -a
git checkout -b 0.4 origin/0.4
```


cat my.cnf 
```
[mysqld]
datadir=/var/lib/mysql
socket=/var/lib/mysql/mysql.sock
user=mysql
# Disabling symbolic-links is recommended to prevent assorted security risks
symbolic-links=0

############################################
# tunes:
#innodb_file_per_table=1
# 8g
#innodb_buffer_pool_size=8589934592
#max_connections=300
#thread_cache_size=64

#basedir = /home/mariadb/mariadb-install/mariadb-10.0.19
#datadir = /stor/benchmark/test
#socket = /tmp/mysql.sock
#performance-schema = false
#max_connections = 1000
#back_log = 150
#table_open_cache = 2000
#key_buffer_size = 16M
#query_cache_type = 0
#join_buffer_size = 32K
#sort_buffer_size = 32K
#innodb_file_per_table = true
#innodb_open_files = 100
#innodb_data_file_path = ibdata1:50M:autoextend
#innodb_flush_method = O_DIRECT_NO_FSYNC
#innodb_log_buffer_size = 16M
#innodb_log_file_size = 1G
#innodb_log_files_in_group = 2
#innodb_flush_log_at_trx_commit = 2
innodb_buffer_pool_size = 32G
#innodb_buffer_pool_instances = 32
#innodb_adaptive_hash_index_partitions = 32
#innodb_thread_concurrency = 0

#InnoDB: Unrecognized value O_DIRECT_NO_FSYNC for innodb_flush_method
#170713 13:58:31 [ERROR] Plugin 'InnoDB' init function returned error.
#170713 13:58:31 [ERROR] Plugin 'InnoDB' registration as a STORAGE ENGINE failed.
#170713 13:58:31 [ERROR] /usr/libexec/mysqld: unknown variable 'innodb_buffer_pool_instances=32'
#170713 13:58:31 [ERROR] Aborting
#InnoDB: Error: combined size of log files must be < 4 GB
#170713 14:01:55 [ERROR] Plugin 'InnoDB' init function returned error.
#170713 14:01:55 [ERROR] Plugin 'InnoDB' registration as a STORAGE ENGINE failed.
#170713 14:01:55 [ERROR] /usr/libexec/mysqld: unknown variable 'innodb_adaptive_hash_index_partitions=32'
#
############################################


[mysqld_safe]
log-error=/var/log/mysqld.log
pid-file=/var/run/mysqld/mysqld.pid
```



### dbstress ###
[---](http://dimitrik.free.fr/db_STRESS-kit.tar)
