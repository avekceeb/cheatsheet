

### Prereq ###

```bash
yum install -y \
binutils \
compat-libcap1 \
e2fsprogs \
e2fsprogs-libs \
glibc-devel \
libgcc \
libstdc++ \
libaio \
ksh \
gcc-c++ libaio-devel gcc compat-libstdc++-33 libstdc++-devel \
libXtst \
libX11 \
libXau \
libxcb \
libXi \
make \
net-tools \
nfs-utils \
sysstat \
smartmontools

```


### Settings ###


```bash
cat > /etc/sysctl.conf<<EOF
fs.file-max = 6815744
kernel.sem =  250 1024000 100 512
kernel.shmmni = 4096
kernel.shmall = 1073741824
kernel.shmmax = 4398046511104
net.core.rmem_default = 262144
net.core.rmem_max = 4194304
net.core.wmem_default = 262144
net.core.wmem_max = 1048576
#fs.aio-max-nr = 1048576
fs.aio-max-nr = 3145728
net.ipv4.ip_local_port_range = 9000 65500
EOF

/sbin/sysctl -p

cat > /etc/security/limits.conf<<EOF
root  soft   nofile   65536 
root   hard   nofile   65536
root   soft   nproc   32768 
root   hard   nproc   32768 
root   soft   stack    10240
root   hard   stack    32768
oracle   soft   nofile   65536 
oracle   hard   nofile   65536
oracle   soft   nproc   32768 
oracle   hard   nproc   32768 
oracle   soft   stack    10240
oracle   hard   stack    32768
grid   soft   nofile   65536 
grid   hard   nofile   65536
grid   soft   nproc   32768 
grid   hard   nproc   32768 
grid   soft   stack    10240
grid   hard   stack    32768
*        soft   memlock  268435456
*        hard   memlock  268435456
*   soft   core   unlimited
*   hard   core   unlimited
EOF

cat >/etc/security/limits.d/90-nproc.conf<<EOF
EOF
```

```bash
service iptables stop
chkconfig iptables off
setenforce 0
sed -i -e 's/SELINUX=.*$/SELINUX=disabled/' /etc/selinux/config

systemctl stop firewalld
systemctl disable firewalld

```

```
hostname --fqdn
yum -y install tigervnc-server
```

### Users ###

```bash
groupadd dba
groupadd oinstall
groupadd oper
useradd -g oinstall -G dba,oper oracle

useradd -g oinstall -G dba,oper grid 
```

### Asm ###

# centos 7
# yum -y install kmod-oracleasm
# wget http://download.oracle.com/otn_software/asmlib/oracleasmlib-2.0.12-1.el7.x86_64.rpm
# yum -y localinstall oracleasmlib-2.0.12-1.el7.x86_64.rpm
# wget http://yum.oracle.com/repo/OracleLinux/OL7/latest/x86_64/getPackage/oracleasm-support-2.1.8-3.el7.x86_64.rpm
# yum -y localinstall oracleasm-support-2.1.8-3.el7.x86_64.rpm


```
yum -y install kmod-oracleasm oracleasm-support

oracleasm configure -i

Default user to own the driver interface []: grid
Default group to own the driver interface []: dba
Start Oracle ASM library driver on boot (y/n) [n]: y
Scan for Oracle ASM disks on boot (y/n) [y]: y
Writing Oracle ASM library driver configuration: done

oracleasm init

oracleasm createdisk VOL1 /dev/sdb1
Writing disk header: done

Instantiating disk: done
[root@crash ~]# oracleasm createdisk VOL2 /dev/sdc1
Writing disk header: done
Instantiating disk: done

[root@crash ~]# oracleasm createdisk VOL3 /dev/sdd1
Writing disk header: done
Instantiating disk: done

```

unzip linux..
cd grid
./runInstaller
./runInstaller -silent -waitforcompletion -responseFile /home/oracle/grid.rsp

```tcp  0 0 :::1521 :::*  LISTEN      9442/tnslsnr``` 


## db ##

./runInstaller


netca # add listener_ora on 1522
lsnrctl start listener_ora

