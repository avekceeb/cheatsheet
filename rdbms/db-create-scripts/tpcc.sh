#!/bin/sh

OLD_UMASK=`umask`
umask 0027
mkdir -p /u01/oracle
mkdir -p /u01/oracle/admin/tpcc/adump
mkdir -p /u01/oracle/admin/tpcc/dpdump
mkdir -p /u01/oracle/admin/tpcc/pfile
mkdir -p /u01/oracle/audit
mkdir -p /u01/oracle/cfgtoollogs/dbca/tpcc
umask ${OLD_UMASK}
PERL5LIB=$ORACLE_HOME/rdbms/admin:$PERL5LIB; export PERL5LIB
ORACLE_SID=tpcc; export ORACLE_SID
PATH=$ORACLE_HOME/bin:$PATH; export PATH
echo You should Add this entry in the /etc/oratab: tpcc:/u01/oracle/home:Y
/u01/oracle/home/bin/sqlplus /nolog @/u01/oracle/admin/tpcc/scripts/tpcc.sql
