SET VERIFY OFF
connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
spool /u01/oracle/admin/tpcc/scripts/cloneDBCreation.log append
shutdown abort;
startup nomount pfile="/u01/oracle/admin/tpcc/scripts/init.ora";
Create controlfile reuse set database "tpcc"
MAXINSTANCES 8
MAXLOGHISTORY 1
MAXLOGFILES 16
MAXLOGMEMBERS 3
MAXDATAFILES 100
Datafile 
'&&file0',
'&&file1',
'&&file2',
'&&file3'
LOGFILE GROUP 1  SIZE 50M,
GROUP 2  SIZE 50M,
GROUP 3  SIZE 50M RESETLOGS;
select name from v$controlfile;
exec dbms_backup_restore.zerodbid(0);
shutdown immediate;
startup nomount pfile="/u01/oracle/admin/tpcc/scripts/inittpccTemp.ora";
Create controlfile reuse set database "tpcc"
MAXINSTANCES 8
MAXLOGHISTORY 1
MAXLOGFILES 16
MAXLOGMEMBERS 3
MAXDATAFILES 100
Datafile 
'&&file0',
'&&file1',
'&&file2',
'&&file3'
LOGFILE GROUP 1  SIZE 50M,
GROUP 2  SIZE 50M,
GROUP 3  SIZE 50M RESETLOGS;
alter system enable restricted session;
alter database "tpcc" open resetlogs;
exec dbms_service.delete_service('seeddata');
exec dbms_service.delete_service('seeddataXDB');
alter database rename global_name to "tpcc";
set linesize 2048;
column ctl_files NEW_VALUE ctl_files;
select concat('control_files=''', concat(replace(value, ', ', ''','''), '''')) ctl_files from v$parameter where name ='control_files';
host echo &ctl_files >>/u01/oracle/admin/tpcc/scripts/init.ora;
host echo &ctl_files >>/u01/oracle/admin/tpcc/scripts/inittpccTemp.ora;
ALTER TABLESPACE TEMP ADD TEMPFILE SIZE 61440K AUTOEXTEND ON NEXT 640K MAXSIZE UNLIMITED;
select tablespace_name from dba_tablespaces where tablespace_name='USERS';
alter user sys account unlock identified by "&&sysPassword";
connect "SYS"/"&&sysPassword" as SYSDBA
alter user system account unlock identified by "&&systemPassword";
select sid, program, serial#, username from v$session;
alter database character set INTERNAL_CONVERT AL32UTF8;
alter database national character set INTERNAL_CONVERT AL16UTF16;
alter system disable restricted session;
