SET VERIFY OFF
spool /u01/oracle/admin/tpcc/scripts/postDBCreation.log append
@/u01/oracle/home/rdbms/admin/catbundleapply.sql;
connect "SYS"/"&&sysPassword" as SYSDBA
set echo on
create spfile='+DATA' FROM pfile='/u01/oracle/admin/tpcc/scripts/init.ora';
connect "SYS"/"&&sysPassword" as SYSDBA
select 'utlrp_begin: ' || to_char(sysdate, 'HH:MI:SS') from dual;
@/u01/oracle/home/rdbms/admin/utlrp.sql;
select 'utlrp_end: ' || to_char(sysdate, 'HH:MI:SS') from dual;
select comp_id, status from dba_registry;
execute dbms_swrf_internal.cleanup_database(cleanup_local => FALSE);
commit;
shutdown immediate;
host /u01/oracle/home/bin/srvctl enable database -d tpcc;
host /u01/oracle/home/bin/srvctl start database -d tpcc;
connect "SYS"/"&&sysPassword" as SYSDBA
spool off
exit;
