
# readme #


##Test Suites##
- [DBT (fair usage implementation of the TPC)](http://osdldbt.sourceforge.net/)
- [TPC Transaction Processing Performance Council](http://www.tpc.org/default.asp)

## Oracle ##

```
SELECT owner, table_name FROM dba_tables
SELECT table_name FROM dba_tables
```

```bash
export ORACLE_HOME=/home/oracle/shiphome
export PATH=${PATH}:${ORACLE_HOME}/bin
export LD_LIBRARY_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/rdbms/lib
export SHLIB_PATH=$ORACLE_HOME/lib:$ORACLE_HOME/rdbms/libexport
export SEQLPLUS=sqlplus
export oracle_dba=system
export oracle_dba_password=manager
#connect_initate_string=/nolog
#export connect_initate_string
#export SYS_CONNECTION_STRING="connect \/ as sysdba"
export ORACLE_SID=fake
```

---------------
### TPC-C ###

```
SELECT * FROM TPCC.WARE WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.WARE ;

SELECT * FROM TPCC.CUST WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.CUST ;

SELECT * FROM TPCC.DIST WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.DIST ;

SELECT * FROM TPCC.HIST WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.HIST ;

SELECT * FROM TPCC.STOK WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.STOK ;

SELECT * FROM TPCC.ITEM WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.ITEM ;

SELECT * FROM TPCC.ORDR WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.ORDR ;

SELECT * FROM TPCC.ORDL WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.ORDL ;

SELECT * FROM TPCC.NORD WHERE ROWNUM <= 1 ;      
SELECT COUNT(*) FROM TPCC.NORD ;
```
#### TPC-C ####

##### TPC-C #####
-------------------------
Stopping Oracle Clusterware stack
CRS-2791: Starting shutdown of Oracle High Availability Services-managed resources on 'ca-sparc66'
CRS-2673: Attempting to stop 'ora.tpccl.db' on 'ca-sparc66'
CRS-2673: Attempting to stop 'ora.LISTENER.lsnr' on 'ca-sparc66'
CRS-2673: Attempting to stop 'ora.LISTENER_ORA.lsnr' on 'ca-sparc66'
CRS-2677: Stop of 'ora.LISTENER_ORA.lsnr' on 'ca-sparc66' succeeded
CRS-2677: Stop of 'ora.LISTENER.lsnr' on 'ca-sparc66' succeeded
CRS-2677: Stop of 'ora.tpccl.db' on 'ca-sparc66' succeeded
CRS-2673: Attempting to stop 'ora.DATA.dg' on 'ca-sparc66'
CRS-2677: Stop of 'ora.DATA.dg' on 'ca-sparc66' succeeded
CRS-2673: Attempting to stop 'ora.asm' on 'ca-sparc66'
CRS-2677: Stop of 'ora.asm' on 'ca-sparc66' succeeded
CRS-2673: Attempting to stop 'ora.evmd' on 'ca-sparc66'
CRS-2677: Stop of 'ora.evmd' on 'ca-sparc66' succeeded
CRS-2673: Attempting to stop 'ora.cssd' on 'ca-sparc66'
CRS-2677: Stop of 'ora.cssd' on 'ca-sparc66' succeeded
CRS-2793: Shutdown of Oracle High Availability Services-managed resources on 'ca-sparc66' has completed
Unable to communicate with the Cluster Synchronization Services daemon.
CRS-4133: Oracle High Availability Services has been stopped.

----------------------

ORA-04031: unable to allocate 4200 bytes of shared memory ("shared
pool","unknown object","sga heap(1,0)","object queue header free ")

--------------------

@?/rdbms/admin/sqlsessend.sql

sqlplus system/manager @?/sqlplus/admin/pupbld
sqlplus / as sysdba @scripts/sql/c_stat.sql
sqlplus / as sysdba @scripts/sql/orst_cre.sql

--------------------
./nrunc.sh test_name 1 1 300
Switching logfiles...

SQL*Plus: Release 12.1.0.2.0 Production on Thu Jul 6 05:39:34 2017

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

Last Successful login time: Thu Jul 06 2017 05:33:21 -04:00

Connected to:
Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, Automatic Storage Management, OLAP, Advanced Analytics
and Real Application Testing options


System altered.


System altered.

Disconnected from Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, Automatic Storage Management, OLAP, Advanced Analytics
and Real Application Testing options
Creating a new runid...
Scheduling UNIX statistics...
../bin/runtpb.exe 1 ../bin/tpcc.exe 9 300 1 SEQ:1 DELAY:1 1 1 0 0 0 0 0 255 1023 8191 x1 s60 e30 utpcc c t0
RUN STATSPACK before

SQL*Plus: Release 12.1.0.2.0 Production on Thu Jul 6 05:39:35 2017

Copyright (c) 1982, 2016, Oracle.  All rights reserved.

SQL> SQL> Connected.
SQL> SQL> SQL> SQL> 05:39:35 SQL> 05:39:35 SQL> 05:39:35 SQL> 05:39:35 SQL> 05:39:35 SQL> 05:39:35 SQL> 05:39:35   2  
PL/SQL procedure successfully completed.

Elapsed: 00:00:00.73
05:39:36 SQL> 
SNAP_BEFORE
-----------
     15

05:39:36 SQL> 05:39:36 SQL> 
Disconnected from Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, Automatic Storage Management, OLAP, Advanced Analytics
and Real Application Testing options

Spawn Program for Benchmark Drivers, V4.7 (Jul  6 2017)
Copyright (c) Oracle Corporation 1995, 1992.  All Rights Reserved.

Creating 1 process.
Process 0 created.

TPC C Driver Version 1.1
Copyright (c) Oracle Corporation 1994, 1995.  All Rights Reserved.

Driver 1 - waiting 1 seconds
Driver 1 - starting ramp up period
Module plnew.c Line 372
Error - ORA-00600: internal error code, arguments: [kddiruFastHashKeyUpdate_1], [2], [0], [], [], [], [], [], [], [], [], []
ORA-06512: at line 321

Irrecoverable error occurred, txn = 1

All processes completed. No errors reported.
RUN STATSPACK after

