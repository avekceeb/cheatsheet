cat >>/dev/null<<-EOF
Applications that perform a lot of memory accesses (several GBs) may obtain performance improvements by using large pages due to reduced Translation Lookaside Buffer (TLB) misses. HugeTLBfs is memory management feature offered in Linux kernel, which is valuable for applications that use a large virtual address space. It is especially useful for database applications such as MySQL, Oracle and others. Other server software(s) that uses the prefork or similar (e.g. Apache web server) model will also benefit.

The CPU’s Translation Lookaside Buffer (TLB) is a small cache used for storing virtual-to-physical mapping information. By using the TLB, a translation can be performed without referencing the in-memory page table entry that maps the virtual address. However, to keep translations as fast as possible, the TLB is usually small. It is not uncommon for large memory applications to exceed the mapping capacity of the TLB. Users can use the huge page support in Linux kernel by either using the mmap system call or standard SYSv shared memory system calls (shmget, shmat).
EOF

# Set the number of pages to be used.
# Each page is normally 2MB, so a value of 20 = 40MB.
# This command actually allocates memory, so this much
# memory must be available.
echo 20 > /proc/sys/vm/nr_hugepages

# Set the group number that is permitted to access this
# memory (102 in this case). The mysql user must be a
# member of this group.
echo 102 > /proc/sys/vm/hugetlb_shm_group

# Increase the amount of shmem permitted per segment
# (12G in this case).
echo 1560281088 > /proc/sys/kernel/shmmax

# Increase total amount of shared memory.  The value
# is the number of pages. At 4KB/page, 4194304 = 16GB.
echo 4194304 > /proc/sys/kernel/shmall

# The final step to make use of the hugetlb_shm_group
# is to give the mysql user an “unlimited” value for the memlock limit.
# This can by done either by editing /etc/security/limits.conf
# or by adding the following command to your mysqld_safe script:

ulimit -l unlimited

# Adding the ulimit command to mysqld_safe causes the root user
# to set the memlock limit to unlimited before switching to the mysql user.
# (This assumes that mysqld_safe is started by root.)

# Large page support in MySQL is disabled by default.
# To enable it, start the server with the --large-pages option.
# For example, you can use the following lines in your server's my.cnf file:

[mysqld]
large-pages

# With this option, InnoDB uses large pages automatically for its buffer pool and additional memory pool.
# If InnoDB cannot do this, it falls back to use of traditional memory
# and writes a warning to the error log: Warning: Using conventional memory pool

# To verify that large pages are being used, check /proc/meminfo again:

#shell> cat /proc/meminfo | grep -i huge
#HugePages_Total:      20
#HugePages_Free:       20
#HugePages_Rsvd:        2
#HugePages_Surp:        0
#Hugepagesize:       4096 kB


mount -t hugetlbfs none /mnt/hp -o pagesize=8M
mount -t hugetlbfs none /mnt/hp -o pagesize=16g
#hugetlbfs: Unsupported page size 16384 MB
#mount: wrong fs type, bad option, bad superblock on none,
#       missing codepage or helper program, or other error
#       (for several filesystems (e.g. nfs, cifs) you might
#       need a /sbin/mount.<type> helper program)
#       In some cases useful info is found in syslog - try
#       dmesg | tail  or so

##############################
[root@ca-sparc66 ~]# cat /proc/meminfo | grep -i huge
HugePages_Total:       4
HugePages_Free:        4
HugePages_Rsvd:        0
HugePages_Surp:        0
Hugepagesize:       8192 kB
[root@ca-sparc66 ~]# cat /proc/cmdline
BOOT_IMAGE=/vmlinuz-4.1.12-39.el6uek.sparc.sparc64 root=/dev/mapper/vg_casparc66-lv_root ro SYSFONT=latarcyrheb-sun16 LANG=en_US.UTF-8 KEYTABLE=us hugepagesz=0x1000000 hugepages=4
