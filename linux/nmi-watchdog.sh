
cat /proc/cmdline 
# ... nmi_watchdog=0

cat /proc/sys/kernel/nmi_watchdog
#1

sysctl kernel.nmi_watchdog
#kernel.nmi_watchdog = 1

ps -ef | grep watch
#root          7      2  0 12:19 ?        00:00:00 [watchdog/0]

grep NMI /proc/interrupts
#NMI:        136        139        125        138      Non-maskable interrupts

