
### disable tcp timestamps

To dynamically disable TCPtime stamping,run the following command:
`echo 0 > /proc/sys/net/ipv4/tcp_timestamps`

To make that change permenant though, you need to add the following line to /etc/sysctl.conf:
`net.ipv4.tcp_timestamps = 0`

IPTables
To be on the safe side, add the following 2 lines to your firewall script:
```
iptables -A INPUT -p icmp --icmp-type timestamp-request -j DROP
iptables -A OUTPUT -p icmp --icmp-type timestamp-reply -j DROP
```

[SKB](http://vger.kernel.org/~davem/tcp_skbcb.html)
[](http://vger.kernel.org/~davem/skb_data.html)
[](http://amsekharkernel.blogspot.co.uk/2014/08/what-is-skb-in-linux-kernel-what-are.html)
[](https://stackoverflow.com/questions/16528868/c-linux-kernel-module-tcp-header)

