
#### debug modules ####

#http://elinux.org/Debugging_The_Linux_Kernel_Using_Gdb

modprobe xfs
ls /sys/module/xfs
#coresize  initsize   notes   sections    taint
#holders   initstate  refcnt  srcversion  uevent

ls /sys/module/xfs/sections/
#__ex_table  __mcount_loc   __tracepoints_ptrs
#_ftrace_events  __tracepoints  __tracepoints_strings

ls /sys/module/xfs/sections/.text
#/sys/module/xfs/sections/.text

cat /sys/module/xfs/sections/.text
#0x0000000010806000

rpm -i kernel-uek-debuginfo-4.1.12-33.el6uek.sparc64.rpm \
       kernel-uek-debuginfo-common-4.1.12-33.el6uek.sparc64.rpm

gdb /lib/modules/$(uname -r)/kernel/fs/xfs/xfs.ko
(gdb) add-symbol-file /usr/lib/debug/lib/modules/4.1.12-33.el6uek.sparc64/kernel/fs/xfs/xfs.ko.debug 0x0000000010806000
Reading symbols from /usr/lib/debug/lib/modules/4.1.12-33.el6uek.sparc64/kernel/fs/xfs/xfs.ko.debug...done.
(gdb) l *(xfs_dir2_sf_to_block+0x2f4) 
0x10833bf4 is in xfs_dir2_sf_to_block (fs/xfs/libxfs/xfs_dir2_block.c:1207).
1202             */
1203            if (offset < newoffset) {
1204                dup = (xfs_dir2_data_unused_t *)((char *)hdr + offset);
1205                dup->freetag = cpu_to_be16(XFS_DIR2_DATA_FREE_TAG);


