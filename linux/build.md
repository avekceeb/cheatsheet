## Building

[howto](http://deathbytape.com/articles/2015/02/17/build-debug-linux-kernel.html)
[more](http://accelazh.github.io/kernel/Build-Linux-Kernel-and-Live-Debugging)
[build small linux howto](https://github.com/MichielDerhaeg/build-linux)

## Configuring ##

[approaches to config](https://lwn.net/Articles/733405/)
[tailor undertaker](http://vamos.informatik.uni-erlangen.de/trac/undertaker/wiki/UndertakerTailor)


```
yum install -y git gcc pkgconfig glib2-devel elfutils-devel rpm-build
cd linux-uek
git tag
git checkout tags/v4.1.12-49
cp /boot/config-$(uname -r) .config

----
Setup is 15516 bytes (padded to 15872 bytes).
System is 8565 kB
CRC 824a2101
Kernel: arch/x86/boot/bzImage is ready  (#1)
linux-stable$ ls -lh arch/x86/boot/bzImage
-rw-rw-r-- 1 ru ru 8.4M Jan 12 16:22 arch/x86/boot/bzImage


```

## Bin package

```bash
### menu:
zypper install ncurses-devel
make menuconfig
make oldconfig
make -j 60 binrpm-pkg 2>&1
rpm -ihv /root/rpmbuild/RPMS/$(uname -m)/kernel-blah.rpm
######
dracut -f /boot/initramfs-2.6.39.img 2.6.39
```

## Make

```bash
git checkout tags/v4.13.1
cp config-4.13.1-crashtest .config
make -s mrproper
# copies current config:
#make ARCH=x86_64 oldnoconfig
cp config-4.13.1-crashtest .config
make ARCH=x86_64 oldconfig
make ARCH=x86_64 V=1 -j3 bzImage
ls -l ./arch/x86/boot/bzImage
make ARCH=x86_64 V=1 -j3 modules
rootfs=/home/ru/build/syzkaller-kernel
make  ARCH=x86_64 INSTALL_MOD_PATH=$rootfs  modules_install  mod-fw=
# rm links build source
rm -rf ./4.14.4-test/build ./4.14.4-test/source
mv ./arch/x86/boot/bzImage $rootfs
#modinfo -k 4.13.1-3-crashtest+ -b /home/ru/linux/build-4.13.1-crashkernel kvm
## installation:
## 1. copy files ##
# todo: install
make INSTALL_MOD_PATH=$(pwd)/4.14.4 LOCALVERSION=-test modules_install  mod-fw=
cp -r ./4.13.1-3-crashtest+ /lib/modules
cp bzImage /boot/vmlinuz-4.13.1-3-crashtest+
## 2. mkramfs ##
dracut -f /boot/initramfs-4.13.1-3-crashtest+.img 4.13.1-3-crashtest+
```

---------------------

### delete unused modules ###

```bash
## all current modules:
lsmod | tail -n+2 | awk '{print $1}'

( for x in af_packet intel_powerclamp crct10dif_pclmul crc32_pclmul crc32c_intel ghash_clmulni_intel i2c_piix4 e1000 pcbc button video ac aesni_intel aes_x86_64 crypto_simd battery glue_helper cryptd pcspkr sr_mod  cdrom ata_generic ata_piix serio_raw sg ; do modinfo $x | sed  -n 's/^filename: *//p' ; done ) | while read y ; do
	cp --parents $y ./tmp
done

rm -rf /lib/modules/4.13.1-3-crashtest+/kernel/
cd ./tmp
cp -r --parents ./lib/ /
```


### build with devtoolset:

    yum install --enablerepo=software_collections devtoolset-6-gcc devtoolset-6-gcc-c++
    scl -l
    scl enable devtoolset-6 "gcc -v"
    scl enable devtoolset-6 "make -j4 bzImage"



### build 2.6 on Debian 4.0 Etch:
	apt-get install cogito git-core
	apt-get install libc6-dev

    git clone https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git --branch linux-2.6.11.y --single-branch
    cd linux-stable/
    wget https://mirrors.edge.kernel.org/pub/linux/devel/binutils/linux-2.6-seg-5.patch
    patch -Np1 -i linux-2.6-seg-5.patch
    vim include/linux/i2c.h
    make bzImage


### Is it debug?

- file bzImage
- objdump --syms

