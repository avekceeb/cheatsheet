

```bash
cd /lib/modules/$(uname -r)
find . -name "*.ko" | head -20

hexdump -C ./kernel/arch/x86/crypto/crc32-pclmul.ko | tail -4
00002590  5e 76 00 00 02 00 00 00  00 00 00 00 02 d2 7e 4d  |^v............~M|
000025a0  6f 64 75 6c 65 20 73 69  67 6e 61 74 75 72 65 20  |odule signature |
000025b0  61 70 70 65 6e 64 65 64  7e 0a                    |appended~.|
000025ba


dmesg | grep -i 'x.*509'
[    2.734755] Asymmetric key parser 'x509' registered
[    2.819527] Loading compiled-in X.509 certificates
[    2.823007] Loaded X.509 cert 'Oracle CA Server: 8f73049bb3a0cc2242facd22fd8f38d82fb049d8'
[    2.824292] Loaded X.509 cert 'Oracle Linux Kernel Module Signing Key: 2bb352412969a3653f0eb6021763408ebb9bb5ab'
[    2.827935] Loaded X.509 cert 'Oracle America, Inc.: Ksplice Kernel Module Signing Key: 09010ebef5545fa7c54b626ef518e077b5b1ee4c'

```
