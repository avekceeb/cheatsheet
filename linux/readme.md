
#### Main ####

- [bugzilla](https://bugzilla.kernel.org/)
- [docs 4.13](https://www.kernel.org/doc/html/v4.13/)
- [torvalds-git](git://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git)
- [linux-stable-git](https://git.kernel.org/pub/scm/linux/kernel/git/stable/linux-stable.git)
- [LKML](https://lkml.org/lkml/2015/10/12/190)
- [Patchwork](https://patchwork.kernel.org/)
- [patch how to](https://kernelnewbies.org/FirstKernelPatch)
- [vmlinux vs bzImage](https://en.wikipedia.org/wiki/Vmlinux)
- [Linux Modules How To](http://www.tldp.org/LDP/lkmpg/2.6/html/lkmpg.html)
- [syscalls](https://unix.stackexchange.com/questions/797/how-can-i-find-the-implementations-of-linux-kernel-system-calls)
- [how to grep syscalls](https://www.cs.utexas.edu/~bismith/test/syscalls/syscalls64.html)
- [create own syscall](https://williamthegrey.wordpress.com/2014/05/18/add-your-own-system-calls-to-the-linux-kernel/)
- [debug](http://www.yonch.com/tech/84-debugging-the-linux-kernel-with-qemu-and-eclipse)
- [kcov](https://www.kernel.org/doc/html/v4.11/dev-tools/kcov.html)
- [oom article LWN](https://lwn.net/Articles/317814/)
- [syscalls table](https://filippo.io/linux-syscall-table/)

--------------------------
## all symbols ##
	cat /proc/kallsyms

## see syscalls:
	cat /proc/kallsyms | grep SyS_ | grep file_range
	arch/x86/entry/syscalls/syscall_64.tbl


## list drivers ##
	ls -r /sys/bus/platform/drivers

## device to driver ##
	ls -l /sys/dev/*/*/device/driver

## device names ##
	ls -l /sys/bus/platform/devices

--------------------------
## how to panic

	# 1)
	echo "c" > /proc/sysrq-trigger

	# 2)
	sudo kill -SEGV 1
	kill -6 1

	# 3)
	swapoff -a
	for r in /dev/ram*; do cat /dev/zero > $r; done

	# 4)

```bash
mkdir /tmp/kpanic
cd /tmp/kpanic
cat >kpanic.c<<EOF
#include <linux/kernel.h>
#include <linux/module.h>
MODULE_LICENSE("GPL");
static int8_t* message = "buffer overrun at 0x4ba4c73e73acce54";
int init_module(void){
    panic(message);
    return 0;
}
EOF

cat > Makefile<<EOF
obj-m += kpanic.o\nall:
\tmake -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules
EOF

make && insmod kpanic.ko
```

--------------------------
```bash

cat >> kernel/sys.c<<EOF
SYSCALL_DEFINE1(crashtestdummy, int, code)
{
    static int8_t* message = "crash test dummy";
    int err = 0;
    char * null_ptr = 0;
    printk("crashtestdummy: op code = %d\n", code);
    if (1983 == code) {
        err = -EFAULT;
        *null_ptr = 'x';
    } else if (1984 == code) {
        err = -EFAULT;
        panic(message);
    }   
    return err;
}
EOF

cat >> arch/x86/entry/syscalls/syscall_64.tbl<<EOF
333     common  crashtestdummy          sys_crashtestdummy
EOF


cat >> include/linux/syscalls.h<<EOF
// my test syscall
asmlinkage long sys_crashtestdummy(int code);

EOF

```

	watch -n1 -d cat /proc/interrupts

[tune irq](https://access.redhat.com/documentation/en-US/Red_Hat_Enterprise_Linux/6/html/Performance_Tuning_Guide/s-cpu-irq.html)

--------------------

	kernel.nmi_watchdog = 1
	kernel.panic_on_io_nmi = 0
	kernel.panic_on_unrecovered_nmi = 0
	kernel.unknown_nmi_panic = 0
	kernel.hardlockup_all_cpu_backtrace = 0
	kernel.max_lock_depth = 1024
	kernel.perf_event_mlock_kb = 516

	kernel.softlockup_panic = 0
	kernel.softlockup_all_cpu_backtrace = 0

----------------------

	rpm -ihv kernel-uek-debuginfo *-common
	crash /usr/lib/debug/lib/modules/4.1.12-112.7.1.el7uek.x86_64/vmlinux  $(pwd)/vmcore
----------------------

## Tracing:

- [lwn](https://lwn.net/Articles/370423/)
- [lwn](https://lwn.net/Articles/365835/)
- [smth](https://jvns.ca/blog/2017/03/19/getting-started-with-ftrace/)
- [kern doc](https://raw.githubusercontent.com/torvalds/linux/v4.4/Documentation/trace/ftrace.txt)

	trace-cmd record -e scsi -e ext4 cp xxx yyy
	man trace-cmd
	man trace-cmd-record


root@t440:~# trace-cmd record -e scsi echo "XXXXXXXXXX" > ./xxxx
CPU0 data recorded at offset=0x5d9000
    0 bytes in size
CPU1 data recorded at offset=0x5d9000
    4096 bytes in size
CPU2 data recorded at offset=0x5da000
    0 bytes in size
CPU3 data recorded at offset=0x5da000
    4096 bytes in size
root@t440:~# 
root@t440:~# man trace-cmd-record
root@t440:~# trace-cmd report
version = 6
CPU 0 is empty
CPU 2 is empty
cpus=4
            echo-4051  [003]  1421.226389: scsi_dispatch_cmd_start: host_no=0 channel=0 id=0 lun=0 data_sgl=4 prot_sgl=0 prot_op=SCSI_PROT_NORMAL cmnd=(READ_10 lba=402926080 txlen=32 protect=0 raw=28 00 18 04 2a 00 00 00 20 00)
          <idle>-0     [001]  1421.234787: scsi_dispatch_cmd_done: host_no=0 channel=0 id=0 lun=0 data_sgl=4 prot_sgl=0 prot_op=SCSI_PROT_NORMAL cmnd=(READ_10 lba=402926080 txlen=32 protect=0 raw=28 00 18 04 2a 00 00 00 20 00) result=(driver=DRIVER_OK host=DID_OK message=COMMAND_COMPLETE status=SAM_STAT_GOOD)
            echo-4051  [003]  1421.235238: scsi_dispatch_cmd_start: host_no=0 channel=0 id=0 lun=0 data_sgl=4 prot_sgl=0 prot_op=SCSI_PROT_NORMAL cmnd=(READ_10 lba=402926112 txlen=32 protect=0 raw=28 00 18 04 2a 20 00 00 20 00)
          <idle>-0     [001]  1421.235503: scsi_dispatch_cmd_done: host_no=0 channel=0 id=0 lun=0 data_sgl=4 prot_sgl=0 prot_op=SCSI_PROT_NORMAL cmnd=(READ_10 lba=402926112 txlen=32 protect=0 raw=28 00 18 04 2a 20 00 00 20 00) result=(driver=DRIVER_OK host=DID_OK message=COMMAND_COMPLETE status=SAM_STAT_GOOD)

root@t440:~# trace-cmd record -F -c -f  cat bugzilla-db.sql > ./2
Segmentation fault (core dumped)
root@t440:~# 

