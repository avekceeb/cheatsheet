
- [kernel org doc for v1](https://www.kernel.org/doc/Documentation/cgroup-v1/)
- [man cgroups] (http://man7.org/linux/man-pages/man7/cgroups.7.html)
- [man ns](http://man7.org/linux/man-pages/man7/namespaces.7.html)
- [man cgroup ns](http://man7.org/linux/man-pages/man7/cgroup_namespaces.7.html)
- [man user ns](http://man7.org/linux/man-pages/man7/user_namespaces.7.html)

    cat /proc/filesystems | grep cgro
    nodev	cgroup
    nodev	cgroup2

    cgroup on /sys/fs/cgroup/systemd type cgroup
        (rw,nosuid,nodev,noexec,relatime,seclabel,xattr,release_agent=/usr/lib/systemd/systemd-cgroups-agent,name=systemd)
    cgroup on /sys/fs/cgroup/devices type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,devices)
    cgroup on /sys/fs/cgroup/pids type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,pids)
    cgroup on /sys/fs/cgroup/cpu,cpuacct type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpu,cpuacct)
    cgroup on /sys/fs/cgroup/hugetlb type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,hugetlb)
    cgroup on /sys/fs/cgroup/cpuset type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,cpuset)
    cgroup on /sys/fs/cgroup/net_cls,net_prio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,net_cls,net_prio)
    cgroup on /sys/fs/cgroup/freezer type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,freezer)
    cgroup on /sys/fs/cgroup/perf_event type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,perf_event)
    cgroup on /sys/fs/cgroup/memory type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,memory)
    cgroup on /sys/fs/cgroup/rdma type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,rdma)
    cgroup on /sys/fs/cgroup/blkio type cgroup (rw,nosuid,nodev,noexec,relatime,seclabel,blkio)

```
rpm -ql libcgroup-tools
/etc/cgconfig.conf
/etc/cgconfig.d
/etc/cgrules.conf
/etc/cgsnapshot_blacklist.conf
/etc/sysconfig/cgred
/usr/bin/cgclassify
/usr/bin/cgcreate
/usr/bin/cgdelete
/usr/bin/cgexec
/usr/bin/cgget
/usr/bin/cgset
/usr/bin/cgsnapshot
/usr/bin/lscgroup
/usr/bin/lssubsys
/usr/lib/systemd/system/cgconfig.service
/usr/lib/systemd/system/cgred.service
/usr/sbin/cgclear
/usr/sbin/cgconfigparser
/usr/sbin/cgrulesengd
```

```
lscgroup | grep libpod
devices:/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
devices:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
pids:/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
pids:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
cpu,cpuacct:/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
cpu,cpuacct:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
hugetlb:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
cpuset:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
net_cls,net_prio:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
freezer:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
perf_event:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
memory:/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
memory:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
blkio:/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
blkio:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope


# cat /sys/fs/cgroup/memory/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope/memory.usage_in_bytes
544768

# cat /sys/fs/cgroup/memory/machine.slice/libpod-conmon-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope/memory.kmem.usage_in_bytes
413696


cat /sys/fs/cgroup/memory/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope/memory.kmem.usage_in_bytes
294912

cat /sys/fs/cgroup/memory/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope/memory.usage_in_bytes
323584

```

```
cat /proc/cgroups
#subsys_name	hierarchy	num_cgroups	enabled
cpuset	6	3	1
cpu	4	6	1
cpuacct	4	6	1
blkio	12	6	1
memory	10	8	1
devices	2	22	1
freezer	8	3	1
net_cls	7	3	1
perf_event	9	3	1
net_prio	7	3	1
hugetlb	5	3	1
pids	3	6	1
rdma	11	1	1

cat /proc/19071/cgroup 
12:blkio:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
11:rdma:/
10:memory:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
9:perf_event:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
8:freezer:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
7:net_cls,net_prio:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
6:cpuset:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
5:hugetlb:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
4:cpu,cpuacct:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
3:pids:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
2:devices:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope
1:name=systemd:/machine.slice/libpod-d292551f49650696eba6766392639d8fd4c65baa5a8a026c65765c44abd835f0.scope

```
