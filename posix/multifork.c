#include <stdlib.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/wait.h>
int main() {

    char buf[8];

    int i;
    for (int k=0; k<3; k++) {
    switch(fork()) {
    case -1:
        exit(1);
        break;
    case 0:
        printf(">>> %d\n", getpid());
        i = read(0, buf, 8);
        //sleep((k+1)*3);
        printf("<<< %d\n", getpid());
        exit(0);
        break;
    default:
        printf("### %d\n", getpid());
        //i = read(0, buf, 1);
        break;
    }
    }
    while (0 < waitpid(-1, 0, 0)) {
        printf("### %d\n", getpid());
    }
    return i;
}
