```bash
sudo touch /var/log/kerntest.log
sudo chown syslog:adm /var/log/kerntest.log
systemctl restart syslog

cat >>/etc/rsyslog.d/50-default.conf<<EOF
local0.*  -/var/log/kerntest.log
EOF
```
[remote](http://www.rsyslog.com/sending-messages-to-a-remote-syslog-server/)

### sender ###
```bash
# UDP
*.*   @172.16.0.8:514
# TCP
#*.* @@172.16.0.8:514 
```

### receiver ###
```bash
cat /etc/rsyslog.conf
module(load="imuxsock") # provides support for local system logging
# provides UDP syslog reception
module(load="imudp")
input(type="imudp" port="514")
# provides TCP syslog reception
#module(load="imtcp")
#input(type="imtcp" port="514")
...
# This one is the template to generate the log filename dynamically,
# depending on the client's IP address.
$template FILENAME,"/var/log/%fromhost-ip%/zlog.log"
# Log all messages to the dynamically formed file.
# Now each clients log (192.168.1.2, 192.168.1.3,etc...),
# will be under a separate directory which is formed by the template FILENAME.
*.* ?FILENAME
```
